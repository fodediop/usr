/* include/config.h.  Generated automatically by configure.  */
/* include/config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define if you have assert.h */
#define HAVE_ASSERT_H 1

/* Define if you have the <ctype.h> header file. */
#define HAVE_CTYPE_H 1

/* Define if you have curses.h */
/* #undef HAVE_CURSES_H */

/* Define if you have the <dlfcn.h> header file. */
/* #undef HAVE_DLFCN_H */

/* Define if you have dlopen() */
#define HAVE_DLOPEN 1

/* Define if you have the <errno.h> header file. */
#define HAVE_ERRNO_H 1

/* Define if you have the `curses' library (-lcurses). */
/* #undef HAVE_LIBCURSES */

/* Define if you have the `dns' library (-ldns). */
/* #undef HAVE_LIBDNS */

/* Define if you have the `ncurses' library (-lncurses). */
#define HAVE_LIBNCURSES 1

/* Define if you have the `nsl' library (-lnsl). */
/* #undef HAVE_LIBNSL */

/* Define if you have the `socket' library (-lsocket). */
/* #undef HAVE_LIBSOCKET */

/* Define if you have log10 */
#define HAVE_LOG10 1

/* Define if you have the <math.h> header file. */
/* #undef HAVE_MATH_H */

/* Define if you have ncurses.h */
#define HAVE_NCURSES_H 1

/* Define if you have snprintf() */
#define HAVE_SNPRINTF 1

/* Define if you have the <stdarg.h> header file. */
#define HAVE_STDARG_H 1

/* Define if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define if you have the <sys/errno.h> header file. */
#define HAVE_SYS_ERRNO_H 1

/* Define if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define if you have the <sys/wait.h> header file. */
#define HAVE_SYS_WAIT_H 1

/* Define if you have the <time.h> header file. */
#define HAVE_TIME_H 1

/* Define if you have vsnprintf() */
#define HAVE_VSNPRINTF 1

/* Define if you have waitpid() */
#define HAVE_WAITPID 1

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE void

/* Define if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define if you can safely include both <sys/time.h> and <time.h>. */
#define TIME_WITH_SYS_TIME 1

/* Define if you need to in order for stat and other things to work. */
/* #undef _POSIX_SOURCE */

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */
