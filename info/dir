-*- Text -*-
This is the file .../info/dir, which contains the topmost node of the
Info hierarchy.  The first time you invoke Info you start off
looking at that node, which is (dir)Top.

File: dir	Node: Top	This is the top of the INFO tree

The Info Directory
******************

  The Info Directory is the top-level menu of major Info topics.
  Type "d" in Info to return to the Info Directory.  Type "q" to exit Info.
  Type "?" for a list of Info commands, or "h" to visit an Info tutorial.
  Type "m" to choose a menu item--for instance,
    "mEmacs<Return>" visits the Emacs manual.
  In Emacs Info, you can click mouse button 2 on a menu item
  or cross reference to follow it to its target.
  Each menu line that starts with a * is a topic you can select with "m".
  Every third topic has a red * to help pick the right number to type.

* Menu: The list of major topics begins on the next line.
  
Texinfo documentation system
* Info: (info).		How to use the documentation browsing system.
* info standalone: (info-stnd).            Read Info documents without Emacs.
* infokey: (info-stnd)Invoking infokey.    Compile Info customizations.
* info: (info)

Emacs stuff
* Emacs: (emacs).	The extensible self-documenting text editor.
* Emacs FAQ: (efaq).	Frequently Asked Questions about Emacs.
* Emacs Lisp Introduction: (eintr).
  			A simple introduction to Emacs Lisp programming.
* Elisp: (elisp).	The Emacs Lisp Reference Manual.
* Dired-X: (dired-x).   Dired Extra Features.
* Ediff: (ediff).	A visual interface for comparing and merging programs.
* Emacs-Xtra: (emacs-xtra).    Specialized Emacs features.
* Org Mode: (org).	Outline-based notes management and organizer.
* PCL-CVS: (pcl-cvs).	Emacs front-end to CVS.
* Speedbar: (speedbar).	File/Tag summarizing utility.
* CL: (cl).		Partial Common Lisp support for Emacs Lisp.
* Ada mode: (ada-mode). Emacs mode for editing Ada code.
* CC mode: (ccmode).	Emacs mode for editing C, C++, Objective-C,
			  Java, Pike, and IDL code.
* Ebrowse: (ebrowse).	A C++ class browser for Emacs.
* Flymake: (flymake).   An on-the-fly syntax checker for Emacs.
* IDLWAVE: (idlwave).	Major mode and shell for IDL and WAVE/CL files.
* Gnus: (gnus).		The news reader Gnus.
* Message: (message).	Mail and news composition mode that goes with Gnus.
* MH-E: (mh-e).		Emacs interface to the MH mail system.
* MIME: (emacs-mime).   Emacs MIME de/composition library.
* PGG: (pgg).   	Emacs interface to various PGP implementations.
* SC: (sc).		Supercite lets you cite parts of messages you're
			  replying to, in flexible ways.
* SMTP: (smtpmail).     Emacs library for sending mail via SMTP.
* Sieve: (sieve).       Managing Sieve scripts in Emacs.
* Autotype: (autotype). Convenient features for text that you enter frequently
                          in Emacs.
* Calc: (calc). 	Advanced desk calculator and mathematical tool.
* Eshell: (eshell).	A command shell implemented in Emacs Lisp.
* EUDC: (eudc).		An Emacs client for directory servers (LDAP, PH).
* Forms: (forms).	Emacs package for editing data bases
			  by filling in forms.
* RefTeX: (reftex).	Emacs support for LaTeX cross-references and citations.
* SES: (ses).           Simple Emacs Spreadsheet
* Tramp: (tramp).	Transparent Remote (file) Access, Multiple Protocol.
                          Edit remote files via a remote shell (rsh,
                          ssh, telnet).
* URL: (url).           URL loading package.
* Widget: (widget).     The "widget" package used by the Emacs Customization
                          facility.
* WoMan: (woman).       Browse UN*X Manual Pages "Wo (without) Man".
* VIPER: (viper).       The newest Emacs VI-emulation mode.
                          (also, A VI Plan for Emacs Rescue
                           or the VI PERil.)
* VIP: (vip).		An older VI-emulation for Emacs.
* Riece: (riece-en).	A IRC client.
* W3M:(emacs-w3m).	Textual Web Browser

Languages
* Perl: (perl).		Perl Documentation
* PM: (pm).		Perl Modules
* PM Unsplit: (pm-unsplit).  Perl Modules unsplit
* NASM: (nasm).		Netwide Assembler
* gp: (gp).                     The GNU Pascal make utility.
* GPC: (gpc).                   The GNU Pascal Compiler.
* Pascal Coding Standards: (gpcs).      GNU Pascal Coding Standards.

GNU 
* Autoconf: (autoconf).         Create source code configuration scripts
* aclocal: (automake)Invoking aclocal.          Generating aclocal.m4
* autoconf: (autoconf)autoconf Invocation.
                                How to create configuration scripts
* autoreconf: (autoconf)autoreconf Invocation.
                                Remaking multiple `configure' scripts
* autoscan: (autoconf)autoscan Invocation.
                                Semi-automatic `configure.ac' writing
* automake: (automake).		Making Makefile.in's
* config.status: (autoconf)config.status Invocation.
                                Recreating a configuration
* configure: (autoconf)configure Invocation.
                                Configuring a package
* ifnames: (autoconf)ifnames Invocation.
                                Listing the conditionals in source code
* Standards: (standards).        GNU coding standards.

Utilities
* Bash: (bash).                     The GNU Bourne-Again SHell.
* BashDB: (bashdb).		    Bash Debugger
* cmp: (diff)Invoking cmp.                      Compare 2 files byte by byte.
* dc: (dc).                   Arbritrary precision RPN "Desktop Calculator".
* Diff: (diff).                 Comparing and merging files.
* diff: (diff)Invoking diff.                    Compare 2 files line by line.
* diff3: (diff)Invoking diff3.                  Compare 3 files line by line.
* Gperf: (gperf).                Perfect Hash Function Generator.
* Groff: (groff).               The GNU troff document formatting system.
* patch: (diff)Invoking patch.                  Apply a patch to a file.
* Screen: (screen).             Full-screen window manager.
* sdiff: (diff)Invoking sdiff.                  Merge 2 files side-by-side.
* Tar: (tar).			Making tape (or disk) archives.
* Make: (make).            Remake files automatically.
* nano: (nano).                 Small and friendly text editor.

