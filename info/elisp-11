This is
/Users/davec/Sources/CVSSources/gnu/emacs/lispref/../info/elisp,
produced by makeinfo version 4.7 from
/Users/davec/Sources/CVSSources/gnu/emacs/lispref/elisp.texi.

INFO-DIR-SECTION Emacs
START-INFO-DIR-ENTRY
* Elisp: (elisp).	The Emacs Lisp Reference Manual.
END-INFO-DIR-ENTRY

   This is edition 2.9 of the GNU Emacs Lisp Reference Manual,
corresponding to Emacs version 22.0.50.

   Copyright (C) 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1998, 1999,
            2000, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.1 or any later version published by the Free Software
     Foundation; with the Invariant Sections being "GNU General Public
     License", with the Front-Cover texts being "A GNU Manual", and
     with the Back-Cover Texts as in (a) below.  A copy of the license
     is included in the section entitled "GNU Free Documentation
     License".

     (a) The FSF's Back-Cover Text is: "You have freedom to copy and
     modify this GNU Manual, like GNU software.  Copies published by
     the Free Software Foundation raise funds for GNU development."


File: elisp,  Node: New Symbols,  Prev: Index,  Up: Top

New Symbols Since the Previous Edition
**************************************

 [index ]
* Menu:

* after-make-frame-functions:            Creating Frames.     (line  36)
* assq-delete-all:                       Association Lists.   (line 213)
* auto-raise-tool-bar-buttons:           Tool Bar.            (line 131)
* auto-resize-tool-bar:                  Tool Bar.            (line 126)
* auto-save-list-file-prefix:            Auto-Saving.         (line 196)
* backup-directory-alist:                Making Backups.      (line  68)
* backward-button:                       Button Buffer Commands.
                                                              (line  43)
* base64-decode-region:                  Base 64.             (line  35)
* base64-decode-string:                  Base 64.             (line  43)
* base64-encode-region:                  Base 64.             (line  12)
* base64-encode-string:                  Base 64.             (line  24)
* beginning-of-defun-function:           List Motion.         (line  81)
* buffer-has-markers-at:                 Information from Markers.
                                                              (line  32)
* button-activate:                       Manipulating Buttons.
                                                              (line  29)
* button-at:                             Manipulating Buttons.
                                                              (line  45)
* button-end:                            Manipulating Buttons.
                                                              (line  20)
* button-get:                            Manipulating Buttons.
                                                              (line  23)
* button-has-type-p:                     Manipulating Buttons.
                                                              (line  41)
* button-label:                          Manipulating Buttons.
                                                              (line  35)
* button-put:                            Manipulating Buttons.
                                                              (line  26)
* button-start:                          Manipulating Buttons.
                                                              (line  17)
* button-type:                           Manipulating Buttons.
                                                              (line  38)
* button-type-get:                       Manipulating Buttons.
                                                              (line  51)
* button-type-put:                       Manipulating Buttons.
                                                              (line  48)
* button-type-subtype-p:                 Manipulating Buttons.
                                                              (line  54)
* byte-to-position:                      Text Representations.
                                                              (line  66)
* charset-bytes:                         Chars and Bytes.     (line  22)
* charset-plist:                         Character Sets.      (line  40)
* clear-face-cache:                      Font Selection.      (line  94)
* clear-image-cache:                     Image Cache.         (line  26)
* clear-this-command-keys:               Command Loop Info.   (line  84)
* clrhash:                               Hash Access.         (line  28)
* color-defined-p:                       Color Names.         (line  23)
* color-gray-p:                          Color Names.         (line  59)
* color-supported-p:                     Color Names.         (line  47)
* color-values:                          Color Names.         (line  65)
* constrain-to-field:                    Fields.              (line  70)
* copy-hash-table:                       Other Hash.          (line  11)
* create-glyph:                          Glyphs.              (line  49)
* create-image:                          Defining Images.     (line  11)
* current-word:                          Buffer Contents.     (line  88)
* default-header-line-format:            Header Lines.        (line  16)
* defimage:                              Defining Images.     (line  30)
* define-button-type:                    Button Types.        (line  12)
* define-hash-table-test:                Defining Hash.       (line  21)
* define-minor-mode:                     Defining Minor Modes.
                                                              (line  11)
* defined-colors:                        Color Names.         (line  39)
* delete-and-extract-region:             Deletion.            (line  35)
* delete-field:                          Fields.              (line  66)
* delete-minibuffer-contents:            Minibuffer Contents. (line  33)
* describe-current-display-table:        Display Table Format.
                                                              (line  84)
* describe-display-table:                Display Table Format.
                                                              (line  80)
* disable-point-adjustment:              Adjusting Point.     (line  16)
* display-backing-store:                 Display Feature Testing.
                                                              (line  96)
* display-color-cells:                   Display Feature Testing.
                                                              (line 121)
* display-color-p:                       Display Feature Testing.
                                                              (line  35)
* display-graphic-p:                     Display Feature Testing.
                                                              (line  25)
* display-grayscale-p:                   Display Feature Testing.
                                                              (line  40)
* display-message-or-buffer:             Displaying Messages. (line  68)
* display-mm-height:                     Display Feature Testing.
                                                              (line  84)
* display-mm-width:                      Display Feature Testing.
                                                              (line  92)
* display-mouse-p:                       Display Feature Testing.
                                                              (line  31)
* display-pixel-height:                  Display Feature Testing.
                                                              (line  80)
* display-pixel-width:                   Display Feature Testing.
                                                              (line  88)
* display-planes:                        Display Feature Testing.
                                                              (line 111)
* display-popup-menus-p:                 Display Feature Testing.
                                                              (line  20)
* display-save-under:                    Display Feature Testing.
                                                              (line 106)
* display-screens:                       Display Feature Testing.
                                                              (line  76)
* display-selections-p:                  Display Feature Testing.
                                                              (line  65)
* display-supports-face-attributes-p:    Display Feature Testing.
                                                              (line  45)
* display-visual-class:                  Display Feature Testing.
                                                              (line 116)
* dolist:                                Iteration.           (line  52)
* dotimes:                               Iteration.           (line  64)
* emacs-save-session-functions:          Session Management.  (line  19)
* emacs-startup-hook:                    Init File.           (line  66)
* end-of-defun-function:                 List Motion.         (line  86)
* face-attribute:                        Attribute Functions. (line  29)
* face-attribute-relative-p:             Attribute Functions. (line  59)
* face-font-family-alternatives:         Font Selection.      (line  55)
* face-font-registry-alternatives:       Font Selection.      (line  66)
* face-font-selection-order:             Font Selection.      (line  26)
* field-beginning:                       Fields.              (line  36)
* field-end:                             Fields.              (line  47)
* field-string:                          Fields.              (line  58)
* field-string-no-properties:            Fields.              (line  62)
* file-expand-wildcards:                 Contents of Directories.
                                                              (line  58)
* find-file-wildcards:                   Visiting Functions.  (line 105)
* find-image:                            Defining Images.     (line  54)
* font-list-limit:                       Font Lookup.         (line  66)
* fontification-functions:               Auto Faces.          (line   9)
* forward-button:                        Button Buffer Commands.
                                                              (line  34)
* frame-parameter:                       Parameter Access.    (line   9)
* gethash:                               Hash Access.         (line  11)
* global-disable-point-adjustment:       Adjusting Point.     (line  25)
* hash-table-count:                      Other Hash.          (line  15)
* hash-table-p:                          Other Hash.          (line   8)
* hash-table-rehash-size:                Other Hash.          (line  27)
* hash-table-rehash-threshold:           Other Hash.          (line  30)
* hash-table-size:                       Other Hash.          (line  33)
* hash-table-test:                       Other Hash.          (line  18)
* hash-table-weakness:                   Other Hash.          (line  23)
* header-line-format:                    Header Lines.        (line  10)
* image-cache-eviction-delay:            Image Cache.         (line  16)
* image-mask-p:                          Image Descriptors.   (line 160)
* image-size:                            Showing Images.      (line  68)
* indicate-empty-lines:                  Usual Display.       (line  75)
* inhibit-field-text-motion:             Word Motion.         (line  43)
* inhibit-modification-hooks:            Change Hooks.        (line  91)
* insert-button:                         Making Buttons.      (line  36)
* insert-text-button:                    Making Buttons.      (line  51)
* keywordp:                              Constant Variables.  (line  20)
* left-margin-width:                     Display Margins.     (line  28)
* line-beginning-position:               Text Lines.          (line  59)
* line-end-position:                     Text Lines.          (line  77)
* locale-coding-system:                  Locales.             (line  11)
* make-backup-file-name-function:        Making Backups.      (line  91)
* make-button:                           Making Buttons.      (line  32)
* make-category-table:                   Categories.          (line  71)
* make-hash-table:                       Creating Hash.       (line   8)
* make-temp-file:                        Unique File Names.   (line  15)
* make-text-button:                      Making Buttons.      (line  47)
* makehash:                              Creating Hash.       (line 103)
* mapc:                                  Mapping Functions.   (line  55)
* maphash:                               Hash Access.         (line  36)
* merge-face-attribute:                  Attribute Functions. (line  64)
* minibuffer-contents:                   Minibuffer Contents. (line  22)
* minibuffer-contents-no-properties:     Minibuffer Contents. (line  28)
* minibuffer-prompt-end:                 Minibuffer Contents. (line  13)
* mouse-on-link-p:                       Links and Mouse-1.   (line  98)
* multibyte-syntax-as-symbol:            Parsing Expressions. (line 172)
* next-button:                           Button Buffer Commands.
                                                              (line  52)
* next-single-char-property-change:      Property Search.     (line 100)
* parse-colon-path:                      System Environment.  (line 162)
* play-sound:                            Sound Output.        (line  14)
* play-sound-file:                       Sound Output.        (line  48)
* play-sound-functions:                  Sound Output.        (line  51)
* plist-member:                          Other Plists.        (line  55)
* pop:                                   List Elements.       (line  60)
* position-bytes:                        Text Representations.
                                                              (line  60)
* previous-button:                       Button Buffer Commands.
                                                              (line  57)
* previous-single-char-property-change:  Property Search.     (line 110)
* print-circle:                          Output Variables.    (line  91)
* print-gensym:                          Output Variables.    (line  95)
* process-query-on-exit-flag:            Query Before Exit.   (line  13)
* process-running-child-p process:       Input to Processes.  (line  71)
* propertize:                            Changing Properties. (line  92)
* push:                                  Building Lists.      (line  35)
* push-button:                           Button Buffer Commands.
                                                              (line  22)
* puthash:                               Hash Access.         (line  15)
* redisplay-dont-pause:                  Forcing Redisplay.   (line  11)
* remhash:                               Hash Access.         (line  20)
* right-margin-width:                    Display Margins.     (line  32)
* scalable-fonts-allowed:                Font Selection.      (line  81)
* scroll-down-aggressively:              Textual Scrolling.   (line 117)
* scroll-up-aggressively:                Textual Scrolling.   (line 130)
* set-face-attribute:                    Attribute Functions. (line  11)
* set-process-query-on-exit-flag:        Query Before Exit.   (line  16)
* set-window-margins:                    Display Margins.     (line  43)
* show-help-function:                    Special Properties.  (line 244)
* show-trailing-whitespace:              Standard Faces.      (line 106)
* small-temporary-file-directory:        Unique File Names.   (line  85)
* subr-arity:                            What Is a Function.  (line 104)
* sxhash:                                Defining Hash.       (line  41)
* system-messages-locale:                Locales.             (line  17)
* system-time-locale:                    Locales.             (line  24)
* temp-buffer-setup-hook:                Temporary Displays.  (line  68)
* text-property-default-nonsticky:       Sticky Properties.   (line  54)
* tool-bar-add-item:                     Tool Bar.            (line  86)
* tool-bar-add-item-from-menu:           Tool Bar.            (line 107)
* tool-bar-button-margin:                Tool Bar.            (line 135)
* tool-bar-button-relief:                Tool Bar.            (line 140)
* tool-bar-map:                          Tool Bar.            (line  71)
* tty-color-alist:                       Text Terminal Colors.
                                                              (line  41)
* tty-color-approximate:                 Text Terminal Colors.
                                                              (line  51)
* tty-color-clear:                       Text Terminal Colors.
                                                              (line  37)
* tty-color-define:                      Text Terminal Colors.
                                                              (line  27)
* tty-color-translate:                   Text Terminal Colors.
                                                              (line  57)
* user-init-file:                        Init File.           (line  70)
* void-text-area-pointer:                Pointer Shape.       (line  16)
* window-body-height:                    Size of Window.      (line  29)
* window-margins:                        Display Margins.     (line  48)
* window-size-fixed:                     Resizing Windows.    (line  98)
* with-syntax-table:                     Syntax Table Functions.
                                                              (line 101)
* with-temp-message:                     Displaying Messages. (line  40)
* x-family-fonts:                        Font Lookup.         (line  27)
* x-font-family-list:                    Font Lookup.         (line  54)


