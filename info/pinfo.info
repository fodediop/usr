This is pinfo.info, produced by makeinfo version 4.0 from pinfo.texi.

   This is documentation for `pinfo', version 0.6.2.  Edition 0.2, date
21 Aug 2000.

                                       Copyright 2000 (C) Przemek Borys

         <pborys@dione.ids.pl>, `http://zeus.polsl.gliwice.pl/~pborys/'

INFO-DIR-SECTION Texinfo
START-INFO-DIR-ENTRY
* Pinfo: (pinfo).           curses based lynx-style info browser.
END-INFO-DIR-ENTRY


File: pinfo.info,  Node: Top,  Next: Invoking,  Prev: (dir),  Up: (dir)

   This documentation is for `pinfo', version 0.6.2.

   Original manpage written on 21 Mar 1999 by Przemek Borys,
<pborys@dione.ids.pl>, `http://zeus.polsl.gliwice.pl/~pborys/'.

   Translated to Texinfo by Stanislav Kuchar, <thorn@slonik.sk>,
`http://slonik.sk/' on 21 Aug 2000.

* Menu:

* Invoking::                    Comman line options.
* Configuration::               Configurable behaviour of pinfo.
* Internationalization Support::  Localization.
* Author::                      Who wrote pinfo.
* Bugs::                        Help debugging.
* Copyright::                   Copying.
* Index::                       Search.

 --- The Detailed Node Listing ---

Configuration

* Configuration file::          Save your preferences.
* Environment::                 Variables.

Configuration file

* Color::                       Color settings.
* Keys::                        Bindable keyboard.
* Options::                     Confiruration options.
* Example config file::         Build in values.

Keys

* Keybindings::                 What keys can be used.


File: pinfo.info,  Node: Invoking,  Next: Configuration,  Prev: Top,  Up: Top

Invoking
********

   `pinfo' [OPTIONS] [INFO_PAGE]

   `pinfo' is a program for viewing info files. You specify which page
you want to read by passing it an INFO_PAGE argument. This argumen t
contains the name of an info page (i.e. `bash'). The program will then
search for it in `./', `/usr/info', and `/usr/local/info'. It will also
automaticaly add the suffix `-info', `-info.Z', `-info.gz', or
`-info.bz2'. At present other suffixes are not recognized, but you can
easily add them to the function `openinfo()' in
`filehandling_functions.c'.

   When the search for info pages fails, man is called with the
INFO_PAGE argument, and it's output is parsed by `pinfo'. This means
that when you don't have the appropriate info page, but have a man page
instead; the man page will be viewed.

   When no INFO_PAGE is specified, the default `dir' page is shown.

Supported OPTIONS are

`-h, --help'
     print help information and exit

`-v, --version'
     print version information and exit

`-m, --manual'
     uses manual page instead of info by default. (`pinfo' `-m' could
     be used as a manual pager). Warning: Everything what follows this
     option is passed to the `man' program. Don't be confused if
     `pinfo' options, which followed `-m' don't work.

     You can also call the man function of `pinfo' in another way.
     When `pinfo' is called with an `argv[0]' (the program file name),
     which contains the word `man' in it's name, the man functions are
     enabled automatically.

     Previously there was a symlink to `pinfo', called `pman', but I
     had to remove it from the distribution, since it's name was in
     conflict with some other utility. Anyway, you can feel free to
     create such a link if you wish.

`-r, --raw-filename'
     uses a raw filename first (i.e.  the name which you specified as
     infopage is considered to be a real file in the current directory).

`-f, --file'
     Same as '-r'.

`-a, --apropos'
     if this is set, apropos is called when no man or info page could be
     found.

`-c, --cut-man-headers'
     if this is set, man parsing code will try to cut out the repeated
     man headers. Use with care. ;)

`-s, --squeeze-lines'
     cut empty lines from manual pages. This option enables autocutting
     of every repeated newline in a manual page.

`-t, --force-manual-tag-table'
     forces manual detection of tag table. This allows you to view info
     pages, which may be corrupted.  (as i.e.  version of jed's pages,
     shipped with RH5.0). The tag table corruption ussualy appears in
     that the info links, which you follow, move you to quite
     unexpected nodes.

`--node=NODENAME, --node NODENAME'
     Go to the node NODENAME of info file.

`-l, --long-manual-links'
     Use long link names in manuals.  On some systems the manual
     hierarchy is divided into subsections like `3ncurses', etc, while
     on other systems all belongs to section `3'. If this option is
     what your system is like, feel free to use it.

`-x, --clear-at-exit'
     Clear screen at exit.

   The options are handled by GNU getopt, so you can here as in other
programs abbreviate the option names to the minimal number of characters
by which the options differ.

   Warning! If you do not have getopt, these options will not work!


File: pinfo.info,  Node: Configuration,  Next: Internationalization Support,  Prev: Invoking,  Up: Top

Configuration
*************

* Menu:

* Configuration file::          Save your preferences.
* Environment::                 Variables.


File: pinfo.info,  Node: Configuration file,  Next: Environment,  Prev: Configuration,  Up: Configuration

Configuration file
==================

   Just take a look at the example config file, *Note Example config
file::, and at the key descriptions, *Note Keys::. Keys available in
manual viewer differ a bit from the keys available in info viewer.

   There are configuration files called `~/.pinforc' and
`[prefix]/etc/pinforc', for local and global configuration (where
prefix is the prefix of the directory, where `pinfo' is installed, i.e.
`/usr/local', or `/usr').

* Menu:

* Color::                       Color settings.
* Keys::                        Bindable keyboard.
* Options::                     Confiruration options.
* Example config file::         Build in values.


File: pinfo.info,  Node: Color,  Next: Keys,  Prev: Configuration file,  Up: Configuration file

Color
-----

   First you must enter a color name (all available color names are
present in the example, *Note Example config file::, and they're self
explanatory, I think.

   There is also a special color COLOR_DEFAULT, which stands for
transparency). Then you enter the foreground color, and the background
color.

   The BOLD attribute means that we want the foreground color to be
highlighted. (i.e.  lightblue, lightgreen).  BLINK attribute is the
blinking attribute, or highlighted background in some other
configurations.


File: pinfo.info,  Node: Keys,  Next: Options,  Prev: Color,  Up: Configuration file

Keys
----

   Now let's move to the key definitions. Here we first put a key name
(again all keys are present in the example); then we enter it's value -
either surrounded by apostrophes, or a keycode number (like in
KEY_REFRESH_1), or its mnemonic code name if it's a special key (like
i.e.  in KEY_FOLLOWLINK_1).

   If you wish to specify key by code value, use the supplied program
'testkey' to obtain the needed value. It mainly is a feature, when you
wan't to add some ctrl+letter keybindings, and similar.

   For each function you can bind two keys, i.e.  you could bind both
Enter and Cursor Right to the FollowLink-function.  As you can see in
the example above, the two key names are KEY_FOLLOWLINK_1 and
KEY_FOLLOWLINK_2.

* Menu:

* Keybindings::                 What keys can be used.


File: pinfo.info,  Node: Keybindings,  Prev: Keys,  Up: Keys

Keybindings
...........

   Here's an explanation of the key names:

KEY_TOTALSEARCH_1
     Key for searching through all nodes of info file.

KEY_TOTALSEARCH_2
     Alternate key for searching through all nodes of info file.

KEY_SEARCH_1
     Key for searching through current node (or manual).

KEY_SEARCH_2
     Alternate key for searching through current node (or manual).

KEY_SEARCH_AGAIN_1
     Key for repeating the last search.

KEY_SEARCH_AGAIN_2
     Alternate key for repeating the last search.

KEY_GOTO_1
     Key for explicitly going to a node (by specifing it's name).

KEY_GOTO_2
     Alternate key for explicitly going to a node (by specifing it's
     name).

KEY_PREVNODE_1
     Key for going to a node marked as 'Prev' in the header.

KEY_PREVNODE_2
     Alternate key for going to a node marked as 'Prev' in the header.

KEY_NEXTNODE_1
     Key for going to a node marked as 'Next' in the header.

KEY_NEXTNODE_2
     Alternate key for going to a node marked as 'Next' in the header.

KEY_UP_1
     Key for scrolling text one line up.  Alternate key for scrolling
     text one line up.

KEY_END_1
     Key for going to the end of the node.  Alternate key for going to
     the end of the node.

KEY_PGDN_1
     Key for going one page down in the viewed node.

KEY_PGDN_2
     Alternate key for going one page down in the viewed node.

KEY_PGDN_AUTO_1
     Key for going to the next node when you're at the end of node
     (default is zero - turned off).

KEY_PGDN_AUTO_2
     Alternate key for going to the next node when you're at the end of
     node (default is space, as for pgdn_2).

KEY_HOME_1
     Key for going to the beginning of the node.

KEY_HOME_2
     Alternate key for going to the beginning of the node.

KEY_PGUP_1
     Key for going one page up in the viewed node.

KEY_PGUP_2
     Alternate key for going one page up in the viewed node.

KEY_PGUP_AUTO_1
     Key for going to the `up' node, when being at the top of node.
     (Default value is zero - turned off).

KEY_PGUP_AUTO_2
     Alternate key for going to the `up' node, when being at the top of
     node.  (Default value is `-', as for pgup_2).

KEY_DOWN_1
     Key for scrolling the text down one line.

KEY_DOWN_2
     Alternate key for scrolling the text down one line.

KEY_TOP_1
     Key for going to the top (first) node.  Alternate key for going to
     the top (first) node.

KEY_BACK_1
     Key for going back (in the history of viewed nodes).

KEY_BACK_2
     Alternate key for going back (in the history of viewed nodes).

KEY_FOLLOWLINK_1
     Key for following a hypertext link.

KEY_FOLLOWLINK_2
     Alternate key for following a hypertext link.

KEY_REFRESH_1
     Key for refreshing the screen (hardcoded is the ^L value).

KEY_REFRESH_2
     Alternate key for refreshing the screen.

KEY_SHELLFEED_1
     Key for calling a shell command, and passing the viewed node to the
     stdin of that command.

KEY_SHELLFEED_2
     Alternate key for calling a shell command, and passing the viewed
     node to the stdin of that command.

KEY_QUIT_1
     Key for exiting the programm.

KEY_QUIT_2
     Alternate key for exiting the programm.

KEY_GOLINE_1
     Key for going to a specified line in file.

KEY_GOLINE_2
     Alternate key for going to a specified line in file.

KEY_PRINT_1
     Key for printing viewed node or man page.

KEY_PRINT_2
     Alternate key for printing viewed node or man page.

   The special mnemonics for keys (which are defined at present) are:

KEY_BREAK

KEY_DOWN

KEY_UP

KEY_LEFT

KEY_RIGHT

KEY_DOWN

KEY_HOME

KEY_BACKSPACE

KEY_NPAGE

KEY_PPAGE

KEY_END
     [Note: this works probably ONLY with linux ncurses]

KEY_F(X)

KEY_CTRL('C')
     this assigns the key value to a ctrl+c combination.  c may be any
     letter you wish.

KEY_ALT('C')
     this assigns the key value to a alt+c combination.  c may be any
     letter you wish. If alt key won't work, you may use ESC+key
     combination.

'C'
     this means a printable character c. The syntax is just like in
     C/C++ ;).

[NUMBER]
     you can also specify key as it's code number.  It is useful e.g.
     when specifing control keys, and some nonstandard keys.  A
     numerical value of zero turns given keybinding off.

   See manual page for curs_getch(3x) for description of their meaning.

   Warning!  Try not to create some serious keybinding conflicts!


File: pinfo.info,  Node: Options,  Next: Example config file,  Prev: Keys,  Up: Configuration file

Options
-------

   The options in the last part of the example configuration file
should be fairly self-explanatory.  The variables that can be set to
true or false do the same things as the commandline arguments with the
same names.

MANUAL
     If this is set to true the default is to first check for a man
     page, instead of a texinfo file.

CUT-MAN-HEADERS
     If set to true, then pinfo tries to cut off the repeated headers
     throughout man pages.

CUT-EMPTY-MAN-LINES
     If set to true, then pinfo tries to cut off the repeated newlines
     (i.e.  it will shorten every repeating newlines to one newline).

RAW-FILENAME
     If set to true, the file argument is taken to be the name of a
     file in the current working directory, i.e.  the directories in
     `INFOPATH' will only be searched if a file with this name is not
     in the working directory.

APROPOS
     If set to true, apropos is called if no info or man page is found.

DONT-HANDLE-WITHOUT-TAG-TABLE
     If set to true, pinfo will not attempt to display texinfo pages
     without tag tables.

HTTPVIEWER
     Set this to the program you want to use to follow http links in
     documents.

FTPVIEWER
     Set this to the program you want to use to follow ftp links in
     documents.

MAILEDITOR
     Set this to your favourite email program, and it will be started
     if you follow an email link in a document.

PRINTUTILITY
     Utility, which you use for printing. I.e.  `lpr'. If you don't use
     any, you may also try something like `cat >/dev/lp1', or sth. ;)

MANLINKS
     This specifies the section names, which may be referenced in your
     man pages (i.e.  Xtoolkit man pages match the section 3Xt (see for
     example XtVaCreateWidget) manpage), Xlib function pages match
     section 3X11, etc. Such extensions may not be recognized by
     default, so it is a good idea to add them).

MAN-OPTIONS
     This specifies the options, which should be passed to the `man'
     program.  (see man(1) for description of what they're like).

STDERR-REDIRECTION
     Pinfo allows you to redirect the stderr output of called
     programms.  For example if you don't want to see man's error
     messages about manual page formatting, you can use
     `STDER-REDIRECTION"> /dev/null"'.  This is the default.

LONG-MANUAL-LINKS
     This is another true/false option, which decides whether your
     system supports long manual section names, or not.  (i.e.
     "3ncurses" instead of "3").

FILTER-0XB7
     This decides, whether you want to convert 0xb7 chars to `o', or
     not.  For example for iso-8859-2 fonts this makes man's list marks
     a bit nicer ;) (look for example at perl's man page, to see how
     those marks look like).

QUIT-CONFIRMATION
     This decides whether you want to use quit confirmation on exit, or
     not.

QUIT-CONFIRM-DEFAULT
     This yes/no option determines the default answer to the
     QUIT-CONFIRMATION dialog.  (default answer is when you press a
     key, that does not match the asked question).

CLEAR-SCREEN-AT-EXIT
     This true/false option determines if you want to have your screen
     cleared at exit, or no.

HIGHLIGHTREGEXP
     This is an option, through which you may pass to pinfo regexps,
     which should be highlighted when working with document.  Warning!
     This may turn very slow if you use it without care!

SAFE-USER
     This option is used to pass the name of user, to which suid when
     pinfo is run with root privileges.

SAFE-GROUP
     This option is used to pass the name of group, to which suid when
     pinfo is run with root privileges.


File: pinfo.info,  Node: Example config file,  Prev: Options,  Up: Configuration file

Example config file
-------------------

     #
     # Here are some colour setting.
     # Whitespace between the entries is optional.
     #
     COL_NORMAL        OLOR_WHITE,    COLOR_BLACK, NO_BOLD, NO_BLINK
     COL_MENUSELECTED  OLOR_RED,      COLOR_BLACK, BOLD,    NO_BLINK
     COL_MENU          OLOR_BLUE,     COLOR_BLACK, BOLD,    NO_BLINK
     COL_NOTESELECTED  OLOR_RED,      COLOR_BLACK, BOLD,    NO_BLINK
     COL_NOTE          OLOR_GREEN,    COLOR_BLACK, BOLD,    NO_BLINK
     COL_TOPLINE       OLOR_YELLOW,   COLOR_BLUE,  BOLD,    NO_BLINK
     COL_BOTTOMLINE    OLOR_YELLOW,   COLOR_BLUE,  BOLD,    NO_BLINK
     COL_MANUALBOLD    OLOR_WHITE,    COLOR_BLACK, BOLD,    NO_BLINK
     COL_MANUALITALIC  OLOR_WHITE,    COLOR_BLACK, BOLD,    NO_BLINK
     COL_URL           OLOR_MAGENTA,  COLOR_BLACK, BOLD,    NO_BLINK
     COL_URLSELECTED   OLOR_RED,      COLOR_BLACK, NO_BOLD, NO_BLINK
     COL_INFOHIGHLIGHT OLOR_WHITE,    COLOR_BLACK, BOLD,    NO_BLINK
     
     #
     # Here are some keybindings as well...
     #
     KEY_TOTALSEARCH_1    �s'
     KEY_TOTALSEARCH_2    �S'
     KEY_SEARCH_1         �/'
     KEY_SEARCH_2         �.'
     KEY_GOTO_1           �g'
     KEY_GOTO_2           �m'
     KEY_HOME_1           �h'
     KEY_HOME_2           �H'
     KEY_PREVNODE_1       �p'
     KEY_PREVNODE_2       �P'
     KEY_NEXTNODE_1       �n'
     KEY_NEXTNODE_2       �N'
     KEY_UP_1             EY_UP
     KEY_UP_2             �u'
     KEY_END_1            EY_END
     KEY_END_2            �e'
     KEY_PGDN_1           EY_NPAGE
     KEY_PGDN_2           � '
     KEY_PGDN_AUTO_1       
     KEY_PGDN_AUTO_2      � '
     KEY_PGUP_1           EY_PPAGE
     KEY_PGUP_2           �b'
     KEY_PGUP_AUTO_1       
     KEY_PGUP_AUTO_2      �b'
     KEY_DOWN_1           EY_DOWN
     KEY_DOWN_2           �d'
     KEY_TOP_1            EY_HOME
     KEY_TOP_2            �t'
     KEY_BACK_1           EY_LEFT
     KEY_BACK_2           �l'
     KEY_FOLLOWLINK_1     EY_RIGHT
     KEY_FOLLOWLINK_2     �\n'
     # 12 is a code for ctrl+l
     KEY_REFRESH_1        2
     KEY_REFRESH_2        �~'
     KEY_SHELLFEED_1      �!'
     KEY_SHELLFEED_2      �1'
     KEY_QUIT_1           �q'
     KEY_QUIT_2           �Q'
     KEY_DIRPAGE_1        �d'
     KEY_DIRPAGE_2        �D'
     KEY_GOLINE_1         �l'
     KEY_GOLINE_2          
     KEY_PRINT_1          �]'
     KEY_PRINT_2           
     KEY_SEARCH_AGAIN_1   �f'
     KEY_SEARCH_AGAIN_2    
     
     #
     # Some options, explained in the man page
     #
     MANUAL                        /alse
     CUT-MAN-HEADERS               =rue
     CUT-EMPTY-MAN-LINES           =rue
     RAW-FILENAME                  /alse
     APROPOS                       /alse
     DONT-HANDLE-WITHOUT-TAG-TABLE /alse
     LONG-MANUAL-LINKS             /alse
     FILTER-0xB7                   =rue
     QUIT-CONFIRMATION             /alse
     QUIT-CONFIRM-DEFAULT          7o
     CLEAR-SCREEN-AT-EXIT          =rue
     STDERR-REDIRECTION            �2> /dev/null"
     HTTPVIEWER                    5ynx
     FTPVIEWER                     5ynx
     MAILEDITOR                    9ine
     MANLINKS                      :8:2:3:4:5:6:7:9:n:p:o:3X11:3Xt
     HIGHLIGHTREGEXP               ash.*has
     SAFE-USER                     7obody
     SAFE-GROUP                    7obody


File: pinfo.info,  Node: Environment,  Prev: Configuration file,  Up: Configuration

Environment
===========

   There is a variable `$INFOPATH', which can specify the paths to  be
searched for info files. It's format is similar to the format of the
`$PATH' variable. An example setting could look like:

   `/usr/info:/usr/somewhere/info:/not/even/in/usr/info'

   etc. paths are separated by colons.


File: pinfo.info,  Node: Internationalization Support,  Next: Author,  Prev: Configuration,  Up: Top

Internationalization Support
****************************

   Pinfo implements general features of gnu gettext library (the thing,
which you need to see national messages ;). But it is not the end.
Pinfo allows you to use national info pages! You only need to put them
to your info directory, into a subdirectory, which is called `$LANG'.


File: pinfo.info,  Node: Author,  Next: Bugs,  Prev: Internationalization Support,  Up: Top

Author
******

   Przemek Borys <pborys@dione.ids.pl>,
`http://zeus.polsl.gliwice.pl/~pborys/'

   If that E-mail address wont work (since the machine where it is being
handled is a bit damaged lately), you can try
<pborys@zeus.polsl.gliwice.pl>, or <pborys@p-soft.silesia.linux.org.pl>.

   There was also a lot of other people, who contributed to this code.
See the `AUTHORS' file.

   Please send bug reports to the author: Przemek Borys
<pborys@dione.ids.pl>, `http://zeus.polsl.gliwice.pl/~pborys/'.

   The author would like to read some comments and suggestions from
you, if any.


File: pinfo.info,  Node: Bugs,  Next: Copyright,  Prev: Author,  Up: Top

Bugs
****

   Please send bug reports to the author, *Note Author::.


File: pinfo.info,  Node: Copyright,  Next: Index,  Prev: Bugs,  Up: Top

Copyright
*********

   This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of this License, or (at
your option) any later version.

   This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

   You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING. If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
021111, USA.

                                       Copyright 2000 (C) Przemek Borys

         <pborys@dione.ids.pl>, `http://zeus.polsl.gliwice.pl/~pborys/'


File: pinfo.info,  Node: Index,  Prev: Copyright,  Up: Top

Index
*****

* Menu:

* Color:                                 Color.
* Command line:                          Invoking.
* Comments:                              Author.
* Config file:                           Example config file.
* Configuration Keys:                    Keybindings.
* Configuration options:                 Options.
* Configure:                             Configuration file.
* Configure keys:                        Keys.
* Copyright:                             Copyright.
* Customization:                         Configuration file.
* Date:                                  Top.
* Description:                           Invoking.
* Enviroment configuration:              Environment.
* How to use it:                         Keys.
* info files:                            Invoking.
* Keybindings:                           Keybindings.
* Keyboard:                              Keys.
* Languages:                             Internationalization Support.
* Look preferences:                      Color.
* Manual:                                Options.
* Options:                               Invoking.
* Search path:                           Environment.
* Who did it:                            Author.



Tag Table:
Node: Top435
Node: Invoking1582
Node: Configuration4971
Node: Configuration file5214
Node: Color6003
Node: Keys6633
Node: Keybindings7521
Node: Options11963
Node: Example config file15691
Node: Environment19147
Node: Internationalization Support19552
Node: Author19996
Node: Bugs20679
Node: Copyright20825
Node: Index21769

End Tag Table
