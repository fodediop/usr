MagicMarker.app (v1.0a11) (Mon 17 Sep 2001)

MagicMarker is a small graphical utility to "mark" processes. Marking involves
setting the performance monitor mark bit (PMM). This is bit 29 in the MSR. 

Yusuf Abdulghani (yusuf@apple.com). 4-5100. 
Architecture and Performance Group

