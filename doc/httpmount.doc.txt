[if viewing in a GUI browser, you might want to shrink the text, eg cmnd-'-']

finder:~=> ~diskimages/bin/httpmount -h
Usage:  httpmount [<baseurl>] - mount disk images
        httpmount -get [-wgetquiet] [<baseurl>] - download disk images
        httpmount -install [<baseurl>] <targetvol> [<makeinstaller opts>]

httpmount is a text-based browser used to locate disk images that you
want to mount, download, or install from.  If you have a reliable, fast
connection, you can use httpmount -install /Volumes/mysparevol to directly
generate the installer partition with makeinstaller.  If you'd rather
download separately, you'll need 'wget' somewhere in your PATH or in the
directory with 'httpmount' (wget existed on 10.0 and has recursive download
capability; doesn't exist on 10.1), and then you can do 'httpmount -get'
and select a whole directory of images (eg. when #1 points to
http://cd.apple.com/CDs/Puma5G62/).

Here's how I take images home with me on my iBook:
boris:/ltmp/imgs=> httpmount -get -wgetquiet
using /usr/bin/curl
        Welcome to httpmount
        You are in DOWNLOAD mode (-get option).
        type control-D or 'q' to exit
1) http://cd.apple.com/CDs/            15) http://cd.apple.com/CDs/Puma5G49/
2) http://cd.apple.com/CDs/Anaconda/   16) http://cd.apple.com/CDs/Puma5G50/
[...]
11) http://cd.apple.com/CDs/Puma5G45/  25) http://cd.apple.com/CDs/Puma5G62/
12) http://cd.apple.com/CDs/Puma5G46/  26) http://cd.apple.com/CDs/Puma5G64/
13) http://cd.apple.com/CDs/Puma5G47/  27) http://cd.apple.com/CDs/Tigger/
14) http://cd.apple.com/CDs/Puma5G48/
?# 26
http://cd.apple.com/CDs/Puma5G64/ was selected
http://cd.apple.com/CDs/Puma5G64/ appears to be a directory
1) http://cd.apple.com/CDs/Puma5G64/
2) http://cd.apple.com/CDs/Puma5G64/PPCDevCD/
3) http://cd.apple.com/CDs/Puma5G64/PPCLocalCD/
4) http://cd.apple.com/CDs/Puma5G64/PPCSeedCD/
5) http://cd.apple.com/CDs/Puma5G64/PPCUpdateCD/
?# 1
http://cd.apple.com/CDs/Puma5G64/ was selected
/Users/soren/bin/wget -q -c -A dmg -X*PPCUpdateCD* -np -r -l 0   4.52s user 57.98s system 41% cpu 2:32.24 total
[note I had wget in ~/bin -- required for recursive download which is in
turn required for makeinstaller to directly understand what you download ...  
wget is available in ~public/bin and http://puppy.apple.com/tools]


Let's make an install partition (see also ~diskimages/doc/makeinstaller):
[jyb:/Volumes] root# ~diskimages/bin/httpmount -install /Volumes/Whack/ -nobless
using /usr/bin/curl
        Welcome to httpmount
        You are in INSTALL mode (-install option).
        You should select an entire directory (using #1).
        type control-D or 'q' to exit
1) http://cd.apple.com/CDs/            15) http://cd.apple.com/CDs/Puma5G49/
2) http://cd.apple.com/CDs/Anaconda/   16) http://cd.apple.com/CDs/Puma5G50/
[...]
11) http://cd.apple.com/CDs/Puma5G45/  25) http://cd.apple.com/CDs/Puma5G62/
12) http://cd.apple.com/CDs/Puma5G46/  26) http://cd.apple.com/CDs/Puma5G64/
13) http://cd.apple.com/CDs/Puma5G47/  27) http://cd.apple.com/CDs/Tigger/
14) http://cd.apple.com/CDs/Puma5G48/
?# 26
http://cd.apple.com/CDs/Puma5G64/ was selected
http://cd.apple.com/CDs/Puma5G64/ appears to be a directory
1) http://cd.apple.com/CDs/Puma5G64/
2) http://cd.apple.com/CDs/Puma5G64/PPCDevCD/
3) http://cd.apple.com/CDs/Puma5G64/PPCLocalCD/
4) http://cd.apple.com/CDs/Puma5G64/PPCSeedCD/
5) http://cd.apple.com/CDs/Puma5G64/PPCUpdateCD/
?# 1
http://cd.apple.com/CDs/Puma5G64/ was selected

[at this point 
makeinstaller -nobless http://cd.apple.com/CDs/Puma5G64/ /Volumes/Whack/
is invoked; see ~diskimages/doc/makeinstaller]
