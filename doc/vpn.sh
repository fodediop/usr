Warning: the following script is extremely sketchy, and should be used with caution. I can use it to connect to Apple on my x86 Linux Mandrake machine. Depending on your distribution, this may work without modification on Redhat 7.x, LinuxPPC, and other flavors of Linux.

1) Save the following script as "vpn.sh"
2) Copy it to your linux machine.
3) Also copy the "identity" and "id_dsa" files from /AppleInternal/Applications/VPN Connect.app/Contents/Resources" to the same directory
4) Make sure you have a recent OpenSSH installed (Ideally 2.9)
5) Run ./vpn.sh. At the username and PASSCODE prompts, enter the right info.

6) There is no way of disconnecting. Kill the pppd process
7) The DNS server is not added automatically. Edit /etc/resolv.conf to add
   "nameserver 17.128.100.10" as the first nameserver entry

Eventually I will figure out a way to integrate this with the existing ppp infrastructure like we have
on X. Until then, this shell script does a pretty good job...

Shantonu
