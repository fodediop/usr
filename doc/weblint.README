
			  Weblint, Version 1.020

    Copyright (c) 1994, 1995, 1996, 1997 Neil Bowers.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the "Artistic License".  You should have received
    a copy of the Artistic License with this distribution, in the file named
    "Artistic".  If not, I'll be glad to provide one.

--------------------------------------------------------------------------

Weblint is a syntax and minimal style checker for HTML: a perl script which
picks fluff off html pages, much in the same way traditional lint picks fluff
off C programs.  Files to be checked are passed on the command-line:

    % weblint *.html

Warnings are generated a la lint -- <filename>(line #): <warning>. E.g.:

    home.html(9): malformed heading - open tag is <H1>, but closing is </H2>

Weblint includes the following features:

    *	by default checks for HTML 3.2 (Wilbur)
    *	46 different checks and warnings
    *	Warnings can be enabled/disabled individually, as per your preference
    *	basic structure and syntax checks
    *	warnings for use of unknown elements and element attributes.
    *	context checks (where a tag must appear within a certain element).
    *	overlapped or illegally nested elements.
    *	do IMG elements have ALT text?
    *	catches elements which should only appear once
    *	flags obsolete elements.
    *	support for user and site configuration files
    *	stylistic checks
    *	checks for html which is not portable across all browsers
    *	flags markup embedded in comments, since this can confuse some browsers
    *	support for Netscape (v4), and Microsoft (v4) HTML extensions

All warnings can be enabled or disabled, using a configuration file,
$HOME/.weblintrc.  A sample configuration file, weblintrc, is included
in the distribution.  Weblint also supports a site-wide configuration
file, which lets a group of people share a common configuration.
See the man page for details.

--------------------------------------------------------------------------

Installation

NOTE:	Certain versions of perl have bugs which are triggered by weblint.
	You shouldn't experience problems if you have 4.036, or 5.004.

A simple Makefile is provided, in which you may want to modify BINDIR
and MANDIR, which specify where the weblint script and manpage should
be installed.

Weblint uses the `newgetopt.pl' and `find.pl' libraries, which are part
of the standard perl library, so this will hopefully not cause any problems.
Please let me know if it does.

The manpage (weblint.1) should be installed in a directory where it
will be picked up by man (e.g., /usr/local/man/man1 on our machines).
A postscript version of the manpage (weblint.ps) is available at:
	ftp://ftp.cre.canon.co.uk/pub/weblint/weblint.ps

Weblint is available via anonymous ftp, either as a gzip'd tar file,
or zip'd archive:
	ftp://ftp.cre.canon.co.uk/pub/weblint/weblint-1.020.tar.gz
	ftp://ftp.cre.canon.co.uk/pub/weblint/weblint-1.020.zip
or you can get it from the weblint home page:
	http://www.cre.canon.co.uk/~neilb/weblint/
The home page also includes a list of other ftp sites where you
can get weblint.

The weblint distribution includes a simple regression testsuite.
Run the testsuite with either of the following commands:

	% make test
	% ./test.pl

If any of the tests fail, please mail the logfile (test.log) to me.

--------------------------------------------------------------------------

I hope you find this useful.  Comments, suggestions and bug reports are
welcome, see my email address or home page, given below.  Sample html
helps when tracking down problems.

I maintain two email lists:

	weblint-announce@cre.canon.co.uk
		Announcements for new versions of weblint.

	weblint-victims@cre.canon.co.uk
		Discussion related to weblint (such as what features should
		be added), as well as announcements for new versions of
		weblint, and pre-release testing.

Email me if you want to be added to either list. You don't need to be
on both lists.

Neil Bowers
Canon Research Centre Europe
<neilb@cre.canon.co.uk>    http://www.cre.canon.co.uk/~neilb/
