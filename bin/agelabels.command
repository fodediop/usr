#!/bin/bash 
CDAPP="$1/Contents/Resources/CocoaDialog.app"
CD="$CDAPP/Contents/MacOS/CocoaDialog"
SLAPP="$1/Contents/Resources/SetLabel"

### Simple example, one bubble
# $CD bubble --debug --title "My first bubble" --text "How do you like it?"

### Changing the colors and icon
# $CD bubble --debug --title "Setting colors" --text "Green to light green" \
# 	--background-top "00cb24" --background-bottom "aefe95" \
# 	--icon "hazard"

### Let's get a little more fancy.  Here are 2 bubbles, with custom icons,
### custom border colors, custom backgrounds, AND custom text colors.
### We'll even mix stock icons and custom icons.
# $CD bubble --debug --titles "Bubble 1" "Second bubble"        \
# 	--texts "Body of bubble 1" "and body of bubble 2" \
# 	--border-colors "2100b4" "a25f0a"                 \
# 	--text-colors "180082" "000000"                   \
# 	--background-tops "aabdcf" "dfa723"               \
# 	--background-bottoms "86c7fe" "fdde88"            \
# 	--icon-files "/Users/davec/Pictures/Icons/icon.png"  \
# 	             "$CDAPP/Contents/Resources/globe.icns"

if [ $# = 1 ] ||  [ ! -d $2 ] ; then
    $CD ok-msgbox --title "Notice" --icon info --text "Nothing to process" --informative-text "You must drag and drop a folder onto the application icon for this program to work"
    exit 1
fi

dir=$2
rv=`$CD dropdown --string-output --text "Setting labels in: $2" --informative-text  "Select nesting level" --icon folder --items "All" "Level 1" "Level 2" "Level 3" --button1 "Ok" --button2 "Cancel"` 
for item in $rv ; do
    case "$item" in
	"Cancel")
	    exit 1
	    ;;
	"All")
	    level=0
	    ;;
	[1-3])
	    level=$item
	    ;;
	*)
	    
    esac
    echo "$item"
done

if [[ $level == 0 ]] ; then
    maxdepth=""
else
    maxdepth="-maxdepth $level"
fi

logger "agelabels $2"											
find $dir "$maxdepth" -iname "[^.]*"  ! -newermt '3 years ago'  -exec $SLAPP Gray {} \;
logger "1 year +"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '1 year ago'  -exec $SLAPP Purple {} \;
logger "1 year"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '6 months ago'  -exec $SLAPP Blue {} \;
logger "6 months"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '3 months ago'  -exec $SLAPP Green {} \;
logger "3 months"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '1 month ago'  -exec $SLAPP Yellow {} \;
logger "1 month"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '2 weeks ago'  -exec $SLAPP Orange {} \;
logger "2 weeks"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '1 week ago'  -exec $SLAPP Red {} \;
logger "1 week"
find $dir "$maxdepth" -iname "[^.]*"  -newermt '1 day ago'  -exec $SLAPP None {} \;
logger "now"

