#!BOURNESHELL
#	program:	f2iaddr
#	release:	VERSION
#	input:		one network address on the command line, consisting of
#			dot-separated octets or words.
#	output:		the inverse address domain name of the address is sent
#			to stdout, or "ERROR" to stdout if problem occurs.
#			Usage printed to stderr, too, if usage error occurs.
#	example:	"134.114.64.24" -> "24.64.114.134.in-addr.arpa."
#	exit value:	0 if nothing goes wrong, 1 if something did.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

AWK=DOMLIB/f2iaddr.awk

if test $# -ne 1; then
	echo "usage: $0 ipaddress" >&2
	echo 'ERROR'
	exit 1
fi

echo $1 | awk -f $AWK
status=$?
if test $status -ne 0; then
	echo 'ERROR'
	exit 1
fi

exit $status
