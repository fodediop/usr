#!BOURNESHELL
#	program:	isequal
#	release:	VERSION
#	input:		two strings such as domain names, IP addrs, etc.
#	output:		"true" if they are identical regardless of case,
#			"false" if not.
#			"ERROR" is output if an error occurred.
#	exit value:	0 if "true", 1 if "false", 2 if error occurred.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

if test $# -ne 2; then
	echo "usage: $0 String string" >&2
	echo 'ERROR'
	exit 2
fi

str1=$1
str2=$2

# If str1 is exactly equal to str2, then return "true" else "false".
# This is a case-less compare (ignores upper/lower case differences).

echo $str1 | grep -i \^$str2\$ > /dev/null
if test $? -eq 0; then
	echo "true"
	exit 0
else
	echo "false"
	exit 1
fi

echo 'ERROR'
exit 2
