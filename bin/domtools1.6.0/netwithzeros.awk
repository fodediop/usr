#
# netwithzeros.awk
#	Supporting file for netwithzeros utility from Domtools package.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

{
	a[1]=0; a[2]=0; a[3]=0; a[4]=0;
	if (NF>=1)  a[1]=$1;
	if (NF>=2)  a[2]=$2;
	if (NF>=3)  a[3]=$3;
	if (NF>=4)  a[4]=$4;
	print a[1] "." a[2] "." a[3] "." a[4];
}
