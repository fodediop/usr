# f2iaddr.awk
#	Forward to Inverse Address Conversion
#	"134.114.64.1" -> "1.64.114.134.in-addr.arpa."
#	If not exactly one field on input, we exit right then with status 1.
#	Multiple lines get processed separately, output to separate lines.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	retval = 0;
}

{
	if (NF != 1) {
		retval = 1;
		exit retval;
	}

	n = split ($0, f, ".");
	for (i=n; i>=1; i--)
		if (f[i] != "")
			printf ("%s.",f[i]);
	printf ("in-addr.arpa.\n");
}

END {
	exit retval;
}
