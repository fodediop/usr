#
# subdom.awk  - parse AXFR records from DIG, look only at the NS records,
#		and print only the sub-domain names.
#		Supporting file for subdom utility from Domtools package.
#
# NOTE: We know that input is axfr output, one record per line.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

# NS and SOA records: print the first field because it's the subdomain name.

$2=="NS" || $2=="SOA" {
	print $1 " subdomain";
	next;
}

# All other records: print the sub-domain name portion of the first field.

{
	len = split ($1, f, ".");
	for (i=2; i<len; i++)
		printf ("%s.", f[i]);
	printf ("%s\n", f[len]);
}
