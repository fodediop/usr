# type.awk
#	Network "thing" type determination code
#	If not exactly one field on input, we exit immediately with status 1.
#	Multiple lines get processed separately, output to separate lines.
#	If some problem occurs, exit 1.  No problems we exit 0.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	retval = 0;
}

# If not exactly one field on each line, error.
NF != 1 {
	retval = 1;
	next;
}

# Anything that ends with "in-addr.arpa." regardless of case is an inverse
# address domain name type of thing.
/[Ii][Nn]-[Aa][Dd][Dd][Rr]\.[Aa][Rr][Pp][Aa]\.$/ {
	print "inverse";
	next;
}

# Anything else that ends with a dot is a normal domain type of thing.
/\.$/ {
	print "domain";
	next;
}

# Anything that has 4 or less dot-separated fields, is completely numeric and
# no fields are null, then it is a forward address type of thing.
# NOTE: I could not get the regular expression matching to check numeric,
# so we do that in the shell script (rewrite this stuff in perl already!)
{
	n = split ($0, f, ".");
	if (n > 4) {
		retval = 1;
		next;
	}
	for (i=1; i<=n; i++) {
		if (f[i] == "") {
			retval = 1;
			next;
		}
	}
	print "forward";
	next;
}

# Anything not in the above categories is an error.
{
	retval = 1;
	next;
}

END {
	exit retval;
}
