		     DOMTOOLS RELEASES AND CHANGES

Version 1.6.0 released 11/21/2000:
 * Added support for DiG 9.x (BIND 9.0.1)
 * Replaced internal digparse utility with digstd utility - you call digstd
   as if it were dig, it calls dig for you with right options for your version
   and returns results in a standard easy-to-parse format.
 * Added SRV record support (ala RFC 2052).
 * Added LOC record support (ala RFC 1876).
 * Minor speed improvement to axfr
 * Numerous bug fixes and support for different local host configurations
 * Updated HOWTOUSE and MANUAL documents

Version 1.5.0 released 01/02/2000:
 * Added tools: mask2bits, bits2mask (netmask computation)
 * Added tool: geniplist (list all possible host IPs given a network and netmask)
 * Fixed gensubnetlist to return all-0 and all-1 subnets, which are legal.
 * Fixed mistake in README (don't have to "make", no C progs anymore).

Version 1.4.0 released 03/18/1999:
 * Domtools is now officially Gnu Public Licensed.
 * BIND 8.1 support - added digparse from Dlint distribution which can handle
   dig versions 2.x and 8.x; digoutany.awk and axfroutany.awk removed from
   distribution.
 * Added "@server.dom.ain." arguments to most utilities to force specific nameserver.
   This enables operation behind firewalls, or to query specific servers.
 * Rewrote addr2net and gensubnetlist in perl (yaay, no more C programs to compile!)
   This means the Domtools package is now platform-independent.
 * axfr: fixed bug in zonecache of root zone (".") tried to create file named
   "." in the zone cache directory; special-cased it to create file "ROOT".
 * Makefile now embeds version number in all scripts before installation.
 * siteinfo: added host recognition of: irc.
 * network, netmask: fixed 3 error detection bugs.
 * check1101 much more forgiving of IP address argument mistakes.
 * makestatic: corrected minor output errors.
 * cachebuild: new output format just like named.root file on Internic FTP site
   (-o option produces old output format).
 * digparse: fixed bug: line 1 had perl path hardcoded.
 * Domtools is year 2000 capable.  It performs no date-related manipulations.

Version 1.3.1 released 01/16/1997:
 * Fixed nasty false-positive bug in soalist [fix submitted by Bill Welch].

Version 1.3 released 12/31/1996:
 * created new soalist tool.
 * digout.awk: removed from distribution; digoutany.awk replaces it.
 * World-writable zone cache directory is now optional.
 * Makefile: physical vs. logical destinations now specified separately.
 * rndarg rewritten in Perl for speed.
 * rnd.c no longer needed; removed from distribution.
 * networktbl, netmasktbl no longer hardcode networks.nau.edu domain;
   you can now specify the domain or disable the feature from Makefile.
 * Copies of README, MANUAL, HOWTOUSE are copied to the lib directory during
   installation for user's reference.
 * gw: combined into netname tool, after boiling code down and discovering
   it was identical.
 * check1101: fixed bug that tried processing domain "ERROR" (result from
   previous query had failed).
 * subzone: fixed bug in identifying if arg is really a zone or not.
 * all sh scripts: removed use of getopt, some Unixes (BSDI) don't have it.
 * hosttbl: more resilient to problems now (tries to continue on).
 * netname, gw: Fixed bug where non-local queries were failing when they
   should have succeeded.
 * new tool: "nsap" for NSAP RR queries.
 * all RR tools: Proper CNAME handling by re-querying.
 * siteinfo: Lifted second-level-domain-only restriction.
 * siteinfo: if SOA RR email field already has an "@" (technically illegal),
   we won't add a second one.
 * localad: fixed bugs for AIX netstat.
 * hosttbl: optional host alias level on command line, default set in Makefile
   (requested by jdc@nuxi.ucc.nau.edu).
 * Author's address updated.
 * Miscellaneous cleanup and improvements.
 * root tool: no longer depends on static list of root servers to query.

Version 1.2:
 * Added descriptions of makestatic, netmasktbl to MANUAL
 * Created cachebuild tool (actually integrated it from separate distribution)
 * New tools: localad, localdom, with writeups in: MANUAL, HOWTOUSE
 * Added code to ptr tool that uses "type" tool on domain name passed in
 * Fixed bug in "type" tool that made it think "nau.edu" was a forward address
 * Netmask tool renamed to "subnetmask".
 * Rewrote subnetmask to use other tools, instead of doing everything by hand.
 * Wrote a new "netmask" tool, see MANUAL
 * Wrote a tool called "network", see MANUAL
 * Wrote a tool called "siteinfo", see MANUAL
 * Fixed major output bug in axfr; it's program-parsable now.
 * Hosttbl.awk revised to deal with new improved axfr output.
 * Fixed bug in subdom (subdom2) so that domain names aren't forced to lower-case.
 * All low-level tools follow CNAME records properly.
 * All tools now parameterized $0 in usage text and error messages.
 * Fixed two infinite recursion bugs in subdom2.
 * Fixed axfr bug with nameservers that don't think they serve the given zone.
 * Rewrote hosts tool in Perl.
 * Renamed the "ad" tool to "address".
 * Filenames in zonecache/ are forced lowercased with periods on the end.

Version 1.1:
 * Lotsa work in my spare time, over the last 3 years
 * Released to public in Feb 1994, but never advertised.

