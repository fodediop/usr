#!BOURNESHELL
#	program:	makestatic
#	release:	VERSION
#	purpose:	Build three static network tables based on DNS queries
#			in the exact syntax of /etc/hosts, /etc/networks, and
#			/etc/netmasks (the last primarily used on Suns), and
#			install them in some globally accessable directory.
#			There are static header and footer files for each,
#			which can be edited as needed to contain comments,
#			special addresses, and other stuff not available
#			via DNS queries.
#	input:		Domain name and destination directory on cmd line,
#			optional nameserver to direct queries to.
#	output:		Three files named "hosts", "networks", "netmasks".
#			or the word "ERROR" (distinguishable from a real
#			record because it has no period on the end).
#	exit value:	0 if succeeds, or exit status of the last file
#			we attempted to build.  We attempt to build each file
#			regardless of whether the previous one(s) succeeded
#			or failed to build.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

TMPHOSTS=/tmp/`basename $0`-hosts.$$
TMPNETWORKS=/tmp/`basename $0`-networks.$$
TMPNETMASKS=/tmp/`basename $0`-netmasks.$$

usage() {
	echo "usage: makestatic [@nameserver] dom.ain. /dest/directory" >&2
	exit 1
}

# Get domain we're supposed to work with from command line.
if test $# -lt 2 -o $# -gt 3; then
	usage
fi

domain=
PUTDIR=
atnameserver=
while test $# -gt 0; do
        case "$1" in
                @*)  atnameserver="$1" ;;
		*)   if test x"$domain" = x""; then
			domain="$1"
		     else
			if test x"$PUTDIR" = x""; then
				PUTDIR="$1"
			else
				usage
			fi
		     fi ;;
        esac
        shift
done

# Verify arguments.

if test ! -d "$PUTDIR"; then
	echo "$0: $PUTDIR is not a directory or doesn't exist." >&2
	usage
fi
if test ! -w "$PUTDIR"; then
	echo "$0: directory $PUTDIR is not writable by me." >&2
	exit 1
fi

# Assume there won't be any problems.
status=0

# Figure out if networktbl and netmasktbl were built with -x option.
networksdomopt=
test x"NETWORKSDOM" != x"" && networksdomopt="-x"


# 1. Build the /etc/hosts table from DNS queries.

DOMBIN/hosttbl $atnameserver $domain > $TMPHOSTS
hoststatus=$?
if test $hoststatus -eq 0; then
	rm $PUTDIR/hosts >/dev/null 2>&1
	cp $TMPHOSTS $PUTDIR/hosts
	chmod 644 $PUTDIR/hosts
else
	status=$hoststatus
fi
rm -f $TMPHOSTS


# 2. Build the /etc/networks table from DNS queries.

DOMBIN/networktbl -h $networksdomopt $atnameserver $domain > $TMPNETWORKS
netwstatus=$?
if test $netwstatus -eq 0; then
	rm $PUTDIR/networks >/dev/null 2>&1
	cp $TMPNETWORKS $PUTDIR/networks
	chmod 644 $PUTDIR/networks
else
	status=$netwstatus
fi
rm -f $TMPNETWORKS


# 3. Build the /etc/netmasks table from DNS queries.

DOMBIN/netmasktbl $networksdomopt $atnameserver $domain > $TMPNETMASKS
netmstatus=$?
if test $netmstatus -eq 0; then
	rm $PUTDIR/netmasks >/dev/null 2>&1
	cp $TMPNETMASKS $PUTDIR/netmasks
	chmod 644 $PUTDIR/netmasks
else
	status=$netmstatus
fi
rm -f $TMPNETMASKS


exit $status
