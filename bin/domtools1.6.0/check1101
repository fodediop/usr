#!BOURNESHELL
#	program:	check1101
#	release:	VERSION
#	purpose:	To examine a zone's resource records and make sure
#			that the optional things from RFC1101 are done right.
#	input:		domain name on command line,
#			optional nameserver to direct queries to.
#	output:		A verbose description of what was found and what
#			looks wrong to stdout.	Usage errors go to stderr.
#	exit value:	0 if nothing went wrong, 1 if something did.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

usage() {
	echo "usage: $0 [@nameserver] dom.ain." >&2
	echo "	     $0 -r [-s] [@nameserver] hostzeroaddress [parentnetmask]" >&2
	exit 1
}
if test $# -lt 1 -o $# -gt 4; then
	usage
fi

SFLAG=0 RFLAG=0 retval=0 arg1= arg2=
atnameserver=
for i do
	case "$i" in
		-s)	SFLAG=1 ;;
		-r)	RFLAG=1 ;;
		@*)	atnameserver="$i" ;;
		*)	if test x"$arg1" = x""; then
				arg1="$i"
			else
				if test $RFLAG -eq 0; then
					usage
				else
					if test x"$arg2" != x""; then
						usage
					fi
					arg2="$i"
				fi
			fi
	esac
done
if test $RFLAG -eq 0; then
	zone="$arg1"
else
	hostzero="$arg1"
	parentnetmask="$arg2"	# can be empty string if not specified on command line
	# Convert hostzero to an in-addr domain name if it's not already
	if test x`DOMBIN/type $hostzero` = x"forward"; then
		hostzero=`DOMBIN/netwithzeros $hostzero`
		hostzero=`DOMBIN/f2iaddr $hostzero`
	fi
fi

if test $SFLAG -eq 0; then
	echo "; Check1101 version VERSION, Copyright (C) 1993 Paul A. Balyoz"
	echo "; Check1101 comes with ABSOLUTELY NO WARRANTY."
	echo "; This is free software, and you are welcome to redistribute it"
	echo "; under certain conditions; see file COPYING for details."
	echo ";"
fi

# Decide which path to follow; domain, or hostzero address

if test $RFLAG -eq 0; then
	echo ";; Checking zone $zone"

# Make sure what they gave us is really a zone served by name servers!

	nameservers=`DOMBIN/ns $atnameserver $zone`
	if test $? -ne 0 -o x"$nameservers" = x""; then
		echo "ERROR: $zone doesn't appear to have any nameservers."
		retval=1
	else
		echo ";; nameservers $nameservers" | tr '\012' ' '
		echo ""
	fi

# Verify zone has one or more host zero addresses,
# and call us recursively to verify them all.

	numnets=0
	hostzeroes=`DOMBIN/ptr $atnameserver $zone`
	if test $? -ne 0; then
		echo "ERROR: $zone has no PTR records pointing to host-zero records."
		retval=1
	else
		for hostzero in $hostzeroes; do
			$0 -s -r $hostzero
			if test $retval -eq 0; then
				retval=$?
			fi
			numnets=`expr $numnets + 1`
		done
	fi

	S=""; if test $numnets -ne 1; then S="s"; fi
	echo ";; $numnets network$S total."

else

#
# -r flag -- verify $hostzero recursively.
#

	echo ";; Checking $hostzero host zero records."
	NONAME=0
	name=`DOMBIN/ptr $atnameserver $hostzero`
	if test $? -ne 0; then
		retval=$?
		echo "ERROR: no $hostzero PTR record pointing to network name."
		NONAME=1
	fi
	net=`DOMBIN/i2faddr $hostzero`
	if test $? -ne 0; then
		echo "ERROR: i2faddr $hostzero failed, maybe you specified the wrong zone."
		exit 1
	fi
	if test $NONAME -eq 0; then
		echo ";; network $net name record $name was found."
	fi

	newhostzero=`DOMBIN/ptr $atnameserver $name`
	if test $? -ne 0; then
		echo "ERROR: $name PTR record not found, it should point to $hostzero"
	fi
	if test x"$newhostzero" = x"$hostzero"; then
		echo ";; pointer from net name to host-zero record exists."
	else
		echo "ERROR: $name PTR record points to $newhostzero instead of $hostzero"
	fi

# Look for a netmask from this host-zero address, and loop through all subnets
# if the netmask implies we're subnetted.

	if test x"$parentnetmask" = x""; then
		parentnetmask=`DOMBIN/addr2mask $net`
	fi
	netmask=`DOMBIN/address $atnameserver $hostzero`
	if test $? -eq 0; then
		echo ";; network $net netmask $netmask record found."
	else
		netmask=$parentnetmask
		echo ";; note: no netmask found for $net, assuming $netmask"
	fi
	if test x"$parentnetmask" = x"$netmask"; then
		echo ";; network is not subnetted."
	else
		echo ";; network is subnetted."

# add loop through all subnets here...

# add recursive call to self with -s -r and parentnetmask here...

	fi
fi

exit $retval
