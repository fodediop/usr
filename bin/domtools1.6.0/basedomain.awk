#
# basedomain.awk
#	Determines the base-domain of a given domain name.
#	"cse.nau.edu." -> "nau.edu."
#	If not exactly one field on input, we exit immediately with status 1.
#	Multiple lines get processed separately, output to separate lines.
#	If some problem occurs, exit 1.  No problems we exit 0.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	retval = 0;
}

{
# If not exactly one field on each line, error.
	if (NF != 1) {
		retval = 1;
		exit retval;
	}

# if root domain, we can't determine what domain that's in, so error.
	if ($0 == ".") {
		retval = 1;
		exit retval;
	}

# break the domain name into pieces and print the answer.
# of course, if we don't see any dots at all, that's an error.
	n = split ($0, f, ".");
	if (n < 2) {
		retval = 1;
		exit retval;
	}

	for (i=2; i<n-1; i++)
		printf ("%s.", f[i]);
	if (n > 2)
		printf ("%s", f[n-1]);
	printf (".\n");
}

END {
	exit retval;
}
