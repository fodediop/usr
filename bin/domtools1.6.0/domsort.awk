#
# domsort.awk
#
#	input:		rainbow.cse.nau.edu.
#	output:		!edu!nau!cse!rainbow rainbow.cse.nau.edu.
#
# The output from here can be sorted with "sort", then run thru "sed"
# to strip off the first token, leaving behind the original good strings
# in domain-name-sorted order.  Exclamation marks are used instead of
# periods because it is a character that comes very early in the
# ascii character set, right after space, and before all other letters,
# numbers, and punctuation.  The problem was that domain names containing
# a hyphen "-" at the critical point were sorting incorrectly.
#
# If you can sort the following two groups of data, you've probably
# solved the problem:
#
#	Input Data 1:
#		smg.ucc.nAu.edu
#		ucc-sunnet.nAu.edu
#		systems1.ucc.nAu.edu
#
#	Input Data 2:
#		a1.ucc.nau.edu.
#		ucc-sunnet.nau.edu.
#		ucc.nau.edu.
#
# The exclamation mark must NOT sort BEFORE the space character,
# so that any given domain comes before it's subdomains.
#
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

{
	num = split ($1, f, ".");
	revdom=f[1];
	for (i=2; i<=num; i++)
		revdom = f[i] "!" revdom;
	print revdom " " $0;
}
