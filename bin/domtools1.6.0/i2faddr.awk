#	Inverse to Forward Address Conversion
#	"1.64.114.134.in-addr.arpa." -> "134.114.64.1"
#	If not exactly one field on input, we exit right then with status 1.
#	Multiple lines get processed separately, output to separate lines.
#	Neither quantity nor numericalicity of the input "octets" are checked.
#	(numericalicity?? :-)
#	The last two dot-separated fields must of course be
#	If not enough fields or we don't see "in-addr.arpa" in some form on
#	the end, we exit 1.  If successful we write output then exit 0.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	retval = 0;
}

{
	if (NF != 1) {
		retval = 1;
		exit retval;
	}

	n = split ($0, f, ".");

# reparse it all to get rid of null fields
	str = "";
	for (i=1; i<=n; i++)
		if (f[i] != "")
			str = str " " f[i];
	n = split (str, f);

# make sure we're still in business
	if (n < 3) {
		retval = 1;
		exit retval;
	}
	if (f[n] !~ /^[Aa][Rr][Pp][Aa]$/ || f[n-1] !~ /^[Ii][Nn]-[Aa][Dd][Dd][Rr]$/) {
		retval = 1;
		exit retval;
	}

# print answer
	for (i=n-2; i>=2; i--)
		printf ("%s.",f[i]);
	print f[1];
}

END {
	exit retval;
}
