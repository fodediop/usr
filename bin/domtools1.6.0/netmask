#!BOURNESHELL
#	program:	netmask
#	release:	VERSION
#	purpose:	Figure out the actual netmask for the given IP address
#			(host or network) by looking for Subnet "A" RRs in
#			the in-addr.arpa. domain hierarchy.  This algorithm
#			comes from RFC1101 section 4.4.	 If the domain database
#			does not include RFC1101 enhancements, this tool will
#			always return the default netmask according to what
#			class the network belongs (A, B, C, ...)
#	input:		IP address in normal format ("134.114.64.2"),
#			optional nameserver to direct queries to.
#	output:		network mask address record if any found,
#			or the word "ERROR".
#	exit value:	0 if succeeds, 1 if fails (and "ERROR" output).
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

if test $# -lt 1 -o $# -gt 2; then
	echo "usage: $0 [@nameserver] ipaddress" >&2
	echo 'ERROR'
	exit 1
fi

ip=
nameservers=
atnameserver=
while test $# -gt 0; do
        case "$1" in
                @*)  atnameserver="$1" nameservers=`echo $1 | sed -e 's/@//'` ;;
                *)  ip="$1" ;;
        esac
        shift
done
if test x"$ip" = x""; then
        echo "$0: No IP address specified; aborting." >&2
        echo "usage: $0 [@nameserver] ipaddress" >&2
        echo 'ERROR'
        exit 1
fi

if test "`DOMBIN/type $ip`" != "forward"; then
	echo "$0: argument $ip is not an IP address." >&2
	echo 'ERROR'
	exit 1
fi

addr="`DOMBIN/netwithzeros $ip`"
if test x"$addr" = x"ERROR"; then
	echo "$0: netwithzeros failed to work on $ip IP address." >&2
	echo 'ERROR'
	exit 1
fi

# Start by applying the default netmask for the network.
mask="`DOMBIN/addr2mask $addr`"
if test x"$mask" = x"ERROR"; then
	echo "$0: addr2mask failed to work on $addr IP address." >&2
	echo 'ERROR'
	exit 1
fi

# Potentially infinite levels of sub-sub-networks.
while true; do
	# Figure out the subnetwork address.
	net="`DOMBIN/addr2net $addr $mask`"
	if test x"$net" = x"ERROR"; then
		echo "$0: addr2net failed to work on $addr address with $mask subnetmask." >&2
		echo 'ERROR'
		exit 1
	fi

	# Force network number into in-addr format.
	d="`DOMBIN/f2iaddr $net`"
	if test x"$d" = x"ERROR"; then
		echo "$0: f2iaddr failed to work on $net network address." >&2
		echo 'ERROR'
		exit 1
	fi

	# Do an "A" RR query to get netmask record, ala RFC 1101.
	oldmask="$mask"
	mask="`DOMBIN/address $atnameserver $d 2> /dev/null`"
	if test $? -ne 0; then
		break
	fi
	# If mask looked up is the default for this network, avoid infinite-loop!
	if test x"$mask" = x"$oldmask"; then
		break
	fi
done

echo "$oldmask"
exit 0
