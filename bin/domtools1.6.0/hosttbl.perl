#!PERLPROG -n
#
# Filter that accepts "axfr" output on stdin, and outputs only those records
# that fit a certain criteria.  The "XXXX" strings get replaced with the domain
# we're interested in, by hosttbl.  Only SOA and NS records for that domain
# will be output, along with any host records immediately inside that domain.
# All other records are discarded.  This is useful when the zone is outside
# the current domain, i.e., we want domain "cse.nau.edu." but that domain
# is not it's own zone, it's in zone "nau.edu.", and so you have to filter
# through the entire campus database to extract only the cse records.
# (Fictitious example!)
#
# Perl is the only script language I've found that has powerful enough
# pattern matching to do what we want.
#
# BUG: the periods in the domain name are interpreted here as
#      "any character", they really should be escaped properly for the
#      perl pattern, but how?!?
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

chop;
@a = split(/[ \t]+/, $_, 9999);
if ($a[1] eq "SOA" || $a[1] eq "NS") {
	print "$_\n" if $a[0] =~ /^XXXX$/i;
} else {
	print "$_\n" if $a[0] =~ /^[a-zA-Z0-9_-]+.XXXX$/i;
}
