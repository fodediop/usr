#
# networktbl.awk
#
# Print one or more lines of tabulated /etc/networks fields based on
# the information given to us on stdin.  Each input line must be in one of
# these forms:
#		naunet 134.114
# or:		naunet 134.114 -h
#
# The first line would generate this for output:
#
#		naunet		134.114
#
# The "-h" in the second example of input means to make a hyphenated 3rd field
# on the output line, if possible:
#
#		naunet		134.114		nau-net
#
# (It basically just looks to see if the network name has "net" at the end;
# if so, it adds a hyphen and reprints the name as the 3rd field.)
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	FIRSTTAB=2;		# 8-space tab stops
	SECONDTAB=4;		# 8-space tab stops
	hostaliases="no";
}

NF>2 && $3=="-h" {
	hostaliases="yes";
}

{
	str = $1;
	len = length(str);

	anytabs=0;
	for (i=1; i<=FIRSTTAB; i++) {
		if (len < i*8) {
			str = str "	";
			anytabs++;
		}
	}
	if (anytabs == 0)
		str = str " ";		# at LEAST use one space as separator...

	str = str $2;
	len = FIRSTTAB*8 + length($2);

	if (hostaliases == "yes") {

		if (substr($1, length($1)-2, 3) == "net") {

			anytabs=0;
			for (i=FIRSTTAB+1; i<=SECONDTAB; i++) {
				if (len < i*8) {
					str = str "	";
					anytabs++;
				}
			}
			if (anytabs == 0)
				str = str " ";		# at LEAST use one space

			hyphenstr = substr($1, 1, length($1)-3) "-net";
			str = str hyphenstr;
			len = SECONDTAB*8 + length(hyphenstr);
		}
	}

	print str;

	hostaliases="no";
}
