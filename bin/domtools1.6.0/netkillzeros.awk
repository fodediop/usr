#
# netkillzeros.awk
#	Supporting file for netkillzeros utility from Domtools package.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {FS="."}

{
	n = NF;
	if (n > 4)
		exit 1;
	else {
		while (n > 0 && ($n)+0 == 0)
			n--;

		for (i=1; i<=n; i++) {
			printf ("%d",($i)+0);
			if (i < n)
				printf (".");
		}
		printf ("\n");

		exit 0;
	}
}
