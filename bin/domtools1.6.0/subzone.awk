#
# subzone.awk - parse AXFR records from DIG, look only at the NS records,
#     		and print only the sub-zone names.  We get the current zone
#		from the SOA record, which must come before any NS records
#		that we are studying, or the NS records are ignored.
#
#		Output usually has multiple copies of each zone, since one
#		is printed for each NS record found.
#
# Copyright (C) 1993-2000 Paul A. Balyoz <pab@domtools.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

BEGIN {
	zonename = "------------------------------------------";  # illegal
}

# SOA records: just remember the zone name

$2 == "SOA" {
	zonename = $1;
	next;
}

# NS records: if it is a subzone, print its name.

$2 == "NS" {
	# ALERT: Should be case-insensitive comparison here, but its not!
	if (length($1) > length(zonename) && \
substr($1,length($1)-length(zonename)+1,length(zonename)) == zonename) {
		print $1;
	}
}
