#!/usr/bin/perl -w
use strict;
my %psdata;
my @parents;

my $stem=$0;
$stem =~ s|^.*/||;
my $os=`uname`;
chomp($os);
my $pscommand="";
$pscommand="ps -ef" if ($os eq "HP/UX");
$pscommand="ps -ef" if ($os eq "SunOS");
$pscommand="ps -auxjwww" if ($os eq "Darwin");
my $version="";

open(PS,"$pscommand|");
while(<PS>){
	$version="10.2" if /STARTED/;
	next if /PID/;
	chomp;
	&decodeline($_);
}
close PS;
&report;
exit;

#------------------------------------------------------------------
sub report{
	my $p;
	my $user;
	my $ppid;
	my $c;
	my $time;
	my $terminal;
	my $cpusage;
	my $command;
	my $buffer;
	foreach $p (sort keys (%psdata)){
		next if (grep(/$p/,@parents));
		$buffer = "\n---------------------------------------\n";
		($user,$ppid,$c,$time,$terminal,$cpusage,$command)=
			split(/\|\%\|/,$psdata{$p});
		$buffer .= sprintf("%8s %6d %9s %7s %8s %s\n",
			$user,$p,$time,$terminal,$cpusage,$command);
		while($p>1){
			$p=$ppid;
			next if ($p == 0);
			($user,$ppid,$c,$time,$terminal,$cpusage,$command)=
				split(/\|\%\|/,$psdata{$p});
			$buffer .= sprintf("%8s %6d %9s %7s %8s %s\n",
				$user,$p,$time,$terminal,$cpusage,$command);
		}
		if ($#ARGV>-1){
			if ($buffer =~ /$ARGV[0]/){
				unless ($buffer =~ /$stem/){
					print $buffer;
				}
			}
		} else {
			print $buffer;
		}
	}
}
#------------------------------------------------------------------
sub decodeline{
	my $line=shift(@_);
	
	my ($user,$pid,$ppid,$c,$time,$terminal,$cpusage,$command);
	my @elements=split(/\s+/,$line);
	shift(@elements) unless $elements[0];
	$user=shift(@elements);
	$pid=shift(@elements);
	return unless ($pid =~ /^\d+$/);
# from this point -ef      is PPID C    STIME TTY TIME COMMAND
# from this point -auxjwww is %CPU %MEM VSZ   RSS  TT  STAT TIME PPID PGID SESS JOBC COMMAND
# from this point -auxjwww is %CPU %MEM      VSZ    RSS  TT  STAT STARTED      TIME  PPID  PGID   SESS JOBC COMMAND if $version=10.2
	if ($pscommand eq 'ps -ef'){
		$ppid=shift(@elements);
		push(@parents,$ppid);
		$c = shift(@elements);
		$time=shift(@elements);
		if ($time =~ /\:/){
			#
		} else {
			$time = $time ." ".$elements[0];
			shift(@elements);
		}
		$terminal=shift(@elements);
		$cpusage = shift(@elements);
		$command=join(" ",@elements);
	} else {
		$cpusage = shift(@elements);
		shift(@elements); # Junk %MEM
		shift(@elements); # Junk VSZ
		shift(@elements); # Junk RSS
		$terminal=shift(@elements);
		$c = shift(@elements); # Actually STAT
		if ($version eq "10.2"){
			shift(@elements); # Junk STARTED
		}
		$time=shift(@elements);
		if ($time =~ /\:/){
			#
		} else {
			$time = $time ." ".$elements[0];
			shift(@elements);
		}
		$ppid=shift(@elements);
		push(@parents,$ppid);
		shift(@elements); # Junk PGID
		shift(@elements); # Junk SESS
		shift(@elements); # Junk JOBC
		$command=join(" ",@elements);
	}
	my $data=join("|%|",$user,$ppid,$c,$time,$terminal,$cpusage,$command);
	$psdata{$pid}=$data;
}

