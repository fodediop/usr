#!/bin/bash
##### ddtool
##### written and mainted by jack waterworth ( jack@redhat.com )

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

# stealing code from rsaw
  if [[ $BASH_VERSINFO -ge 4 ]]; then
    declare -A c
    c[reset]='\033[0;0m'
    c[grey]='\033[00;30m';  c[GREY]='\033[01;30m';  c[bg_GREY]='\033[40m'
    c[red]='\033[0;31m';    c[RED]='\033[1;31m';    c[bg_RED]='\033[41m'
    c[green]='\E[0;32m';  c[GREEN]='\033[1;32m';  c[bg_GREEN]='\033[42m'
    c[orange]='\033[0;33m'; c[ORANGE]='\033[1;33m'; c[bg_ORANGE]='\033[43m'
    c[blue]='\033[0;34m';   c[BLUE]='\033[1;34m';   c[bg_BLUE]='\033[44m'
    c[purple]='\033[0;35m'; c[PURPLE]='\033[1;35m'; c[bg_PURPLE]='\033[45m'
    c[cyan]='\033[0;36m';   c[CYAN]='\033[1;36m';   c[bg_CYAN]='\033[46m'
  fi
readonly FAIL=[${c[red]}-${c[reset]}]
readonly BAD=[${c[red]}+${c[reset]}]
readonly WARN=[${c[orange]}+${c[reset]}]
readonly OK=[${c[green]}+${c[reset]}]


usage() {
    echo "usage: $PROGNAME options"
    echo ""
    echo "OPTIONS:"
    echo "   -f --file                Specifies the file that will be parsed"
    echo "   -s --search              The amount of data we should search through"
    echo "   -p --primary             Treat all superblocks as primary superblocks"
    echo "   -l --loop                Create a loopback device to test the superblock"
    echo "   -d --dump                Run dumpe2fs against detected superblocks \(requires -l\)"
    echo "   -t --table               show output in a nice formatted table"
    echo "   -v --verbose             Shows hexdump output of superblocks"
    echo "   -x --debug               Show VERY detailed output"
    echo "   -h --help                show this help"
    echo ""
    echo "Examples:"
    echo "   Search first 10 MB of data of file /dev/sda:"
    echo "   $PROGNAME --file /dev/sda --search 10"
    echo ""
    echo "   Test each detected superblock by using loopback:"
    echo "   $PROGNAME --file /dev/sda --loop"

	exit 0;
}

cmdline() {
    # got this idea from here:
    # http://kirk.webfinish.com/2009/10/bash-shell-script-to-use-getopts-with-gnu-style-long-positional-parameters/
    local arg=
    for arg
    do
        local delim=""
        case "$arg" in
            #translate --gnu-long-options to -g (short options)
            --help)	args="${args}-h ";;
            --verbose)	args="${args}-v ";;
            --file)	args="${args}-f ";;
            --search)	args="${args}-s ";;
            --primary)	args="${args}-p ";;
            --loop)	args="${args}-l ";;
            --dump)	args="${args}-d ";;
            --table)	args="${args}-t ";;
            --debug)    args="${args}-x ";;



            #pass through anything else
            *) [[ "${arg:0:1}" == "-" ]] || delim="\""
                args="${args}${delim}${arg}${delim} ";;
        esac
    done

    #Reset the positional parameters to the short options
    eval set -- $args

    while getopts "vhpldtf:s:" OPTION
    do
         case $OPTION in
         v)
             VERBOSE=1
             ;;
         x)
             DEBUG=1
             ;;
         h)
             usage
             exit 0
             ;;
         f)
             FILE=$OPTARG
             ;;
         s)
             SEARCH=$OPTARG
             ;;
         p)
		PRIMARY=1
             ;;
         l)
             [[ "$USER" != "root" ]] \
		&& echo -e "$BAD you must be root to utilize loop devices" \
	        || readonly LOOP=1;
             ;;
         d)
	     DUMP=1
             ;;
         t)
		TABLE=1
		DUMP=0
		PRIMARY=0
		VERBOSE=0
		
             ;;
        esac
    done

    return 0
}

missing() {
    local file=$1

    [[ ! -e $file ]] 
}

loop_create() {
  local DISK=$1
  local OFFSET=$2

  [[ "$USER" != "root" ]] \
	&& return 1;

  echo -e "\t\t$WARN creating loopback at starting offset $OFFSET"
  losetup -o$OFFSET /dev/loop7 $DISK &>/dev/null;
}

loop_destroy() {
  losetup -d /dev/loop7;
}

use_dumpe2fs() {

	if [ "$DEBUG" != "1" ]; then
		dumpe2fs /dev/loop7 2>/dev/null \
	         | grep -e Backup -e Primary -e ^Last -e volume -e UUID \
	         | sed 's/^/\t\t\t/g'
	else
		dumpe2fs /dev/loop7
	fi
}

use_blkid() {
	echo -e "\t\t$WARN attempting to access superblock";
	echo -e "\t\t\t$(blkid /dev/loop7)";

	if [ "$?" -lt 1 ]; then
		echo -e "\t\t$OK Sucessfully found superblock";

                [[ "$DUMP" -eq "1" ]] \
                	&& use_dumpe2fs

         	echo -e "\t\t$OK Create your partition at $(( (0x$i / 512) - 2))";
         else
                echo -e "\t\t$FAIL Failed. Unable to determine partition";
         fi;
}

scan() {
  local DISK=$1
  local SIZE=$2

  local TIMES=$(echo $(( $SIZE / 10 )))
  local SKIP=0
  local AMOUNT=10
  local FS_UUID="0"
  local FS_UUID_OLD="0"
  local PRIMARY_SP="0"

  local B_OFFSET
  local S_SECTOR
  local S_TYPE
  local L_M_TIME
  local F_UUID
  local F_LABEL

  [[ "${TABLE}" == "1" ]] \
	&& echo -e "byte_offset\tstart_sector\tsb_type\tlast_mount_time\tfs_uuid\t\t\tlabel" \
	&& echo -e "-----------\t------------\t-------\t---------------\t-------\t\t\t-----"

 # allow us to have a size less than 10
  [[ "${TIMES}" == "0" ]] \
	&& TIMES="1" \
	&& AMOUNT=$SIZE

    # we dont want to do everything at once
    # we want to see thigns as we find them
    # this splits the search up in 10mb increments
    for x in $(seq 1 $TIMES);
     do

        [[ "$x" == "1" ]] \
		&& SKIP="0" \
		|| SKIP=$(( 10485760 * ( $x ) ))

       # this adds the dots to show we're doing something
        echo -en "\r[ $(echo "scale=2; 100 * ($SKIP / ($SIZE * 1048576))" | bc) % complete -- $SKIP / $((1048576 * $SIZE)) ]"


	for i in $(hexdump $DISK -s $SKIP -n $(( 1048576 * $AMOUNT )) | awk '$6 ~ /ef53/ && $7 ~ /000[0-4]/ && $8 ~ /000[1-3]/ {print $1}' );
	  do 
		[[ $(hexdump -s $((0x$i + 24)) $DISK -n2 | awk '{print $NF}' | head -1) != "0000" ]] \
			&& continue

		local SP_BG=$(hexdump -s $((0x$i + 42)) $DISK -n2 | awk '{print $NF}' | head -1)
                local FS_UUID="$(hexdump -s$(( 0x$i + 56 )) -n8 $DISK | awk '{print $2$2$3$4$5}')"
		if [ "$PRIMARY" != "1" ] && [ ${SP_BG} != "0000" ]; then
				[[ "$PRIMARY_SP" == "0" ]] \
					&& echo -e "\r     $FAIL WARNING! backup superblock detected without primary!"
				if [ "${TABLE}" != "1" ]; then
                                	echo -e "\r     $OK Backup ext2 superblock detected at $(( 0x$i )) bytes\t(blockgroup $(( 0x${SP_BG} )))" 
			 	else
					B_OFFSET=$(( 0x$i ))
					S_SECTOR=$(( (0x$i / 512) - 2))
					S_TYPE="backup"
					F_UUID=$FS_UUID
	                                echo -ne "\r                                                    "
					echo -e "\r$B_OFFSET\t$S_SECTOR\t\t$S_TYPE\tlast_mount_time\t$F_UUID\tlabel"
				fi
		else
	
			# FS_UUID_OLD is not getting set correctly below due to break.fixing it here
	                if [ "$FS_UUID" == "${FS_UUID_OLD}" ] && [ "$PRIMARY" != "1" ]; then
	                        if [ "${TABLE}" != "1" ]; then
	                                echo -e "\r     $WARN Stale ext2 superblock detected at $(( 0x$i )) bytes" 
					FS_UUID_OLD=$FS_UUID 
					break
				else
                                        FS_UUID_OLD=$FS_UUID
                                        B_OFFSET=$(( 0x$i ))
                                        S_SECTOR=$(( (0x$i / 512) - 2))
                                        S_TYPE="stale"
                                        F_UUID=$FS_UUID
                                        echo -ne "\r                                                    "
                                        echo -e "\r$B_OFFSET\t$S_SECTOR\t\t$S_TYPE\tlast_mount_time\t$F_UUID\tlabel"
					break;
				fi
			fi

			PRIMARY_SP="1"
			if [ "${TABLE}" != "1" ]; then
				echo -e "\r     $OK ext2 superblock detected at $(( 0x$i )) bytes"
				echo -e "\t$OK estimated starting sector $(( (0x$i / 512) - 2))"
			else
				B_OFFSET=$(( 0x$i ))
				S_SECTOR=$(( (0x$i / 512) - 2))
				S_TYPE="primary"
				F_UUID=$FS_UUID
				echo -ne "\r                                                    "
				echo -e "\r$B_OFFSET\t$S_SECTOR\t\t$S_TYPE\tlast_mount_time\t$F_UUID\tlabel"
			fi

		        [[ "$VERBOSE" == "1" ]] \
				&& hexdump -Cs $(echo $(( 0x$i -32))) -n1024 $DISK

			if [ "$LOOP" == "1" ]; then

				loop_create $DISK $(( 0x$i - 48 - 1024))
				use_blkid
				loop_destroy
			fi
		fi

		# this is to determine if we're hitting a new fs.
		FS_UUID_OLD=$FS_UUID;
	done
    done
}

main() {
   # parse arguments
    cmdline $ARGS

   # make sure we specified a file
    [[ -z $FILE ]] \
	&& echo -e "$FAIL --file is a required paramater" \
	&& usage;

   # check to see if file exists
    missing $FILE \
	&& echo -e "$FAIL $FILE does not exist" \
        && exit 1;

   # check to see if a search size is set
    [[ -z $SEARCH ]] \
        && echo -e "$WARN --search not specified. defaulting to 10 MB" \
	&& readonly SEARCH="10"

   # lets do the scan
    echo -e "$OK starting scan on $FILE"
    scan $FILE $SEARCH

    echo -e "\r$OK scanning complete                                           "

    exit 0;
}

main;
