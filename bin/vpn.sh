#!/bin/sh

# FILE: vpn.sh
# AUTH: Shantonu Sen (ssen) [did you take it from connect.sh?]
# DATE: 14 Sept 2001
# DESC: do what vpn connect does


export PATH="/usr/bin:/bin:/usr/sbin:/sbin"


TARGET_USER=vpn
TARGET_HOSTNAME=vpn-scv-x.apple.com
IDENTITY_DIRECTORY=.


TARGET_IPADDRESS=$(dig +short $TARGET_HOSTNAME)
DEFAULT_ROUTER=$(netstat -rn | grep ^0.0.0.0 | awk '{print $2}')

if [ "$UID" -ne "0" ]; then
    echo "$(basename "$0"): Must be run as root"
    exit 1
fi

read -p "Username: " USER
if [ $? -ne 0 ]; then
    echo "Error getting username"
    exit 1
fi

read -s -p "Enter PASSCODE: " PASSCODE; echo
if [ $? -ne 0 ]; then
    echo "Error getting passcode"
    exit 1
fi

echo ---------------------------------
echo Username: $USER
echo VPN Account: ${TARGET_USER}@${TARGET_HOSTNAME}
echo VPN IP Address: $TARGET_IPADDRESS
echo Default Router: $DEFAULT_ROUTER
echo ---------------------------------

echo "Adding route to VPN host"
route add -host $TARGET_IPADDRESS gw $DEFAULT_ROUTER
if [ $? -ne 0 ]; then
    echo "Error adding route to VPN host"
    exit 1
fi

echo "Establishing tunnel"

pppd ipcp-accept-local ipcp-accept-remote                     \
    local noauth nocrtscts nodefaultroute novj                \
    connect-delay 10000 nodetach                              \
    pty "/usr/bin/ssh -P -o 'BatchMode=yes'                   \
	-c blowfish -e none -i ${IDENTITY_DIRECTORY}/identity \
	-i ${IDENTITY_DIRECTORY}/id_dsa                       \
	-x -t -l $TARGET_USER $TARGET_HOSTNAME"               \
    connect "/usr/sbin/chat -v -e                             \
	Username:-BREAK-Username: $USER PASSCODE: $PASSCODE" &

COUNT=30
while [ $COUNT -gt 0 ]; do
    ifconfig | grep $TARGET_IPADDRESS > /dev/null
    if [ $? -eq 0 ]; then
	break;
    fi
done

if [ $COUNT -eq 0 ]; then
    echo "Error setting up tunnel"
    route delete -host $TARGET_IPADDRESS gw $DEFAULT_ROUTER
    exit 1
fi

echo "Routing net-17 traffic through VPN connection"
route add -net 17.0.0.0/8 gw $TARGET_IPADDRESS
if [ $? -ne 0 ]; then
    echo "Error adding net-17 route"
    route delete -host $TARGET_IPADDRESS gw $DEFAULT_ROUTER
    exit 1
fi
