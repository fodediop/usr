#!/bin/bash
MODE=$(uname -a|tr " " "\n"|tail -1)

if [[ "$MODE" == "x86_64" ]] ; then
   echo "Your OS mode is $MODE which is 64-bit"
else
   echo "Your OS mode is $MODE which is 32-bit"
fi

echo
uname -a
