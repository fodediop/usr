#!/usr/bin/perl -W
# $Id: fixperms.pl,v 1.10 2003/08/22 21:20:55 tph Exp $
# Copyright (c) 2002-2003 by Atomic Bird LLC, <http://www.atomicbird.com/>

# Run Mac OS X Repair Permissions

# Only boot disk permissionss can be verified or repaired, as with the GUI
# version of Disk Utility.

# Usage:
# fixperms.pl -[vra]
# -v	: Verify permissions
# -r	: Repair permissions
# If no argument is specified, "-v" is assumed.

use Getopt::Std;
use File::Basename;
use File::stat;
use strict;

sub CheckVersion;
my $SYSTEM_VERS 	= "/System/Library/CoreServices/SystemVersion.plist";

# List of files that get bogus errors due to misuse of HFS+ by Apple installers.  If
# this script is run on an HFS+ disk, errors related to these files will be suppressed.
# A new 10.3 install doesn't have this problem, however the check is retained since many users will
# be upgrading from earlier releases of Mac OS X and therefore may still have this problem.
my @hfsignore = (
		"/usr/share/man/man3/DB.3",
		"/usr/share/man/man3/db.3"
	     );

# List of files (currently just 1) that get "special" permissions from the GUI version of
# Disk Utility.  These will get a different fix from the GUI version than they do from
# the command-line version unless something special is done here, so this array covers that.
# When adding new entries here, specify perms as octal.  Make sure to include the leading "0".
# Note that it may be necessary to add an "owner" field here at some point, if Apple decides to
# add even more ugly hacks to Disk Utility...
my @specials;
if (CheckVersion($SYSTEM_VERS, "10.3.0", "ProductVersion", "<")) {
	# In 10.3 Disk Utility doesn't seem to do this any more, so this initialization only applies
	# to earlier releases of Mac OS X.
	@specials = (
			{
			"path" => "/System/Library/Filesystems/hfs.fs/hfs.util",
			# Apple records this as decimal 33261 (duh).
			"perms" => 0100755
			}
	       );
} else {
	@specials = ();
}

our ($opt_v, $opt_r);
getopts("vr");

$opt_v = 1 if ((!$opt_v) && (!$opt_r));

print basename($0), " started ", `date`, "\n";
print "Verifying boot disk permissionss\n" if ($opt_v);
print "Repairing boot disk permissionss\n" if ($opt_r);

if (! -e "/usr/sbin/diskutil") {
	print "\n";
	print "Cannot run, the command-line version of Disk Utility is not insalled\n";
	print "This probably means you have not upgraded to Mac OS X 10.2 (Jaguar)\n";
	exit(0);
}

my $diskname = "/";
my $hfs = 0;
print "Information on $diskname:\n";

open DISKINFO, "/usr/sbin/diskutil info $diskname |";
while (<DISKINFO>) {
	next if /Failed to get UTF8 encoding for name/;
	next if /Couldn't open device/;
	print;
	$hfs = 1 if (/File System:.*HFS+/);
}
close DISKINFO;

my $diskutil_cmd = "";
if ($opt_v) {
	print "Verifying permissions on disk $diskname\n\n";
#	system("/usr/sbin/diskutil verifyPermissions $diskname");
	$diskutil_cmd = "/usr/sbin/diskutil verifyPermissions $diskname";
}
if ($opt_r) {
	print "Repairing permissions on disk $diskname\n\n";
#	system("/usr/sbin/diskutil repairPermissions $diskname");
	$diskutil_cmd = "/usr/sbin/diskutil repairPermissions $diskname";
}

open DISKUTIL, "$diskutil_cmd |";
while (<DISKUTIL>) {
	next if /Failed to get UTF8 encoding for name/;
	next if /Couldn't open device/;
	# Suppress message that makes it look like the script's slacking off.
	next if /The privileges have been partially verified/;
	if ($hfs) {
		my $ignore = 0;
		my $ignorename;
		foreach $ignorename (@hfsignore) {
			if ($_ =~ /$ignorename/) {
				$ignore = 1;
				last;
			}
		}
		print unless ($ignore == 1);
	} else {
		print;
	}
}
# Now handle "specials"
print "\nChecking special permissions...\n";
my $special;
foreach $special (@specials) {
	my $file = $$special{'path'};
	my $perms = $$special{'perms'};
	my $st = stat($file);
	# Print message regardless of state, since that's what Disk Utility does.
	printf "Applying special permissions of %o (%d) for $file\n", $perms, $perms;
	if (($st) && ($st->mode != $perms)) {
		chmod $perms, $file;
	}
}
print "\nPrivilege repair complete.\n";
exit(0);

sub CheckVersion
{
    my $path            = $_[0];
    my $version         = $_[1];
    my $keyName         = $_[2];
    my $operator        = $_[3];
    
    my $oldSeperator;
    my $plistData;
    my @items;
    my $item;
    my $versiondata;
    my $i;
    my @theVersionArray;
    my %versiondata;
    my @versionArray;

    # if there's no THERE there, we return FALSE
    if (! -e $path) {
        return 0;
    }

    if (!$operator) {
        $operator = "==";
    }

    $oldSeperator = $/;
    $/ = \0;

    open( PLIST, "$path") || do {
        return 0;
    };

    $plistData = <PLIST>;
    $plistData =~ /<dict>(.*?)<\/dict>/gis;

    @items = split(/<key>/, $plistData);

    shift @items;
    foreach $item (@items) {
        $item =~ /(.*?)<\/key>.*?<string>(.*?)<\/string>/gis;
        $versiondata{ $1 } = $2;
    }

    close(PLIST);

    $/ = $oldSeperator;

    @theVersionArray = split(/\./, $versiondata{$keyName});
    for ($i = 0; $i < 3; $i++) {
        if(!$theVersionArray[$i]) {
            $theVersionArray[$i] = '0';
        }
    }

    @versionArray = split(/\./, $version);
    
    my $actualVersion;

    for ($i = 0; $i < 3; $i++) {
        if (($theVersionArray[$i] != $versionArray[$i]) or ($i == 2)) {

            $actualVersion = $theVersionArray[$i];
            $version = $versionArray[$i];

            last;
        }
    }

    my $expression = '$actualVersion ' . $operator . ' $version';
    if( eval ($expression) )
    {
        return 1;
    }
    else
    {
        return 0;
    }

}
