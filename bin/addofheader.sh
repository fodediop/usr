#!/bin/bash 

for file in "$@" 
	do
		head -n 1 "$file"|grep "File" > /dev/null
		if [ $? -eq 0 ]
			then
			echo " $file already had header"
		else
			cat <<EOF > tmp
\	File:		\$Id\$
\
\	Contains:	...
\
\	Copyright:	� 2004 by Apple Computer, Inc., all rights reserved.
\
\		\$Log\$

EOF
cat $file >> tmp
mv tmp $file
		fi
done
rm tmp