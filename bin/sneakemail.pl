#!/usr/bin/perl 

#--------------------------------------------------------------------------
# Command line client for use with sneakemail.com
# By Kevin Swope webmaster@sneakemail.com 2000
#
# This software is in the public domain.  There are no restrictions on any
# sort of usage of this software.
#--------------------------------------------------------------------------


use strict;
use LWP::UserAgent;       
use Getopt::Long;


$|=1; # turn off line bufferning, for progress messages


#--------------------------------------------------------------------------
# Customize: set these to personalize this script
#--------------------------------------------------------------------------
my $USERNAME = "tgunr"; 
my $PASSWORD = "spectre";
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# "global" stuff
#--------------------------------------------------------------------------

my $version = "1.1"; # for -version and update

# stuff for easy config
my $gFirstHost = "sneakemail.com"; # ask this host where account is
my $gAccountFinderPath = "php/client_account_finder.php";
my $gPort = 80; # used web server to make things easy and safer.
my $gServerPath = "php/client.php"; # path to server script
my $gUpdateInfoPath = "client_update_info.txt";
my $gSpinePattern = ""; # vertical lines on left of tree view
my $gTreeIndentInc = 3; # how much to indent tree view each step

# shared stuff
my %gOptionHash = (); # holds command line options, set by Getopt::Long

#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# get command line arguments and process for errors
#--------------------------------------------------------------------------
$Getopt::Long::ignorecase = 0; # default is 1, wtf?
my @optSpecifiers = qw(v h help H tree D u=s p=s a f:s fid=i na=s nf=s
		       version vinfo no_update);
my $optSuccess = GetOptions(\%gOptionHash, @optSpecifiers);
unless ($optSuccess) {
  print "use -h for help\n";
  exit(1);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# see if we can set username password from user defined vars
#--------------------------------------------------------------------------
if ($USERNAME and not $gOptionHash{'u'}) {
  $gOptionHash{'u'} = $USERNAME;
}

if ($PASSWORD and not $gOptionHash{'p'}) {
  $gOptionHash{'p'} = $PASSWORD;
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# print version
#--------------------------------------------------------------------------
if ($gOptionHash{'version'}){
  print "version $version\n";
  exit(0);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# see if we can help ( -h or -H )
#--------------------------------------------------------------------------
unless (keys %gOptionHash) { # expecting something for nothing :)
  print "-h for help, -H for more verbose help\n";
  exit(0); 
}

if ($gOptionHash{'h'} || $gOptionHash{'help'}) { 
  usage('terse'); 
  exit(0); 
}

if ($gOptionHash{'H'}){ 
  usage('verbose'); 
  exit(0); 
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check argument values starting with '-', a sure sign switches and params got fu
#--------------------------------------------------------------------------
if (grep /^-/, values %gOptionHash) {
  print "Will not allow any option values that start with an \"-\",\n";
  print "use \\\\- for the desired value.\n";
  print "If you are surprised by this response than you probably\n";
  print "have two switches in a row, like \"-nf -f\"\n";
  exit(1);
} 
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# now unescape those values that began \\- 
#--------------------------------------------------------------------------
while (my($option, $value) = each %gOptionHash) {
  $gOptionHash{$option} =~ s/^\\//o;
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# username/password wont be enough to do anything, why bother asking later.
#--------------------------------------------------------------------------
my %gOptionHashTemp = %gOptionHash;

delete @gOptionHashTemp{qw /u p v/};

unless (keys %gOptionHashTemp) { 
  print "Nothing to do.\n";
  exit(0); 
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# all following actions require a -u and -p, prompt if not in cmdline params
#--------------------------------------------------------------------------
unless ($gOptionHash{'u'}) { 

  print "username: ";
  $gOptionHash{'u'} = <STDIN>;
  chomp $gOptionHash{'u'};

  unless ($gOptionHash{'u'}) {
    print "username required\n";
    exit(0); 
  }

}

unless ($gOptionHash{'p'}) { 
  
  # cant hide echoing in a platform independent way, sucks.

  print "************************************************\n";
  print "Warning, password will be visible.\n";
  print "************************************************\n";
  print "password: ";

  $gOptionHash{'p'} = <STDIN>;
  chomp $gOptionHash{'p'};
  
  # final check
  unless ($gOptionHash{'p'}) {
    print "password required\n";
    exit(0); 
  }
  
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# alternate gFirstHost so I can debug locally 
#--------------------------------------------------------------------------
if($ENV{SNEAKEMAIL_FIRST_HOST}) { 
  $gFirstHost = $ENV{SNEAKEMAIL_FIRST_HOST};
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# client asks about which sneakemail cluster your account is on.
#--------------------------------------------------------------------------
print "quering server on $gFirstHost \n" if ($gOptionHash{'v'});
print "getting account host for user $gOptionHash{'u'}... " if ($gOptionHash{'v'});

my ($accountHostName, $response) = 
  getAccountHost($gFirstHost, $gAccountFinderPath, $gOptionHash{'u'});

unless ($accountHostName) { # something went wrong
  # either an http server error or a sm server error
  cliResponseErrorHandler($response);
}

if ($accountHostName eq 'none') {
  print "not found\n";
  exit(0);
}

print "$accountHostName\n" if ($gOptionHash{'v'}); # after finding account host bedazzle user with its name
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check for updates and notify
#--------------------------------------------------------------------------
unless ($gOptionHash{'no_update'}){


  print "checking version... " if ($gOptionHash{'v'});
  my ($newVersion, $updatePath, $updateInfoHashRef) = checkUpdates($version, $accountHostName, $gUpdateInfoPath);
  print "done\n" if ($gOptionHash{'v'});

  # announce new version
  if ($newVersion) {

    if ($gOptionHash{'vinfo'}) { # print new version info
      
      print "\n";
      print "sneakemail client version $newVersion available\n";
      print "you are currently using $version\n";
      print "\n";

      while (my($ver, $info) = each %$updateInfoHashRef) {

	print "---------------\n";
	print "changes in $ver\n";
	print "---------------";
	print "$info";
	print "\n";

      }

      print "\n";
      print "to stop announcements use -no_update\n";
      print "\n";
      print "to download go to http://$accountHostName/$updatePath\n";

      print "\n";
      exit(0);
      
    }else { # simple announcement
      print "new client version available, use -vinfo for details\n";
    }

  }


}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check for updates
#--------------------------------------------------------------------------
sub checkUpdates{

  my($version, $accountHostName, $gUpdateInfoPath) = @_;

  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
  
  my $requestObj = 
    new HTTP::Request('GET', "http://$accountHostName/$gUpdateInfoPath?"); 
  
  my $response = $ua->request($requestObj); 
  
  unless ($response->is_success()) {
    return (undef);
  }

  my $content =  $response->content();
  
  # eliminate all data not relevant to update 
  # (cut back from current version, inclusive)
  my ($relContent) = $content =~ /\[version:$version\].*?\[path.*?\].*?(\[.*)/sg;

  # cut out whitespace
  while ($relContent =~ s%\n\n%\n%gs){}

  # make array of all newer versions
  my @versions = $relContent =~ /\[version:(.*)\]/g;

  # figure out the latest version
  my $latest = @versions[$#versions];

  # get path of latest version
  my ($latestPath) = $relContent =~ /\[version:$latest\][^\[]*\[path:(.*)\]/g;

  my (%versionPlusData) = $relContent =~ /\[version:(.*?)\].*?\[path.*?[]]([^\[]*)/gs;

  return($latest, $latestPath, \%versionPlusData);


}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# not really authentication, but check user/password so we can exit now with a warning
#--------------------------------------------------------------------------
my ($authenticated, $response) = 
  authenticate($accountHostName, $gServerPath, $gOptionHash{'u'}, $gOptionHash{'p'});

unless (defined $authenticated) { # server/connection error
  cliResponseErrorHandler($response);
  exit(1);
}

unless ($authenticated) {
  print "username/password error\n";
  exit(0);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# Download folder and sneakemail address data for account
# so we can do most of the error checking and sorting locally.
# Put them into fancy hashes $addressHashRef and $folderHashRef.
#--------------------------------------------------------------------------
print "getting account data from $accountHostName... " if ($gOptionHash{'v'});

undef $response;
my ($damnedErrorFlag, $addressHashRef, $response) = 
  getAddresses($accountHostName, $gServerPath, $gOptionHash{'u'}, $gOptionHash{'p'});

if ($damnedErrorFlag) { 
  cliResponseErrorHandler($response);
  exit(1);
}

undef $response;
my ($damnedErrorFlag, $folderHashRef, $response) =
  getFolders($accountHostName, $gServerPath, $gOptionHash{'u'}, $gOptionHash{'p'});

if ($damnedErrorFlag) {
  cliResponseErrorHandler($response);
  exit(1);
}

print "done\n" if ($gOptionHash{'v'});
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# show tree, draw a nice tree view, inspired by unix "tree"
#--------------------------------------------------------------------------
if ($gOptionHash{'tree'}){
  printTree($addressHashRef, $folderHashRef);
  exit(0);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# list addresses
#--------------------------------------------------------------------------
if ($gOptionHash{'a'}){
  printAddresses($addressHashRef);
  exit(0);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# list folders
# $gOptionHash{'f'} will be zero if -f doesnt have a parameter
#--------------------------------------------------------------------------
if (defined $gOptionHash{'f'} && not ($gOptionHash{'nf'} || $gOptionHash{'na'}) ){
  printFolders($folderHashRef);
  exit(0);
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# new address
#--------------------------------------------------------------------------
if ($gOptionHash{'na'}){

  my $inFolder = $gOptionHash{'f'};
  my $fid = $gOptionHash{'fid'};

  unless ($inFolder || $fid) { # wants to use top folder
    $fid = 0;
  }else{ # specified folder

    # make sure folder exists if its specified
    unless (folderExists($folderHashRef, $inFolder, $fid)) {
      
      if ($fid) {
	print "folder with id $fid doesn't exist\n";
      }else {
	print "folder \"$inFolder\" doesn't exist\n";
      }
      exit(0);

    }
  
  }

  # check same name folders and exit with warning to use folder ids
  if ($inFolder) {
    if (checkForDupFolderNames($folderHashRef, $inFolder)){
      print "multiple folders with name $inFolder, use -tree to find right one\n";
      print "replace -f with -fid and use folder id\n";
      exit(0);
    }
  }


  # make sure label is unique 
  if (labelExists($addressHashRef, $gOptionHash{'na'})) {
      print "label \"" . $gOptionHash{'na'} . "\" already exists\n";
      exit(0);
  }

  my ($newAddress, $response) = 
    newAddress($accountHostName, $gServerPath, 
	       $gOptionHash{'u'}, $gOptionHash{'p'},
	       $gOptionHash{'na'}, $folderHashRef, $inFolder, $fid);

  unless ($newAddress) { # server/connection/client error
    cliResponseErrorHandler($response);
    exit(1);
  }

  print "\"" . $gOptionHash{'na'} . "\" " . $newAddress ."\n";
  exit(0);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# new folder
#--------------------------------------------------------------------------
if ($gOptionHash{'nf'}){
  
  my $inFolder = $gOptionHash{'f'};
  my $fid = $gOptionHash{'fid'};
  
  unless ($inFolder || $fid) {	# wants to use top folder
    $fid = 0;
  }else {			# figure out if folder exists
    
    # make sure folder exists if its specified
    unless (folderExists($folderHashRef, $inFolder, $fid)) {
      
      if ($fid) {
	print "folder with id $fid doesn't exist\n";
      }else {
	print "folder \"$inFolder\" doesn't exist\n";
      }
      exit(0);
      
    }
    
  }
  

  # check same name folders and exit with warning to use folder ids
  if ($inFolder) {
    if (checkForDupFolderNames($folderHashRef, $inFolder)){
      print "multiple folders with name $inFolder, use -tree to find right one\n";
      print "replace -f with -fid and use folder id\n";
      exit(0);
    }
  }

  my ($folderId, $response) = 
    newFolder($accountHostName, $gServerPath, 
	       $gOptionHash{'u'}, $gOptionHash{'p'},
	       $folderHashRef,  $gOptionHash{'nf'}, $inFolder, $fid);

  unless ($folderId) { # server/connection/client error
    cliResponseErrorHandler($response);
    exit(1);
  }
  
  unless ($inFolder || $fid) {

      print "folder \"" . $gOptionHash{'nf'} . "\" (id:$folderId) created in top folder\n";

  }else {

    if ($fid) {
      $inFolder = folderNameFromId($folderHashRef, $fid);
      print "folder \"" . $gOptionHash{'nf'} . "\" (id:$folderId) created in folder \"$inFolder\"\n";
    }else {
      print "folder \"" . $gOptionHash{'nf'} . "\" (id:$folderId) created in folder \"$inFolder\"\n";
    }

  }
  
  exit(0);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# nothing but functions beyond this point
#--------------------------------------------------------------------------
exit(0);
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check if folder exists
#--------------------------------------------------------------------------
sub folderExists{

    my ($folderHashRef, $folderName, $fid) = @_;

    if ($fid) {  # if folder id is used
      
      ( grep { $_ eq $fid } keys %$folderHashRef ) && return 1;

    }else { # check for name of folder

      ( grep { $_->{"name"} eq $folderName } values %$folderHashRef ) && return 1;
      
    }

    return 0; # not found

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check if label exists, you need a unique label name
#--------------------------------------------------------------------------
sub labelExists{

    my ($addressHashRef, $label) = @_;

    ( grep { $_->{"label"} eq $label } values %$addressHashRef ) && return 1;

    return 0; # not found

}
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
# check for dupe folder names so we can warn to use folder ids instead
#--------------------------------------------------------------------------
sub checkForDupFolderNames{

  my ($folderHashRef, $folderName) = @_;

  my %folderHash = %$folderHashRef;

  my @dups = ();
  while (my($id, $dataRef) = each %folderHash) {

    if ( $folderHash{$id}->{name} eq $folderName ) {
      unshift @dups, $id;
    }

  }

  if (@dups > 1) {
    return 1;
  }  

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# get folder name from id
#--------------------------------------------------------------------------
sub folderNameFromId{

  my ($folderHashRef, $id) = @_;

  my %folderHash = %$folderHashRef;

  my $name = $folderHash{$id}->{name};

  return $name;

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# get folder id from name
#--------------------------------------------------------------------------
sub folderIdFromName{

  my ($folderHashRef, $name) = @_;

  # just loop through and find id for corresponding name
  while (my($id, $data) = each %$folderHashRef) {
    if ($name eq $data->{name}) { 
      return $id;
    }
  }

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# create new address
#--------------------------------------------------------------------------
sub newAddress{
  
  my ($accountHostName, $serverPath, 
      $username, $password, 
      $label, $folderHashRef, $folderName, $fid) = @_;
  
  if ($folderName) { # convert name to folder id (fid)
    $fid = folderIdFromName($folderHashRef, $folderName);
  }
  
  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
  
  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$accountHostName/$serverPath?" . 
		      "username=$username&password=$password&" . 
		      "na=yes&label=$label&folder=$fid"); 
  
  my $response = $ua->request($requestObj); 
  
  my $content =  $response->content();
  
  # check content for completeness
  unless ($content =~ /\[client:new address:end\].*\[client:end\]/s ) { # completed
    return (undef, $response);
  }
  
  # extract address
  my ($newAddress) = $content =~ /\[client:new address:address:(.*)\]/;
  
  return ($newAddress, $response);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# create new folder
#--------------------------------------------------------------------------
sub newFolder{

  my ($accountHostName, $serverPath, 
      $username, $password, 
      $folderHashRef, $folderName, $inFolderName, $fid) = @_;
  
  if ($inFolderName) { # convert name to folder id (fid)
    
    # just loop through and find id for corresponding name
    while (my($id, $data) = each %$folderHashRef) {
      if ($inFolderName eq $data->{name}) { $fid = $id; }
    }
    
  }
  
  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
  
  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$accountHostName/$serverPath?" . 
		      "username=$username&password=$password&" . 
		      "nf=yes&name=$folderName&folder=$fid"); 
  
  my $response = $ua->request($requestObj); 
  
  my $content =  $response->content();
  
  # check content for completeness
  unless ($content =~ /\[client:new folder:end\].*\[client:end\]/s ) { # completed
    return (undef, $response);
  }
  
  
  my ($folderId) = $content =~ /\[client:new folder:id:(.*)\]/;

  return ($folderId, $response);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# list addresses
#--------------------------------------------------------------------------
sub printAddresses{

  my($addressHashRef) = @_;

  print "\n";

  # build up array of data for sorting and formatting purposes
  my @dataRefArray = ();
  my $longestLabel;
  while (my($id, $dataRef) = each %$addressHashRef) {

    # figure out longest label while where here
    (length($dataRef->{label}) > $longestLabel) && ( $longestLabel = ( length($dataRef->{label}) ) );

    unshift @dataRefArray, $dataRef;

  }

    # sort by label
    @dataRefArray = sort { lc $a->{label} cmp lc $b->{label}} @dataRefArray;

    foreach my $dataRef (@dataRefArray) {
      print $dataRef->{label};
      print " " x ($longestLabel - length $dataRef->{label});
      print " "; # spacer
      print $dataRef->{sm_address};
      print "@";
      print $dataRef->{domain};
      print "\n";
    }

    print "\n";

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# list folders
#--------------------------------------------------------------------------
sub printFolders{

  my($folderHashRef) = @_;

  print "\n";

  # build up array of data for sorting and formatting purposes
  my @dataRefArray = ();
  my $longestName;
  while (my($id, $dataRef) = each %$folderHashRef) {

    # figure out longest name while where here
    #(length($dataRef->{name}) > $longestName) && ( $longestName = ( length($dataRef->{name}) ) );

    # sneak in folder id
    $dataRef->{id} = $id;

    unshift @dataRefArray, $dataRef;

  }

    # sort by name
    @dataRefArray = sort { lc $a->{name} cmp lc $b->{name}} @dataRefArray;

    foreach my $dataRef (@dataRefArray) {
      print $dataRef->{name};
      #print " " x ($longestName - length $dataRef->{name});
      print " "; # spacer
      print "(id:";
      print $dataRef->{id};
      print ")";
      print "\n";
    }

    print "\n";

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# show tree, draw a nice tree view, inspired by unix "tree"
#--------------------------------------------------------------------------
sub printTree{

  my ($addressHashRef, $folderHashRef) = @_;

  print "\n";
  print "   Top\n";

  my $topId = 0; # start it with top level folder id
  printContents($addressHashRef, $folderHashRef, $topId); 

  print "\n";

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# utilities for printTree
#--------------------------------------------------------------------------

# cool recursive fuction, prints addresses and dirs in abc order (scary)
sub printContents{

  my ($addressHashRef, $folderHashRef, $folderId) = @_;

  my $childListRef = 
    childrenOf($addressHashRef, $folderHashRef, $folderId); # everbody (addresses + folders)

  $gSpinePattern .= (" " x $gTreeIndentInc) . "|";

  while (my $child = shift @$childListRef) {

    my $spine;
    unless (@$childListRef) { # last element
      #remove pipe and add tick
      chop $gSpinePattern;
      $gSpinePattern .= "`";
    }

    if (childIsFolder($child)) { # put slash at end
      print $gSpinePattern . "--" . childsName($child) . " (id:" . childsId($child) . ")\n";
    }else {
      if ($gOptionHash{'v'}) { # add address to label
	print $gSpinePattern . "--" . childsName($child) . 
	  " (" . childsAddress($child) . '@' . childsDomain($child) . ")\n";
      }else {
	print $gSpinePattern . "--" . childsName($child) . "\n";
      }

    }

    unless (@$childListRef) { # last element
      # now remove tick and add space
      chop $gSpinePattern;
      $gSpinePattern .= " ";
    }


    if (childIsFolder($child)) { # recursion part
      printContents($addressHashRef, $folderHashRef, childsId($child));
    }

  }

  $gSpinePattern = substr $gSpinePattern, 0, -($gTreeIndentInc + 1);


}

sub childsId{
  my $childRef = shift;
  return $childRef->[1];
}

# return whether child is a folder or not
sub childIsFolder{
  my $childRef = shift;
  return $childRef->[2];
}

# return string which is childs name
sub childsName{
  my $childRef = shift;
  return $childRef->[0];
}

sub childsAddress{ # doesnt work with folders, returns blank
  my $childRef = shift;
  return $childRef->[3];
}

sub childsDomain{ # doesnt work with folders, returns blank
  my $childRef = shift;
  return $childRef->[4];
}

# returns a sorted list of references to datastructures that
# contains (name, id, isFolder)
sub childrenOf{

  my ($addressHashRef, $folderHashRef, $parentFolderId) = @_;

  my @children = ();

  # add folders to array that are in $parentFolderId
  while( my($id, $dataRef) = each %$folderHashRef) {
    if ($dataRef->{"parent_folder_id"} eq $parentFolderId) {
      unshift @children, [$dataRef->{"name"}, $id, 1];
    }
  }

  # add addresses to array that are in $parentFolderId
  while( my($id, $dataRef) = each %$addressHashRef) {
    if ($dataRef->{"folder_id"} eq $parentFolderId) {
      unshift @children, [$dataRef->{"label"}, $id, 0, $dataRef->{"sm_address"}, $dataRef->{"domain"}];
    }
  }

  # sort all by name
  @children = sort {lc($a->[0]) cmp lc($b->[0])} @children;

  return \@children;

}

#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# build up a nested hash of sneakemail addresses
#--------------------------------------------------------------------------
sub getAddresses{

  my($accountHostName, $serverPath, $username, $password) = @_;

  # would have like to just retured an undefined hash
  # but defined(hash) or (array) is all fu.
  # cant just keys %addressHash because the user just might not have
  # any addresses currently
  my $damnedErrorFlag = 0;

  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
 
  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$accountHostName/$serverPath?username=$username&password=$password&a=yes"); 
  
  my $response = $ua->request($requestObj); 

  my $content =  $response->content();

  # check content for completeness
  unless ($content =~ /\[client:list addresses:end\].*\[client:end\]/s ) { # completed
    $damnedErrorFlag = 1;
    return ($damnedErrorFlag, undef, $response);
  }
	  
	  
  # isolate the good parts
  my ($rawAddressData) = $content =~ /\[client:list addresses:start\](.*)\[client:list addresses:end\]/s;

  # make array of just the address data
  my (@dataArray) =  $rawAddressData =~ /\[client:list addresses:(.*?)\]/g;


  my %addressHash = (); # load this up with refs to arrays, each array being a seperate address data struct

  while (@dataArray) {

    # key of top hash will be address id
    my $key = shift @dataArray;
    $addressHash{$key} = { 
			  label => shift @dataArray,
			  sm_address => shift @dataArray,
			  domain => shift @dataArray,
			  folder_id => shift @dataArray,
			 }

  }

  return($damnedErrorFlag, \%addressHash, $response);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# build up a nested hash of sneakemail folders
# a lot of this code came from getAddresses()
#--------------------------------------------------------------------------
sub getFolders{


  my($accountHostName, $serverPath, $username, $password) = @_;

  # would have like to just retured an undefined hash
  # but defined(hash) or (array) is all fu.
  # cant just keys %folderHash because the user just might not have
  # any folderes currently
  my $damnedErrorFlag = 0;

  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
 
  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$accountHostName/$serverPath?username=$username&password=$password&f=yes"); 
  
  my $response = $ua->request($requestObj); 

  my $content =  $response->content();

  # check content for completeness
  unless ($content =~ /\[client:list folders:end\].*\[client:end\]/s ) { # completed
    $damnedErrorFlag = 1;
    return ($damnedErrorFlag, undef, $response);
  }
	  
	  
  # isolate the good parts
  my ($rawFolderData) = $content =~ /\[client:list folders:start\](.*)\[client:list folders:end\]/s;

  # make array of just the folder data
  my (@dataArray) =  $rawFolderData =~ /\[client:list folders:(.*?)\]/g;


  my %folderHash = (); # load this up with refs to arrays, each array being a seperate address data struct

  while (@dataArray) {

    # key of top hash will be folder id
    my $key = shift @dataArray;
    $folderHash{$key} = { 
			  name => shift @dataArray,
			  parent_folder_id => shift @dataArray,
			 }

  }

  return($damnedErrorFlag, \%folderHash, $response);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# check username and password so we can warn about it, (not authentication)
#--------------------------------------------------------------------------
sub authenticate{


  my($accountHostName, $serverPath, $username, $password) = @_;

  my $ua = new LWP::UserAgent;
  $ua->env_proxy();
 
  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$accountHostName/$serverPath?username=$username&password=$password&auth=yes"); 
  
  my $response = $ua->request($requestObj); 

  my $content =  $response->content();

  my $nl = "\n";

  if ($content =~ /$nl\[client:authenticated\]$nl/s){ # be careful (^$), a folder could be named "[client:authenticated]"
    return (1, $response);
  }elsif ($content =~ /$nl\[client:username\/password incorrect\]$nl/s) {
    return (0, $response);
  }else {
    return (undef, $response);
  }

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# what to do on command line if server response is not good
#--------------------------------------------------------------------------
sub cliResponseErrorHandler{
  
  my $response = shift;
  
  # first check web server response codes
  if ($response && $response->is_error()) {
    print "\n";
    print "A server error has occured.\n";
    print $response->message() . "\n";
    print "\n";
    exit(1);
  }
  
  # error not a server error so something is very wrong with client.
  # since server scripts dont give good error just throw up hands.

  print "\n";
  print "Sorry, an unanticipated server error has occured.\n";
  print "Please send a quick email to webmaster\@sneakemail.com.\n";
  print "Let us know an error has occurred and your username, thanks!\n\n";
  print "It would be really helpful if you ran that command again\n";
  print "with a \"-D\" switch added and redirected it to a file\n";
  print "and sent it to us. :)\n";
  print "\n";
  
  if ($gOptionHash{'D'}){

    # get requested url and remove password.
    my $base = $response->base() if $response;
    $base =~ /(&password=)(.*?)(&)/;
    $base = $` . $1 . '*' x length($2) . $3 . $';
    
    print "--------------------------------------------------------------------\n";
    print $base . "\n";
    print $response->as_string() if $response;
    print "--------------------------------------------------------------------\n";
  }
  
}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# where client asks about which sneakemail cluster your account is on.
#--------------------------------------------------------------------------
sub getAccountHost{

  my($gFirstHost, $gAccountFinderPath, $username) = @_;

  my $ua = new LWP::UserAgent;
  $ua->env_proxy();

  my $requestObj = 
    new HTTP::Request('GET', 
		      "http://$gFirstHost/$gAccountFinderPath?username=$username"); 
  
  my $response = $ua->request($requestObj); 

  my $content =  $response->content();

  my($accountHostName) = $content =~ /\[client_account_finder:host (.*)\]/;

  return ($accountHostName, $response);

}
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# prints out a usage message.
#--------------------------------------------------------------------------
sub usage{
  
  my $helpLevel = shift;
  
  if ($helpLevel eq 'terse') {

    print "\n";
    print "Note: will prompt for username and/or password if omitted.\n";
    print "\n";
    print ">sneakemail.pl -H (verbose help)\n";
    print ">sneakemail.pl -version\n";
    print ">sneakemail.pl -u user -p passwd --tree (show tree view)\n";
    print ">sneakemail.pl -u user -p passwd -v --tree (show tree view with addresses)\n";
    print ">sneakemail.pl -u user -p passwd -a (list labels and addresses)\n";
    print ">sneakemail.pl -u user -p passwd -f (list folders)\n";
    print ">sneakemail.pl -u user -p passwd -nf my_folder (new folder)\n";
    print ">sneakemail.pl -u user -p passwd -nf my_other_folder -f my_folder\n";
    print ">sneakemail.pl -u user -p passwd -nf my_other_folder -fid 123\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address (new address)\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address -f my_folder\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address -fid 123\n";
    print "\n";
  
  }elsif($helpLevel eq 'verbose') {

    print "\n";
    print "Note: will prompt for username and/or password if omitted.\n";
    print "\n";
    print "show version\n";
    print ">sneakemail.pl -version\n";
    print "\n";
    print "show tree of labels and folders\n";
    print ">sneakemail.pl -u user -p passwd --tree\n";
    print "\n";
    print "show tree of labels, folders and addresses\n";
    print ">sneakemail.pl -u user -p passwd -v --tree\n";
    print "\n";
    print "list folders\n";
    print ">sneakemail.pl -u user -p passwd -f\n";
    print "\n";    
    print "list labels and addresses\n";
    print ">sneakemail.pl -u user -p passwd -a\n";
    print "\n";    
    print "create new folder my_folder in top folder\n";
    print ">sneakemail.pl -u user -p passwd -nf my_folder\n";
    print "(must quote names with whitespace \"my folder\")\n";
    print "\n";    
    print "create new folder my_other_folder in folder my_folder\n";
    print ">sneakemail.pl -u user -p passwd -nf my_other_folder -f my_folder\n";
    print "\n";    
    print "create new folder my_other_folder in folder my_folder using folder id\n";
    print ">sneakemail.pl -u user -p passwd -nf my_other_folder -fid 123\n";
    print "(folder id's are shown with folder listings)\n";
    print "\n";    
    print "create new address in top folder\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address\n";
    print "\n";    
    print "create new address in folder my_folder\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address -f my_folder\n";
    print "\n";    
    print "create new address in folder my_folder using folder id\n";
    print ">sneakemail.pl -u user -p passwd -na my_new_address -fid 123 \n";
    print "(folder id's are shown with folder listings)\n";
    print "\n";
   
  }
  
}
#--------------------------------------------------------------------------
