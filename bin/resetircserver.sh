#!/bin/sh

############################################################
#  Program: resetchatserver
#  Author : Dave Carlton
############################################################


## BEGIN SCRIPT

ssh chat.root service ircd-ratbox stop
sleep 5
ssh chat.root service ircd-ratbox start
curl -X POST --data-urlencode 'payload={"channel": "#game-management", "username": "webhookbot", "text": "IRC server reset. This is posted to slack and comes from a script on davec laptop.", "icon_emoji": ":recycle:"}' https://hooks.slack.com/services/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
