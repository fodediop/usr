#!/bin/sh -vx

# create TFTP directory
if [ ! -d /private/tftpboot/oftech ] 
then 
	if [ ! -d /private/tftpboot ]
	then # does not exist so I create it
		mkdir /private/tftpboot
		mkdir /private/tftpboot/oftech
	else # it does but not oftech
		mkdir /private/tftpboot/oftech
	fi
fi

# set permissions
chmod -R 777 /private/tftpboot

# setup inetd.conf
perl -i -pe 's/^#tftp/tftp/' /etc/inetd.conf

# restart inetd
kill -HUP `cat /var/run/inetd.pid`

# create a drop folder
if [ ! -d ~/ELoad ]
then
	ln -s /private/tftpboot/oftech ~/ELoad
fi

# setup edir task

if [ ! `grep -c "/private/tftpboot/oftech" /etc/crontab` -ge 1 ] 
then
cat <<EOF >> /etc/crontab
*/1	*	*	*	*	root	ls -lt /private/tftpboot/oftech > /private/tftpboot/oftech/.edir
EOF
fi

cat <<EOF

Your ELoad server is now setup, you should now go to each client computer
and enter OpenFirmware by holding down cmd-opt-O-F keys on powerup. At the
prompt type the following command replacing the dummy IP number with the
IP number of this server. 

setenv default-server-ip 99.99.99.99
sync-nvram

Since this server may have more than one Ethernet 
controller I have listed all of the assigned IP numbers for this server below
along with the most likely which for most systems is en0.

EOF

echo "All assigned IP numbers:"
ifconfig -a|grep -e "inet " |cut -d " " -f2

echo

echo "Most likely (en0):"
ifconfig en0|grep -e "inet " |cut -d " " -f2

cat <<EOF

I have created a drop folder in your home folder named ELoad. 
You can test your setup by dropping in any file into the ELoad folder and after 
waiting for a minute (for the cron task to create the directory) typing

edir

You should see the name and date of the file you dropped into the ELoad folder.

If it all works as planned you can now drop any file into the Eload folder and
execute it with the command.

eload filename

Cheers!

---
davec@apple.com
EOF

		