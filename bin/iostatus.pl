#!/usr/bin/perl

use strict;
use POSIX qw(strftime);

my $donedisks=0;
my $increment=shift;
my $fieldRE = '\s+(\d+\.\d+)\s+(\d)+\s+(\d+\.\d+)';

open(FILE,"iostat -n 99 -d -w 1 |") || die "Can't open iostat - $!";
while (<FILE>) {
 chop;
 next if (/KB/);   # ignore header
 if (/disk/) {     # grab disk names
   next if $donedisks++;
   s/^\s+|\s+$//g;
   s/\s+/,/g;
   my $now_string = strftime("%F,%T",localtime(time));
   my @disks = split(/,/); print "#$now_string,$_\n";
   next;
 }

 if (/$fieldRE/) {  # good info!
  my $result='';
  while (/^\s*$fieldRE/) {
    my ($kbt,$tps,$mbs) = ($1,$2,$3);
    s/^\s*$fieldRE//;
    $result .= "$mbs,";
  }
  $result =~ s/,$//;
  my $now_string = strftime("%F,%T",localtime(time));
  print "$now_string,$result\n";
  }
}
close FILE;
