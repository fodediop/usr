#!/usr/bin/perl
eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

# $Id: lwp-request,v 2.1 2002/01/03 02:07:02 gisle Exp $
#
# Simple user agent using LWP library.

=head1 NAME

onlyone.pl - Remove duplicate lines

=head1 SYNOPSIS

 onlyone.pl source [dest]

=head1 DESCRIPTION

This program can be used to eliminate duplicate lines from a file. If no destination is specified, output is to STDOUT.

=back


=head1 AUTHOR

Dave Carlton <davec@mac.com>

=cut


$/ = "\r";
$mac = "";

while ($_ = $ARGV[0], /^-/) {
	shift;
	last if /^--$/;
		/^-mac/ && ($mac = "\r");
}

if ($#ARGV > 0) {
	$dest = $ARGV[$#ARGV];
	$#ARGV -= 1;
	open ($DEST, "> $dest") or die "Can't open $dest";
} else {
	$DEST = * STDOUT;
}


while ( <> ) {
	print $DEST "$_$mac" unless $seen{$_}++;
}
