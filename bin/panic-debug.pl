#!/usr/bin/perl -w

#Load basic perl modules

use strict;
use Getopt::Long;
use Pod::Usage;
use FindBin;

#Find panic-debug modules

if(-e "/AppleInternal/Library/Perl/panic-debug/" && $FindBin::RealBin eq "/usr/local/bin")
{
    use lib "/AppleInternal/Library/Perl/panic-debug/";
}
else
{
    use lib $FindBin::RealBin;
}

#Load panic-debug modules

use GdbFuncs;
use UserIO;

#Setup some variables

my $version = "3.1";

my $interactive_mode = 0;
my $target;
my $build;
my $mach_kernel;

my $do_setup = 1;
my $display_usage;

my $ExtensionsBuilder = $FindBin::RealBin."/extensions_builder";
my $SymbolBuilder = $FindBin::RealBin."/symbol_builder";
my $kext_dir;
my $session_tmp = "/tmp/panic-debug.".time();
my $symfile_dir = $session_tmp;
my $output_file;
my $contact;
my $contact_email;
my $use_arp;
my $mac_addr = 0;

my $notes = "panic-debug $version automatically turns on kernel arp.\nYou should be able to directly connect to the machine using recent builds of gdb.\n";
my $mail_home = 0;
my $use_hotline = 1;

sub parse_args
{

	my $verbose;
	my $mach;
        my $buildname;
	my $use_static_arp = 1;

	GetOptions ('help|?' => \$display_usage, 'extensions-dir:s' => \$kext_dir, 'static-arp!' => \$use_static_arp, 'setup!' => \$do_setup, 'mach_kernel:s' => \$mach, 'mac-addr:s' => \$mac_addr, 'verbose:i' => \$verbose, 'panic-hotline!' => \$use_hotline) || pod2usage(2);

	if($display_usage)
	{
		pod2usage(-verbose => 2);
		exit 0;
	}

	$use_arp  = $use_static_arp;

	if($verbose)
	{
		user_setverbosity($verbose);
	}
	else
	{
		user_setverbosity(3);
	}	

	if($#ARGV == -1)
	{
		splash();
		run_interactive_mode();
	}
	elsif($#ARGV == 0)
	{
		$target = $ARGV[0];
		if(!defined($mach) || !defined($kext_dir))
		{
			user_out(1, "If you do not specify a build you must specify both an extensions-dir and a mach_kernel to use.\n");
			exit 5;
		}
	}
	elsif($#ARGV == 1)
	{
		$target = $ARGV[0];
		$build = $ARGV[1];
	}
	else
	{
		pod2usage(2);
		exit 0;
	}
        
        if($mach)
	{
		 $mach_kernel = $mach;
	}
        else
        {
                $buildname = $build;
                if($buildname =~ m/^(\w*?)[0-9].*/)
		{
               		$buildname = $1;
			if(-e glob("~rc/Updates/$buildname/$build"))
			{
               			$mach_kernel = glob("~rc/Updates/$buildname/$build/Symbols/xnu/mach_kernel");
			}
			elsif(-e glob("~rc/Updates/$buildname/$build.test"))
			{
				$mach_kernel = glob("~rc/Updates/$buildname/$build.test/Symbols/xnu/mach_kernel");
			}
		}
		if(! -e $mach_kernel)
		{
			print "DEBUG $mach_kernel\n";
			user_out(1, "panic-debug could not find symbls for the build $build.\n");
			exit 12;
		}
        }
        
	if(!$kext_dir)
	{
		$kext_dir = "/tmp/Extensions.$build";
	}
        my($x);

        $x=1;

        while(-e $ENV{"HOME"}."/Library/Logs/panic-debug/panic-$target-$build-$x" || -e $ENV{"HOME"}."/Library/Logs/panic-debug/panic-$target-$build-$x.short")
        {   
                $x++
        }
        $output_file = $ENV{"HOME"}."/Library/Logs/panic-debug/panic-$target-$build-$x";
}


sub splash
{
        user_out(2, "panic-debug, version $version by Louis Gerbarg <mailto:louis\@apple.com> \n\n");
        user_out(2, "Please files bugs about this script against component \"TestTool(panic-debug)\"\n");
	user_out(2, "version \"X\"\n\n");
	user_out(2, "This version of panic-debug will only run on Jaguar or newer systems, and it will only be able to debug Jaguar or newer systems\n");
        user_out(2, "Press Return to continue, or Control-C to cancel.\n");
	user_pause(3);
}

sub confirm_build_data
{

        user_out(2, "WARNING: Do not run this if you have installed a root that has\n");
        user_out(2, "replaced the kernel or a kernel extension, and make sure you are\n");
        user_out(2, "running $build.\n\n");
        user_out(2, "Press Return to continue, or Control-C to cancel.\n");
        user_pause(3);
}

sub usage
{
}

sub run_interactive_mode
{
        print "Entering Interactive mode.\n";
        $target = user_query(3, "Please enter the hostname of the panicked machine: ", "Targetless", ,"You entered the machine  \"","\", is that correct");
        $build = user_query(3, "Please enter the build the panicked machine is running (.i.e Cheetah4K78): ", "Buildless", "You entered the build \"","\", is that correct");
	$use_hotline = user_yesno(3, "Can you leave the panicked machine alone in case a kernel guru would like to look at it?");
}

sub load_kmodsyms
{
        
        my($file);
        my($dir);

        opendir($dir, $symfile_dir);
        foreach $file (sort readdir($dir))
        {
		if(($file ne ".") && ($file ne ".."))
                {
			to_gdb("add-symbol-file $symfile_dir/$file");
			to_gdb("y");
		}
	}
	flush_gdb(1, "/dev/null");
}

sub mods_in_backtrace
{

	my($retval) = 0;

	open(BACKTRACE, "$output_file.short");

	while(<BACKTRACE>)
	{
		if(m/odules/)
		{
			$retval = 1;
		}
	}
	close(BACKTRACE);

	return $retval;
}

sub get_shortinfo
{
	insert_user_message_gdb("Getting panic log");
	to_gdb("paniclog");
	insert_user_message_gdb("Getting backtrace");
	to_gdb("bt");

	flush_gdb(1, "$output_file.short");
}

sub get_longinfo
{
	insert_user_message_gdb("Getting panic log");
	to_gdb("paniclog");
	insert_user_message_gdb("Getting full backtrace");
	to_gdb("bt full");
	insert_user_message_gdb("Getting register data");
	to_gdb("info all-reg");
	insert_user_message_gdb("Getting zone data");
	to_gdb("zprint");
	insert_user_message_gdb("Getting stack data");
	to_gdb("showallstacks");

	flush_gdb(1, $output_file);
}

sub mail_hotline
{
	my($radar) = $_[0];
        my($mail);
        my($file);
	

        open($mail, "| /usr/sbin/sendmail -oi -t -odi");
        print $mail "From: $contact_email\n";
                print $mail "To: panichotline\@group.apple.com\n";
                print $mail "Bcc: panic\@spectrum-tigger.apple.com\n";
                print $mail "Subject: APR: $target panic on $build\n\n";
                print $mail $contact;
		print $mail $notes;
		print $mail "\nRadar: $radar\n";
	open($file, $output_file.".short");
	while(<$file>)
	{
		print $mail $_;
	}
	close($file);
	close($mail);
}

sub mail_panic
{
	my($mail);
	my($file);

                open($mail, "| /usr/sbin/sendmail -oi -t -odi");
                print $mail "From: $contact_email\n";
                print $mail "To: panic\@pectrum-tigger.apple.com\n";
                print $mail "Subject: APR(long): $target panic on $build\n\n";
	open($file, "$output_file.short");
	while(<$file>)
	{
		print $mail $_;
	}
	close($mail);
}

sub get_prefs
{

	my($prefs_file);
	my($contact_name);
	my($contact_info);

	if(-e $ENV{"HOME"}."/Library/Preferences/com.apple.panic-debug")
	{
		open($prefs_file, $ENV{"HOME"}."/Library/Preferences/com.apple.panic-debug");
		while(<$prefs_file>)
		{
			if(m/^EMAIL (.+)/)
			{
				$contact_email = $1;
				chomp($contact_email);
			}
			elsif(m/^NAME (.+)/)
			{
				$contact_name = $1;
				chomp($contact_name);
			}
			elsif(/^INFO (.+)/)
			{
				$contact_info = $1;
				chomp($contact_info);
			}
		}
		close($prefs_file);
		user_out(2, "Loaded stored prefs\n");
		user_out(2, "Name: $contact_name\nEmail: $contact_email\nOther Info: $contact_info\n");
		user_out(2, "To reset prefences delete ~/Library/Preferences/com.apple.panic-debug\n");
	}
	else
	{
                $contact_name = user_query(3, "What is your name: ", "Nameless", "You entered \"","\", is that correct");
                chomp($contact_name);
                $contact_email = user_query(3, "What is your email-address: ", "Mailless", "You entered \"","\", is that correct");
                chomp($contact_email);
                $contact_info = user_query(3, "Please provide some way for a kernel member to contact you (phone or email): ", "Friendless", ,"You entered\"", "\" is that correct?");
                chomp($contact_info);
		if(user_yesno(3, "Would you like this info saved and automatically entered in the future "))
		{
			open($prefs_file, ">".$ENV{"HOME"}."/Library/Preferences/com.apple.panic-debug");
			print $prefs_file "NAME $contact_name\n";
			print $prefs_file "EMAIL $contact_email\n";
			print $prefs_file "INFO $contact_info\n";
		}
	}

	$contact = "Contact Info:\nName: $contact_name\nEmail: $contact_email\nOther Info: $contact_info\n\n";
}

parse_args();
confirm_build_data();
if($use_hotline)
{
	get_prefs();
}
`mkdir -p $session_tmp`;
`mkdir -p ~/Library/Logs/panic-debug`;

user_out(2, "Connecting with GDB (this may take a few moments).\n");

if($do_setup)
{
	setup_gdb($target, $mach_kernel, $use_arp, $mac_addr);
}




open_gdb($target, $mach_kernel);
get_shortinfo();
if(mods_in_backtrace())
{
	if(! -e $kext_dir)
	{
		system("$ExtensionsBuilder $build $kext_dir");
	}
	close_gdb();
	system("$SymbolBuilder $target $mach_kernel $kext_dir $symfile_dir");
	open_gdb($target, $mach_kernel);
	load_kmodsyms();
	get_shortinfo();
}
user_out(2, "Please immediately file a Radar containing the info in $output_file.short againt \"Kernel Panic\" version \"X\"\n");
if($use_hotline)
{
	my($radar);
	$radar = user_query(3, "Please enter the the radar number of the bug you just filed: ", "0000000", ,"You enter the number \"","\", is that correct");
	mail_hotline($radar);
}
get_longinfo();
close_gdb();
user_out(2, "A more detailed debug session is available at $output_file, please add it to the radar.\n");
if($mail_home)
{
	mail_panic();
}
`rm -rf $session_tmp`;

__END__

=head1 NAME

panic-debug - A tool for gathering data about Mac OS X kernel crashes

=head1 SYNOPSIS

panic-debug [--mach_kernel=<path> --extensions-dir=<path> --(no)setup --(no)panic-hotline --(no)static-arp --mac-addr=<addr>] [machine [build]]

=head1 OPTIONS

=over 8

=item B<-mach_kernel>

This argument takes a path to a mach_kernel which panic-debug will use for symbols.

=item B<--extensions-dir>

This argument takes a path to an extensions directory which panic-debug will use for symbols and extension dependency information.

=item B<--setup>

This flag will tell panic-debug it is alright  alter some of the state on the remote machine in order to enable arp. This is panic-debug's default behaviour.

=item B<--nosetup>

This flag tells panic-debug it cannot alter the state on the remote machine. It is useful if you are using panic-debug to reconnect t o machine you have already connected to.

=item B<--panic-hotline>

This flag tells panic-debug to email a report to the panic-hotline. This is panic-debugs default behabiour.

=item B<--nopanic-hotline>

This flags tells panic-debug not to mail the panic-hotline.

=item B<--static-arp>

This flag tells panic-debug to make a static arp  entry for the machine. This is the default behaviour.

=item B<--nostatic-arp>

This flag tells panic-debug not to make a static arp  entry for the machine.

=item B<--mac-addr>

This argument allows tells panic debug to use the specified mac-addr when making a static-arp entry.

=item B<machine>

This may be the DNS name, IP address, or NetInfo entry for he panicked machine.

=item B<build>

This is the canonical (i.e. Jaguar6C35) buildname. If this is not specified then it may be necessary to specify both a mach_kernel and an extensions-dir.

=back

=head1 DESCRIPTION

B<panic-debug> will connect to a remote kernel and gather information including panic data, register state, stack data, and the active kernel backtrace.

=cut
