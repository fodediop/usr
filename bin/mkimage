#!/bin/sh

release=$1
version=$2
path=$3
megs=$4

IMAGE_MOUNT=/nbmnt

disk_name=UNKNOWN

cleanup()
{
    echo cleaning up...
    if [ ! "${disk_name}" = "UNKNOWN" ]; then
	echo "${disk_name}"
	/sbin/umount "${IMAGE_MOUNT}"
	/usr/bin/hdiutil eject "${disk_name}"
    fi
    exit 1
}

if [ "${release}" = "" -o "${version}" = "" -o "${path}" = "" ] ; then
	echo "mkimage <release> <version> <image root path>"
	exit 1
fi

if [ ! -d "${path}" ] ; then
    echo "path ${path} doesn't exist"
    cleanup
fi

image_name="${release}${version}" 

root_uid=`/usr/bin/id -u`
if [ ! "${root_uid}" = "0" ] ; then
    echo "You must be root to run this command"
    exit 1
fi

echo creating NetBoot image package for "${image_name}"
if [ ! -e "${image_name}.nbi" ] ; then
	mkdir "${image_name}.nbi" || exit 1
elif [ ! -d "${image_name}.nbi" ] ; then
	echo "${image_name}.nbi}" is not a directory
	exit 1
fi

cd "${image_name}.nbi"

if [ "${megs}" = "" ] ; then
	megs=1999
fi

if [ ! -e "${image_name}.dmg" ] ; then
	/usr/bin/hdiutil create "${image_name}" -megabytes "${megs}" -layout NONE || exit 1
fi

dev_name=`/usr/bin/hdid "${image_name}.dmg" -nomount | sed 's:^\([^ 	]*\)[	 ]*$:\1:' || exit 1`
raw_dev_name=`echo -n ${dev_name} | sed 's:/dev/r/:/dev/:'`
disk_name=`basename ${dev_name}`
echo "disk_name is ${disk_name}"
/sbin/newfs_hfs -v "${image_name}" "${raw_dev_name}" || cleanup

mkdir "${IMAGE_MOUNT}"

echo "Mounting ${dev_name} on ${IMAGE_MOUNT}"
/sbin/mount -t hfs "${dev_name}" "${IMAGE_MOUNT}" || cleanup

echo Invoking installer
export CM_BUILD=CM_BUILD
/usr/sbin/installer -pkg "${path}/System/Installation/Packages/OSInstall.mpkg" -target "${IMAGE_MOUNT}" || cleanup 

echo Enabling root login with no password
rm "${IMAGE_MOUNT}"/var/db/.AppleMultiCDInstall
touch "${IMAGE_MOUNT}"/var/db/.AppleSetupDone
nicl -raw "${IMAGE_MOUNT}"/var/db/netinfo/local.nidb <<EOF
cd /users/root
create . passwd
quit
EOF

echo Copying booter, kernel
cp "${IMAGE_MOUNT}"/usr/standalone/ppc/bootx.bootinfo booter
cp "${IMAGE_MOUNT}"/mach_kernel mach.macosx
echo "Creating mkext cache"
kextcache -l -n -m mach.macosx.mkext "${IMAGE_MOUNT}"/System/Library/Extensions

echo Creating NBImageInfo.plist
~admin/bin/gennbinfo "${release}" "${version}" > NBImageInfo.plist

sleep 5

echo "All done, cleaning up"
echo "${disk_name}"
/sbin/umount "${IMAGE_MOUNT}"
/usr/bin/hdiutil eject "${disk_name}"

exit 0
