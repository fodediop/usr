#!/usr/bin/python
import os
import sha

from base64 import decodestring as b64decode
from base64 import encodestring as b64encode
from binascii import unhexlify as hex2bin

# These are some example generated keys for you convinience, replace the before you start selling your app ;-)
pubKey = u'0x9F7B984C1758F45A31E162D27B76A2460DE570C1F2C6C51080707FC5C52D480EDCF54F57B54BA577A28908DDABE26C974C243605348366C649C455D10B4EB382C36763A60F8CEEC24AE54035C4CEA0108B31DD1D9915450B967B510B0737933CA428780D997E6DB394238E65A53919B127A76B9FEB618411A5B211A3FA6B873F'
privKey = u'0x6A5265880F90A2E6CBEB9736FCF9C1840943A0814C84836055A0552E8373855F3DF8DF8FCE326E4FC1B0B093C7EC4864DD6D7958CDACEF2EDBD839360789CD00CA2324E6E883678C92BF48E26661DFAFD06E45ED5231B2CE56E7292AB314B24424DAD91F65DA5B16E15A3DE856A197887012064EB70346B1E14343F41973882B'

def hex2dec(s):
	return int(s, 16)

def dec2hex(n):
	val = "%X" % n
	while len(val) < 256:
		val = '0' + val
	return val

def powmod(x,a,m):
	r=1
	while a>0:
		if a%2==1: r=(r*x)%m
		a=a>>1; x=(x*x)%m
	return r

def reverse(s):
	rs = ""
	for x in s:
		rs = x + rs
	return rs

def getSignature(information):
	
	keys = information.keys()
	keys.sort()
	
	total = u''.join([information[key] for key in keys]).replace(u"'", u"'\\''")
	
	hash = sha.new(total.encode('utf-8')).hexdigest()
	
	paddedHash = '0001'

	for i in range(0, 105):
		paddedHash += 'ff'

	paddedHash += '00' + hash
		
	decryptedSig = hex2dec(paddedHash)
	
	sig = powmod(decryptedSig, hex2dec(privKey), hex2dec(pubKey))
	sig = dec2hex(sig)
	sig = hex2bin(sig)
	sig = b64encode(sig)

	return sig

def licenceData(license_info):
	
	# license_info should be a dict with all the licence items
	
	keys = license_info.keys()
	keys.sort()
	
	signature = getSignature(license_info)
	
	licence_data = u"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
	licence_data += u"<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n"
	licence_data += u"<plist version=\"1.0\">\n<dict>\n"

	for key in keys:

		licence_data += u"\t<key>" + key + u"</key>\n"
		licence_data += u"\t<string>" + license_info[key] + u"</string>\n"

	licence_data += u"\t<key>Signature</key>\n"
	licence_data += u"\t<data>" + signature + u"</data>\n"
	licence_data += u"</dict>\n"
	licence_data += u"</plist>\n"
	
	return licence_data

print licenceData({u'name':u'koen'})
