#!/usr/bin/perl -w

use strict;
use Sys::Syslog qw(:DEFAULT :macros); 
use XML::Simple;
use LWP::UserAgent;
use URI::URL;
use Getopt::Std;
use Carp::Heavy;	# needed to fix error when compiled for win32

use vars qw($opt_h $opt_u $opt_p $opt_l $opt_c);

openlog($0, '', LOG_DAEMON);       # don't forget this

getopts('c:hl:p:u:');

my ($account, $proxy, $logfile, $csv, %FAPHash, $LOGFH);
my $url = "http://my.wildblue.net/content/fap-sso/proxy_username.asp?user_name=";

$account = 'tgunr@wildblue.net';
$proxy = 'http://12.189.32.60:80/';

sub read_config_file {
    if (!open(CONFIG,"< /Users/davec/bin/fapzilla.cfg")) {
    	syslog('warning', "Could not read config file");
        return 0;
    }

    while (<CONFIG>) {
        chomp;                  # no newlines
        s/#.*//;                # no comments
        s/^\s+//;               # no leading white space
        s/\s+$//;               # no trailing white space
        next unless length;     # anything left ??
        my ($var, $value) = split(/\s*=\s*/, $_, 2);
        $FAPHash{$var} = $value;
    }
    close(CONFIG);
    return 1;
}

sub print_usage {
    print "usage: $0 -u email [-p proxy] [-l file] [-c char]\n\n";
    print " -h help\n";
    print " -u username (WB primary email address)\n";
    print " -p proxy server to use (optional)\n";
	print " -l file (optional - file to log results to)\n";
	print " -c char (optional - csv character, defaults to a space)\n\n";
    print "Example:  $0 -u USER\@wildblue.net -p http://12.189.32.60:80/\n\n";
    exit(0);
}

# Start of main program

print_usage() if $opt_h;

read_config_file();

$account = $opt_u || $FAPHash{Email};
$proxy   = $opt_p || $FAPHash{Proxy};
$logfile = $opt_l || $FAPHash{Logfile};
$csv     = $opt_c || $FAPHash{CSV_seperator} || ' ';

print_usage if !$account;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime time;
$mon++;
$year += 1900;

$url .= $account;

#printf("URL: %s\n", $url);
#printf("Proxy: %s\n", $proxy) if $proxy;
#printf("Logfile: %s\n", $logfile) if $logfile;
#printf("CSV: %s\n", $csv) if $csv;

if ($logfile) {
	if (!open($LOGFH, ">> $logfile")) {
		syslog('warning', "Couldn't open the logfile $logfile, sending it to stdout\n");
		open($LOGFH, ">&STDOUT");
	}
} else {
	open($LOGFH, ">&STDOUT");
}

# Create a new UserAgent object
my $ua = new LWP::UserAgent;
$ua->agent("fapzilla/1.0");

# If proxy server specified, set it in the User Agent object
if (defined $proxy) {
    my $uri = new URI::URL $url;
    my $scheme = $uri->scheme;
    $ua->proxy($scheme, $proxy);
}

my $request = new HTTP::Request('GET', $url);
my $response = $ua->request($request);
 
if ($response->is_error) {
    printf $LOGFH "%d/%d/%d %02d:%02d:%02d%sERROR%sERROR", $mon,$mday,$year,$hour,$min,$sec,$csv,$csv;
    exit 1;
}
 
my $body =  $response->content;

## Process the XML
my $opt = XMLin($body);

printf $LOGFH "%d/%d/%d %02d:%02d:%02d", $mon,$mday,$year,$hour,$min,$sec;
print $LOGFH "$csv$opt->{set}->{'Actual Usage Upload'}->{value}";
print $LOGFH "$csv$opt->{set}->{'Actual Usage Download'}->{value}\n";

syslog('notice',"Uploaded $opt->{set}->{'Actual Usage Upload'}->{value}MB Downloaded $opt->{set}->{'Actual Usage Download'}->{value}MB");

closelog();
