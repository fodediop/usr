#!/bin/bash
#  Remove language files from  source if they don't exist in the destination 
#
#  Created by Dave Carlton on 2009-02-19.
#  Copyright (c) 2009 __MyCompanyName__. All rights reserved.
#

DEST="/System/Library/"
cd "/Previous Systems.localized/2009-02-17_1848/System/Library"
TEMP=`mktemp -p /tmp dac.XXXX`
find . -iname "*.lproj" -not -iname "english.lproj" > $TEMP
cat $TEMP | while read ; do
	if [ ! -e "$DEST$REPLY" ] ; then
		echo "$REPLY"
		rm -fr "$REPLY"
	fi
done
