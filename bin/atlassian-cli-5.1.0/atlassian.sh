#!/bin/bash
# This script makes it easier to manage cli version, user, and password settings for various CLIs
# Customize this for your installation. Be careful on upgrades or rename it to something else.
#
# Examples:
#     atlassian.sh confluence --action getServerInfo
#     atlassian.sh jira --action getServerInfo
#     atlassian myOtherConfluence --action getServerInfo
#     atlassian all --action run --file actions.txt
#
# Use settings to customize java environment values globally or for a particular server
# Examples:
#     settings="-Duser.language=en -Dfile.encoding=UTF-8"
#     settings="$settings -Duser.timezone=EDT"

application=$1

# - - - - - - - - - - - - - - - - - - - - START CUSTOMIZE FOR YOUR INSTALLATION !!!
user='automation'
password='automation'
settings=''

if [ "$application" = "all" ]; then
    string="all-cli-5.1.0.jar"
elif [ "$application" = "confluence" ]; then
    string="confluence-cli-5.1.0.jar --server https://confluence.example.com --user $user --password $password"
elif [ "$application" = "jira" ]; then
    string="jira-cli-5.1.0.jar --server https://jira.example.com --user $user --password $password"
elif [ "$application" = "fisheye" ]; then
    string="fisheye-cli-5.1.0.jar --server https://fisheye.example.com --user $user --password $password"
elif [ "$application" = "crucible" ]; then
    string="crucible-cli-5.1.0.jar --server https://crucible.example.com --user $user --password $password"
elif [ "$application" = "bamboo" ]; then
    string="bamboo-cli-5.1.0.jar --server https://bamboo.example.com --user $user --password $password"
elif [ "$application" = "bitbucket" ]; then
    string="bitbucket-cli-5.1.0.jar --server https://bitbucket.example.com --user $user --password $password"
elif [ "$application" = "hipchat" ]; then
    string="hipchat-cli-${hipchatVersion}.jar --server https://hipchat.example.com --token xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

# - - - - - - - - - - - - - - - - - - - - - END CUSTOMIZE FOR YOUR INSTALLATION !!!

elif [ "$application" = "" ]; then
    echo "Missing application parameter. Specify an application like confluence, jira, or similar."
    echo "$0 <application name> <application specific parameters>"
    exit -99
else
    echo "Application $application not found in $0"
    exit -99
fi

java $settings -jar `dirname $0`/lib/$string "${@:2}"
