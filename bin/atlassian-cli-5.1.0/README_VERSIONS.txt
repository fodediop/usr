Included with this distribution of the Atlassian Command Line Interface:

all-cli-5.1.0
bamboo-cli-5.1.0
bitbucket-cli-5.1.0
confluence-cli-5.1.0
crucible-cli-5.1.0
fisheye-cli-5.1.0
hipchat-cli-5.1.0
jira-cli-5.1.0