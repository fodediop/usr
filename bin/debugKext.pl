#!/usr/bin/perl -w

use English;

if ($#ARGV != 3) {
	print "debugKext <train> <version> <kext> <address>\n";
	print "<train> - build train\n";
	print "<version> - version of train\n";
	print "<kext> - kernel extension\n";
	print "<address> - address where <kext> was loaded in memory\n";
	print "sample: debugKext Puma 5A14 SharedIP 0x1234\n";
	exit(1);
}

$train = $ARGV[0];
$buildVers = $train . $ARGV[1];
$kext = $ARGV[2];
$addr = $ARGV[3];

print "Figuring out version and paths...\n\n";

$xnuVers = `~rc/bin/getvers $buildVers xnu`;
chomp($xnuVers);

$xnuRevision = `~rcdev/xbs-Current/jbin/Jasper_getBuildVersions $buildVers xnu`;
chomp($xnuRevision);

$kextVers = `~rc/bin/getvers $buildVers $kext`;
chomp($kextVers);

$kextRevision = `~rcdev/xbs-Current/jbin/Jasper_getBuildVersions $buildVers $kext`;
chomp($kextRevision);

$newKextSym = "~/" . $kext . ".sym";

@thisIsATemp = split(/\~/, $xnuRevision);
$xnuRevision = $thisIsATemp[1];

@thisIsATemp = split(/\~/, $kextRevision);
$kextRevision = $thisIsATemp[1];

if (!$xnuVers or !$xnuRevision)
{
	print "Couldn't determine xnu version for " . $buildVers . ".\n";
	exit(2);
}

if (!$kextVers or !$kextRevision)
{
	print "Couldn't determine " . $kext . " version for " . $buildVers
		. ".\n";
	exit(2);
}

$xnuRoot = "~rc/Software/" . $train . "/Roots/xnu/" . 
		$xnuVers . ".root~" . $xnuRevision . "/mach_kernel";
$xnuSym = "~rc/Software/" . $train . "/Symbols/xnu/" . 
		$xnuVers . ".sym~" . $xnuRevision . "/mach_kernel";
$kextSym = "~rc/Software/" . $train . "/Symbols/" . $kext . "/" .
	$kextVers . ".sym~" . $kextRevision . "/" . $kext .
	".kext/Contents/MacOS/" . $kext . "@" . $addr;
$kextSrc = "~rc/Software/" . $train . "/Projects/" . $kext . "/" . $kextVers;

print "xnuRoot: " . $xnuRoot . "\n";
print "xnuSym: " . $xnuSym . "\n";
print "kextSym: " . $kextSym . "\n";
print "kextSrc: " . $kextSrc . "\n";

`kmodsyms -k $xnuRoot -o $newKextSym $kextSym`;
print "\nSym file is in " . $newKextSym . ".\n";
print "Use add-symbol-file " . $newKextSym . " after connecting.\n\n";
system "/usr/bin/gdb -s " . $newKextSym . " -d " . $kextSrc . " " . $xnuSym;
`rm -f $newKextSym`;
