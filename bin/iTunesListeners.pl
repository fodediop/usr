#!/usr/bin/perl -w
# Who's listening to my iTunes.
# Man, my perl skills have degraded...
# zellers@apple.com, 4/28/03

my (@stat) = `netstat`;

my %seen;

my (@listeners);
my (@lurkers);

foreach (@stat) {
		chop;
		my (@arr) = split(/[\s]+/, $_);

		if (($arr[0] eq "tcp4" or $arr[0] eq "tcp6")) {
			
			my @segs = split(/\./, $arr[4]);
			my $host = $segs[0];

			if (defined $host and $host ne "" and $arr[3] =~ m/3689/) {
			   if (! defined $seen{$host}) {
				  $seen{$host} = 1;

				  next  if ($host eq "localhost" or $host eq "*");

				  my $gr = `niutil -read /applerd /machines/$host | grep -i owner`;

				  if (length $gr > 2) {
					  chop $gr;

					  my $info = "$host " . $gr;
					  
					  if ($arr[2] ne "0") {
						  push(@listeners, $info);
					  } else {
						  push(@lurkers, $info);
					  }
				  }
			  }
		   }
		}
}

if (scalar @listeners == 0) {
	print "No Listeners!\n";
} else {
	print "Listeners\n  ";
	print join("\n  ", @listeners);
}

if (scalar @lurkers == 0) {
	print "No Lurkers!\n";
} else {
	print "\n\nLurkers:\n  ";
	print join("\n  ", @lurkers);
}

print "\n\nDon't steal music!\n";

