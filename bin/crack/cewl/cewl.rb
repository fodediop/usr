#!/usr/bin/env ruby

# == CeWL: Custom Word List Generator
#
# CeWL will spider a target site and generate up to three lists:
#
# * A word list of all unique words found on the target site
# * A list of all email addresses found in mailto links
# * A list of usernames/author details from meta data found in any documents on the site
#
# == Usage
#
# cewl [OPTION] ... URL
#
# -h, --help:
#    show help
#
# --depth x, -d x:
#    depth to spider to, default 2
#
# --min_word_length, -m:
#    minimum word length, default 3
#
# --email, -e:
#    include any email addresses found duing the spider
#
# --meta, -a:
#    include any meta data found during the spider
#
# --no-words, -n
#    don't output the wordlist
#
# --offsite, -o:
#    let the spider visit other sites
#
# --write, -w file:
#    write the words to the file
#
# --ua, -u user-agent:
#    useragent to send
#
# --meta-temp-dir directory:
#    the temporary directory used by exiftool when parsing files, default /tmp
#
# -v
#    verbose
#
# URL: The site to spider.
#
# Author:: Robin Wood (dninja@gmail.com)
# Copyright:: Copyright (c) Robin Wood 2008
# Licence:: GPL
#

require 'getoptlong'
require 'spider'
require 'http_configuration'
require './cewl_lib'

# Doing this so I can override the allowed? fuction which normally checks
# the robots.txt file
class MySpider<Spider
	# Create an instance of MySpiderInstance rather than SpiderInstance
	def self.start_at(a_url, &block)
		rules = RobotRules.new('Ruby Spider 1.0')
		a_spider = MySpiderInstance.new({nil => a_url}, [], rules, [])
		block.call(a_spider)
		a_spider.start!
	end
end

# My version of the spider class which allows all files
# to be processed
class MySpiderInstance<SpiderInstance
	# Force all files to be allowed
	def allowed?(a_url, parsed_url)
		true
	end
end

# A node for a tree
class TreeNode
	attr :value
	attr :depth
	attr :key
	attr :visited, true
	def initialize(key, value, depth)
		@key=key
		@value=value
		@depth=depth
		@visited=false
	end

	def to_s
		if key==nil
			return "key=nil value="+@value+" depth="+@depth.to_s+" visited="+@visited.to_s
		else
			return "key="+@key+" value="+@value+" depth="+@depth.to_s+" visited="+@visited.to_s
		end
	end
	def to_url_hash
		return({@key=>@value})
	end
end

# A tree structure
class Tree
	attr :data
	@max_depth
	@children

	# Get the maximum depth the tree can grow to
	def max_depth
		@max_depth
	end

	# Set the max depth the tree can grow to
	def max_depth=(val)
		@max_depth=Integer(val)
	end
	
	# As this is used to work out if there are any more nodes to process it isn't a true empty
	def empty?
		if !@data.visited
			return false
		else
			@children.each { |node|
				if !node.data.visited
					return false
				end
			}
		end
		return true
	end

	# The constructor
	def initialize(key=nil, value=nil, depth=0)
		@data=TreeNode.new(key,value,depth)
		@children = []
		@max_depth = 2
	end

	# Itterator
	def each
		yield @data
			@children.each do |child_node|
			child_node.each { |e| yield e }
		end
	end

	# Remove an item from the tree
	def pop
		if !@data.visited
			@data.visited=true
			return @data.to_url_hash
		else
			@children.each { |node|
				if !node.data.visited
					node.data.visited=true
					return node.data.to_url_hash
				end
			}
		end
		return nil
	end

	# Push an item onto the tree
	def push(value)
		key=value.keys.first
		value=value.values_at(key).first
		
		if key==nil
			@data=TreeNode.new(key,value,0)
		else
			if key==@data.value
				child=Tree.new(key,value, @data.depth+1)
				@children << child
			else
				@children.each { |node|
					if node.data.value==key && node.data.depth<@max_depth
						child=Tree.new(key,value, node.data.depth+1)
						@children << child
					end
				}
			end
		end
	end
end

opts = GetoptLong.new(
	[ '--help', '-h', GetoptLong::NO_ARGUMENT ],
	[ '--depth', '-d', GetoptLong::OPTIONAL_ARGUMENT ],
	[ '--min_word_length', "-m" , GetoptLong::REQUIRED_ARGUMENT ],
	[ '--no-words', "-n" , GetoptLong::NO_ARGUMENT ],
	[ '--offsite', "-o" , GetoptLong::NO_ARGUMENT ],
	[ '--write', "-w" , GetoptLong::REQUIRED_ARGUMENT ],
	[ '--ua', "-u" , GetoptLong::REQUIRED_ARGUMENT ],
	[ '--meta-temp-dir', GetoptLong::REQUIRED_ARGUMENT ],
	[ '--meta', "-a" , GetoptLong::NO_ARGUMENT ],
	[ '--email', "-e" , GetoptLong::NO_ARGUMENT ],
	[ "-v" , GetoptLong::NO_ARGUMENT ]
)

# Display the usage
def usage
	puts"cewl 2.0 Robin Wood (dninja@gmail.com) (www.digininja.org)

Usage: cewl [OPTION] ... URL
	--help, -h: show help
	--depth x, -d x: depth to spider to, default 2
	--min_word_length, -m: minimum word length, default 3
	--offsite, -o: let the spider visit other sites
	--write, -w file: write the output to the file
	--ua, -u user-agent: useragent to send
	--no-words, -n: don't output the wordlist
	--meta, -a: include meta data
	--email, -e: include email addresses
	--meta-temp-dir directory: the temporary directory used by exiftool when parsing files, default /tmp
	-v: verbose

	URL: The site to spider.

"
	exit
end

verbose=false
ua=nil
url = nil
outfile = nil
offsite = false
depth = 2
min_word_length=3
email=false
meta=false
wordlist=true
meta_temp_dir="/tmp/"

begin
	opts.each do |opt, arg|
		case opt
		when '--help'
			usage
		when "--meta-temp-dir"
			if !File.directory?(arg)
				puts "Meta temp directory is not a directory\n"
				exit
			end
			if !File.writable?(arg)
				puts "The meta temp directory is not writable\n"
				exit
			end
			meta_temp_dir=arg
			if meta_temp_dir !~ /.*\/$/
				meta_temp_dir+="/"
			end
		when "--no-words"
			wordlist=false
		when "--meta"
			meta=true
		when "--email"
			email=true
		when '--min_word_length'
			min_word_length=arg.to_i
			if min_word_length<1
				usage
			end
		when '--depth'
			depth=arg.to_i
			if depth<1
				usage
			end
		when '--offsite'
			offsite=true
		when '--ua'
			ua=arg
		when '-v'
			verbose=true
		when '--write'
			outfile=arg
		end
	end
rescue
	usage
end

if ARGV.length != 1
	puts "Missing url argument (try --help)"
	exit 0
end

url = ARGV.shift

# Must have protocol
if url !~ /^http(s)?:\/\//
	url="http://"+url
end

# The spider doesn't work properly if there isn't a / on the end
if url !~ /\/$/
	url=url+"/"
end

words_arr=[]
email_arr=[]
url_stack=Tree.new
url_stack.max_depth=depth
usernames=Array.new()

# Do the check here so we don't do all the processing then find we can't open the file
if outfile!=nil
	begin
		outfile_file=File.new(outfile,"w")
	rescue
		puts "Couldn't open the output file for writing"
		exit
	end
else
	outfile_file=$stdout
end

begin
	# If you want to use a proxy, uncomment the next 2 lines and the matching end near the bottom
	#http_conf = Net::HTTP::Configuration.new(:proxy_host => 'tb', :proxy_port => 3128)
	#http_conf.apply do
		MySpider.start_at(url) do |s|
			if ua!=nil
				s.headers['User-Agent'] = ua
			end

			s.add_url_check do |a_url|
				allow=true
				# Extensions to ignore
				if a_url =~ /(\.zip$|\.gz$|\.zip$|\.bz2$|\.png$|\.gif$|\.jpg$|#)/
					if verbose
						puts "Ignoring internal link or graphic: "+a_url
					end
					allow=false
				else
					if /^mailto:(.*)/i.match(a_url)
						if email
							email_arr<<$1
							if verbose
								puts "Found #{$1} on page #{a_url}"
							end
						end
						allow=false
					else
						if !offsite
							allow=a_url =~ %r{^#{url}.*}
							if !allow && verbose
								puts "Offsite link, not following: "+a_url
							end
						end
					end
				end
				allow
			end

			s.on :success do |a_url, resp, prior_url|
				if verbose
					puts "Visiting: #{a_url} referred from #{prior_url}, got response code #{resp.code}"
				end
				# strip html tags
				words=resp.body.gsub(/<\/?[^>]*>/, "")

				# may want 0-9 in here as well in the future but for now limit it to a-z so
				# you can't sneak any nasty characters in
				if /.*\.([a-z]+)(\?.*$|$)/i.match(a_url)
					file_extension=$1
				else
					file_extension=""
				end

				if meta
					begin
						output_filename=meta_temp_dir+"cewl_tmp"
						output_filename += "."+file_extension unless file_extension==""
						out=File.new(output_filename, "w")
						out.print(resp.body)
						out.close

						usernames+=process_file(output_filename, verbose)
					rescue
						puts "Couldn't open the meta temp file for writing - "+$!
						exit
					end
				end

				# don't get words from these file types. Most will have been blocked by the url_check function but
				# some are let through, such as .css, so that they can be checked for email addresses

				# this is a bad way to do this but it is either white or black list extensions and 
				# the list of either is quite long, may as well black list and let extra through
				# that can then be weeded out later than stop things that could be useful
				if file_extension !~ /^((doc|dot|ppt|pot|xls|xlt|pps)[xm]?)|(ppam|xlsb|xlam|pdf|zip|gz|zip|bz2|css|png|gif|jpg|#)$/
					if email
						# Split the file down based on the email address regexp
						words.gsub!(/\b([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})\b/i)

						# If you want to pull email addresses from the contents of files found, such as word docs then move
						# this block outside the if statement
						# I've put it in here as some docs contain email addresses that have nothing to do with the targer
						# so give false positive type results
						words.each { |word|
							while /\b([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})\b/i.match(word)
								if verbose
									puts "Found #{$1} on page #{a_url}"
								end
								email_arr<<$1
								word=word.gsub(/#{$1}/, "")
							end
						}
					end
				
					# remove any symbols
					words.gsub!(/[^a-z0-9]/i," ")
					# add to the array
					words_arr+=words.split(" ")
				end
			end
			s.store_next_urls_with url_stack

		end
	#end
rescue
	puts "Couldn't access the site"
	puts "Error: "+$!
	exit
end

email_arr.delete_if { |x| x.chomp==""}
words_arr.delete_if { |x| x.chomp==""}
usernames.delete_if { |x| x.chomp==""}
email_arr.uniq!
email_arr.sort!
words_arr.uniq!
words_arr.sort!
usernames.uniq!
usernames.sort!

if min_word_length!=nil
	words_arr.delete_if { |x| x.length < min_word_length }
end

if wordlist
	outfile_file.puts words_arr.join("\n")
end

if email
	if wordlist||verbose
		outfile_file.puts
	end
	outfile_file.puts "Email addresses found"
	outfile_file.puts email_arr.join("\n")
end

if meta
	if email||wordlist
		outfile_file.puts
	end
	outfile_file.puts "Meta data found"
	outfile_file.puts usernames.join("\n")
end

if outfile!=nil
	outfile_file.close
end
