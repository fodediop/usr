
Dave Grohl 2.1
http://www.davegrohl.org/

Please do not use this for evil.

Keep in mind, this will run very slow on Mountain Lion, but that's the whole point of PBKDF.   It seems Apple's throttled it to take about a tenth of a second to encrypt which means about 20 guesses per a second on a dual-core machine (on a good day).  To see how useful dave used to be, try enabling SMB file-sharing for your user and then give it a spin.

Version 2 has been completely re-written with a more modern code-base.  Some features haven't been moved over yet, but other more awesome features have been added.

---------------------------------------------------------

Version 2.1
	-Distributed Attacks! Use all the computing power your wallets can provide!
	-A completely re-written threading engine dishes out the workload more sensibly
	-A much less sucky user-interface. (now shows guessed passwords for each thread when prompted)
	-Many performance and reliability improvements

---------------------------------------------------------

Version 2.01
	-Dave handles large numbers much better.  (D'oh!)

---------------------------------------------------------

Version 2.0
	-Mountain Lion with PBKDF2
	-More reliable method of loading the hashes.
	-Can specify your own user plist to crack (like from another computer)
	-Dumps full ShadowHashData in human-readable format
	-Can specify any number of threads
	-General performance increased

---------------------------------------------------------

  -u,  --user=USERNAME      Crack a user's password.
  -v,  --verbose            Show guessed passwords.
  -d,  --dictionary         Dictionary attack only.
  -i,  --incremental        Incremental attack only.
  -p,  --plist=PLIST        Crack a password from a user plist.
  -m,  --min                Minimum number of digits for the incremental attack.
  -M,  --max                Maximum number of digits for the incremental attack.
  -c,  --characters=CHARS   Define your own custom character set to use.
  -C,  --char-set=SET       Use one of these predefined character sets.
                  09        The ten arabic numerals
                  az        The lower-case alphabet
                  az09      The lower-case alphabet with numerals (default)
                  azAZ09    The full alphabet with numbers
                  all       The full alphabet with numbers and symbols

  -D,  --distributed        Start cracking on all available DaveGrohl servers.
  -S,  --server             Run in server mode.
  -P,  --port=PORT          Run server on a the specified port.

  -s,  --shadow=USERNAME    Dump a user's ShadowHashData.
  -j,  --john=USERNAME      Dump a user's hash formatted for John the Ripper.
  -l,  --list=USERNAME      List the used hash types for a user.
  -h,  --help               Show this help text ...but you knew that.
