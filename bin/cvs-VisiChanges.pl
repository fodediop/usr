#!/usr/bin/perl -w


sub printNote($)
{
	my $note = shift;
	my $dashes = "-"x40;
	print "\n$dashes\nNOTE:\n\n$note\n\n$dashes\n\n";
}

sub runDiff($)
{
	my $file = shift;
	`~public/bin/visidiff $file &`;
warn "Ran diff on $file";
}

sub driveUpdate()
{
	my $extras = join(" ", @ARGV);
	my $msg = `cvs -n -q update $extras`;

	my (@lines) = split(/[\r\n]+/, $msg);

	foreach (@lines) {
		my $tok;
		my $fil;
		
		$tok = substr($_, 0, 1);
		$fil = substr($_, 2, length $_);
		
		if ($tok eq "?") {
			&printNote("$fil is not in the CVS hierarchy.  Remember to add it.");
		} else {
			if ($tok eq "C") {
				&printNote("$fil conflicts with TOT.");
				&runDiff($fil);
			} elsif ($tok eq "M") {
				&runDiff($fil . " " . $extras);
			} elsif ($tok eq "U") {
				&runDiff($fil . " " . $extras);
			}
		}
	}
}

sub usage()
{
	print qq~
VisiChanges: view changes for an entire CVS hierarchy.

cd to the directory you want to view changes for, then run VisiChanges.

This does a non-destructive update (cvs -n -q update) and runs visidiff on the
results.

By Steve Zellers, x4-1612.
~;

	exit(-1);
}

if ($ARGV[0] eq "--help" || $ARGV[0] eq "-help" || $ARGV[0] eq "-h") {
	&usage();
	exit
}

my $r0 = `open /Developer/Applications/FileMerge.app`;
my $r1 = `sleep 3`;

&driveUpdate();

exit 0;
