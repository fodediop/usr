#!/bin/bash
#  diffwrap
#
#  Created by Dave Carlton on 2008-06-04.
#  Copyright (c) 2008 polyMicro Systems. All rights reserved.
#

# Configure your favorite diff program here. 
DIFF="/Users/davec/bin/compare" 
# Subversion provides the paths we need as the sixth and seventh 
# parameters. 
LEFT=${6} 
RIGHT=${7} 
# Call the diff command (change the following line to make sense for 
# your merge program). 
echo $DIFF -wait --left $LEFT --right $RIGHT 
$DIFF -wait $LEFT $RIGHT 
# Return an errorcode of 0 if no differences were detected, 1 if some were. 
# Any other errorcode will be treated as fatal. 
