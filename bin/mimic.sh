#!/bin/bash

# setup stuff
REMOTESFILE=~/.mimic/remotes
CMDLOG=~/Library/Logs/mimic.log
DEBUG=0

if [[ ! -d ~/.mimic ]] ; then
    mkdir ~/.mimic
fi

while getopts "nths" Option
do
    case $Option in
	n )  
	    echo "mimic: Creating new remotes"
	    if [[ -e $REMOTESFILE ]] ; then
		rm $REMOTESFILE
	    fi
	    shift
	    ;;
	t ) echo "mimic: Testing"
	    DEBUG=1
	    shift
	    ;;
	h ) echo "mimic [-h] [-t] [-n]"
	    echo "      -h Help"
	    echo "      -t Test, nothing is execute"
	    echo "      -n New, creates new remotes list"
	    exit 1
	    ;;
	s ) echo "mimic: Remote list:"
	    cat $REMOTESFILE
	    ;;
    esac
done

if [[ ! -e $REMOTESFILE ]] ; then
   echo "mimic: no remotes found, enter remote DNS name or IP seperated by spaces"
   echo "       you need at least 1 host (or what's the point :)"
   read -p "Enter remote names|IP: " -a REMOTE
   count=${#REMOTE[*]}  
   i=0
   while [ "$i" -lt "$count" ]
   do
       echo ${REMOTE[$i]} >> $REMOTESFILE
       let i=$i+1
   done
fi
REMOTES=`cat $REMOTESFILE`

function exec-cmd () {
    cmd="$1"
    shift
    for host in $REMOTES 
    do
	if [[ $DEBUG == 0 ]] ; then
	    set -x
	    $cmd "$@"
	    LOGTEXT="`date \"+%m/%d %H:%M:%S\"` $host: $cmd \"$@\""
	    local IFS=
	    ssh $host $cmd $@ 2> /tmp/mimic.tmp
	    if [[ $? == 0 ]] ; then
		echo "$LOGTEXT" >> $CMDLOG
	    else
		echo -n `date "+%m/%d %H:%M:%S"` "$host:" >> $CMDLOG
		cat /tmp/mimic.tmp >> $CMDLOG
	    fi
	    set +x
	else
	    echo $cmd $@
	    echo ssh $host $cmd $@
	fi
    done
}  

function exec-ls () {
    cmd="ls"
    local IFS=
    for host in $REMOTES 
    do
	echo "mimic: $host"
	set -x
	ssh $host $cmd "$@" 
	set +x
    done
}
  
CMD=`basename "$0"`
case $CMD in
    mmv ) exec-cmd mv "$@" ;;
    mrm ) exec-cmd rm "$@" ;;
    mcp ) exec-cmd cp "$@" ;;
    mchmod ) exec-cmd chmod "$@" ;;
    mchown ) exec-cmd chown "$@" ;;
    mchgrp ) exec-cmd chgrp "$@" ;;
    mrmdir ) exec-cmd rmdir "$@" ;;
    mls ) exec-ls "$@" ;;
esac

