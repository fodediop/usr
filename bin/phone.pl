#!/usr/bin/perl -w

my $fullAccounting = 0;

sub usage()
{
	print qq!
phone [-f] [-u] <name> ...

	Returns the the ldap information for a name.  Use -u if you know
	the exact apple email address.  Use -f if you want a full
	accounting of the person in a nice format.

	By Steve Zellers, x4-1612
	(Based on ~public/bin/ldap by sspies)

!;
}

sub clean($)
{
	my $s = shift;
	return "<<not listed>>" if (! defined $s);
	$s =~ s/[\r\n\t]+//g;
	return $s;
}

sub packTo($$)
{
	my $s = shift;
	my $len = shift;
	return substr($s . " "x$len, 0, $len);
}

sub breakAt($$)
{
	my $s = shift;
	my $leader = shift;
	my $spaces = "\n" . " "x$leader;
	$s =~ s/\$/$spaces/g;
	return $s;
}

sub dumpRecordShort($)
{
	my $rr = shift;
	my $phone = packTo(clean($rr->{'telephonenumber'}), 20);
	my $email = packTo(clean($rr->{'mail'}), 25);
	my $name = clean($rr->{'cn'});
	print "$phone\t$email\t$name\n";
}

sub dumpRecordLong($)
{
	my $rr = shift;
	my @lhs;
	my @rhs;
	my $maxl = 0;
	my $count = 0;

	foreach my $key (sort keys %$rr) {
		$count += 1;

		my $val = clean($rr->{$key});
		push(@lhs, $key);
		push(@rhs, $val);
		
		my $l = length $key;
		if ($l > $maxl) {
			$maxl = $l;
		}
	}

	for (my $i = 0;  $i < $count;  $i++) {
		print packTo(shift @lhs, $maxl);
		print "  : ";
		print breakAt(shift @rhs, $maxl + 4);
		print "\n";
	}
	print "\n";
}

sub dumpRecord($)
{
	my $rr = shift;
	if ($fullAccounting == 0) {
		dumpRecordShort($rr);
	} else {
		dumpRecordLong($rr);
	}
}

sub genericSearch($)
{
	my $ss = shift;
	my $cmd = qq!~public/bin/ldapsearch -h ldap.apple.com -b "ou=People, o=Apple Computer" $ss!;

	open (SRC, "$cmd|") || die "Can't execute $cmd";
	my $rr = undef;
	while (<SRC>) {
		chop;
		next if ($_ eq "");
		my (@row) = split(/=/, $_);
		if ($row[0] eq "appledsId") {
			if (defined $rr) {
				dumpRecord($rr);
			}
			my %storage;
			$rr = \%storage;
		}
		if (defined $rr) {
			$rr->{$row[0]} = $row[1];
		}
	}
	dumpRecord($rr) if defined $rr;
	close (SRC);
}

sub generalSearch($)
{
	my $cn = shift;
	genericSearch(qq~-s sub cn="\*$cn\*"~);
}

sub userSearch($)
{
	my $user = shift;
	$user .= "\@apple.com" if ($user !~ m/\@/);
	genericSearch(qq~mail=$user~);
}

if (@ARGV == 0) {
   usage();
}

while ($_ = shift @ARGV) {
	if ($_ eq "-f") {
		$fullAccounting = 1;
	} elsif ($_ ne "-u") {
		generalSearch($_);
	} else {
		my $user = shift @ARGV;
		usage() if ! defined $user;
		userSearch($user);
	}
}
		
