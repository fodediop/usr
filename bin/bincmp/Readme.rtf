{\rtf0\ansi{\fonttbl\f0\fswiss Helvetica;\f1\fmodern Courier;}
{\colortbl;\red255\green255\blue255;}
\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\ql\qnatural

\f0\fs24 \cf0 BinCmp - binary compare tool\
\
When building a revision of a body of software, it is often useful to be able to determine which pieces of the build output are actually changed. For some types of files this is straightforward - they can be compared on a byte for byte basis and they are either identical or different. But this approach will not work well for all types of files. Some files contents may be altered slightly due to changes in the environment in which they are built, and it may be difficult or impossible to exactly recreate a build output. A typical example would be an executable which incorporates the build date/time stamp. Unfortunately, the files which are most interesting to compare are also the most likely to exhibit this sort of problem, so another aproach is needed.\
\
Instead of simply comparing files byte for byte, a more intelligent utility could take advantage of whatever known structure is present. This tool could interpret the file contents and determine whether whether a literal difference between the files is 'significant' according to some set of rules. For example, if an executable file contains its build time, the tool could determine that everything other than the build time is identical and so the two builds are effectively the same.\
\
Of course there will always be risks associated with such an approach. It is certainly possible to write code which behaves differently depending upon when it was compiled. It is also possible that an arbitrary bit pattern may trick the utility and cause an incorrect set of rules to be applied to a particular file. But these risks can be mitigated by a conservative implementation coupled with human oversight.\
\
The 'bincmp' utility attempts to accomplish these goals. The general strategy for the implementation is to provide a general framework for comparing the contents of files which allows for plugging in different rulesets to account for resolvable differences. The utility attempts to apply these rulesets, or 
\i compare methods
\i0 , to match sections of the two files against each other. Each method examines a pieces of the files according to the rules it implements and reports one of the following: 1) the pieces are equivalent, 2) the pieces are partially equivalent, 3) the pieces are different, 4) the ruleset does not apply, or 5) an error occurred. Compare methods have been implemented for several different file types: library (.a) files, mach-o files, and fat files. Additionally, less structured methods have been implemented, such as a 'binary' method to compare for strict byte for byte identity. These will be described in more detail later.\
\
The tool begins by calling each of the enabled methods, asking them to compare the entire contents of both files. The methods which apply to a particular file type will check for the appropriate magic header and and proceed to compare the files, or abort as not applicable. In general, these more structured methods either match all or nothing with one iteration. More simplistic methods like 'binary' simply plunge through the data, and may match a few or many bytes. If only part of the files match the file offsets are advanced to the first difference and all the enabled compare methods are given another chance using the new offset as a seed. The files are deemed equivalent if some combination of compare methods can account for matching the entire contents of one file to the entire contents of the other. Care has been taken to ensure that every time a literal difference between the files is deemed insignificant a message is logged for review. Additionally, detailed information about which pieces of the files were matched by which methods may be obtained to allow a review of what the program has done.\
\
The compare methods build upon and utilize one another. For example, both a library archive (.a) and a fat executable are essentially several mach-o files glued together with some extra header info. The methods that parse these file types parse out the file offsets for the contained mach-o files, then compare the appropriate ranges for the "sub-files". This allows the mach-o method to recognize and compare the contained mach-o according to the same rules as if the files had been extracted in the filesystem. In turn, mach-o uses the binary method to do the work when comparing the machine code. Thus, implementing a new method for a 'container' file type is fairly straightforward, and the usual benefits of code reuse apply.\
\
The bincmp usage may be obtained by running the program with no arguments. Additional information specific to the particular compare methods is available by running the program with the -info argument.\
\
Below is a sample of the bincmp output for discussion. For this run, two different builds of the tool itself were compared. There were no source changes, but the project version was changed.\
\

\f1 $ bincmp bincmp.old bincmp.new -structureDetail\
Matched byte ranges:\
+ (00036c - 0003a4)/(00036c - 0003a4): mach-o: structure of type struct segment_command\
+ (000390 - 000394)/(000390 - 000394):    mach-o: struct segment_command->filesize (variable)\
+ (000418 - 000430)/(000418 - 000430): mach-o: structure of type struct symtab_command\
+ (00042c - 000430)/(00042c - 000430):    mach-o: struct symtab_command->strsize (variable)\
+ (00fee4 - 00fffe)/(00fee4 - 00fffe): mach-o: segment "__TEXT", section "__const" contents\
+ (00ff54 - 00ffb3)/(00ff54 - 00ffaf):    vers_string: matched version strings "PROGRAM:bincmp PROJECT:bincmp-7.dev DEVELOPER:behnke BUILT:Fri Feb 25 11:29:31 PST 2000" <--> "PROGRAM:bincmp PROJECT:bincmp-7 DEVELOPER:behnke BUILT:Fri Feb 25 11:32:34 PST 2000"\
+ (00ffb3 - 00fff4)/(00ffaf - 00fff4):       padding: zerofilled data\
+ (00fff4 - 00fff9)/(00fff4 - 00fff5):    equivalent_strings: "7.dev" <--> "7"\
+ (00fff9 - 00fffe)/(00fff5 - 00fffe):    padding: zerofilled data\
+ (010000 - 013594)/(010000 - 013594): mach-o: segment "__DATA", section "__data" contents\
+ (012a14 - 012a41)/(012a14 - 012a41):    timestamp: "__TIME__ was 11:29:17, __DATE__ was 02/25/00." <--> "__TIME__ was 11:32:21, __DATE__ was 02/25/00."\
+ (014000 - 0325e8)/(014000 - 0325e8): mach-o: symbol table\
+ (031e74 - 031e80)/(031e74 - 031e80):    mach-o: structure of type struct nlist\
+ (031e74 - 031e78)/(031e74 - 031e78):       mach-o: struct nlist->n_un.n_strx (variable)\
+ (031e80 - 031e8c)/(031e80 - 031e8c):    mach-o: structure of type struct nlist\
+ (031e80 - 031e84)/(031e80 - 031e84):       mach-o: struct nlist->n_un.n_strx (variable)\
...\
+ (031f70 - 031f7c)/(031f70 - 031f7c):    mach-o: structure of type struct nlist\
+ (031f70 - 031f74)/(031f70 - 031f74):       mach-o: struct nlist->n_un.n_strx (variable)\
+ (032710 - 05637c)/(032710 - 056378): mach-o: string table\
+ (056074 - 0560e5)/(056074 - 0560e1):    pathsuffix: matched paths "/private/Network/Servers/paragon/space/setdev/projects/behnke/bincmp/bincmp-7.dev/bincmp.build/derived_src/vers.c" <--> "/private/Network/Servers/paragon/space/setdev/projects/behnke/bincmp/bincmp-7/bincmp.build/derived_src/vers.c"\
Other marked byte ranges in bincmp.old:\
Other marked byte ranges in bincmp.new:\
EQUIVALENT\

\f0 \
The output is a listing showing how the byte ranges were matched from bincmp.old to bincmp.new. Each line in the output contains the following information: \
	mark  (start1 - end1)/(start2 - end2): description\
The mark is a single character showing whether the byte range is IDENTICAL (whitespace), EQUIVALENT (a plus), or DIFFERENT (an asterisk). Since these files were EQUIVALENT, they contain no DIFFERENT ranges, by definition. And by default matched byte ranges which are IDENTICAL are not logged, but they can be retrieved by specifying -showAllRanges on the command line.\
The start/end pairs show the byte offsets of the start end end of the byte range. The file which was specified first on the command line appears first in the output.\
Finally, the description contains information about what method generated the notation, as well as a description of what the byte range contains. For example, the line:\

\f1 + (012a14 - 012a41)/(012a14 - 012a41):    timestamp: "__TIME__ was 11:29:17, __DATE__ was 02/25/00." <--> "__TIME__ was 11:32:21, __DATE__ was 02/25/00."
\f0 \
was generated by the method named "timestamp", and shows that the timestamp method resolved the text of the strings that were literally different, as well as where they occur in the files. Notice also that this description line is indented relative to the preceeding description. This reflects that the range to which it referrs is entirely contained within the range described earlier, and is intended to invoke a sense for the structure of the file. Typically the mark for a range which contains several other ranges will be the same as the least matching range it contains, so differences will propagate to the broadest marked range in which they occur.\
\
There is a generic implementation for comparing various data structures which typically occur in files. The output will commonly contain descriptions like "structure of type xxx" to refer to the whole structure, or "struct xxx->yyy" to refer to the field named yyy in the instance of structure xxx. Typically, any change in the data contained in a structure will render the files DIFFERENT. However some fields are allowed to change value. These will be denoted by "(variable)", as in the output above. Also, some structures contain fields whose contents are unspecified. These will be denoted by "(ignored)". In either of these cases if the data differs the field (and therefore the entire structure) will be marked EQUIVALENT rather than DIFFERENT. (For brevity individual fields are not reported by default. To view reports for individual fields use -structureDetail, as in the above run.)\
\
Finally, it is possible that there are some ranges of bytes between the files where the correspondence cannot be determined. (This is typically zero padding.) Therefore, the output actually includes three separate byte range dumps under the headings "Matched byte ranges", and "Other marked byte ranges in file ...". The output above only includes the common case where all the byte ranges from one file have corresponding byte ranges in the other file and the correspondence can be determined. If there were any "extra" byte ranges they will be shown inder the "Other marked byte ranges" heading.\
\
Finally, the last line of the output is one of IDENTICAL, EQUIVALENT, or DIFFERENT.}
