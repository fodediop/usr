#!/usr/bin/perl
#
#  itunes.pl
#  Provide an iTunes v4.0 host address as the first argument, and this script
#  will dump their playlist into a file named "playlist-dump.txt"
#  For additional debugging information, supply an additional argument.
#
#  Created by Steve White on Fri May 09 2003.
#  Copyright (c) 2003 Steve White. All rights reserved.
#
use IO::Socket;

$debug = 1 if (defined($ARGV[1]));

$SIG{INT} = sub { 
	if (defined($sessionId)) {
		print "[" . scalar localtime() . "] ^C! Logging out from $hostname\n";
		getServerResponse("/logout?session-id=$sessionId");
	}
	exit;
};

$SIG{ALRM} = sub {
	print "[" . scalar localtime() . "] Timed out while trying to connect\n";
	exit;
};

%handlers = (adbc=>1, adbs=>1, avdb=>1, mlcl=>1, mlit=>1, mlog=>1, msrv=>1, mupd=>1, apro=>2, mper=>2, mpro=>2, mslr=>2, abst=>3, aeNV=>3, asbr=>3, asco=>3, asda=>3, asdb=>3, asdc=>3, asdk=>3, asdm=>3, asdn=>3, asrv=>3, assp=>3, assr=>3, assz=>3, astc=>3, astm=>3, astn=>3, asur=>3, asyr=>3, mctc=>3, miid=>3, mikd=>3, mimc=>3, mlid=>3, mrco=>3, msal=>3, msau=>3, msbr=>3, msdc=>3, msex=>3, msix=>3, mspi=>3, msqy=>3, msrs=>3, mstm=>3, mstt=>3, msup=>3, mtco=>3, musr=>3, muty=>3, asal=>4, asar=>4, ascm=>4, ascp=>4, asdt=>4, aseq=>4, asfm=>4, asgn=>4, asul=>4,  minm=>4);

%tagtypes = (abpl=>"daap.baseplaylist", aeSP=>"com.apple.itunes.smart-playlist", asco=>"daap.songcompilation", asdb=>"daap.songdisabled", asdk=>"daap.songdatakind", asrv=>"daap.songrelativevolume", asur=>"daap.songuserrating", mikd=>"dmap.itemkind", msal=>"dmap.supportsautologout", msau=>"dmap.authenticationmethod", msbr=>"dmap.supportsbrowse", msex=>"dmap.supportsextensions", msix=>"dmap.supportsindex", mslr=>"dmap.loginrequired", mspi=>"dmap.supportspersistentids", msqy=>"dmap.supportsquery", msrs=>"dmap.supportsresolve", msup=>"dmap.supportsupdate", muty=>"dmap.updatetype", asbr=>"daap.songbitrate", asbt=>"daap.songbeatsperminute", asdc=>"daap.songdisccount", asdn=>"daap.songdiscnumber", astc=>"daap.songtrackcount", astn=>"daap.songtracknumber", asyr=>"daap.songyear", mcty=>"dmap.contentcodestype",aeNV=>"com.apple.itunes.norm-volume", assp=>"daap.songstoptime", assr=>"daap.songsamplerate", asst=>"daap.songstarttime", assz=>"daap.songsize", astm=>"daap.songtime", mctc=>"dmap.containercount", mcti=>"dmap.containeritemid", miid=>"dmap.itemid", mimc=>"dmap.itemcount", mlid=>"dmap.sessionid", mpco=>"dmap.parentcontainerid", mrco=>"dmap.returnedcount", msdc=>"dmap.databasescount", mstm=>"dmap.timeoutinterval", mstt=>"dmap.status", mtco=>"dmap.specifiedtotalcount", musr=>"dmap.serverrevision", mper=>"dmap.persistentid", asal=>"daap.songalbum", asar=>"daap.songartist", ascm=>"daap.songcomment", ascp=>"daap.songcomposer", asdt=>"daap.songdescription", aseq=>"daap.songeqpreset", asfm=>"daap.songformat", asgn=>"daap.songgenre", asul=>"daap.songdataurl", mcna=>"dmap.contentcodesname", minm=>"dmap.itemname", msts=>"dmap.statusstring", asda=>"daap.songdateadded", asdm=>"daap.songdatemodified", apro=>"daap.protocolversion", mpro=>"dmap.protocolversion", abal=>"daap.browsealbumlisting", abar=>"daap.browseartistlisting", abcp=>"daap.browsecomposerlisting", abgn=>"daap.browsegenrelisting", abro=>"daap.databasebrowse", adbs=>"daap.databasesongs", aply=>"daap.databaseplaylists", apso=>"daap.playlistsongs", arif=>"daap.resolveinfo", arsv=>"daap.resolve", avdb=>"daap.serverdatabases", mbcl=>"dmap.bag", mccr=>"dmap.contentcodesresponse", mcon=>"dmap.container", mdcl=>"dmap.dictionary", mlcl=>"dmap.listing", mlit=>"dmap.listingitem",mlog=>"dmap.loginresponse", msrv=>"dmap.serverinforesponse", mudl=>"dmap.deletedidlisting", mupd=>"dmap.updateresponse");

$playlistdumpfile="playlist-dump.txt";
open(PLAYLISTDUMPFILE, ">$playlistdumpfile") || die "Can't open $playlistdumpfile for writing: $!\n";

$hostname=$ARGV[0];

%data = ();
print "[" . scalar localtime() . "] Contacting $hostname\n";
alarm(30);
$buffer = getServerResponse("/server-info");
alarm(0);
while (length($buffer) > 0) {
	$tag = substr($buffer, 0, 4);
	$buffer = substr($buffer, 4);

	if ($handlers{$tag}) {
		$data{$tag} = parse($handlers{$tag});
		print "$tagtypes{$tag} = $data{$tag}\n" if ($debug);
	} else {
		 print "[31;1mERROR[0m: Unknown tag: '$tag'\n";
	}
}

if ($data{msau}) { #auth method
	print "[" . scalar localtime() . "] $hostname requires authentication. Quitting.\n";
	exit 1;
}

%data = ();
print "[" . scalar localtime() . "] Logging into $hostname\n";
$buffer = getServerResponse("/login");
while (length($buffer) > 0) {
	$tag = substr($buffer, 0, 4);
	$buffer = substr($buffer, 4);

	if ($handlers{$tag}) {
		$data{$tag} = parse($handlers{$tag});
		print "$tagtypes{$tag} = $data{$tag}\n" if ($debug);
	} else {
		 print "[31;1mERROR[0m: Unknown tag: '$tag'\n";
	}
}

$sessionId = $data{mlid};
exit if (!defined($sessionId));

%data = ();
$buffer = getServerResponse("/update?session-id=$sessionId&revision-number=1");
while (length($buffer) > 0) {
	$tag = substr($buffer, 0, 4);
	$buffer = substr($buffer, 4);
	if ($handlers{$tag}) {
		$data{$tag} = parse($handlers{$tag});
		print "$tagtypes{$tag} = $data{$tag}\n" if ($debug);
	} else {
		 print "[31;1mERROR[0m: Unknown tag: '$tag'\n";
	}
}
$revisionNumber = $data{musr};

%data = ();
$buffer = getServerResponse("/databases?session-id=$sessionId&revision-number=$revisionNumber");
while (length($buffer) > 0) {
	$tag = substr($buffer, 0, 4);
	$buffer = substr($buffer, 4);
	if ($handlers{$tag}) {
		$data{$tag} = parse($handlers{$tag});
		print "$tagtypes{$tag} = $data{$tag}\n" if ($debug);
	} else {
		 print "[31;1mERROR[0m: Unknown tag: '$tag'\n";
	}
}

$databaseNumber = $data{miid};

print "[" . scalar localtime() . "] Getting ".$hostname."'s database\n";
$bigbuffer = getServerResponse("/databases/$databaseNumber/items?type=music&meta=dmap.itemid,dmap.itemname,dmap.itemkind,dmap.persistentid,daap.songalbum,daap.songartist,daap.songbitrate,daap.songcompilation,daap.songcomposer,daap.songdisccount,daap.songdiscnumber,daap.songdisabled,daap.songformat,daap.songgenre,daap.songdescription,daap.songsamplerate,daap.songsize,daap.songtime,daap.songtrackcount,daap.songtracknumber,daap.songuserrating,daap.songyear,daap.songdatakind,daap.songdataurl&session-id=$sessionId&revision-number=$revisionNumber");
print "[" . scalar localtime() . "] Processing ".$hostname."'s database\n";

getServerResponse("/logout?session-id=$sessionId");

$curItem = 0;
%data = ();
while (length($bigbuffer)>0) {
	checkBufferSize(8192);
	while (length($buffer)>=4) {
		$tag = substr($buffer, 0, 4);
		$buffer = substr($buffer, 4);
		if ($tag eq "mlit") {
			$tagvalue = parse(1);
			$curItem++;
			if ($curItem > 0 && !$debug) {
				print PLAYLISTDUMPFILE "$hostname\t$data{miid}\t$data{minm}\t$data{mikd}\t$data{mper}\t$data{asal}\t$data{asar}\t$data{asbr}\t$data{asbt}\t$data{ascm}\t$data{asco}\t$data{ascp}\t$data{asda}\t$data{asdm}\t$data{asdc}\t$data{asdn}\t$data{asdb}\t$data{aseq}\t$data{asfm}\t$data{asgn}\t$data{asdt}\t$data{asrv}\t$data{assr}\t$data{assz}\t$data{asst}\t$data{assp}\t$data{astm}\t$data{astc}\t$data{astn}\t$data{asur}\t$data{asyr}\t$data{asdk}\t$data{asul}\t$data{aeNV}\n";
				%data = ();
			}
		} elsif ($handlers{$tag}) {
			$data{$tag} = parse($handlers{$tag});
			print "$tagtypes{$tag} = $data{$tag}\n" if ($debug);
		} else {
			print "[31;1mERROR[0m: Unknown tag: '$tag'\n";
		}
	}
}
close(PLAYLISTDUMPFILE);
print "[" . scalar localtime() . "] Processed $curItem items from $hostname\n";

sub getServerResponse {
	my ($url) = @_;

	$mybuffer="";
	my ($sock) = new IO::Socket::INET(PeerAddr => $hostname,
					  PeerPort => 3689,
					  Proto    => 'tcp') or die "Socket failed: $!\n";

	print $sock "GET $url HTTP/1.0\r\n\r\n";
	print "[" . scalar localtime() . "] $url\n" if ($debug);
	$headerDone = 0;
	while (<$sock>) {
		$mybuffer .= $_ if ($headerDone);
		$headerDone = 1 if ($_ =~ /^\r\n$/ && ! $headerDone);
	}
	close($sock);
	return($mybuffer);
}

sub parse {
	my ($type) = @_;

	checkBufferSize(4);
	($value) = unpack("H8", $buffer);
	$buffer = substr($buffer, 4);
	return ($value) if ($type == 1);

	$length = oct("0x$value");
	checkBufferSize($length);
	if ($type == 2 || $type == 3) {
		($value) = unpack("H".($length*2), $buffer);
		$buffer = substr($buffer, $length);
		return ($value) if ($type == 2);

		$value = oct("0x$value");
		return ($value);
	}

	$string = substr($buffer, 0, $length);
	$buffer = substr($buffer, $length);

	return ($string);
}

sub checkBufferSize {
	my ($requestedLength) = @_;

	if (length($buffer) < $requestedLength) {
		$buffer .= substr($bigbuffer, 0, 8192);
		$bigbuffer = substr($bigbuffer, 8192);
	}
}
