:;exec emacs -batch -l $0 "$*"

(defun between (num min max) (and (> num min) (< num max)))
(defun within (num min max)  
  (unless (eq num nil)
    (and (>= num min) (<= num max))))
(defun indexof (value list)
  (catch 'loop
    (let ((index nil)(i 0))
      (dolist (aValue list)
	(if (equal aValue value)
	    (throw 'loop i))
	(setq i (1+ i))))
))

(defun current-word-bounds (&optional strict really-word)
  "Return the symbol or word that point is on (or a nearby one) as a string.
The return value includes no text properties.
If optional arg STRICT is non-nil, return nil unless point is within
or adjacent to a symbol or word.  In all cases the value can be nil
if there is no word nearby.
The function, belying its name, normally finds a symbol.
If optional arg REALLY-WORD is non-nil, it finds just a word."
  (save-excursion
    (let* (
	   (oldpoint (point))
	   (start (point))
	   (end (point))
	   (syntaxes (if really-word "w" "w_"))
	   (not-syntaxes (concat "^" syntaxes)))
      (skip-syntax-backward syntaxes) (setq start (point))
      (goto-char oldpoint)
      (skip-syntax-forward syntaxes) (setq end (point))
      (when (and (eq start oldpoint) (eq end oldpoint)
		 ;; Point is neither within nor adjacent to a word.
		 (not strict))
	;; Look for preceding word in same line.
	(skip-syntax-backward not-syntaxes
			      (save-excursion (beginning-of-line)
					      (point)))
	(if (bolp)
	    ;; No preceding word in same line.
	    ;; Look for following word in same line.
	    (progn
	      (skip-syntax-forward not-syntaxes
				   (save-excursion (end-of-line)
						   (point)))
	      (setq start (point))
	      (skip-syntax-forward syntaxes)
	      (setq end (point)))
	  (setq end (point))
	  (skip-syntax-backward syntaxes)
	  (setq start (point))))
      ;; If we found something nonempty, return it as a string.
      (unless (= start end)
	(list start end)))))

(defun msg (string) (princ (concat string "\n")))

(defun parse-crashlog (fileName &optional starting-line)
  "Parse a crash log report"
  (interactive "fFile: ")
  (setq crashlogfile (find-file-noselect fileName))
  (if crashlogfile 
      (save-current-buffer
	(set-buffer crashlogfile)
	(goto-line 1)
	(unless (eq starting-line nil)
	  (goto-line starting-line))
	  
	(let ( (start nil) (regValue nil)
	       opoint beg end value regName regNames codeType codeTypes
	       (typeX86-64 "X86-64 (Native)")
	       (typeX86 "X86 (Native)")
	       (typePPCT  "PPC (Translated)")
	       )
	  (re-search-forward "^Code Type:\s+")
	  (setq opoint (point))
	  (end-of-line)
	  (setq codeType (buffer-substring-no-properties opoint (point)))
	  (setq codeTypes (list typeX86-64 typeX86 typePPCT))
	  (if (not (eq nil (member codeType codeTypes)))
	      (progn
		(cond (
		       (equal codeType typeX86-64)
		       (setq regNames (list "rax" "rbx" "rcx" "rdx" "rdi" "rsi" "rbp" "rsp" 
					     "r8" "r9" "r10" "r11" "r12" "r13" "r14" "r15"
					     "rip" "rfl")))
		       (
			(or (equal codeType typePPCT) (equal codeType typeX86))
			(setq regNames (list "eax" "ebx" "ecx" "edx" "edi" "esi" "ebp" "esp")))
		       (t (msg "Unkown type"))
		       )
		(re-search-forward "^Thread [0-9]+ crashed with")
		(setq regValues (list))
		(setq regHexValues (list))
		(dolist (aRegName regNames) 
		  (progn
		    (setq regName (concat aRegName ": "))
		    (search-forward regName)
		    (setq bounds (current-word-bounds))
		    (setq regValue (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
		    (setq regHexValues (cons regValue regHexValues))
		    (setq regValue (string-to-number regValue 16))
		    (setq regValues (cons regValue regValues))))
		(setq regValues (reverse regValues))
		(setq regHexValues (reverse regHexValues))
		(search-forward "Binary Images:")
		(re-search-forward "^")
		(setq opoint (point))
		(forward-page)
		(beginning-of-line)
		(or (looking-at page-delimiter)
		    (end-of-line))
		(setq end (point))
		(goto-char opoint)
		(setq beg (point))
		(setq linecount (count-lines beg end))
		;; (setq reglist (list regEAX regEBX regECX regEDX regEDI regESI regEBP regESP))
		(while (not (eq linecount 0))
		  (if (looking-at "^\s*0x")
		      (progn
			(forward-word)
			(setq bounds (current-word-bounds))
			(setq loRange (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
			(setq loRange (string-to-number loRange 16))
			(forward-word)
			(setq bounds (current-word-bounds))
			(setq hiRange (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
			(setq hiRange (string-to-number hiRange 16))
			(re-search-forward " /")
			(goto-char (1- (point)))
			(setq foundHome (looking-at ".*davec"))
			(setq foundReg nil)
			(dolist (regValue regValues)
			  (if (within regValue loRange hiRange)
			      (progn
				(setq foundReg t)
				(setq foundRegName (nth (indexof regValue regValues) regNames))
				(setq foundRegHexValue (nth (indexof regValue regValues) regHexValues))
			    )
			    ))
			(if (and (eq foundReg nil) (eq foundHome nil))
			    (progn
			      (beginning-of-line)
		  	      (insert "#REGNONE ")
			      )
			  (if (eq foundReg nil) 
			      (progn
				(beginning-of-line)
		  	      (insert "#REGNONE ")
				)
			    (beginning-of-line)
			    (insert (concat "#REG: " foundRegName))
			    )
			  )
			)
		    (forward-line)
		    (setq linecount (1- linecount))
		    )
		  )
		(princ (buffer-string))
		)
	    (msg "File is not recognized as a known architecture")
	    )))))  

(if (not (= 1 (length command-line-args-left)))
    (msg "No file name given!")
      (parse-crashlog (car command-line-args-left)))
