#!/usr/bin/perl

@files = `ls -l "$ARGV[0]"`;

foreach $file ( @files ) {
	process($file, 'fh00');
}

sub process {
	my($filename, $input) = @_;
	$input++;
	unless (open($input, $filename)) {
	print STDERR "Can't open $filename: $!\n";
	return;
	}	
	local $_;
	while (<$input>) {
		print "$_\n";
	}
}
