#!/usr/bin/perl

@unique_tags=("Name");
$/=undef;
$s=<>;
while($s=~m:<key>(\d*)</key>(.*?)<dict>(.*?)</dict>:sg){
	($db_id,
	$dict)=($1,
	$3);while($dict=~m:<key>(.*?)</key>(.*?)<(.*?)>(.*?)</\3>:sg){
		$h{
			$db_id
		}->{
			$1
		}=$4;
	}
};

@db_ids=keys %h;foreach $db_id (@db_ids){
	%f=%{
		$h{
			$db_id
		}
	};$uid=join"<>",
	@f{
		@unique_tags
	};push@{
		$uid_hash{
			$uid
		}
	},
	$db_id;
}while(($uid,$key_list_ref)=each %uid_hash){
	@key_list=@{
		$key_list_ref
	};
	next unless@key_list>1;
	print"( @key_list ) $uid\n";
} 