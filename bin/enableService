#!/bin/sh
#
#  Author:         Steve Ko (sko@apple.com, x2-7073)
#  Last Modified:  24-March-2000
#
#  Enables an inetd service (such as ftp or telnet).

if whoami | grep "^root" > /dev/null
then
    :
else
    echo "You must have root privileges to run enableService"
    exit 1
fi

if [ $# != 1 ]
then
    echo "Usage:"
    echo "   enableService <service>"
    echo
    echo "Examples:"
    echo "   enableService ftp"
    echo "   enableService telnet"
    exit 2
fi

INETD_CONF_FILE="/etc/inetd.conf"
INETD_PROCESS="inetd"

# Check to see if it is already enabled
if grep "^$1" ${INETD_CONF_FILE} > /dev/null
then
    echo "$1 already enabled."
    exit 0
fi

# Check to see if it is a valid service
if grep "^#$1" ${INETD_CONF_FILE} > /dev/null
then
    :
else
    echo "$1 is not a valid service."
    exit 3
fi

# If we get to here, we know that $1 is a valid, disabled service

# Backup inetd configuration file
mv -f ${INETD_CONF_FILE} ${INETD_CONF_FILE}~

# Modify inetd configuration file
/usr/bin/sed -e "s/^#$1/$1/" ${INETD_CONF_FILE}~ > ${INETD_CONF_FILE}

# Send SIGHUP signal to inetd process so that it re-reads
# the configuration file
kill -s HUP `cat /var/run/inetd.pid`

echo "$1 enabled"
exit 0
