#!/bin/bash 
logger "Checking PN IRC tunnel"
ps -ef|grep "[1]6667:127.0.0.1:6667"

if [[ $? != 0 ]] ; then 
		ssh -fCN -L 16667:127.0.0.1:6667 davec.playnet.com
		logger "Restarted PN IRC Tunnel"
fi
