#!/usr/bin/perl

LINE: while ( <> ) {
	next if (! /Error:/ );
	if (/Permission denied/) {
		print $_;
		$_ = <>;
		$_ = <>;
		s/^.*\]\s+(.*)/$1/;
		s/ /\\ /g;
		print $_;
		next;
	}
	if (/Error removing/ ) {
		print $_;
		$_ = <>;
		$_ = <>;
		s/^.*\]\s+(.*)/$1/;
		s/ /\\ /g;
		print $_;
		next;
	}
	if ( /resolve conflict/ ) {
		print $_;
		$_ = <>;
		s/^.*\]\s+(.*)/$1/;
		print $_;
		next;
	}
	if (/.*File not found.*/ ) {
		print $_;
		$_ = <>;
		$_ = <>;
		s/^.*\]\s+(.*)/$1/;
		print $_;
		next;
	}
	if ( /already exists/ ) {
		print $_;
		$_ = <>;
		$_ = <>;
		s/^.*\]\s+(.*)/$1/;
		print $_;
		next;
	}

	print $_;
}