#!/bin/sh

echo "This script enables many optional services on this machine."
echo "The services enabled are: Core dumps, SSH secure login service,"
echo "root login, and the CrashCatcher utility."
echo ""
echo "It is intended that this utility be run once after a fresh install."
echo "If the services mentioned have already been started, this script"
echo "may cause disruption of service until the machine is rebooted."
echo ""
echo -n "Do you want to proceed? [YES] "
read ANS
case "${ANS:=YES}" in
	[Yy][Ee][Ss])
		;;
	[Yy])
		;;

	*)
		echo "As you wish."
		echo "The end."
		exit 1
		;;
esac

PRIV=~appkit/bin/really
TODAY=`/bin/date`
ME=`whoami`
echo ""
echo "Please set root password when prompted for below."
echo ""
$PRIV passwd root

echo Enabling SSH and coredumps.
cat /etc/hostconfig >/tmp/hostconfig.$ME
cat << EOF >> /tmp/hostconfig.$ME

# test machine config items set $TODAY by $ME
#
SSHSERVER=-YES-
COREDUMPS=-YES-
EOF

$PRIV mv -f /etc/hostconfig /etc/hostconfig.backup
$PRIV mv /tmp/hostconfig.$ME /etc/hostconfig

/bin/sync
$PRIV /bin/sh /etc/rc.common
/bin/sleep 1
$PRIV /System/Library/StartupItems/SSH/SSH

echo "Enabling telnet."
/usr/bin/sed -e "s/^#telnet/telnet/" /etc/inetd.conf > /tmp/inetd.conf.tmp
$PRIV mv /tmp/inetd.conf.tmp /etc/inetd.conf
$PRIV kill -s HUP `cat /var/run/inetd.pid`

echo "Enabling ftp."
/usr/bin/sed -e "s/^#ftp/ftp/" /etc/inetd.conf > /tmp/inetd.conf.tmp
$PRIV mv /tmp/inetd.conf.tmp /etc/inetd.conf
$PRIV kill -s HUP `cat /var/run/inetd.pid`

echo "Disabling auto login."
$PRIV niutil -destroyprop -u root . \/localconfig\/autologin username

echo Machine now configured for testing.
