#!/bin/sh

# Script came to from NeXT where ~cvs/bin was the master copy.  Dave Payne
# and Corey Steele now (Dec 2000) maintain copies of the scripts on
# cvs.apple.com and src.apple.com.

# Changes
#  4 Dec 2000 allow for no '-f' option (sspies)

#ident $Id$

HOME=/tmp; export HOME

cat=cat
mail=mail
rm=rm

program="`basename $0`"

usage="Usage: $program [OPTIONS] \"directory files...\"
Appends CVS log messages to log files.
Options:
-m, --mail-to USER     Send mail to USER containing the log report.
-s, --no-status        Don't include the \`cvs status -v' messages.
-f, --logfile LOGFILE  Write the log messages to LOGFILE.
--help                 Print this usage message.
CVS provides the list of modified directories and files as a single
quoted argument, so this program accepts only a single argument of
space-separated file names.
"

# Here is what the output looks like:
#
#    From: woods@kuma.domain.top
#    Subject: CVS update: testmodule
#
#    Date: Wednesday November 23, 1994 @ 14:15
#    Author: woods
#
#    Update of /local/src-CVS/testmodule
#    In directory kuma:/home/kuma/woods/work.d/testmodule
#    
#    Modified Files:
#    	test3 
#    Added Files:
#    	test6 
#    Removed Files:
#    	test4 
#    Log Message:
#    - wow, what a test
#
# (and for each file the "cvs status -v" output is appended unless -s is used)
#
#    ==================================================================
#    File: test3           	Status: Up-to-date
#    
#       Working revision:	1.41	Wed Nov 23 14:15:59 1994
#       Repository revision:	1.41	/local/src-CVS/cvs/testmodule/test3,v
#       Sticky Options:	-ko
#    
#       Existing Tags:
#    	local-v2                 	(revision: 1.7)
#    	local-v1                 	(revision: 1.1.1.2)
#    	CVS-1_4A2                	(revision: 1.1.1.2)
#    	local-v0                 	(revision: 1.2)
#    	CVS-1_4A1                	(revision: 1.1.1.1)
#    	CVS                      	(branch: 1.1.1)

logfile=
mail_recipients=
print_status=yes
files=

while [ $# -gt 0 ]; do
  case "$1" in
    -m | --mail | --mail-to)
      if [ $# -le 1 ]; then
        echo "$program: Missing argument for \`$1'."
        echo "$usage"
        exit 1
      fi
      shift
      mail_recipients="$mail_recipients $1"
      ;;
    -s | --no-status)
      print_status=no
      ;;
    -f | --log | --logfile | --log-file)
      if [ $# -le 1 ]; then
        echo "$program: Missing argument for \`$1'."
        echo "$usage"
        exit 1
      fi
      shift
      logfile="$1"
      ;;
    -h | --help)
      echo "Checks files for unresolved merges."
      echo "$usage"
      exit 1
      ;;
    -*)
      echo "$program: Unknown option: $option"
      echo "$usage"
      exit 1
      ;;
    *)
      if [ -z "$files" ]; then
        files="$1"
      else
        echo "$program: unrecognized parameter: $1"
        echo "$usage"
        exit 1
      fi
      ;;
  esac
  shift
done

if [ -z "$files" ]; then
  echo "$program: Too few parameters."
  echo "$usage"
  exit 1
fi

set $files

if [ $# -lt 2 ]; then
  echo "$program: Invalid parameter: \`$files'"
  echo "$usage"
  exit 1
fi
directory="$1"
shift

# get a login name for the guy doing the commit....
#

login="`whoami`"
if [ -z "$login" ]; then
  login=nobody
fi

date="`date`"

tmpfile="/tmp/cvs-log-$$"

# Collect the log message that comes in on stdin.
#
( 
  echo "Date:   $date"
  echo "Author: $login"
  echo ""
  $cat -
  if [ "$print_status" = "yes" ]; then
    for file
    do
      cvs -nQq status -v $file
    done
  fi
) > $tmpfile

# Send mail, if there's anyone to send to!
#

if [ -n "$mail_recipients" ]; then
  $mail -s "CVS update: $directory" $mail_recipients < $tmpfile ||
    (echo "Failed to mail log messages"; exit 1)
fi

# Append the info to the logfile.
#
if [ -n "$logfile" ]; then
	echo "***********************************************************" >> $logfile
	$cat $tmpfile >> $logfile
fi

$rm -f $tmpfile

exit 0
