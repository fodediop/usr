#!/usr/bin/perl 

$version="2.8.3";
$compatible="2.8";

#TODO 
#Add support for local symbol directories (for development kexts, etc) <1.0+>
#Fix niggling dependency issues
#clean up code base (-w and Strict are no good currently, development has just been so fast)
#Redo commandline parsing
#Make script more robust against NFS failures
#Remove workarounds for glitches in gdbserver (must be done in lockstep with gdbserver fixes)
#add random port selection and collision avoidance (gdbserver is where the code for this will actually go)
#abstract all remaining input and output


#revision history

#0.0   initial conversion of jchang's script to perl
#0.0.2 alterations to run gdb interactively
#0.0.3 fixes to work around nfs issues

#0.5   first working revision (feature eqquivalent to panic-debug)
#0.5.1 initial revision of kmod to CFBundle mapping code 
#0.5.2 code cleanup(we have subroutines now ;-) 
#0.5.3 removed lots of vestigial code
#0.5.4 modifired to run in batch and to do post processing cleanup
#0.5.5 fixed edge case where project and kext names do not match
#0.5.6 added code to cache mapping files
#0.5.7 initial revision of dependency mapping code
#0.5.8 dependency analysis code works
#0.5.9 initial workaround for caches breaking on NFS
#0.5.10 more NFS workarounds
#0.5.11 screw NFS workarounds, I have a better idea
#0.5.12 added togdb and fromgdb wrappers. They have really nice debugging capabilities
#0.5.13 fixed some potentially troublesame regexps

#0.8   Demoable
#0.8.1 Removed network dirs after botched demo
#0.8.2 Gutted dependency code
#0.8.3 General clean up
#0.8.4 Added command line arg parser
#0.8.5 Added concept of local run
#0.8.6 Added standalone mode
#0.8.7 Fixed globbing problems

#0.9   New unified cache code!!
#0.9.1 Improvements to unified caching
#0.9.2 Improvements to verbose mode
#0.9.3 Moved back to gdb interactive backend
#0.9.4 Added automail support (untested, the server is not ready yet)
#0.9.5 Fixed some bugs with local code
#0.9.6 Added interactive mode
#0.9.7 Fixed a nasty regression in	the dependency code
#0.9.8 Fixed edge case which involved superclassing something in mach_kernel
#0.9.9 New dependency reversal code
#0.9.10 Multiple levels of extra_output now available
#0.9.11 Fixed (hopefully) autoflushing for notification issue
#0.9.12 Disabling some incomplete options in prep for release

#1.0.0 Well my machine just panicked and this picked up the pieces, I guess it is GM time
#1.0.1 Enabling automailing
#1.0.2 Improved commandline parsing (flag coalescing)
#1.0.3 Cleaned up output file header
#1.0.4 Some small cleanups, much better commenting
#1.0.5 Improvements in verbose mode interaction
#1.0.6 Added scope protection to a lot of functions (global variables...)
#1.0.7 Added test to make sure we have gdb
#1.0.8 Workaround for niutil flakiness
#1.0.9 Merged together the local and network cache generation code
#1.0.10 More generic netcache building code (so more people than jus I will be able to build them)

#1.1 Release
#1.1.1 Added support for multiple report addresses
#1.1.2 Enabled manual mac assignment
#1.1.3 Added a debug_build setting that autdetects if it is not a release, and enables a bunch of debug options
#1.1.4 added sanity checking for symbols
#1.1.5 disabled debug options (don't need them, and they are not reliable)

#1.2 Release (quashed)
#1.2.1 Fixed subkext parsing stuff
#1.2.2 Changed mailing options

#1.3 Release
#1.3.1 Turned off verbose code

#1.9 Not for release, prep for 2.0
#1.9.1 Fixed .sym race condition
#1.9.2 abstracted the connecting and disconnecting from gdb
#1.9.3 modified togdb and fromgdb to work with gdbserver
#1.9.4 Added no query path to get info for panic debug
#1.9.5 fixed some stall problems with gdbserver
#1.9.6 Added automatic NetInfo/MAC determination
#1.9.7 Altered a lot of the prompt text
#1.9.8 Added could do auto strip .apple.com from NetInfo names info it is given
#1.9.9 updated usage
#1.9.10 slowed down niutil reget attempts
#1.9.11 get extra kernel info from user
#1.9.12 improved server handoff code
#1.9.13 add a little user level sanity checking
#1.9.14 altered CoreOS emails

#2.0 Release (Spikard) (may I be forgiven for the sins I have committed)

#2.0.1 Altered a lot of menu test
#2.0.2 Fixed MAC addr regression
#2.0.3 Save short info

#2.1 Release
#2.1.1 Backout and nasty hacks
#2.1.2 Removed gdbserver workarounds

#2.2 Release
#2.2.1 Fixed multi-server support

#2.3 Release
#2.3.1 Got rid of nonblocking socket hack (it was no longer necessary, and had a potential race)
#2.3.2 Added new radar question

#2.4 Release
#2.4.1 Altered APR mail behaviour
#2.4.2 Added comments about panic-debug radar component
#2.4.3 Added Time stamp to hour wait
#2.4.4 Added sourcecache support

#2.4.5 Release

#2.4.6 Fixed time display bug (2753991)
#2.4.7 Fixed cache building bug (2752750)
#2.4.8 Fixed log file bug (2754001)

#modules I need

use IO::Socket;
use IPC::Open3;
use POSIX;
#use Mail::Mailer;

#Perl settings

#Autoflush, I need to fix this

#  $| = 1;

#Global settings

#Figure out if we are a debug build

#if($0 =~ /panic_debug_beta/)
#{
	#$debug_build = 1;
#}
#else
#{
#	$debug_build = 0
#}

#debug setting, I used to set it programmativly, now I use the -d flag (disabled in public builds)

$debug = 0;

#verbose is on by default I may add a flag to quelch output at some point.

$verbose = 1;

#This was added for debugging the cache generation code, it stops the script before gdb runs.
#I use it now in the netcache building code

$no_kdp = 0;

#Determines how much the script attempts to grab from the panicked machine

$output_extra = 0;

#Standalone mode, currently not enabled. The code is there, but untested. I need to merge
#the $standalone, $use_local_files, and $use_nfs_files at some point.

$standalone = 0;

#Turns on and off mail reporting

$mail_reporting = 0;

#Email address to send mails to

$report_addresses[0] = "panic\@spectrum-tigger.apple.com";

#Use the panic hotline:

$use_hotline = 1;

#if(!$debug_build)
#{
#	$report_addresses[1] = "panichotline\@group.apple.com";
#}

#Some default paths

$cache = "/var/tmp";
$netcache = "/Network/Servers/riemann/homes/saruman/gerbargl/spdcache";
$temp = "/tmp";

#This allows for user specified functional overrides of any of the above variables.
#Just initialize them in ~/.pdrc


if(-e $ENV{"HOME"}."/.pdrc")
{
	require $ENV{"HOME"}."/.pdrc";
}

#main code

#print "Setting up\n" if $verbose;
test_config();
#print "Processing arguments\n" if $verbose;
process_boot_args();
if($use_hotline)
{
	if(user_yesno("Can you leave this machine and the panicked machine alone to give a kernel engineer a chance to look at?"))
	{
	
		$contact_info = "";	
		
		$contact_temp = user_query("What is your name: ", "You entered \"","\", is that correct?");
		chomp($contact_temp);
		$from = user_query("What is your email-address: ", "You entered \"","\", is that correct?");
		chomp($from);
		$contact_info .= "$contact_temp\n";
		$contact_temp = user_query("Please provide some way for a kernel member to contact you (phone or email): ","You entered\"", "\" is that correct?");
		chomp($contact_temp);
		$contact_info .= "$contact_temp\n";
		$panic_message =  user_query("Please enter message that appears on the first line of the panic:", "You entered \"","\", is that correct?"); 
		chomp($panic_messagep);
		$contact_info .= "$contact_temp\n";
		
	
	
		
		
		print "If no one contacts you within an hour, this script will begin attempt to dump as much data as possible from the kernel.";
		print "After that you may reboot your panicked your machine\n";
		print "DO NOT RESTART THIS MACHINE OR THE PANICKED MACHINE!!!\n";
		print "DO NOT CLOSE THIS TERMINAL SESSION!!!!\n\n";
		$use_hotline = 1;
	}
	else
	{
		$use_hotline = 0;
	}
}


if(!$standalone)
{
	print "Preprocessing module mappings and dependencies\n" if $verbose;
	load_cache();
	if(! -e "/SourceCache")
	{
		`~appkit/bin/really /bin/ln -s ~rc/Software/Puma/Projects /SourceCache`;
	}
}
if(!$no_kdp)
{
	print "Adding arp entry\n" if $verbose;
	setup_arp_entry();
	print "Initiating remote debugger\n" if $verbose;
	run_gdb();
	if($mail_reporting)
	{
		foreach $address (@report_addresses)
		{
			print "Mailing panic report to $address\n" if $verbose;
			#print "MAIL cat $output_file | mail -s \"Automated Panic Report (panic-debug+ $version): $build panic on $machine\" $address";
			`mail -s \"Automated Panic Report (panic-debug+ $version): $build panic on $machine\" $address < $output_file`;
		}
	}
	`rm -rf $temp/spdtemp`;
	print "Panic information recovery is complete. You may restart the panicked machine\n" if $verbose;
}

sub test_config
{
	if(! -e "/usr/bin/gdb")
	{
		print "panic-debug needs gdb to run. Please install the developer tools.\n";
		exit 1;
	}
}

#Figures out stuff we need that can be directly derived from the boot arguments

sub process_boot_args
{
	
	my($x);
	my($y);
	my($boot_args);
	my($last);
	my($expected_args);
	
	$x = 0;
	$y = $#ARGV;
	$use_nfs_files = 1;
	$use_local_files = 0;
	$expected_args = 2;
	
	if($#ARGV == -1)
	{
		interactive_mode();
	}
	else
	{
		while( ($y - $x) != ($expected_args - 1))
		{
			if($ARGV[$x] =~ /-(.+)/)
			{
				#print "FLAGS $flags\n";
				$flags = $1;
				while(!$done)
				{
					if($flags =~ /(.)(.*)/)
					{
						$flag = $1;
						$flags = $2;
						if(!$flags)
						{
							$last = 1;
						}
						else
						{
							$last =0;
						}
						if($flag eq "n")
						{
							print "You have selected to use data from the network. This will provide the ";
							print "most data, but requires you are on Apple's NetInfo network.\n\n";
			
							$use_nfs_files = 1;
						}
						#elsif($flag eq "l")
						#{
						#	print "You have selected to use data from the machine you are currently running on ";
						#	print "for this to provide correct data you must be using the same build as the ";
						#	print "panicked machine.\n\n";
		
						#	$use_local_files = 1;
						#	$use_nfs_files = 0;
						#}
						#elsif($flag eq "o")
						#{
						#	$output_file = $ARGV[$x+1];
						#	$x++;
						#	if(!$last)
						#	{
						#		usage();
						#		exit 3;
						#	}
						#}
						#elsif($flag eq "x")
						#{
						#	$output_extra++;
						#}
						#elsif($flag eq "s")
						#{
						#	#I may opt to never support this, since the info copied from the screen
						#	#will be better
						#
						#	$standalone = 1;
						#}
						elsif($flag eq "c")
						{
							$no_kdp = 1;
							$cache = $ENV{"HOME"}."/spdcache";
							$expected_args -= 1;
						}
						else
						{
							usage();
							exit 2;
						}
					}
					else
					{
						$done = 1;
					}
				}
			}
			$x++;
		}

		if($no_kdp)
		{
			$build = $ARGV[$x];
		}
		else
		{
			$machine = $ARGV[$x];
			$build = $ARGV[$x+1];
		}
	}


		
	if(!$no_kdp)
	{
	
		if($machine =~ m/(.+)\.apple\.com/)
		{
			$machine = $1;
		}
		#print "$machine\n";
		if($machine =~ m/([0-9a-f]{1,2}:){5}[0-9a-f]{1,2}/i)
		{
			#print "MAC\n";
			$mac = $machine;
			$machine = user_query("Please enter either the IP address or hostname the panicked machine most recently had: ","You entered \"","\" is this correct?");
		}
		else
		{
			#print "NI\n";
			$done = 0;

			#Workaround for flakey niutil

			$nitutil_attmptes = 0;
			while($nitutil_attmptes < 10)
			{
				$mac = `niutil -read /applerd /machines/$machine 2> /dev/null`;
				if($mac =~ m/en_address:\s(.+)\s/)
				{
					$mac = $1;
					$nitutil_attmptes = 1024;
					$done = 1;
				}
				else
				{
					$nitutil_attmptes++;
					sleep 2;
				}
			}
			
			if(!$done || !($mac =~ m/([0-9a-f]{1,2}:){5}[0-9a-f]{1,2}/i))
			{
				print "An error occured attempting to get $machine\'s MAC address from NetInfo.\n";
				$mac = user_query("Please enter either the MAC address of the panicked machine.","You entered \"","\" is this correct?");
			}
		}
	}
	$buildname = $build;
	$buildname =~ m/^(\w*?)[0-9].*/;
	$buildname = $1;

	$buildmajor = $build;
	$buildmajor =~ m/^\w*?([0-9]).*/;
	$buildmajor = $1;
	
	$buildminor = $build;
	$buildminor =~ m/^\w*?[0-9][A-Z](.*)/;
	 $buildminor = $1;

	$buildletter = $build;
	$buildletter = m/^\w*?[0-9]([A-Z]).*/;
	$buildletter = $1;

	if( ($buildletter ne "A") && ($buildmajor => 6) && ($buildminor => 19) )	{
		$use_gdbserver = 0;
	}
	else
	{
		$use_gdbserver = 1;
	}

	$xnu = `~rc/bin/getvers $build xnu`;
	chomp($xnu);

	print "\n";
	print "panic-debug, version $version by Louis Gerbarg <mailto:louis\@apple.com> \n\n";

	print "WARNING: Do not run this if you have installed a root that has\n";
	print "replaced the kernel or a kernel extension, and make sure you are\n";
	print "running $build. Is should say $xnu on the screen of the panicked\n";
	print "machines screen.\n\n";
	print "Please files bugs about this script against component \"Test tools(panic-debug)\"\n";
	print "version \"X\"\n\n";
	print "Press Return to continue, or Control-C to cancel.\n";

	<STDIN>;


	if(!defined($output_file) && !$no_kdp)
	{
		if(! -e $ENV{"HOME"}."/Library/Logs")
		{
			`mkdir ~/Library/Logs`;
		}
		if(! -e $ENV{"HOME"}."/Library/Logs/panic-debug")
		{
			`mkdir ~/Library/Logs/panic-debug`;
		}
		$done = 0;
		$x = 1;
		while(!$done)
		{
			if(! -e $ENV{"HOME"}."/Library/Logs/panic-debug/panic-$machine-$build-$x")
			{
				$output_file = $ENV{"HOME"}."/Library/Logs/panic-debug/panic-$machine-$build-$x";
				$done = 1;
			}
			else
			{
				$x++
			}
		}
	}
	if($use_nfs_files)
	{
		$build_dir = "/Network/Servers/seaport/release/Software/$buildname/Updates/$build";
		$build_dir = "/Network/Servers/seaport/release/Updates/$buildname/$build";
		if(! -e $build_dir)
		{
			$build_dir = "/Network/Servers/seaport/release/Updates/$buildname/$build.test";
			if(! -e $build_dir)
			{
				print "Could not find build in ~rc Updates. Please copy the info from the\n";
				print "screen and report by hand.\n\n";
				exit 1;
			}
		}
		
		$symbol_dir = "$build_dir/Symbols";
		$mach_kernel = "$symbol_dir/xnu/mach_kernel";

		$gdbinit = "~louis/bin/gdbinit";
	}
	elsif($use_local_files)
	{
		$mach_kernel = "/mach_kernel";
		$debug_root = $0;
		$debug_root =~ /(.+)\/.+/;
		$debug_root = $1;
		$gdbinit = "$debug_root/.gdbinit";
		$arp_wrapper = "$debug_root/arp_wrapper";
	}
	if(! -e $mach_kernel)
	{
		print "Could not load kernel symbol table.\n";
		print "You will need to copy down the modules in backtrace,\n";
		print "as well as the module dependencies and include them\n";
		print "with the radar. Maybe you entered the wrong build name.\n";

		if(user_yesno("Do you want to run without the symbol table information?\n"))
		{
		}
		else
		{
			exit 5;
		}
	}
}
	
	

sub interactive_mode
{
	print "Entering Interactive mode.\n";
	$machine = user_query("Please enter the hostname of the panicked machine:","You entered the machine \"","\", is that correct?");
	$build = user_query("Please enter the build the panicked machine is running (.i.e Cheetah4K78):", "You entered the build \"","\", is that correct?");
	#if(user_yesno("Do you want to get symbol data from nfs (you must be on Apple's NetInfo network to do this)?")
	{
		$use_nfs_files = 1;
	}
	#else
	#{
	#	if(user_yesno("Would you like to manually specify the MAC address of the machine (you need to do this if you are not on netinfo)?"))
	#	{
	#		$mac = <>;
	#		chomp($mac);
	#	}
	#	if(user_yesno("Do you want to get symbol data from this machine (only do this if this machine is running the same build of Mac OS X as the panicked machine)?"))
	#	{
	#		$use_local_files = 1;
	#	}
	#}
	if(user_yesno("Do you want extra debugging info (this will take longer)?"))
	{
		$output_extra = 1;
		if(user_yesno("Do you want even more debugging info (this will take a *LONG* time)?"))
		{
			$output_extra = 2;
		}
	}
	if(user_yesno("Do you want to save output to a particular file (it will be saved in ~/Library/Logs/panic-debug/ by default)?"))
	{
		$output_file = user_query("Output File:");
		
	}
}

sub usage
{
	print "usage: panic-debug [machine build]\n";
	print "\t-n look for symbols on Apple's NetInfo network [default]\n";
#	print "\t-l look for symbols from the machine it is being run on\n";
#	print "\t\tBoth machines must be running the same build!!!!!\n";
#	print "\t-s will cause the script to run in standalone (no symbol data)\n";
#	print "\t-o will cause the script to write its out to the specified file.\n";
	print "\t\tIf no file is specified the output will be written to\n";
	print "\t\t~/Library/Logs/panic-debug/\n";
	print "\t-x output extra debugging info (this will take longer)\n";
	print "\tmachine may be the panicked machines NetInfo name\n";
	print "\t\tor its ethernet MAC Address\n";
	print "\tbuild is the name of the build the machine is running\n";
	print "\t\t(.i.e Puma5F26)\n";
}

#Our kernel has broken #! setuid (Radar 2269166), which complicates this
#I use a small C shim for local mode, and really on NetInfo (since I can't
#but a setuid root binary there

sub setup_arp_entry
{
	if($use_nfs_files)
	{
		`~appkit/bin/really /usr/sbin/arp -s $machine $mac`;
	}
	else
	{
		`$arp_wrapper $machine $mac`;
	}
}

#This function loads the cache files. Failing that it builds them
#This is the big, nasty, hard part of all this
#I deal with a lot of edge cases, some of which are probably bugs
#I will file Radars soon

sub load_cache
{
	my($build_cache);
	my(@dependencies);
	my($kmod_identifier);

	#Local and NFS runs use different cache sets. Currently I am only supporting NFS
	#The code loosk for stored caches on the local system, then in the netcache, then
	#if it can't find them it builds them.

	if($use_nfs_files)
	{
		if(-e "$cache/nfs_cache.$build")
		{
			restore_file_to_hash("$cache/dependency_cache.$build", \%dependency_cache);
			restore_file_to_hash("$cache/nfs_cache.$build", \%path_cache);
		}
		elsif(-e "$netcache/nfs_cache.$build")
		{
			`cp $netcache/dependency_cache.$build $cache/dependency_cache.$build`;
			restore_file_to_hash("$cache/dependency_cache.$build", \%dependency_cache);
			`cp $netcache/nfs_cache.$build $cache/nfs_cache.$build`;
			restore_file_to_hash("$cache/nfs_cache.$build", \%path_cache);
		}
		else
		{
			$build_cache = 1;
		}
	}
	elsif($use_local_files)
	{
		if(-e "$cache/local_cache.$build")
		{
			restore_file_to_hash("$cache/dependency_cache.$build", \%dependency_cache);
			restore_file_to_hash("$cache/local_cache.$build", \%path_cache)
		}
		elsif(-e "$netcache/local_cache.$build")
		{
			`cp $netcache/dependency_cache.$build $cache/dependency_cache.$build`;
			restore_file_to_hash("$cache/dependency_cache.$build", \%dependency_cache);
			`cp $netcache/local_cache.$build $cache/local_cache.$build`;
			restore_file_to_hash("$cache/local_cache.$build", \%path_cache);
		}
		else
		{
			$build_cache = 1;
		}
	}
	if($build_cache)
	{
		print "No network cache found, building local cache (this may take several minutes).\n";
		if($use_local_files)
		{
			build_cache_entries("/System/Library");
		}
		elsif($use_nfs_files)
		{
			build_cache_entries($symbol_dir);
		}

				#This code is to deal with incorrect dependency declarations in our kexts
				#Take the case of IODVDStorageFamily. It depends on IOCDStorageFamily
				#and IOStorageFamily (and it declares its dependencies in that order).
				#IOCDStorageFamily also depends on IOStorageFamily, which means that
				#IOStorage is the more primitive dependency. If you pass them to kmodsysms
				#they must go from most primitive to least primitive dependency (at least
				#C++ based modules most) or it will cause an error. This is not an issue
				#when we use kextload because of the preprocessing logic it does before
				#the kmodload is performed.

				foreach $kmod_identifier (keys %dependency_cache)
				{
						@dependencies = split(/ /,$dependency_cache{$kmod_identifier});

						for($x = 0; $x <= $#dependencies; $x++)
						{
								for($y = 0; $y <= $#dependencies; $y++)
								{
										if((defined($dependency_cache{$dependencies[$x]})) && ($dependency_cache{$dependencies[$x]} =~ m/$dependencies[$y]/))
										{
												if($x < $y)
												{
														($dependencies[$x],$dependencies[$y]) = ($dependencies[$y], $dependencies[$x]);
												}
										}
								}
						}
						$dep_temp = $dependencies[0];
						for($x = 1; $x <= $#dependencies; $x++)
						{
								$dep_temp .= " $dependencies[$x]";
						}
						$dependency_cache{$kmod_identifier} = $dep_temp;
				}
				if($use_nfs_files)
				{
						save_hash_to_file("$cache/dependency_cache.$build", \%dependency_cache);
						save_hash_to_file("$cache/nfs_cache.$build", \%path_cache);
				}
				elsif($use_local_files)
				{
						save_hash_to_file("$cache/dependency_cache.$build", \%dependency_cache);
						save_hash_to_file("$cache/local_cache.$build", \%path_cache);
				}
		}
}


sub build_cache_entries
{

	my($kext_dir);
        my($done);
        my($done2);
        my($build_cache);
        my(@module_names);
        my($dir);
        my($file);
        my(@temp_file);
        my($kmod_name);
        my($kmod_identifier);
        my($identifier_found);
        my($dep_temp);
        my(@dependencies);
        my($x);
        my($y);


	$kext_dir = $_[0];
	print "DIR $kext_dir\n" if $debug;
	@module_names = `ls $kext_dir`;
	foreach $index (0 .. $#module_names)
	{

		#This is a nasty hack to deal with the fact that some kext's have 
		#different project names (i.e. AppleSCCSerial is in a project named
		#kxAppleSCCSerial)

		$dir = $module_names[$index];
		$dir =~ m/\s*(.+)\s*/;
		$dir = $1;
		print "SUB $dir/\n" if $debug;
		@temp_file =`ls $kext_dir/$dir | grep \".kext\" `;
		foreach $file (@temp_file)
		{
			#  $file = $temp_file[0];
			$file =~ m/\s*(.+)\.kext/;
			$kmod_name = $1;
			$kmod_path = "$kext_dir/$dir/$kmod_name.kext/Contents/MacOS/$kmod_name";
			$done = 1;
			print "TESTING $file\n" if $debug;
			if(-e "$kext_dir/$dir/$kmod_name.kext/Contents/Info.plist")
			{
					print "KEXT $kmod_name.kext\n" if $debug;
					open(KMOD, "$kext_dir/$dir/$kmod_name.kext/Contents/Info.plist");
					$done = 0;
			}
			if(-e "$kext_dir/$dir/$kmod_name.kext/Contents/PlugIns")
			{
				
				build_cache_entries("$kext_dir/$dir/$kmod_name.kext/Contents");
			}
		}
		$identifier_found = 0;
		while(!$done)
		{
			$done2 = 0;

			$_ = <KMOD>;
			if(!defined($_))
			{
				$done = 1;
			}

			#The identifier_found variable makes sure we only grab the first
			#CFBundleIdentifier tag. If I was using a full CFXMLParser I would be
			#more intelligent, but this works, and is faster than spawing something else

			elsif(m/<key>CFBundleIdentifier<\/key>/ && !$identifier_found)
			{
				while(!$done2)
				{
					$_ = <KMOD>;

					if(m/<string>(.+)<\/string>/)
					{
						$kmod_identifier = $1;
						$path_cache{$kmod_identifier} = $kmod_path;
						$done2 = 1;
						$identifier_found = 1;
						print "IDENTIFIER $kmod_identifier\n" if $debug;
					}
				}
			}
			elsif(m/<key>OSBundleLibraries<\/key>/)
			{
				$dep_temp = "";
				while(!$done)
				{
					$_ = <KMOD>;
					if(m/<\/dict>/)
					{
						$done = 1;
						chop($dep_temp);
						$dependency_cache{$kmod_identifier} = $dep_temp;
					}
					elsif(m/<key>(.+)<\/key>/)
					{
						$dep_temp .= "$1 ";
						print "DEPENDS $1\n" if $debug;
					}
				}
			}
		}
		close(KMOD);
	}	
}


sub run_gdb
{

	my($done);
	my(%bt);

	my($hours);
	my($minutes);
	
	#We open gdb interactively, at some point I will start monitoring the error

	start_gdbserver();
	connect_gdbserver();
		

	
	#Basic setup for kernel debugging

	togdb("symbol-file $mach_kernel");
	togdb("target remote-kdp");
	togdb("attach $machine");
	togdb("source $gdbinit");
	if($use_gdbserver)
	{
		to_gdb("set kdp_flag = 3");
	}
	togdb("p proc0");
	togdb("source $gdbinit");
	if(!$standalone)
	{
		#We get the kmod addresses here
		#Then we load the symbols from build_kmodsyms

		togdb("echo SPD_STATUS Building kmod symbol files\\n");
		togdb("showallkmods");
		togdb("echo SPD_MARKER1\\n");
		build_kmodsyms();
		#print "DEBUG\n";
	}
	
	if($use_hotline)
	{
		#We then mark our entry and start getting info from the kernel
		togdb("echo SPD_STATUS Getting panic hotline information\\n");
		togdb("echo SPD_MARKER_HOTLINE\\n");
		togdb("bt");
		togdb("echo SPD_MARKER_HOTLINE2\\n");
		
		$done = 0;
	
		while(!$done)
		{
			$_ = fromgdb();
			if(m/SPD_MARKER_HOTLINE/)
			{
				$done = 1;
			}
		}
		
		$done = 0;
		
		open($quick_debug, ">$output_file.short");
		print $quick_debug "$panic_message\n\n";
		#open($hotline_mail, "| /usr/sbin/sendmail -oi -t -odi");
		#$hotline_mail ->open(	'From' => $from,
		#			`To` => "louis\@apple.com",
		#			`Subject` => "Automated Panic Report (panic-debug+ $version): $build panic on $machine");
		#open(HOTLINE_MAIL, ">$temp/hotline.log");
		$host = `hostname`;
 		chomp($host);
		while(!$done)
		{
			$_ = fromgdb();
			if(m/SPD_MARKER_HOTLINE2/)
			{
				$done = 1;
				print "\n" if $verbose;
			}
			elsif(m/SPD_STATUS\s(.+)/)
			{
				chomp($_);
				print "\n$1" if $verbose;
			}
			elsif(m/\(gdb\)\s+$/)
			{
			}
			elsif(m/^\043(\d*)\s+0x([a-fA-F0-9]+)/)
			{
				$bt{$1} = $2;
				user_dot();
				print $quick_debug $_;
			}
			elsif(m/No stack/)
			{
				print "The connection to the panicked machine does not appear to be functioning\n";
				print "Please try re-running the script, and make sure you have entered the correct network information.\n";
				print "If this problem persists please copy the panic into Radar by hand.\n\n";
				togdb("DE");
				exit 8;
			}
			elsif($_)
			{
				user_dot();
				print $quick_debug $_;
			}
		}
		#foreach $key (keys %bt)
		#{
		#	togdb("x/i 0x$bt{$key}");
		#}
		#togdb("echo BT_CONSISTENCY_MARKER\\n");
		
		$done = 0;
		$consistent = 1;
		#while(!$done)
		#{
		#	$_ = fromgdb();
		#	if(m/0x[a-fA-D0-9]+\s+\<.+\>:\s+(\.+)\s/)
		#	{
		#		if(!($1 =~ m/b.+l/))
		#		{
		#			$consistent = 0;
		#		}
		#	}
		#	if(m/BT_CONSISTENCY_MARKER/)
		#	{
		#		$done = 1;
		#	}
		#} 
		#print "Consistent $consistent\n";	 
		close($quick_debug);
		print "PLEASE IMMEDIATELY FILE A RADAR for this panic and paste or enclose the information saved in $output_file.short.\n";
		$radar = user_query("What is the Radar \# of the filed report?", "You entered \"","\" is this correct?");
		chomp($radar);
		open($hotline_mail, "| /usr/sbin/sendmail -oi -t -odi");
		print $hotline_mail "From: $from\n";
		print $hotline_mail "To: panichotline\@group.apple.com\n";
		#print $hotline_mail "To: merlin\@panic.apple.com\n";
		print $hotline_mail "Bcc: panic\@spectrum-tigger.apple.com\n";
		print $hotline_mail "Subject: APR: $machine panic on $build\n\n";
		print $quick_debug "$contact_info\n\n";
		if($use_gdbserver)
		{
			print $hotline_mail "Debug server is $host port 5212\n";
		}
		else
		{
			print $hotline_mail "Panicked machine is waiting for gdb reconnect\n";
		}
		open($quick_debug, "$output_file.short");
		print $hotline_mail "Radar $radar has been filed about this panic\n";
		while(<$quick_debug>)
		{
			print $hotline_mail $_;
		}
		close($quick_debug);
		print $hotline_mail "\n\nNotes: gdbserver has been completed, and should now be compliant.\n";
		print $hotline_mail "enough with the telnet spec to work with just about any commandline telnet client.\n";
		print $hotline_mail "Do not send cntrl characters across the link, it may break things.\n";
		print $hotline_mail "The gdbserver may be on an arbitrary port.\n";
		print $hotline_mail "Send mail to louis\@apple.com with questions and feedback.\n";

		close($hotline_mail);
		disconnect_gdbserver();
		#`mail -s \"Automated Panic Report (panic-debug+ $version): $build panic on $machine\" panichotline\@group.apple.com < $temp/hotline.log`;
#		`mail -s \"Automated Panic Report (panic-debug+ $version): $build panic on $machine\" louis\@apple.com < $temp/hotline.log`;
		
		$hours = (localtime)[2];
		$minutes= (localtime)[1];
		if($minutes < 10)
		{
			$minutes = "0$minutes";
		}
		print "Waiting for kernel debugging guru! (will wait for 60 minutes\n";
		print "from ".$hours.":".$minutes.")\n";
		if(!$use_gdbserver)
		{
			exit "Please do not restart the panicked machine for as long at least an hour!!";
		}
		print "DO NOT RESTART THIS MACHINE OR THE PANICKED MACHINE!!!\n";
		print "DO NOT CLOSE THIS TERMINAL SESSION!!!!\n";
		sleep 3600;
		connect_gdbserver();
		#wait_fortime_out
	}
	
	print "THIS MAY TAKE 15-20 MINUTES'\n";
	print "DO NOT RESTART THIS MACHINE OR THE PANICKED MACHINE!!!\n";
	print "DO NOT CLOSE THIS TERMINAL SESSION!!!!\n\n";
	togdb("echo SPD_MARKER2\\n");
	togdb("echo SPD_STATUS Getting backtrace\\n");
	togdb("bt full");
	togdb("echo SPD_STATUS Getting register data\\n");
	togdb("info all-reg");
	togdb("echo SPD_STATUS Getting zone allocation data\\n");
	togdb("zprint");
	togdb("echo SPD_STATUS Getting kmod data\\n");
	togdb("showallkmods");
	if(!$standalone)
	{
		togdb("echo SPD_STATUS Getting stack data\\n");
		togdb("showallstacks\n");
		#if($output_extra)
		#{
		#	togdb("echo SPD_STATUS Getting task data\\n");
		#	togdb("showalltasks");
		#	togdb("echo SPD_STATUS Getting activation data\\n");
		#	togdb("showallacts");
		#	togdb("echo SPD_STATUS Getting ipc data\\n");
		#	togdb("showallipc");
		#	togdb("echo SPD_STATUS Getting vm data\\n");
		#	togdb("showallvm");
		#	if($output_extra > 1)
		#	{
		#		togdb("echo SPD_STATUS Getting vme data\\n");
		#		togdb("showallvme");
		#		togdb("echo SPD_STATUS Getting rights data\\n");
		#		togdb("showallrights");
		#	}
		#}
	}

	#Mark the end of data

	togdb("echo SPD_MARKER3\\n");
	togdb("detach");

	open(OUTPUT, ">$output_file");

	print OUTPUT "Panic from $machine running $build\n";

	
	#Read until we hit the last marker, print out the status stuff we embedded
	#in the data stream as we come across them

	$done = 0;

	while(!$done)
	{
		$_ = fromgdb();
		if(m/SPD_MARKER2/)
		{
			$done = 1;
		}
	}

	$done = 0;

	while(!$done)
	{
		$_ = fromgdb();
		if(m/SPD_MARKER3/)
		{
			$done = 1;
			print "\n" if $verbose;
		}
		elsif(m/SPD_STATUS\s(.+)/)
		{
			chomp($_);
			print "\n$1" if $verbose;
		}
		elsif(/\(gdb\)\s+$/)
		{
		}
		elsif(m/No stack/)
		{
			print "The connection to the panicked machine does not appear to be functioning\n";
			print "Please try re-running the script, and make sure you have entered the correct network information.\n";
			print "If this problem persists please copy the panic into Radar by hand.\n\n";
			togdb("DE");
			exit 8;
		}
		elsif(/No symbol table info available/)
		{
			#print "ERROR: An error my have occured lodading the symbol table. Please copy the data that is on the screen, what I have gotten may not be sufficent.\n";
		}
		elsif($_)
		{
			user_dot();
			print OUTPUT $_;
		}
	}
	detach_gdbserver();
	close(OUTPUT);
	print "PLEASE FILE A RADAR for this panic and paste or enclose the information saved in $output_file.\n";

}

#This function is pretty simple, I decided to keep the complexity in the cache generation
#That way I do it less often.

sub build_kmodsyms
{
	my($done) = 0;
	
	#In this file we read through the data from gdb, and see where the
	#the kmods are loaded. I filter out the faked names from stuff in
	#mach_kernel (address 0x00000000)
	
	while(!$done)
	{
	
		$_ = fromgdb();
		
		if(m/^.+?\s+..(.+?)\s+.+\s+\d+\s+\d+\s+.+?\s+(.+?)\s/)
		{
			$kmod_identifier = $2;
			chomp($kmod_identifier);
			$kmod_addr = $1;
			if(defined($path_cache{$kmod_identifier}) && ($kmod_addr ne "00000000")) 
			{
				$kmod_addrs{$kmod_identifier}=$kmod_addr;
			}
		}
		elsif(m/SPD_MARKER1/)
		{
			$done = 1;
		}
	}
	
	#build the call to kmods, based on the dependency caches
	if(! -e "$temp/spdtemp")
	{
		`mkdir $temp/spdtemp`;
	}
	foreach $kmod_identifier (keys %kmod_addrs)
	{
		$kmod_addr = $kmod_addrs{$kmod_identifier};
		$kmodsyms_temp = "kmodsyms -k $mach_kernel ";
		@dependencies = split(/ /,$dependency_cache{$kmod_identifier});
		foreach $dependency (@dependencies)
		{
			if(defined($kmod_addrs{$dependency}))
			{
				$kmodsyms_temp .= " -d $path_cache{$dependency}\@0x$kmod_addrs{$dependency}";
			}
		}
		$kmodsyms_temp .= " -o $temp/spdtemp/$kmod_identifier.sym $path_cache{$kmod_identifier}\@0x$kmod_addr";
		
		`$kmodsyms_temp 2> /dev/null`;
		if(-e "$temp/spdtemp/$kmod_identifier.sym")
		{
			togdb("add-symbol-file $temp/spdtemp/$kmod_identifier.sym");
		}
	}
}

#Really simple hash serialization functions. They are very limited, but
#since CFBundleIdentifiers do not have commas or spaces in them they are
#garanteed to work on the data I give to them. I use them for loading and 
#storing the cache files

sub save_hash_to_file
{
	my($data) = $_[1];
	my($file) = $_[0];
	
	open(SAVE_FILE, ">$file");
	foreach $data_item (keys %$data)
	{
		print SAVE_FILE "$data_item,$$data{$data_item}\n";
	}
	close(SAVE_FILE);
}

sub restore_file_to_hash
{
	my($data) = $_[1];
	my($file) = $_[0];

	open(RESTORE_FILE, "$file");
	while(<RESTORE_FILE>)
	{
		m/(.+)\,(.+)/;
		$$data{$1}=$2;
		print "$1 $$data{$1}\n" if ($debug > 1);
	}
}

sub nonblock
{
	my $socket = shift;
	my $flags;
	
	$flags = fcntl($socket, F_GETFL, 0)
		or die "Can't get flags for socket: $!\n";
	fcntl($socket, F_SETFL, $flags | O_NONBLOCK)
		or die "Can't set socket nonblocking: $!\n";
}

#I was repeating a less robust version of this all over the verbose code
#so I broke it out and made it work much better

sub user_yesno
{
	my($query) = $_[0];
	my($yesno);
	my($done);
	my($retval);

	$done = 0;
	
	while(!$done)
	{
		print "$query(y/n) ";
		$yesno = <STDIN>;
		chomp($yesno);
		if($yesno eq "y")
		{
			$retval = 1;
			$done = 1;
		}
		elsif($yesno eq "n")
		{
			$retval = 0;
			$done = 1;
		}
	}

	return $retval;
}

sub user_query
{
	my($query) = $_[0];
	my($confirmation1) = $_[1];
	my($confirmation2) = $_[2];
	my($response);
	my($retval);
	my($done);

	$done = 0;
	while(!$done)
	{
		print "$query ";
		$response = <STDIN>;
		chomp($response);
		if($response)
		{
			if(user_yesno("$confirmation1$response$confirmation2"))
			{
				$retval = $response;
				$done = 1;
			}
		}
	}
	print "\n";

	return $retval;
}

sub user_dot
{
	$| = 1;
	print "." if $verbose;
	$| = 0;
}

sub user_out
{
}	

#These are thin wrappers for gdb access, they allow me to snoop the pipe traffic for debugging

sub togdb
{
	my($out) = $_[0];

	if($use_gdbserver)
	{
		print $gdbsocket "$out\n";
	}
	else
	{
		print TOGDB "$out\n";
	}
	print "\>$out\n" if $debug;
	
}

sub fromgdb
{
	my($in);
	

	if($use_gdbserver)
	{
		$in = <$gdbsocket>;
	}
	else
	{
		$in = <FROMGDB>;
	}
	if($in)
	{	
		print "\<$in" if $debug;
	}

	return $in;
}

sub start_gdbserver
{
	
	my($x);
	
	$x = 0;
if($use_gdbserver) {	
	if(fork())
	{
		`/Network/Servers/riemann/homes/saruman/gerbargl/bin/gdbserver` || die "1\n";
		exit 1;
	}
	else
	{
		while(! -e "$temp/spd_handoff")
		{
			sleep 1;
			$x++;
			if($x == 20)
			{
				print "Timeout: Cannot connect to gdbserver\n\n";
				exit 9;
			}
		}
		open($spd_handoff, "$temp/spd_handoff");
		while(<$spd_handoff>)
		{
			if(m/(\d+)/)
			{
				$port = $1;
			#	print "PORT $port\n" if $debug;
			}
		}
		close($spd_handoff);
		`rm $temp/spd_handoff`;
	}
}
}

sub connect_gdbserver
{
	if($use_gdbserver)
	{
		$gdbsocket = IO::Socket::INET->new( Proto => 'tcp',
						PeerAddr => 'localhost',
						PeerPort => $port) || die "Could not connect to gdbserver on port $port\n";
		#nonblock($gdbsocket);
		togdb("DC");
	}
	else
	{
		 $pid = open3(*TOGDB, *FROMGDB, *GDBERROR, "gdb");
	}
}

sub disconnect_gdbserver
{
	if($use_gdbserver)
	{
		togdb("DI");
		close($gdbserver);
	}
	else
	{
		`kill $pid`;
#		togdb("detach");
		close(TOGDB);
		close(FROMGDB);
	}
}

sub detach_gdbserver
{
	if($use_gdbserver)
	{
		togdb("DE");
		close($gdbserver);
	}
	else
	{
#		togdb("detach");
		close(TOGDB);
		close(FROMGDB);
	}
}
