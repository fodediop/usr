#!/bin/tcsh

# image-me.sh, Copyright 2002, Mike Bombich
# This script handles the self-imaging process. This is intended
# to replace the functionality of the revrdist prefs file.
# This is the script that you will modify to customize how this
# particular computer is imaged. The customizable options are "syncDirs"
# and "args".
# This script can be run from the cron whenever.

## Indicate the directories that should be synchronized
# Encode the spaces with "%space"
#set syncDirs = (.hidden Applications Applications%space(Mac%spaceOS%space9) System System%spaceFolder Library usr bin sbin mach_kernel private)
set syncDirs = ( Applications )

# Determine if script was run by root
set user = `whoami`
if ( $user != root ) then
	echo "You must run this script as root, else you may not have privileges"
	echo "to properly restore all files."
	exit  1
endif


## Setup the argument components
set masterDir = "/Volumes/imacnroll/MacOSX/MasterBase"
set rsync = "/usr/bin/rsync"
set ssh = "--rsh=/usr/bin/ssh"
set recursive = "--recursive"
set preserve = "--times -og --links"
set prog = "--progress"
set stats = "--stats"
set delete = "--delete"
set remoteUser = "admin"
set server = "imacnroll"

## Assemble the arguments
#set args = ($prog $stats $ssh $recursive $preserve $delete)
set args = ($prog $stats $ssh $recursive $preserve)

# Synch the directories one at a time
foreach dir ($syncDirs)
	set theDir = `echo $dir | sed 's/\%space/\ /g'`
	$rsync $args ${remoteUser}@${server}:${masterDir}/${theDir}/ /${theDir}/

#       # Repair any custom folder icon flags if Dev tools present
#       if (-e /Developer/Tools/SetFile) then
#               set iconFiles = `find $targetDir -name "Icon?" | tr -d '\r' | sed 's/\ /%space/g'`
#
#               foreach icon ($iconFiles)
#                       set dcIcon = "`echo "$icon" | sed 's/%space/\ /g'`"
#                       /Developer/Tools/SetFile -a C "$dcIcon:h"
#               end
#       endif

end
