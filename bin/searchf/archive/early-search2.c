/* 
 */

/*---------------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ar.h>

#define compile 			__compile__
#define execute 			__execute__
#define out_file 			__out_file__
#define out_line 			__out_line__
#define out_before 			__out_before__
#define out_after 			__out_after__
#define match_words 		__match_words__
#define match_icase 		__match_icase__
#define match_lines			__match_lines__
#define eolbyte				__eolbyte__
#define filename 			__filename__
#define matcher				__matcher__
#define filename_mask		__filename_mask__

#define setmatcher			__setmatcher__
#define install_matcher 	__install_matcher__
#define error				__error__
#define fatal				__fatal__
#define getopt_long			__getopt_long__
#define getopt_long_only	__getopt_long_only__

#include "system.h"
char const *matcher = "egrep";

#include "grep.h"

extern int setmatcher();

extern void (*compile)();					/* routine to compile the reg. expr.	  */
extern char *(*execute)();					/* routine to apply it 					  */

extern char *filename;						/* file to be search 					  */

extern int out_file;						/* show filenname with matches 			  */
extern int out_line;						/* output matches with line nbr (-n) 	  */
extern int out_before;						/* lines of leading context 			  */
extern int out_after;						/* lines of trailing context 			  */
extern int match_words;						/* match whole words (-w)				  */
extern int match_icase;						/* ignore case (-i)						  */
extern int filename_mask;					/* If 0, output nulls after filenames (-Z)*/

struct stats {
  struct stats *parent;
  struct stat stat;
};

static struct stats stats_base;

static jmp_buf abort_jmp;
int in_grep;

static int show_help;
#ifdef DEBUG    		
static int debug_options, saw_e;
#endif

/*------------------------------------------------------------------------------------*/

#ifdef __STDC__
void error(const char *mesg, int errnum)
#else
void error(char *mesg, int errnum)
#endif
{
	if (errnum)
		fprintf(stderr, "%s: %s: %s\n", "grep", mesg, strerror(errnum));
	else
		fprintf(stderr, "%s: %s\n", "grep", mesg);
}

#ifdef __STDC__
void fatal(const char *mesg, int errnum)
#else
void fatal(char *mesg, int errnum)
#endif
{
	if (errnum)
		error(mesg, errnum);
	else
		error(mesg, errnum);
	
	if (in_grep)
		longjmp(abort_jmp, 1);
}

/*---------------------------------------------------------------------------------*/
/*
	search [options] [-e] PATTERN [FILE...]	search acceptable files
	search [--esc] [-c] [-h] -s] -f FILE...	list acceptable (c, h, s, all) files
	
	-A n | --after-context=n			show n context lines before
	-a | --all | --any					search any kind of file (just like grep and egrep)
	-B n | --before-context=n			show n context lines after
	-C [n] | --context[=n]				same as -B n -A n (default 2)
	-c | --code							search only c and c++ files
	-E | --extended-regexp				egrep (default)
	-e PATTERN | --regexp=PATTERN		specify PATTERN if it it begins with '-'
	--esc								escape blanks in -f display
	-f | --accepted-files				show only files which search will accept from the
										list (only --esc, -c, -h, and -s allowed with -f)
	-G | --basic-regexp					grep instead of egrep
	-H									show filename
	-h | --hdrs | --headers				search only headers
	--help								display help info
	-i | --ignore-case					ignore case
	-l | --follow-links					follow file links
	-n | --line-number					show line numbers
	-s | --asm							search only asm files
	-t | --all-text | --any-text		search all text files
	-w | --word-regexp					match words (exact match)
*/

static char const short_options[] = "0123456789A:B:C:EGHX:ace:fhilnstwy";
static char *prog;

static struct option long_options[] =
{
	{"accepted-files", no_argument, NULL, 'f'},
	{"after-context", required_argument, NULL, 'A'},
	{"all", no_argument, NULL, 'a'},
	{"all-text", no_argument, NULL, 't'},
	{"any", no_argument, NULL, 'a'},
	{"any-text", no_argument, NULL, 't'},
	{"asm", no_argument, NULL, 's'},
	{"basic-regexp", no_argument, NULL, 'G'},
	{"before-context", required_argument, NULL, 'B'},
	{"code", no_argument, NULL, 'c'},
	{"context", optional_argument, NULL, 'C'},
#ifdef DEBUG 
	{"debug-the-options", no_argument, &debug_options, 1},
#endif
	{"esc", no_argument, NULL, 257},
	{"extended-regexp", no_argument, NULL, 'E'},
	{"follow-links", no_argument, NULL, 'l'},
	{"hdrs", no_argument, NULL, 'h'},
	{"headers", no_argument, NULL, 'h'},
	{"help", no_argument, &show_help, 1},
	{"ignore-case", no_argument, NULL, 'i'},
	{"line-number", no_argument, NULL, 'n'},
	{"regexp", required_argument, NULL, 'e'},
	{"with-filename", no_argument, NULL, 'H'},
	{"word-regexp", no_argument, NULL, 'w'},
	{0, 0, 0, 0}
};

static int no_file, word_regexp, ignore_case, line_numbers, before_context, after_context;
static int code_files, hdr_files, asm_files;
static int show_only_accepted_files, follow_links, esc, allfiles, all_text_files;
static char *pattern;

/*------------------------------------------------------------------------------------*/
#ifdef DEBUG
static void display(char *buffer, int n)
{
	int i;
	
    if (n > 32) n = 32;
    
    for (i = 0; i < n; ++i)
    	fprintf(stdout, "%.2X ", buffer[i]);
	fprintf(stdout, "\n");
	
    for (i = 0; i < n; ++i)
    	fprintf(stdout, "%-2c ", isascii(buffer[i]) ? buffer[i] : ' ');
	fprintf(stdout, "\n");
}
#endif
/*---------------------------------------------------------------------------------*/

static char *code_suffixes[] = {
	".c",
	".cc",
	".cp",
	".cpp",
	".cxx",
	".c++",
	".m",
	".C",
	".i",
	".ii",
	".y",
	NULL
};

static char *hdr_suffixes[] = {
	".h",
	".hp",
	".hpp",
	".hxx",
	".ih",
	".xh",
	".xih",
	NULL
};

static char *asm_suffixes[] = {
	".s",
	".S",
	NULL
};

/*
 * Determine the language extension for a specified pathname is one defined in
 * the allowed_suffixes[] table.  The function returns 1 if it is and 0 if it
 * isn't.
 */
static int determine_suffix(char *pathname, char *allowed_suffixes[])
{
	char *basename, *suffix;
	int  i, len;
	
	if ((basename = strrchr(pathname, '/')) != NULL)
		basename = pathname + 1;
	else
		basename = pathname;
		
	if ((suffix = strrchr(basename, '.')) == NULL)
		return (0);
	
	len = strlen(++suffix);
	if (len <= 0)
		return (0);
		
	for (i = 0; allowed_suffixes[i]; ++i)
		if (strncmp(suffix, &allowed_suffixes[i][1], len) == 0)
			return (1);
			
	return (0);
}

/*
 * Returns size of sample read, sample in buffer, and the open file's file descriptor.
 * The file is not closed unless the returned fd is < 0.
 */
 
static int sample_file(char *pathname, char *buffer, int *fd, int *has_nulls, int amount)
{
	int n, i;
	
    if ((*fd = open(pathname, O_RDONLY)) < 0)
    	return (0);
    	
    if ((n = read(*fd, (char *)buffer, amount)) == -1) {
    	close(*fd);
    	*fd = -1;
    	return (0);
    } 
        
    buffer[n] = '\0';
    
	for (i = 0; i < n; ++i)
		if (buffer[i] == 0) {
			break;
		}
		
    *has_nulls = (i < n);
    
    return (n);
}

typedef enum {
	TEXT_FILE,
 	BINARY_FILE,
 	SCRIPT_FILE,
 	AR_FILE,
 	DOT_FILE,
 	NOT_FTS_F,
 	CANT_LSTAT,
 	NOT_S_ISREG
} Classification;

#ifdef DEBUG
static char *show_clasification(Classification what_is_it)
{
	static char its[20];
	
	switch (what_is_it) {
		case BINARY_FILE:	return ("BINARY_FILE\n");
		case AR_FILE:		return ("AR_FILE");
		case DOT_FILE:		return ("DOT_FILE");
		case NOT_FTS_F:		return ("NOT_FTS_F");
		case CANT_LSTAT:	return ("CANT_LSTAT");
		case NOT_S_ISREG:	return ("NOT_S_ISREG");
		default:			sprintf(its, "%ld", (int)(what_is_it));
							return (its);
	}
}
#endif

/*
 * Return 1 if there's even a possibility that the file can be searched.
 */
static int can_be_searched(FTSENT *entry, Classification *what_is_it, int dots_ok,
						   struct stat *sb)
{
    if (entry->fts_info != FTS_F) {				/* accept only "regular files" 			*/
    	*what_is_it = NOT_FTS_F;
    	return (0);
	}
	
    if (!dots_ok && *entry->fts_accpath=='.') { /* ignore .files of any kind			*/
    	*what_is_it = DOT_FILE;
    	return (0);
	}
	
    if (lstat(entry->fts_accpath, sb) != 0) {	/* now we have to look a little deeper 	*/
    	*what_is_it = CANT_LSTAT;
    	return (0);
	}
	
    if (!S_ISREG(sb->st_mode)) {				/* double check it's a regular file 	*/
    	*what_is_it = NOT_S_ISREG;
    	return (0);
    }
    
    return (1);									/* a searchable file					*/
}

/*
 * Returns ptr to pathname if what_is_it = TEXT_FILE | SCRIPT_FILE and fd >= 0.
 * Returns NULL if what_is_it is anything else (fd < 0).
 */
static char *filter_text_files(FTSENT *entry, Classification *what_is_it, int *fd)
{
    int n, has_nulls;
    struct stat sb;
	unsigned char buffer[128];
	
	if (!can_be_searched(entry, what_is_it, 0, &sb))
		return (NULL);							/* no possibility of it being searched	*/
    
    /* We have a possible candidate.  It might be an executable file but it might also 	*/
    /* be a script which is acceptable.  So we will sample the first 127 characters of	*/
    /* the file.  Odds are, that if its some kind of archive or binary it will have at	*/
    /* least one 0-byte in that 127.  If it does we treat it as some kind of binary and	*/
    /* reject it.  All others are accepted.  This is probably not fool-proof.  But good	*/
    /* enough for our purposes.															*/
    
	n = sample_file(entry->fts_accpath, buffer, fd, &has_nulls, 127);
	if (!n || has_nulls) {						/* has some nulls, so reject it			*/
    	if (fd >= 0) close(*fd);
    	*what_is_it = BINARY_FILE;				/* most likely a binary file			*/
		return (NULL);
	}
	
	if (sb.st_mode & S_IXUSR) {					/* no nulls && executable				*/
    	*what_is_it = SCRIPT_FILE;				/* most likely it's a script which is ok*/
	    return (entry->fts_path);
	}
	
	if (n >= SARMAG && strncmp((char *)buffer, ARMAG, SARMAG) == 0) {
    	if (fd >= 0) close(*fd);
    	*what_is_it = AR_FILE;					/* try to filter out archives too		*/
		return (NULL);
	}
	
	*what_is_it = TEXT_FILE;
	return (entry->fts_path);					/* if it makes it to here, assume text	*/
}

static char *esc_blanks(char *pathname)
{
	char 		*b;
	static char esc_pathname[2*MAXPATHLEN];
	
	b = esc_pathname;
	while (*pathname) {
		if (*pathname == ' ')
			*b++ = '\\';
		*b++ = *pathname++;
	}
	*b = 0;
	
	return (esc_pathname);
}

/*---------------------------------------------------------------------------------*/

static int grepdir(char const *dir, struct stats *stats);
static void fatal(const char *mesg, int errnum);
static char *xrealloc(char *ptr, size_t size);
extern char *__savedir__(const char *dir, off_t name_size);

/*
 * Returns size of sample read, sample in buffer, and the open file's file descriptor.
 * The file is not closed unless the returned fd is < 0.
 */
 
static int sample_file1(int fd, char *buffer, int *has_nulls, int amount)
{
	int n, i;
	
    if ((n = read(fd, (char *)buffer, amount)) == -1)
    	return (0);
        
    buffer[n] = '\0';
    
	for (i = 0; i < n; ++i)
		if (buffer[i] == 0)
			break;
		
    *has_nulls = (i < n);
    
    return (n);
}

static int can_be_searched1(char *file, struct stat *sb, Classification *what_is_it, int dots_ok)
{
	char *p;
	
    if (!S_ISREG(sb->st_mode)) {
    	*what_is_it = NOT_S_ISREG;
    	return (0);
    }
	
	p = strrchr(file, '.');
	if (!p)
		p = file - 1;
	
    if (!dots_ok && p[1] == '.') { 				/* ignore .files of any kind			*/
    	*what_is_it = DOT_FILE;
    	return (0);
	}
    
    return (1);									/* a searchable file					*/
}

/*
 * Returns ptr to pathname if what_is_it = TEXT_FILE | SCRIPT_FILE and fd >= 0.
 * Returns NULL if what_is_it is anything else (fd < 0).
 */
static char *filter_text_files1(char *file, struct stat *sb, Classification *what_is_it, int fd)
{
    int n, has_nulls;
	unsigned char buffer[128];
	
	if (!can_be_searched1(file, sb, what_is_it, 0))
		return (NULL);							/* no possibility of it being searched	*/
    
    /* We have a possible candidate.  It might be an executable file but it might also 	*/
    /* be a script which is acceptable.  So we will sample the first 127 characters of	*/
    /* the file.  Odds are, that if its some kind of archive or binary it will have at	*/
    /* least one 0-byte in that 127.  If it does we treat it as some kind of binary and	*/
    /* reject it.  All others are accepted.  This is probably not fool-proof.  But good	*/
    /* enough for our purposes.															*/
    
	n = sample_file1(fd, buffer, &has_nulls, 127);
	if (!n || has_nulls) {						/* has some nulls, so reject it			*/
    	*what_is_it = BINARY_FILE;				/* most likely a binary file			*/
		return (NULL);
	}
	
	if (sb->st_mode & S_IXUSR) {				/* no nulls && executable				*/
    	*what_is_it = SCRIPT_FILE;				/* most likely it's a script which is ok*/
	    return (file);
	}
	
	if (n >= SARMAG && strncmp((char *)buffer, ARMAG, SARMAG) == 0) {
    	*what_is_it = AR_FILE;					/* try to filter out archives too		*/
		return (NULL);
	}
	
	*what_is_it = TEXT_FILE;
	return (file);								/* if it makes it to here, assume text	*/
}

static int grep(int fd, char *file, struct stats *stats)
{
	int 			accept, status;
	char			*p;
	Classification  what_is_it;
	
	if (lstat(file, &stats->stat) != 0)
		return 1;
		
  	if (file && S_ISDIR(stats->stat.st_mode)) {
		(void)close(fd);
		return grepdir(file, stats) - 2;
    }
    
	if (allfiles) {
		if (can_be_searched1(file, &stats->stat, &what_is_it, 1))
			if (show_only_accepted_files) {
				fprintf(stdout, "%s\n", esc ? esc_blanks(file) : file);
				status = 0;
			} else
				status = egrep(no_file, word_regexp, ignore_case, line_numbers, before_context,
		  	 	      			after_context, pattern, file, fd);
	} else {
		p = filter_text_files1(file, &stats->stat, &what_is_it, fd);
		if (p) {
			accept = all_text_files;
			if (!accept) {
				if (code_files)
					accept = determine_suffix(file, code_suffixes);
				if (hdr_files)
					accept = (accept || determine_suffix(file, hdr_suffixes));
				if (asm_files)
					accept = (accept || determine_suffix(file, asm_suffixes));
			}
			
			if (accept)
				if (show_only_accepted_files) {
					fprintf(stdout, "%s\n", esc ? esc_blanks(p) : p);
					status = 0;
				} else {
					if (lseek(fd, 0, SEEK_SET) >= 0) {
						status = egrep(no_file, word_regexp, ignore_case, line_numbers,
							   			before_context, after_context, pattern, p, fd);
						if (status >= 0)
							pattern = NULL;
					}
				}
		}
	}
}

static int grepfile(char *file, struct stats *stats)
{
  	int fd, count, status;

  	if (file) {
		while ((fd = open(file, O_RDONLY)) < 0 && errno == EINTR)
			continue;

		if (fd < 0) {
	 		if (errno == EISDIR) {
				if (lstat(file, &stats->stat) != 0)
		  			return 1;
	      		return grepdir(file, stats);
	    	}
	  		return 1;
		}
	} else
		fd = 0;
	
	count = grep(fd, file, stats);
  
  	if (count < 0)
		status = count + 2;
	else if (file)
		while (close(fd) != 0)
	  		if (errno != EINTR)
	      		break;

  return status;
}

static int grepdir(char const *dir, struct stats *stats)
{
	int 		   status = 1;
	struct stats *ancestor;
	char 		   *name_space;

	for (ancestor = stats; (ancestor = ancestor->parent) != 0; )
		if (ancestor->stat.st_ino == stats->stat.st_ino &&
    		ancestor->stat.st_dev == stats->stat.st_dev) {
	  		fprintf (stderr, "%s: warning: %s: recursive directory loop\n", prog, dir);
			return 1;
      	}

  	name_space = __savedir__(dir, (unsigned)stats->stat.st_size);

  	if (!name_space) {
 		if (!errno)
			fatal("Memory exhausted", 0);
    } else {
		size_t dirlen = strlen(dir);
      	int needs_slash = !(dirlen == 0 || dir[dirlen - 1] == '/');
      	char *file  = NULL;
      	char *namep = name_space;
      	struct stats child;
      	
      	child.parent = stats;
      	//out_file    += !no_file;
      	
      	while (*namep) {
	  		size_t namelen = strlen(namep);
	  		file = xrealloc(file, dirlen + 1 + namelen + 1);
	  		strcpy(file, dir);
	  		file[dirlen] = '/';
	  		strcpy(file + dirlen + needs_slash, namep);
	  		namep += namelen + 1;
	  		status &= grepfile(file, &child);
		}
		
      	//out_file -= !no_file;
      	if (file)
        	free(file);
      
      	free(name_space);
    }

  	return status;
}

static int search1(char **argv)
{
	int status;

	if (*argv)
		while (*argv)
			status = grepfile(*argv++, &stats_base);
	else
		status = grepfile(NULL, &stats_base);

  	return (status);
}

/*---------------------------------------------------------------------------------*/

int my_filter(int fd, char *file, struct stats *stats)
{
	int 			accept, status;
	char			*p;
	Classification  what_is_it;
	
	if (allfiles) {
		if (can_be_searched1(file, &stats->stat, &what_is_it, 1)) {
			if (show_only_accepted_files) {
				fprintf(stdout, "%s\n", esc ? esc_blanks(file) : file);
				return (0);
			}
			return (1);
		}
	} else {
		off_t currpos = lseek(fd, 0, SEEK_CUR);
		p = filter_text_files1(file, &stats->stat, &what_is_it, fd);
		if (p) {
			accept = all_text_files;
			if (!accept) {
				if (code_files)
					accept = determine_suffix(file, code_suffixes);
				if (hdr_files)
					accept = (accept || determine_suffix(file, hdr_suffixes));
				if (asm_files)
					accept = (accept || determine_suffix(file, asm_suffixes));
			}
			
			if (accept) {
				if (show_only_accepted_files) {
					fprintf(stdout, "%s\n", esc ? esc_blanks(p) : p);
					return (0);
				}
				
				if (lseek(fd, currpos, SEEK_SET) >= 0)
					return (1);
			}
		} else if (lseek(fd, currpos, SEEK_SET) >= 0)
			return (1);
			
	}
	
	return (0);
}

#define compile 		__compile__
#define execute 		__execute__
#define out_file 		__out_file__
#define out_line 		__out_line__
#define out_before 		__out_before__
#define out_after 		__out_after__
#define match_words 	__match_words__
#define match_icase 	__match_icase__
#define match_lines		__match_lines__
#define eolbyte			__eolbyte__
#define filename 		__filename__
#define matcher			__matcher__
#define filename_mask	__filename_mask__
#define directories		__directories__

#define setmatcher		__setmatcher__
#define install_matcher __install_matcher__

extern int setmatcher();

extern void (*compile)();					/* routine to compile the reg. expr.	  */
extern char *(*execute)();					/* routine to apply it 					  */

extern char *filename;						/* file to be search 					  */

//char const *matcher = "egrep";
extern char *matcher;

extern int match_lines;						/* -x */
extern unsigned char eolbyte;				/* -z */

extern int out_file;						/* show filenname with matches 			  */
extern int out_line;						/* output matches with line nbr (-n) 	  */
extern int out_before;						/* lines of leading context 			  */
extern int out_after;						/* lines of trailing context 			  */
extern int match_words;						/* match whole words (-w)				  */
extern int match_icase;						/* ignore case (-i)						  */
extern int filename_mask;					/* If 0, output nulls after filenames (-Z)*/

static int search2(char **argv)
{
	int status;
	extern int directories;
	
	directories   = 1;
	out_file   	  = (no_file == 0);
	match_words   = word_regexp;
	match_icase   = ignore_case;
	out_line      = line_numbers;
	out_before	  = before_context;
	out_after	  = after_context;
	match_lines   = 0;
	eolbyte		  = '\n';
	filename_mask = ~0;
	
	if (setjmp(abort_jmp))
		return (-2);

	in_grep = 1;
	
  	if (!install_matcher(matcher) && !install_matcher("default"))
		return (-3);
	
	(*compile)(pattern, strlen(pattern));
	
	if (*argv)
		while (*argv) {
			status = __grepfile__(strcmp(*argv, "-") == 0 ? NULL : *argv, &stats_base);
			++argv;
		}
	else
		status = __grepfile__(NULL, &stats_base);
	
	in_grep = 0;
	
  	return (status);
}

/*---------------------------------------------------------------------------------*/

/* Convert STR to a positive integer, storing the result in *OUT.
 * If STR is not a valid integer, return -1 (otherwise 0). 
 */
static int ck_atoi(char const *str, int *out)
{
	char const *p;
	
	for (p = str; *p; p++)
    	if (*p < '0' || *p > '9')
      		return -1;

  	*out = atoi(optarg);
  	
  	return 0;
}

static void fatal(const char *mesg, int errnum)
{
  	if (errnum)
    	fprintf(stderr, "%s: %s: %s\n", prog, mesg, strerror (errnum));
  	else
    	fprintf(stderr, "%s: %s\n", prog, mesg);
  	exit(2);
}

static char *xrealloc(char *ptr, size_t size)
{
	char *result = ptr ? realloc(ptr, size) : malloc(size);
  
  	if (size && !result)
    	fatal ("memory exhausted", 0);
  	
  	return result;
}

static void usage(char *why, int status)
{
	if (status != 0) {
		if (why)
			fprintf(stderr, "%s: %s\n", prog, why);
		fprintf(stderr, "Usage: %s [OPTION]... PATTERN [FILE...]\n", prog);
		fprintf(stderr, "       %s [--esc] [-chs] -f FILE...\n", prog);
      	fprintf (stderr, "Try \"%s --help\" for more information.\n", prog);
	} else {
		fprintf(stdout, "Usage: %s [OPTION]... PATTERN [FILE...]\n"
						"       Search for PATTERN in each \"acceptable\" FILE or standard input.\n"
						"       Example: %s -i 'hello world' file1.c ~/my_project\n"
						"\n"
						"       %s [--esc] [-chs] -f FILE...\n"
						"       Display only the \"acceptable\" files (FILE should be a directory)\n"
						"       Example: %s -f ~/my_project\n"
						"\n"
						"       An \"acceptable\" file is one %p considers as a text file.\n"
						"\n", prog, prog, prog, prog, prog);
		fprintf(stdout, "Acceptable file selection:\n"
						"  -a, --all, --any           accept any kind of file (just like grep)\n"
						"  -c, --code                 C or C++ source code files (any text files with\n"
						"                             the suffix .c, .cc, .cp, .cpp, .cxx, .c++, .m,\n"
						"                             .C, .i, .ii, .y)\n"
						"  -h, --hdrs, --headers      header files (any text file with the suffix\n"
                        "                             .h, .hp, .hpp, .hxx, .ih, .xh, .xih)\n"
						"  -s, --asm                  assembler files (any text file with the suffix\n"
						"                             .s, .S)\n"
						"  -t, --all-text, --any-text accept all text files (default if -c, -h, -s\n"
						"                             and -a are all not specified)\n"
						"\n");
						
		fprintf(stdout, "Miscellaneous:\n"
		                "  -f, --accepted-files       output accepted (text) files (only --esc, -c,\n"
						"                             -h, -s, and -t are allowed with -f)\n"
						"  --esc                      escape blanks in filenames generated with -f\n"
  	  			      	"  --help                     display this help and exit\n"
						"  -l, --follow_links         follow links when searching all files (i.e,\n"
						"                             when -a is specified)\n"
  	  					"\n");
						
		fprintf(stdout, "Regexp selection and interpretation:\n"
 	          	  		"  -E, --extended-regexp      PATTERN is an extended regular expression\n"
						"  -G, --basic-regexp         PATTERN is a basic regular expression\n"
						"  -e, --regexp=PATTERN       use PATTERN as a regular expression\n"
						"  -i, --ignore-case          ignore case distinctions\n"
						"  -w, --word-regexp          force PATTERN to match only whole words\n"
  	  					"\n");
						
		fprintf(stdout, "Output control:\n"
						"  -n, --line-number          print line number with output lines\n"
  	  					"\n");
						
		fprintf(stdout, "Context control:\n"
						"  -B, --before-context=NUM   print NUM lines of leading context\n"
						"  -A, --after-context=NUM    print NUM lines of trailing context\n"
						"  -C, --context[=NUM]        print NUM (default 2) lines of output context\n"
						"                             unless overridden by -A or -B\n"
						"  -NUM                       same as --context=NUM\n"
  	  					"\n");
						
		fprintf(stdout, "With no FILE, or when FILE is -, read standard input.  Exit status\n"
						"is 0 if match, 1 if no match, and 2 if trouble.\n");
	}
	
	exit(status);
}

#ifdef DEBUG    		
static show_options(FILE *f, char *argv[])
{
	fprintf(f, "%s ", prog);
	if (allfiles)
		fprintf(f, "--all(a) ");
	if (all_text_files)
		fprintf(f, "--all-text(t) ");
	if (code_files)
		fprintf(f, "--code(c) ");
	if (hdr_files)
		fprintf(f, "--hdrs(h) ");
	if (asm_files)
		fprintf(f, "--asm(s) ");
	if (follow_links)
		fprintf(f, "--follow_links(l) ");
	if (line_numbers)
		fprintf(f, "--line-number(n) ");
	if (before_context)
		fprintf(f, "--before-context=%ld(B) ", before_context);
	if (after_context)
		fprintf(f, "--after-context=%ld(A) ", after_context);
	if (word_regexp)
		fprintf(f, "--word-regexp(w) ");
	if (ignore_case)
		fprintf(f, "--ignore-case(i) ");
	if (esc)
		fprintf(f, "--esc ");
	if (show_only_accepted_files)
		fprintf(f, "--accepted-files(f) ");
	
	if (pattern) {
		if (saw_e) {
			fprintf(f, "-e ");
			for (;;) {
				while (*pattern && *pattern != '\n')
					fprintf(f, "%c", *pattern++);
				if (!*pattern || (*pattern == '\n' && *++pattern == 0))
					break;
				fprintf(f, " -e ");
			}
			fprintf(f, " ");
		} else
			fprintf(f, "%s ", pattern);
	}
	
	if (*argv)
		while (*argv)
			fprintf(f, "%s ", *argv++);
	else if (!show_only_accepted_files)
		fprintf(f, "< stdin");
	
	fprintf(f, "\n");
	exit(0);
}
#endif		

int main(int argc, char *argv[])
{
  	int 			status, pattern_len, opt, default_context, len, illegal_with_f;
  	unsigned int	digit_args_val;
  	extern char 	*optarg;
  	extern int  	optind;
  
   	prog = argv[0];
  	if (prog && strrchr(prog, '/'))
    	prog = strrchr(prog, '/') + 1;
 
  	no_file = digit_args_val = pattern_len = illegal_with_f = default_context = 0;
	before_context = after_context = -1;
  	pattern = NULL;
	
	//while ((opt = getopt_long_only(argc, argv, short_options, long_options, NULL)) != -1)
	while ((opt = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
	    switch (opt) {
		    case '0':
		    case '1':
		    case '2':
		    case '3':
		    case '4':
		    case '5':
		    case '6':
		    case '7':
		    case '8':
		    case '9':
				digit_args_val = 10 * digit_args_val + opt - '0';
				default_context = digit_args_val;
				illegal_with_f = 1;
				break;
			
		    case 'A':
				if (optarg)
			    	if (ck_atoi (optarg, &after_context))
			      		fatal("invalid context length argument", 0);
				illegal_with_f = 1;
				break;
			
		    case 'B':
				if (optarg)
			    	if (ck_atoi (optarg, &before_context))
			      		fatal("invalid context length argument", 0);
				illegal_with_f = 1;
				break;
			
		    case 'C':
				if (optarg) {
			    	if (ck_atoi (optarg, &default_context))
			      	fatal("invalid context length argument", 0);
			  	}
			  	default_context = 2;
				illegal_with_f = 1;
				break;
				
	     	case 'E':
				matcher = "egrep";
				illegal_with_f = 1;
				break;
				
	      	case 'G':
				matcher = "grep";
				illegal_with_f = 1;
				break;
#if 0			
	      	case 'H':
				no_file = 0;
				illegal_with_f = 1;
				break;
#endif	      	
	      	case 'X':
	      		if (strcmp(optarg, "egrep") != 0 && strcmp(optarg, "grep") != 0)
					fatal("invalid context length argument", 0);
				else
					matcher = optarg;
				illegal_with_f = 1;
				break;
				
			case 'a':
				allfiles = 1;
				//illegal_with_f = 1;
				break;
			
			case 'c':
				code_files = 1;
				break;
			
      		case 'e':
				len = strlen(optarg);
				pattern = xrealloc(pattern, pattern_len + len + 1);
				strcpy(&pattern[pattern_len], optarg);
				pattern_len += len;
				pattern[pattern_len++] = '\n';
				illegal_with_f = 1;
#ifdef DEBUG
				saw_e = 1;
#endif
				break;
			
			case 'f':
				show_only_accepted_files = 1;
				break;
				
			case 'h':
				hdr_files = 1;
				break;
				
      		case 'i':
      		case 'y':
				ignore_case = 1;
				illegal_with_f = 1;
				break;
				
			case 'l':
				follow_links = 1;
				illegal_with_f = 1;
				break;
				
     		case 'n':
				line_numbers = 1;
				illegal_with_f = 1;
				break;
				
			case 's':
				asm_files = 1;
				break;
    		
			case 't':
				all_text_files = 1;
				break;
				
			case 'w':
				word_regexp = 1;
				illegal_with_f = 1;
				break;
				
			case 257:
				esc = 1;
				break;
				
      		case 0:							/* long options without a code */
				break;
      		
      		default:
				usage(NULL, 2);
				break;
		}
  		
  		if (show_help)
    		usage(NULL, 0);
    		
		if (show_only_accepted_files && illegal_with_f)
			usage("illegal options specified with -f (--accepted-files)", 2);
		
		if (follow_links && !(allfiles || all_text_files))
			usage("-l (--follow_links) only allowed with -a (--all, --any) or -t (--all-text, --any-text)", 2);
		
		if (all_text_files && allfiles)
			usage("-a (--all, --any) or -t (--all-text, --any-text) but not both", 2);
			
		if (!code_files && !hdr_files && !asm_files && !allfiles)
			all_text_files = 1;
			
  		if (after_context < 0)
    		after_context = default_context;
  		if (before_context < 0)
    		before_context = default_context;
  		
		if (pattern) {
			if (pattern_len == 0)
   	        	usage("null pattern not allowed", 2);
   	        pattern[pattern_len] = 0;
   	    } else if (!show_only_accepted_files) {
   	    	if (optind < argc) {
   				pattern = argv[optind++];
   	    	} else
				usage("no pattern", 2);
  		}
#if 0  		
  		if (argc - optind > 1)
    		no_file = 0;
#endif
#ifdef DEBUG    		
    	if (debug_options)
    		show_options(stderr, &argv[optind]);
#endif

    	status = 0;
    	
   	  	if (optind < argc)
   	  		status = search2(&argv[optind]);
   	    else if (!show_only_accepted_files) {
   	    	if (egrep(no_file, word_regexp, ignore_case, line_numbers, before_context,
   	    			   after_context, pattern, "", 0) < 0)
   	    		status = 1;
		} else
			usage("pattern not allowed with -f (--accepted-files)", 2);
			
   	  	exit(status);
}
