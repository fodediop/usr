/* 
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ar.h>

#define getopt_long			__getopt_long__
#define getopt_long_only	__getopt_long_only__

#include "egrep_getopt.h"
#include "egrep.h"

static int show_help;
#ifdef DEBUG    		
static int debug_options, saw_e;
#endif

/*
	search [options] [-e] PATTERN [FILE...]	search acceptable files
	search [--esc] [-c] [-h] -s] -f FILE...	list acceptable (c, h, s, all) files
	
	-A n | --after-context=n				show n context lines before
	-a | --all | --any						search any kind of file (just like grep and egrep)
	-B n | --before-context=n				show n context lines after
	-C [n] | --context[=n]					same as -B n -A n (default 2)
	-c | --code								search only c and c++ files
	-E | --extended-regexp					egrep (default)
	-e PATTERN | --regexp=PATTERN			specify PATTERN if it it begins with '-'
	--esc									escape blanks in -f display
	-f | --accepted-files					show only files which search will accept from the
											list (only --esc, -c, -h, and -s allowed with -f)
	-G | --basic-regexp						grep instead of egrep
	-H										show filename
	-h | --hdrs | --headers					search only headers
	--help									display help info
	-i | --ignore-case						ignore case
	-l | --follow-links						follow file links
	-n | --line-number						show line numbers
	-s | --asm								search only asm files
	-t | --all-text | --any-text			search all text files
	-w | --word-regexp						match words (exact match)
*/

static char const short_options[] = "0123456789A:B:C:EGHX:ace:fhilnstwy";
static char *prog;

static struct option long_options[] =
{
  {"accepted-files", no_argument, NULL, 'f'},
  {"after-context", required_argument, NULL, 'A'},
  {"all", no_argument, NULL, 'a'},
  {"all-text", no_argument, NULL, 't'},
  {"any", no_argument, NULL, 'a'},
  {"any-text", no_argument, NULL, 't'},
  {"asm", no_argument, NULL, 's'},
  {"basic-regexp", no_argument, NULL, 'G'},
  {"before-context", required_argument, NULL, 'B'},
  {"code", no_argument, NULL, 'c'},
  {"context", optional_argument, NULL, 'C'},
#ifdef DEBUG 
  {"debug-the_options", no_argument, &debug_options, 1},
#endif
  {"esc", no_argument, NULL, -1},
  {"extended-regexp", no_argument, NULL, 'E'},
  {"follow-links", no_argument, NULL, 'l'},
  {"hdrs", no_argument, NULL, 'h'},
  {"headers", no_argument, NULL, 'h'},
  {"help", no_argument, &show_help, 1},
  {"ignore-case", no_argument, NULL, 'i'},
  {"line-number", no_argument, NULL, 'n'},
  {"regexp", required_argument, NULL, 'e'},
  {"with-filename", no_argument, NULL, 'H'},
  {"word-regexp", no_argument, NULL, 'w'},
  {0, 0, 0, 0}
};

static int outfile, before_context, after_context, word_regexp, ignore_case, line_numbers,
		   code_files, hdr_files, asm_files, show_only_accepted_files, follow_links, esc,
		   allfiles, all_text_files;
static char *pattern;

static char *ftsinfo[] = {
"",
"FTS_D",							/* preorder directory */
"FTS_DC",							/* directory that causes cycles */
"FTS_DEFAULT",						/* none of the above */
"FTS_DNR",							/* unreadable directory */
"FTS_DOT",							/* dot or dot-dot */
"FTS_DP",							/* postorder directory */
"FTS_ERR",							/* error; errno is set */
"FTS_F",							/* regular file */
"FTS_INIT",							/* initialized only */
"FTS_NS",							/* stat(2) failed */
"FTS_NSOK",							/* no stat(2) requested */
"FTS_SL",							/* symbolic link */
"FTS_SLNONE",						/* symbolic link without target */
"FTS_W"								/* whiteout object */
};

static void display(char *buffer, int n)
{
	int i;
	
    if (n > 32) n = 32;
    
    for (i = 0; i < n; ++i)
    	fprintf(stdout, "%.2X ", buffer[i]);
	fprintf(stdout, "\n");
	
    for (i = 0; i < n; ++i)
    	fprintf(stdout, "%-2c ", isascii(buffer[i]) ? buffer[i] : ' ');
	fprintf(stdout, "\n");
}

static int sample_file(char *pathname, char *buffer, int amount, int *has_nulls)
{
	int fd, n, i;
	
    if ((fd = open(pathname, O_RDONLY)) < 0)
    	return (0);
    	
    if ((n = read(fd, (char *)buffer, amount)) == -1) {
    	close(fd);
    	return (0);
    } 
    
    close(fd);
    
    buffer[n] = '\0';
    
	for (i = 0; i < n; ++i)
		if (buffer[i] == 0) {
			break;
		}
		
    *has_nulls = (i < n);
    
    return (n);
}

static char *filter_source_files(FTSENT *entry)
{
    struct stat sb;
    int n, has_nulls;
	unsigned char buffer[128];

    if (entry->fts_info != FTS_F)				/* accept only "regular files" 			*/
    	return (NULL);

    if (*entry->fts_accpath == '.')				/* ignore .files of any kind
    	return (NULL);

    if (lstat(entry->fts_accpath, &sb) != 0)	/* now we have to look a little deeper 	*/
    	return (NULL);

    if (!S_ISREG(sb.st_mode))					/* double check it's a regular file 	*/
    	return (NULL);
    
    /* We have a possible candidate.  It might be an executable file but it might also 	*/
    /* be a script which is acceptable.  So we will sample the first 127 characters of	*/
    /* the file.  Odds are, that if its some kind of archive or binary it will have at	*/
    /* least one 0-byte in that 127.  If it does we treat it as some kind of binary and	*/
    /* reject it.  All others are accepted.  This is probably not fool-proof.  But good	*/
    /* enough for our purposes.															*/
    
	n = sample_file(entry->fts_accpath, buffer, 127, &has_nulls);
	if (!n || has_nulls)						/* has some nulls, so reject it			*/
		return (NULL);
		
	if (sb.st_mode & S_IXUSR)					/* no nulls && executable				*/
	    return (entry->fts_path);				/* most likely it's a script which is ok*/
	
	if (n >= SARMAG && strncmp((char *)buffer, ARMAG, SARMAG) == 0)
		return (NULL);							/* try to filter out archives too		*/
	
	return (entry->fts_path);					/* if it makes it to here, we'll use it	*/
}

static void filter_files(FTSENT *entry)
{
	struct stat sb;
	char   *stat_says = 0;
	int	   i, n, has_nulls;
	unsigned char buffer[128];
	
	switch (entry->fts_info) {
		case FTS_D:					/* preorder directory */
		case FTS_DC:				/* directory that causes cycles */
		case FTS_DEFAULT:			/* none of the above */
		case FTS_DNR:				/* unreadable directory */
		case FTS_DOT:				/* dot or dot-dot */
		case FTS_DP:				/* postorder directory */
		case FTS_ERR:				/* error; errno is set */
		case FTS_F:					/* regular file */
		case FTS_INIT:				/* initialized only */
		case FTS_NS:				/* stat(2) failed */
		case FTS_NSOK:				/* no stat(2) requested */
		case FTS_SL:				/* symbolic link */
		case FTS_SLNONE:			/* symbolic link without target */
		case FTS_W:					/* whiteout object */
					;
	}
	
	if (lstat(entry->fts_accpath, &sb) != 0) {
		fprintf(stdout, "errno = %d\n", errno);
		exit(1);
	}
	
	if (S_ISDIR(sb.st_mode))
		stat_says = "/";
	else if (S_ISLNK(sb.st_mode))
		stat_says = "@";
	else if (S_ISREG(sb.st_mode)) {
		if (sb.st_mode & S_IXUSR) {
			n = sample_file(entry->fts_accpath, buffer, 127, &has_nulls);
		    stat_says = (!n || has_nulls) ? "*" : " (script)";
		} else {
			stat_says = "";
			n = sample_file(entry->fts_accpath, buffer, 127, &has_nulls);
			if (!n || has_nulls)
				stat_says = " (binary?)";
			else if (n >= SARMAG && strncmp((char *)buffer, ARMAG, SARMAG) == 0)
				stat_says = " (archive)";
		}
	}
	
	#if 1
	if ((!*stat_says && *entry->fts_accpath != '.') || strcmp(stat_says, " (script)") == 0)
		fprintf(stdout, "%s\n", entry->fts_path);
	#else
	fprintf(stdout, "%5s  %-10s  %s%s\n", sb.st_size ? "" : "empty",
			 ftsinfo[entry->fts_info], entry->fts_path, stat_says);
	#endif
}

static int search(char **argv)
{
	register FTSENT *entry;
	int 			rval;
	FTS 			*tree;
	char		    *p, *b, buffer[2*MAXPATHLEN];
	
	if ((tree = fts_open(argv, FTS_PHYSICAL/* | FTS_NOSTAT*/, (int (*)())NULL)) == NULL)
		err(1, "ftsopen");
	
	for (rval = 0; (entry = fts_read(tree)) != NULL;) {
		switch (entry->fts_info) {
			case FTS_D:
				continue;
				break;
			case FTS_DP:
				break;
				continue;
			case FTS_DNR:
			case FTS_ERR:
			case FTS_NS:
				(void)fflush(stdout);
				warnx("%s: %s",
				    entry->fts_path, strerror(entry->fts_errno));
				rval = 1;
				continue;
		}
		
		#if 0
		filter_files(entry);
		#else
		p = filter_source_files(entry);
		if (p) {
			b = buffer;
			while (*p) {
				if (*p == ' ')
					*b++ = '\\';
				*b++ = *p++;
			}
			*b = 0;
			fprintf(stdout, "%s\n", buffer);
		}
		#endif
	}
	
	if (errno)
		err(1, "fts_read");
	
	return (rval);
}

/*---------------------------------------------------------------------------------*/

/* Convert STR to a positive integer, storing the result in *OUT.
 * If STR is not a valid integer, return -1 (otherwise 0). 
 */
static int ck_atoi(char const *str, int *out)
{
	char const *p;
	
	for (p = str; *p; p++)
    	if (*p < '0' || *p > '9')
      		return -1;

  	*out = atoi(optarg);
  	
  	return 0;
}

static void fatal(const char *mesg, int errnum)
{
  	if (errnum)
    	fprintf(stderr, "%s: %s: %s\n", prog, mesg, strerror (errnum));
  	else
    	fprintf(stderr, "%s: %s\n", prog, mesg);
  	exit(2);
}

static char *xrealloc(char *ptr, size_t size)
{
	char *result = ptr ? realloc(ptr, size) : malloc(size);
  
  	if (size && !result)
    	fatal ("memory exhausted", 0);
  	
  	return result;
}

static void usage(char *why, int status)
{
	if (status != 0) {
		if (why)
			fprintf(stderr, "%s: %s\n", prog, why);
		fprintf(stderr, "Usage: %s [OPTION]... PATTERN [FILE...]\n", prog);
		fprintf(stderr, "       %s [--esc] [-chs] -f FILE...\n", prog);
      	fprintf (stderr, "Try \"%s --help\" for more information.\n", prog);
	} else {
		fprintf(stdout, "Usage: %s [OPTION]... PATTERN [FILE...]\n"
						"       Search for PATTERN in each \"acceptable\" FILE or standard input.\n"
						"       Example: %s -i 'hello world' file1.c ~/my_project\n"
						"\n"
						"       %s [--esc] [-chs] -f FILE...\n"
						"       Display only the \"acceptable\" files (FILE should be a directory)\n"
						"       Example: %s -f ~/my_project\n"
						"\n"
						"       An \"acceptable\" file is one %p considers as a text file.\n"
						"\n", prog, prog, prog, prog, prog);
		fprintf(stdout, "Acceptable file selection:\n"
						"  -a, --all, --any           accept any kind of file (just like grep)\n"
						"  -c, --code                 C or C++ source code files (any text files with\n"
						"                             the suffix .c, .cc, .cp, .cpp, .cxx, .c++, .m,\n"
						"                             .C, .i, .ii, .y)\n"
						"  -h, --hdrs, --headers      header files (any text file with the suffix\n"
                        "                             .h, .hp, .hpp, .hxx, .ih, .xh, .xih)\n"
						"  -s, --asm                  assembler files (any text file with the suffix\n"
						"                             .s, .S)\n"
						"  -t, --all-text, --any-text accept all text files (-c + -h + -s + any other\n"
						"                             file accepted as text\n"
						"\n");
						
		fprintf(stdout, "Miscellaneous:\n"
		                "  -f, --accepted-files       output accepted (text) files (only --esc, -c,\n"
						"                             -h, and -s are allowed with -f)\n"
						"  --esc                      escape blanks in filenames generated with -f\n"
  	  			      	"  --help                     display this help and exit\n"
						"  -l, --follow_links         follow links when searching all files (i.e,\n"
						"                             when -a is specified)\n"
  	  					"\n");
						
		fprintf(stdout, "Regexp selection and interpretation:\n"
 	          	  		"  -E, --extended-regexp      PATTERN is an extended regular expression\n"
						"  -G, --basic-regexp         PATTERN is a basic regular expression\n"
						"  -e, --regexp=PATTERN       use PATTERN as a regular expression\n"
						"  -i, --ignore-case          ignore case distinctions\n"
						"  -w, --word-regexp          force PATTERN to match only whole words\n"
  	  					"\n");
						
		fprintf(stdout, "Output control:\n"
						"  -n, --line-number          print line number with output lines\n"
						"  -H, --with-filename        print the filename for each match\n"
  	  					"\n");
						
		fprintf(stdout, "Context control:\n"
						"  -B, --before-context=NUM   print NUM lines of leading context\n"
						"  -A, --after-context=NUM    print NUM lines of trailing context\n"
						"  -C, --context[=NUM]        print NUM (default 2) lines of output context\n"
						"                             unless overridden by -A or -B\n"
						"  -NUM                       same as --context=NUM\n"
  	  					"\n");
						
		fprintf(stdout, "With no FILE, or when FILE is -, read standard input.  If less than\n"
  	  		            "two FILEs given, assume -H.  Exit status is 0 if match, 1 if no match,\n"
  	  			        "and 2 if trouble.\n");
	}
	
	exit(status);
}

#ifdef DEBUG    		
static show_options(FILE *f, char *argv[])
{
	fprintf(f, "%s ", prog);
	if (allfiles)
		fprintf(f, "--all(a) ");
	if (all_text_files)
		fprintf(f, "--all-text(t) ");
	if (code_files)
		fprintf(f, "--code(c) ");
	if (hdr_files)
		fprintf(f, "--hdrs(h) ");
	if (asm_files)
		fprintf(f, "--asm(s) ");
	if (follow_links)
		fprintf(f, "--follow_links(l) ");
	if (outfile)
		fprintf(f, "--with-filename(H) ");
	if (line_numbers)
		fprintf(f, "--line-number(n) ");
	if (before_context)
		fprintf(f, "--before-context=%ld(B) ", before_context);
	if (after_context)
		fprintf(f, "--after-context=%ld(A) ", after_context);
	if (word_regexp)
		fprintf(f, "--word-regexp(w) ");
	if (ignore_case)
		fprintf(f, "--ignore-case(i) ");
	if (show_only_accepted_files)
		fprintf(f, "--accepted-files(f) ");
	
	if (pattern) {
		if (saw_e) {
			fprintf(f, "-e ");
			for (;;) {
				while (*pattern && *pattern != '\n')
					fprintf(f, "%c", *pattern++);
				if (!*pattern || (*pattern == '\n' && *++pattern == 0))
					break;
				fprintf(f, " -e ");
			}
			fprintf(f, " ");
		} else
			fprintf(f, "%s ", pattern);
	}
	
	if (*argv)
		while (*argv)
			fprintf(f, "%s ", *argv++);
	else if (!show_only_accepted_files)
		fprintf(f, "< stdin");
	
	fprintf(f, "\n");
	exit(0);
}
#endif		

int main(int argc, char *argv[])
{
  	int 			status, pattern_len, opt, default_context, len, illegal_with_f;
  	unsigned int	digit_args_val;
  	extern char 	*optarg;
  	extern int  	optind;
  
   	prog = argv[0];
  	if (prog && strrchr(prog, '/'))
    	prog = strrchr(prog, '/') + 1;
 
  	outfile = digit_args_val = pattern_len = illegal_with_f = default_context = 0;
	before_context = after_context = -1;
  	pattern = NULL;
	
	while ((opt = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
	    switch (opt) {
		    case '0':
		    case '1':
		    case '2':
		    case '3':
		    case '4':
		    case '5':
		    case '6':
		    case '7':
		    case '8':
		    case '9':
				digit_args_val = 10 * digit_args_val + opt - '0';
				default_context = digit_args_val;
				illegal_with_f = 1;
				break;
			
		    case 'A':
				if (optarg)
			    	if (ck_atoi (optarg, &after_context))
			      		fatal("invalid context length argument", 0);
				illegal_with_f = 1;
				break;
			
		    case 'B':
				if (optarg)
			    	if (ck_atoi (optarg, &before_context))
			      		fatal("invalid context length argument", 0);
				illegal_with_f = 1;
				break;
			
		    case 'C':
				if (optarg) {
			    	if (ck_atoi (optarg, &default_context))
			      	fatal("invalid context length argument", 0);
			  	}
			  	default_context = 2;
				illegal_with_f = 1;
				break;
				
	     	case 'E':
				which_grep = "egrep";
				illegal_with_f = 1;
				break;
				
	      	case 'G':
				which_grep = "grep";
				illegal_with_f = 1;
				break;
			
	      	case 'H':
				outfile = 1;
				illegal_with_f = 1;
				break;
	      	
	      	case 'X':
	      		if (strcmp(optarg, "egrep") != 0 && strcmp(optarg, "grep") != 0)
					fatal("invalid context length argument", 0);
				else
					which_grep = optarg;
				illegal_with_f = 1;
				break;
				
			case 'a':
				allfiles = 1;
				illegal_with_f = 1;
				break;
			
			case 'c':
				code_files = 1;
				break;
			
      		case 'e':
				len = strlen(optarg);
				pattern = xrealloc(pattern, pattern_len + len + 1);
				strcpy(&pattern[pattern_len], optarg);
				pattern_len += len;
				pattern[pattern_len++] = '\n';
				illegal_with_f = 1;
#ifdef DEBUG
				saw_e = 1;
#endif
				break;
			
			case 'f':
				show_only_accepted_files = 1;
				break;
				
			case 'h':
				hdr_files = 1;
				break;
				
      		case 'i':
      		case 'y':
				ignore_case = 1;
				illegal_with_f = 1;
				break;
				
			case 'l':
				follow_links = 1;
				illegal_with_f = 1;
				break;
				
     		case 'n':
				line_numbers = 1;
				illegal_with_f = 1;
				break;
				
			case 's':
				asm_files = 1;
				break;
    		
			case 't':
				all_text_files = 1;
				illegal_with_f = 1;
				break;
				
			case 'w':
				word_regexp = 1;
				illegal_with_f = 1;
				break;
				
			case -1:
				esc = 1;
				break;
				
      		case 0:							/* long options without a code */
				break;
      		
      		default:
				usage(NULL, 2);
				break;
		}
  		
  		if (show_help)
    		usage(NULL, 0);
    		
		if (show_only_accepted_files && illegal_with_f)
			usage("illegal options specified with -f (--accepted-files)", 2);
		
		if (follow_links && !(allfiles || all_text_files))
			usage("-l (--follow_links) only allowed with -a (--all, --any) or -t (--all-text, --any-text)", 2);
		
		if (all_text_files && allfiles)
			usage("-a (--all, --any) or -t (--all-text, --any-text) but not both", 2);
			
  		if (after_context < 0)
    		after_context = default_context;
  		if (before_context < 0)
    		before_context = default_context;
  		
		if (pattern) {
			if (pattern_len == 0)
   	        	usage("null pattern not allowed", 2);
   	        pattern[pattern_len] = 0;
   	    } else if (optind < argc) {
   			pattern = argv[optind++];
   	    } else if (!show_only_accepted_files)
			usage("no pattern", 2);
  		
  		if (argc - optind > 1)
    		outfile = 1;

#ifdef DEBUG    		
    	if (debug_options)
    		show_options(stderr, &argv[optind]);
#endif

    	status = 0;
    	
   	  	if (optind < argc)
   	  		status = search(&argv[optind]);
   	    else if (!show_only_accepted_files) {
   	    	if (egrep(outfile, word_regexp, ignore_case, line_numbers, before_context,
   	    			   after_context, pattern, "", 0) < 0)
   	    		status = 1;
		} else
			usage("pattern not allowed with -f (--accepted-files)", 2);
			
   	  	exit(status);
}
