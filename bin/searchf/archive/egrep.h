/*
    egrep.h - header to call a function implementing an egrep search
    
    Copyright Apple Computer, Inc. 2000
    Ira L. Ruben
*/   
  
#ifndef _EGREP_H_
#define _EGREP_H_

#include <sys/cdefs.h>
__BEGIN_DECLS

extern char *which_grep;			/* preset to "egrep", can be set to "grep" */

extern int egrep __P((int no_file,	  	/* suppress filenname with matches (-h) */
      		      int word_regexp,	  	/* match whole words (-w) */
       		      int ignore_case,	  	/* ignore case (-i) */
       		      int line_numbers,	  	/* output matches with their file line nbr (-n)  */
       		      int before_context, 	/* show n lines of context before match (-B n)  */
       		      int after_context,  	/* show n lines of context after matched (-A n) */
       	 	      char *pattern,	  	/* regular expression pattern  */
       		      char *pathname, int fd)); /* closed filename to grep or open file descr. */
/*
    This is more-or-less functionally to the following egrep command:

    egrep [-w|--word-regexp] [-i|--ignore-case] [-n|--line-number]
	  [-A n|--after-context=n] [-B m|--before-context=m]
	  [-n|--no-filename] --no-messages|-s -r pattern pathname

    No error messages are reported about nonexistent or unreadable files.  Thus the
    explicit --no-messages or -s (the return result indicates errors or success,
    see below).  Since search will always recurs on directories, -r is always
    implied.
    
    Because this is a function a already opened file descriptor can be specified
    instead of a filename.  If a filename is passed it is opened and closed prior
    to return.  If a file descriptor is passed the file is not closed.  The filename
    is always required (for outfile option or if the file is to be opened).  A NULL
    filename implies stdin is to be used.  A fd < 0 implies open the file while a
    fd >= 0 implies uses the specified file descriptor (fd == 0 is stdin as usual).

    The function outputs to stdout the matches for the specified exactly as egrep reports
    them according to the options (big surprise, eh? -- we're using the damn code!).
    But all errors cause a return code to return.  Only "fatal" errors are displayed
    (memory allocation failures, I/O errors).

    The egrep() function can be called repeatedly for searching for patterns over any
    number of files.  However if it is desired to apply the SAME pattern over multiple
    files then the pattern may be passed as NULL after the first call.  There's nothing
    prohibiting passing the pattern each time.  But it's more efficient to pass null
    after the first time if the pattern doesn't change to avoid having to reparse
    the pattern.  If it's not changing, why bother?  So passing NULL is the convention
    used to indicate that the pattern has been previously parsed.

    return = -1		could not open specified pathname (fd was passed < 0)
    	     -2		fatal error occurred and reported to stderr
    	     -3		internal error (?) - could not set up grep parser and execution
    	     >=0	successful matching - n lines matched (possibly 0 matches)
    	     
    The egrep library allows callers to use the full functionality (and speed) of grep
    from their programs.  It is provided as an easier to use (and probably faster) 
    substitute to REGEX(3).  In regex() you pass a single string.  Here entire
    directories are passed.
*/
__END_DECLS

#endif /* _EGREP_H_ */
