/*--------------------------------------------------------------------------------------*
 |                                                                                      |
 |                  SearchCmd.c - Implement a text-only form of [e]grep                 |
 |                                                                                      |
 |                        search [options] [-e] PATTERN [FILE...]                       |
 |                        search [--esc] [-c] [-h] -s] -f FILE...                       |
 |                                                                                      |
 |                                     Ira L. Ruben                                     |
 |                                       07/30/00                                       |
 |                                                                                      |
 |                      Copyright Apple Computer, Inc. 2000-2002                        |
 |                                 All rights reserved.                                 |
 |                                                                                      |
 *--------------------------------------------------------------------------------------*

 NAME
	search - search (code) text files for patterns.
 
 SYNOPSIS 
    	search [options] [-e] PATTERN [FILE...]
    	search [--esc] [-c] [-h] [-s] [-t] [-a] -f FILE...
 
 DESCRIPTION
 	Search is a special form of [e]grep used mainly for searching text files in
	code projects.  While [e]grep will attempt to search all files presented to it
	search only will search text files.  It can be specifically limited to searching
	just C/C++ files, headers, or assembler files.  If the entire project contains
	nothing but source files, search is approximately equivalent to the following
	egrep command:
	
	egrep [-w|--word-regexp] [-i|--ignore-case] [-n|--line-number]
	      [-A n|--after-context=n] [-B m|--before-context=m]
	      [-n|--no-filename] --no-messages|-s -r PATTERN [FILE...]
        
 	No error messages are reported about nonexistent or unreadable files.  Thus the
        explicit --no-messages (-s).  Since search will always recurses on directories,
	-r is always implied.
  
  OPTIONS
	-A NUM, --after-context=NUM		
	       Print NUM lines of trailing context after  matching lines.
	
	-a | --all | --any
 	       Search any kind of file (just like [e]grep).   Files need not
	       be text.  This is added solely for [e]grep compatibility but
	       you should probably just use [e]grep instead.
	
	-B NUM, --before-context=NUM
	       Print NUM lines of leading context before  matching lines.
	 
	-C [NUM], --context[=NUM]
	       Print NUM lines (default 2) of output context.  Same as 
	       -B NUM -A NUM when neither -B nor -C are specified.
	
	-4, --forth
		  Search only Forth and Open Firmware files. These include .of, .fo, .4th,
		  .fas, .fs, .fth .factor
		  
	-c, --code
	      Search only C and C++ files.  These are text files with any one of
	      the extensions .c, .cc, .cp, .cpp, .cxx, .c++, .m, .C, .i, .ii, .y.
	
	-E, --extended-regexp
	      Interpret PATTERN as an extended regular expression (egrep).
	
	-e PATTERN, --regexp=PATTERN
	      Use PATTERN as the pattern; useful to protect patterns beginning with -.
	
	--esc
	      When using --accepted-files (-f) to only display "accepted files" (see
	      below) any blanks in the filenames will be escaped.
	
	-f, --accepted-files
	      Show only the files which search will accept for searching subject
	      to the --code (-c), --hdrs (-h), --asm (-s) and -all-text (text)
	      command.  Only these options and --esc are allowed with -f.
	
	-G, --basic-regexp
	      Interpret PATTERN as a basic regular expression.
       
	-h, --hdrs, --headers
	      Search only header files.  These are text files with any one of
	      the extensions .h, .hp, .hpp, .hxx, .ih, .xh, .xih.
	
	--help
	      Output a brief help message.
	
	-i, --ignore-case
	      Ignore case distinctions in both the PATTERN and the input files.	
	      
	-n, --line-number			show line numbers
	      Prefix each line of output with the line number within its input
	      file.
	      
	-s, --asm				search only asm files
	      Search onlyassembler files.  These are text files with any one of
	      the extensions .s, .S.
       
	-t, --all-text, --any-text
	      Search all text source code files; equivalent to specifying
	      --code (-c), --hdrs (-h), --forth (-4), and --asm (-s).  Any other text file
	      is also accepted.  This is the default when none of these source
	      code options or --all (-a) are not specified.
	
	-w, --word-regexp			match words (exact match)
	      Select  only those lines containing matches that form whole
	      words.  The test is that the matching substring must either
	      be at the beginning of the line, or preceded by a non-word
	      constituent character.   Similarly,  it  must be either at
	      the end of the line or followed by a non-word constituent
              character.  Word-constituent characters are letters, digits,
	      and the underscore.  In other words this is an "exact" word
	      search.
	      
	See the grep(1) for documentation on regular expresions and extended
	regular expression.  Where possible most options have the same meaning
	as [e]grep.  The excptions are -a, -c, -f, -h, -s.  Also the default
	pattern supported is --extended-regexp (-E) instead of --basic-regexp
	(-G).
	
	The text files "accepted" for searching --code (-c), --hdrs (-h), --forth (-4)
	and	--asm (-s) files may be displayed instead of searching for a pattern.
	Hence the second form of search command,
	
	   search [--esc] [-c] [-h] [-s] [-t] [-a] -f FILE...
        
	When -all (-a) is spacified accepted files are any regular files.
	
	If -a is not specified then accepted files are those "text" files with
	the appropriate extension(s), are not link files, archive files, or
	.files.  A rather rudamentary test is done to determine whether a file
	is text; if it doesn't have any nulls in the first 127 bytes of the
	file.  It's not perfect but it's sufficient in most cases.
	
DIAGNOSTICS
       Normally, exit status is 0 if matches were found, and 1 if no matches
       were found.  Exit status is 2 if there were syntax errors in the pattern,
       or other system errors.
*/
/*--------------------------------------------------------------------------------------*
 * Implementation note:                                                                 *
 *    The "ground rules" here are that we do not want to explicitly change the original *
 *    grep sources.  That way we can replace them with newer versions as they become    *
 *    available.  So the Makefile for this command uses a sed script to modify grep.c.  *
 *                                                                                      *
 *    Like this file, grep.c contains the main program, option processing,              *
 *    initialization, and of course the call to start searching.  This file replaces    *
 *    all of that and initiates the start search call.  But to do that we use the sed   *
 *    script to "seal off" the parts of grep.c we don't want and adding a few call      *
 *    backs to add our text filter.                                                     *
 *--------------------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <time.h>
#include <unistd.h>
#include <ar.h>

#include "system.h"			    /* these headers are owned by grep source	*/
#include "getopt.h"			    /* use egrep's getopt() to handle --options	*/
char const *matcher = "egrep";		    /* default to egrem instead of "grep"	*/

#include "grep.h"			    /* hey, why not ?				*/				

/* These are the data defined in grep.c that we need to set. Where not already globally	*/
/* visible (i.e., static) the sed script removes the 'static' attribute for these.	*/

extern void (*compile)();		    /* routine to compile the reg. expr.	*/
extern int out_file;			    /* show filenname with matches		*/
extern int out_line;			    /* output matches with line nbr (-n)	*/
extern int out_before;			    /* lines of leading context			*/
extern int out_after;			    /* lines of trailing context		*/
extern int match_words; 		    /* match whole words (-w)			*/
extern int match_icase; 		    /* ignore case (-i) 			*/
extern int filename_mask;		    /* If 0, output nulls after filenames (-Z)	*/
extern int directories; 		    /* always set to 1 imply -r 	      	*/
extern int errseen;			    /* error reported				*/
extern char *prog;

struct stats {				    /* a little bit of "internal" knowledge   	*/
  struct stats *parent;
  struct stat stat;
};

static struct stats stats_base;		    /* grep.c pushed this thing around		*/

/* Here's the routines in grep.c which we call.  The sed script removed the static	*/
/* attribute in those routines that had them.						*/

extern int setmatcher(char const *m);
extern int install_matcher(char const *name);
extern void error(const char *mesg, int errnum);
extern void fatal(const char *mesg, int errnum);
extern int ck_atoi(char const *str, int *out);
extern int grepfile(char const *file, struct stats *stats);

/*--------------------------------------------------------------------------------------*/
/* Our own globals.									*/

static int no_file, word_regexp, ignore_case, line_numbers, before_context, after_context;
static int code_files, forth_files, hdr_files, asm_files;
static int show_only_accepted_files, follow_links, esc, allfiles, all_text_files;
static char *pattern;

static jmp_buf abort_jmp;
static int in_grep;

static int show_help;
#ifdef DEBUG		
static int debug_options, saw_e;
#endif

/*--------------------------------------------------------------------------------------*/
/* The options tables for getopt().							*/

static char const short_options[] = "0123456789A:B:C:EGHX:ace:fhinstwy";

static struct option long_options[] =
{
    {"accepted-files", no_argument, NULL, 'f'},
    {"after-context", required_argument, NULL, 'A'},
    {"all", no_argument, NULL, 'a'},
    {"all-text", no_argument, NULL, 't'},
    {"any", no_argument, NULL, 'a'},
    {"any-text", no_argument, NULL, 't'},
    {"asm", no_argument, NULL, 's'},
    {"basic-regexp", no_argument, NULL, 'G'},
    {"before-context", required_argument, NULL, 'B'},
    {"code", no_argument, NULL, 'c'},
    {"forth", no_argument, NULL, '4'},
    {"context", optional_argument, NULL, 'C'},
#ifdef DEBUG
    {"debug-the-options", no_argument, &debug_options, 1},
#endif
    {"esc", no_argument, NULL, 257},
    {"extended-regexp", no_argument, NULL, 'E'},
#if 0
    {"follow-links", no_argument, NULL, 'l'},
#endif
    {"hdrs", no_argument, NULL, 'h'},
    {"headers", no_argument, NULL, 'h'},
    {"help", no_argument, &show_help, 1},
    {"ignore-case", no_argument, NULL, 'i'},
    {"line-number", no_argument, NULL, 'n'},
    {"regexp", required_argument, NULL, 'e'},
    {"with-filename", no_argument, NULL, 'H'},
    {"word-regexp", no_argument, NULL, 'w'},
    {0, 0, 0, 0}
};

/*--------------------------------------------------------------------------------------*/
#if 0
/*-------------------------------*
 | fatal - report "fatal" errors |
 *-------------------------------*
 
 We use it for reporting unrecoveralbe errors.  But if grep calls it we longjmp() to
 recover from grep's failure.
 
 Note the fatal() routine in grep.c was removed by the sed script.
*/

void fatal(const char *mesg, int errnum)
{
    if (errnum)
	error(mesg, errnum);
    else
	error(mesg, errnum);

    if (in_grep)
	longjmp(abort_jmp, 1);

    exit(2);
}
#endif

/*--------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------*
 | Tables of source name extensions which we consider as C/C++, Forth, Factor, headers, or assmebler |
 *-------------------------------------------------------------------------------------------*/

static char *code_suffixes[] = {
    ".c", ".cc", ".cp", ".cpp", ".cxx", ".c++", ".m", ".C", ".i", ".ii", ".y",
    NULL
};

static char *forth_suffixes[] = {
  ".of", ".fo", ".4th", ".fth", ".fas", ".fs", ".forth", ".factor",
    NULL
};

static char *hdr_suffixes[] = {
    ".h", ".hp", ".hpp", ".hxx", ".ih", ".xh", ".xih",
    NULL
};

static char *asm_suffixes[] = {
    ".s", ".S",
    NULL
};


/*-------------------------------------------------------------------------*
 | determine_suffix - check to see if a pathname has the desired extension |
 *-------------------------------------------------------------------------*
 
 Determines the language extension for a specified pathname is one defined in
 the allowed_suffixes[] table (one of the above tables).  The function returns 1 if
 it is and 0 if it isn't.
*/
static int determine_suffix(char *pathname, char *allowed_suffixes[])
{
    char *basename, *suffix;
    int  i, len;

    if ((basename = strrchr(pathname, '/')) != NULL)	/* get basename			*/
	basename = pathname + 1;
    else
	basename = pathname;
	
    if ((suffix = strrchr(basename, '.')) == NULL)	/* find extension		*/
	return (0);

    len = strlen(++suffix);
    if (len <= 0)
	return (0);
	
    for (i = 0; allowed_suffixes[i]; ++i)		/* look it up			*/
	if (strncmp(suffix, &allowed_suffixes[i][1], len) == 0)
	    return (1);					/* got it			*/
	
    return (0);						/* not one we're looking for	*/
}

/*--------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*
 | sample_file - take a "small bite" out of the file to see if it has nulls |
 *--------------------------------------------------------------------------*

 A small (amount) sample of the file corresponding the already open file's file
 descriptor (fd) is read into the buffer.  If the sample contains even one null
 has_nulls is set to 1 otherwise it is set to 0.  The number of bytes read is
 returned as the function result.  If 0 is returned there was either a read error
 or the file is empty.
 
 The intent of this function is to distinguish between what we think are binary
 files and text files.  Any file with a null is considered as binary and will
 be rejected.  Similarly any file we cannot read or is empty.
 
 Note the file's current read position is not changed across a call to this routine.
 
 The buffer must be at least amount+1 long.
*/

static int sample_file(int fd, char *buffer, int *has_nulls, int amount)
{
    int   n, i;
    off_t currpos = lseek(fd, 0, SEEK_CUR);
    
    if (currpos < 0)				/* protect current read position	*/
    	return (0);
	
    if ((n = read(fd, (char *)buffer, amount)) == -1) {/* take a nibble out of the file	*/
	(void)lseek(fd, currpos, SEEK_SET);
	return (0);				/* oops!					*/
    }
    
    if (lseek(fd, currpos, SEEK_SET) < 0)	/* reset to previous position		*/
	return (0);				/* huh?					*/
    
    buffer[n] = '\0';

    for (i = 0; i < n; ++i)			/* check for a null			*/
	if (buffer[i] == 0)
	    break;
	
    *has_nulls = (i < n);			/* return whether we found a null	*/
    return (n);					/* and how much we read			*/
}

/*--------------------------------------------------------------------------------------*/

/*-----------------------------------------------*
 | can_be_searched - see if a file is searchable |
 *-----------------------------------------------*

 Returns 1 if there's if we initially think it's searchable and 0 otherwise.  This
 test does not consider whehter the file is a text file or not.  It only checks that
 the file is a "regular file", and if it's a .file, whether those are permitted or
 not according to the dots_ok argument.
 
 Both can_be_searched() and filter_text_files() which follows it classify files
 according to the following classifications:						*/

 typedef enum {				/* File type classifications:			*/
      TEXT_FILE,			/* 	a text file (hurray!)			*/
      BINARY_FILE,			/*	a binary file (yuk)			*/
      SCRIPT_FILE,			/*	a executable text script file		*/
      AR_FILE,				/*	archive file (at least we think it is)	*/
      DOT_FILE,				/*	.file (any kind)			*/
      NOT_S_ISREG			/*	not a regular file			*/
  } Classification;									/*
  
  These things don't come into play too much. But they're good for debugging this junk :-)
*/

static int can_be_searched(char *file, struct stat *sb, Classification *what_is_it, int dots_ok)
{
    if (!S_ISREG(sb->st_mode)) {		/* if not a regular file		*/
	*what_is_it = NOT_S_ISREG;
	return (0);				/* ..we'll reject it			*/
    }
	
    if (!dots_ok) {				/* are .files allowed?			*/
	char *p = strrchr(file, '/');		/* ...no, see if we got one		*/
	if (!p)
	    p = file - 1;
	
	if (p[1] == '.') {			/* ignore .files of any kind		*/
	    *what_is_it = DOT_FILE;
	    return (0);
	}
    }

    return (1); 				/* a searchable file (so far)		*/
}


/*-------------------------------------------------------------*
 | filter_text_files - check to see if the file is a text file |
 *-------------------------------------------------------------*
 
 Returns ptr to pathname if we accept the file (open file descriptor fd) as text and
 NULL if we don't.  The what_is_it Classification is also returned to tell what we
 think the file is.
*/
static char *filter_text_files(char *file, struct stat *sb, Classification *what_is_it, int fd)
{
    int n, has_nulls;
    unsigned char buffer[128];

    if (!can_be_searched(file, sb, what_is_it, 0))
	return (NULL);				/* no possibility of it being searched	*/

    /* We have a possible candidate.  It might be an executable file but it might also	*/
    /* be a script which is acceptable.  So we will sample the first 127 characters of	*/
    /* the file.  Odds are, that if its some kind of archive or binary it will have at	*/
    /* least one 0-byte in that 127.  If it does we treat it as some kind of binary and */
    /* reject it.  All others are accepted.  This is probably not fool-proof.  But good */
    /* enough for our purposes. 							*/

    n = sample_file(fd, buffer, &has_nulls, 127);
    if (!n || has_nulls) {			/* has some nulls, so reject it 	*/
	*what_is_it = BINARY_FILE;		/* most likely a binary file		*/
	return (NULL);
    }

    if (sb->st_mode & S_IXUSR) {		/* no nulls && executable		*/
	*what_is_it = SCRIPT_FILE;		/* most likely it's a script which is ok*/
	return (file);
    }

    if (n >= SARMAG && strncmp((char *)buffer, ARMAG, SARMAG) == 0) {
	*what_is_it = AR_FILE;			/* try to filter out archives too	*/
	return (NULL);
    }

    *what_is_it = TEXT_FILE;
    return (file);				/* if it makes it to here, assume text	*/
}

/*--------------------------------------------------------------------------------------*/

/*--------------------------------------------------*
 | esc_blanks - escape all the blanks in a pathname |
 *--------------------------------------------------*/

static char *esc_blanks(char *pathname)
{
    char	*b;
    static char esc_pathname[2*MAXPATHLEN];

    b = esc_pathname;
    while (*pathname) {
	if (*pathname == ' ')
	    *b++ = '\\';
	*b++ = *pathname++;
    }
    *b = 0;

    return (esc_pathname);
}


/*-----------------------------------------------------*
 | my_filter - filter out files we don't want searched |
 *-----------------------------------------------------*
 
 The sed script edits in this call-back to my_filter() at a point in grep() where
 it is ready to start searching a single file.  This routine takes a look at that
 file to see if it's acceptable according to our options.  We return 1 if it's
 acceptable and 0 if not.  grep() will simply exit with a 0-line match if we
 reject the file.
 
 The file is already open, fd is the file descriptor.  So it's ready for sampling
 by can_be_searched().  The stats struct contains a stat output for the file so
 we can check that as well.
 
 Note that if we have the --accepted-files (-f) option we just display the filename
 from here (possibly with escaped blanks, --esc) and return 0 making grep() think we
 rejected the file so it won't search it.
*/

int my_filter(int fd, char *file, struct stats *stats)
{
    int 	    accept, status;
    char	    *p;
    Classification  what_is_it;

    if (allfiles) {			/* -all (-a) allows most files through		*/
	if (can_be_searched(file, &stats->stat, &what_is_it, 1)) {
	    if (show_only_accepted_files) { /* show the
		fprintf(stdout, "%s\n", esc ? esc_blanks(file) : file);
		return (0);		/* "reject" files if we're only showing names	*/
	    }
	    return (1);			/* accept file					*/
	}
    } else {				/* -c, -h, -s filtering...			*/
	p = filter_text_files(file, &stats->stat, &what_is_it, fd);
	if (p) {			/* if we have a text file...			*/
	    accept = all_text_files;	/* ...-t accepts -c, -h, or -s, and other text	*/
	    if (!accept) {		/* ...if not -t, filter according to options	*/
		if (code_files)
		    accept = determine_suffix(file, code_suffixes);
		if (forth_files)
		    accept = determine_suffix(file, forth_suffixes);
		if (hdr_files)
		    accept = (accept || determine_suffix(file, hdr_suffixes));
		if (asm_files)
		    accept = (accept || determine_suffix(file, asm_suffixes));
	    }
	    
	    if (accept) {		/* if acceptable...				*/
	    	if (!show_only_accepted_files)
	    	    return (1);		/* ...search it if not -f			*/
	    	fprintf(stdout, "%s\n", esc ? esc_blanks(p) : p);
	    }
	}
    }

    return (0);				/* reject file					*/
}


/*---------------------------------------------------*
 | is_link - check to see if the file is a link file |
 *---------------------------------------------------*

 The sed script edits in this call-back to is_link() in grepdir() where its walking
 down a directory's member files.  We get spurious errors attempting to grep link
 files.  So the sed script edits in the call back to skip the link files during the
 walk.  1 is returned to skip the file and 0 to continue on with processing.
 
 If grepdir() processes a link file it will eventually lead to error reports which
 we don't want cluttering up the search results.  We caould also have circular links
 which grepdir() will detect.  But by rejecting links in the first place these can't
 occur either.
 
 Of course if the file is not a link file we continue on with normal processing.
 If there's something wrong it will be cought eventually.
*/
int is_link(char const *file)
{
    struct stat sb;

    return (lstat(file, &sb) == 0 && S_ISLNK(sb.st_mode));
}

/*--------------------------------------------------------------------------------------*/

/*------------------------------*
 | search - main search control |
 *------------------------------*
 
 This is where we initialize grep and process the list of specified files (if any) to
 be searched.  Everything from here to the end of this file is replacement code for
 what is in grep.c.
*/

static int search(char **argv)
{
    int status;

    directories   = 1;
    out_file	  = (no_file == 0);
    match_words   = word_regexp;
    match_icase   = ignore_case;
    out_line	  = line_numbers;
    out_before	  = before_context;
    out_after	  = after_context;
    eolbyte	  = '\n';
    filename_mask = ~0;
	
    /* Initialize grep()...								*/
    
    //if (setjmp(abort_jmp))
	//return (2);

    in_grep = 1;

    if (!install_matcher(matcher) && !install_matcher("default"))
	return (-3);

    (*compile)(pattern, strlen(pattern));

    /* Process each file...								*/
    
    if (*argv)
	while (*argv) {
	    status &= grepfile(strcmp(*argv, "-") == 0 ? NULL : *argv, &stats_base);
	    ++argv;
	}
    else
	status = grepfile(".", &stats_base);

    in_grep = 0;

    if (fclose(stdout) == EOF)
    	error("writing output", errno);

    return (status);
}

/*--------------------------------------------------------------------------------------*/

/*---------------------------*
 | usage - display help info |
 *---------------------------*
 
 Used for command line errors and --help.  Exit with the specified status.
*/

static void usage(char *why, int status)
{
    if (status != 0) {				/* if coommand line error...		*/
	if (why)
	    fprintf(stderr, "%s: %s\n", prog, why);
	fprintf(stderr, "Usage: %s [OPTION]... PATTERN [FILE...]\n", prog);
	fprintf(stderr, "       %s [--esc] [-chs] -f FILE...\n", prog);
	fprintf (stderr, "Try \"%s --help\" for more information.\n", prog);
    } else {					/* --help				*/
	fprintf(stdout, "Usage: %s [OPTION]... PATTERN [FILE...]\n"
			"       Search for PATTERN in each \"acceptable\" FILE or standard input.\n"
			"       Example: %s -i 'hello world' file1.c ~/my_project\n"
			"\n"
			"       %s [--esc] [-chs] -f FILE...\n"
			"       Display only the \"acceptable\" files (FILE should be a directory)\n"
			"       Example: %s -f ~/my_project\n"
			"\n"
			"       An \"acceptable\" file is one %s considers as a text file.\n"
			"\n", prog, prog, prog, prog, prog);
	fprintf(stdout, "Acceptable file selection:\n"
			"  -a, --all, --any           accept any kind of file (just like grep)\n"
			"  -c, --code                 C or C++ source code files (any text files with\n"
			"                             the suffix .c, .cc, .cp, .cpp, .cxx, .c++, .m,\n"
			"                             .C, .i, .ii, .y)\n"
			"  -4, --forth                Forth or OpenFirmware source code files (any text files with\n"
			"                             the suffix .fo, .of, .4th, .fth, .fas .fs .factor\n"
			"  -h, --hdrs, --headers      header files (any text file with the suffix\n"
			"                             .h, .hp, .hpp, .hxx, .ih, .xh, .xih)\n"
			"  -s, --asm                  assembler files (any text file with the suffix\n"
			"                             .s, .S)\n"
			"  -t, --all-text, --any-text accept all text files (default if -c, -h, -s\n"
			"                             and -a are all not specified)\n"
			"\n");
			
	fprintf(stdout, "Miscellaneous:\n"
			"  -f, --accepted-files       output accepted (text) files (only --esc, -c,\n"
			"                             -h, -s, and -t are allowed with -f)\n"
			"  --esc                      escape blanks in filenames generated with -f\n"
			"  --help                     display this help and exit\n"
#if 0			
			"  -l, --follow_links         follow links when searching all files (i.e,\n"
#endif
			"                             when -a is specified)\n"
			"\n");
			
	fprintf(stdout, "Regexp selection and interpretation:\n"
			"  -E, --extended-regexp      PATTERN is an extended regular expression\n"
			"  -G, --basic-regexp         PATTERN is a basic regular expression\n"
			"  -e, --regexp=PATTERN       use PATTERN as a regular expression\n"
			"  -i, --ignore-case          ignore case distinctions\n"
			"  -w, --word-regexp          force PATTERN to match only whole words\n"
			"\n");
			
	fprintf(stdout, "Output control:\n"
			"  -n, --line-number          print line number with output lines\n"
			"\n");
			
	fprintf(stdout, "Context control:\n"
			"  -B, --before-context=NUM   print NUM lines of leading context\n"
			"  -A, --after-context=NUM    print NUM lines of trailing context\n"
			"  -C, --context[=NUM]        print NUM (default 2) lines of output context\n"
			"                             unless overridden by -A or -B\n"
			"  -NUM                       same as --context=NUM\n"
			"\n");
			
	fprintf(stdout, "With no FILE the current directory (\".\") is assumed. When FILE is -,\n"
			"read standard input.  Exit status is 0 if match, 1 if no match, and 2\n"
			"if trouble.\n");
    }

    exit(status);
}


#ifdef DEBUG	
/*------------------------------------------------------*
 | show_options - display what we think the options are |
 *------------------------------------------------------*/

static show_options(FILE *f, char *argv[])
{
    fprintf(f, "%s ", prog);
    if (allfiles)
	fprintf(f, "--all(a) ");
    if (all_text_files)
	fprintf(f, "--all-text(t) ");
    if (code_files)
	fprintf(f, "--code(c) ");
    if (forth_files)
	fprintf(f, "--forth(4) ");
    if (hdr_files)
	fprintf(f, "--hdrs(h) ");
    if (asm_files)
	fprintf(f, "--asm(s) ");
    if (follow_links)
	fprintf(f, "--follow_links(l) ");
    if (line_numbers)
	fprintf(f, "--line-number(n) ");
    if (before_context)
	fprintf(f, "--before-context=%ld(B) ", before_context);
    if (after_context)
	fprintf(f, "--after-context=%ld(A) ", after_context);
    if (word_regexp)
	fprintf(f, "--word-regexp(w) ");
    if (ignore_case)
	fprintf(f, "--ignore-case(i) ");
    if (esc)
	fprintf(f, "--esc ");
    if (show_only_accepted_files)
	fprintf(f, "--accepted-files(f) ");

    if (pattern) {
	if (saw_e) {
	    fprintf(f, "-e ");
	    for (;;) {
		while (*pattern && *pattern != '\n')
		    fprintf(f, "%c", *pattern++);
		if (!*pattern || (*pattern == '\n' && *++pattern == 0))
		    break;
		fprintf(f, " -e ");
	    }
	    fprintf(f, " ");
	} else
	    fprintf(f, "%s ", pattern);
    }

    if (*argv)
	while (*argv)
	    fprintf(f, "%s ", *argv++);
    else if (!show_only_accepted_files)
	fprintf(f, "< stdin");

    fprintf(f, "\n");
    exit(0);
}
#endif	

/*--------------------------------------------------------------------------------------*/

/*--------------------------------*
 | main - main program for search |
 *--------------------------------*/

int main(int argc, char *argv[])
{
    int 	    status, pattern_len, opt, default_context, len, illegal_with_f;
    unsigned int    digit_args_val;
    extern char     *optarg;
    extern int	    optind;

    prog = argv[0];
    if (prog && strrchr(prog, '/'))
	prog = strrchr(prog, '/') + 1;

    no_file = digit_args_val = pattern_len = illegal_with_f = default_context = 0;
    before_context = after_context = -1;
    pattern = NULL;

    //while ((opt = getopt_long_only(argc, argv, short_options, long_options, NULL)) != -1)
    while ((opt = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
	switch (opt) {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
		digit_args_val = 10 * digit_args_val + opt - '0';
		default_context = digit_args_val;
		illegal_with_f = 1;
		break;
	
	    case 'A':
		if (optarg)
		    if (ck_atoi (optarg, &after_context))
			fatal("invalid context length argument", 0);
		illegal_with_f = 1;
		break;
	
	    case 'B':
		if (optarg)
		    if (ck_atoi (optarg, &before_context))
			fatal("invalid context length argument", 0);
		illegal_with_f = 1;
		break;
	
	    case 'C':
		if (optarg) {
		    if (ck_atoi (optarg, &default_context))
		    fatal("invalid context length argument", 0);
		}
		default_context = 2;
		illegal_with_f = 1;
		break;
		
	    case 'E':
		setmatcher("egrep");
		illegal_with_f = 1;
		break;
		
	    case 'G':
		setmatcher("grep");
		illegal_with_f = 1;
		break;
#if 0		
	    case 'H':
		no_file = 0;
		illegal_with_f = 1;
		break;
#endif		
	    case 'X':
		if (strcmp(optarg, "egrep") != 0 && strcmp(optarg, "grep") != 0)
		    fatal("invalid context length argument", 0);
		else
		    setmatcher(optarg);
		illegal_with_f = 1;
		break;
		
	    case 'a':
		allfiles = 1;
		//illegal_with_f = 1;
		break;
	
	    case 'c':
		code_files = 1;
		break;
	
	    case '4':
		forth_files = 1;
		break;
	
	    case 'e':
		len = strlen(optarg);
		pattern = xrealloc(pattern, pattern_len + len + 1);
		strcpy(&pattern[pattern_len], optarg);
		pattern_len += len;
		pattern[pattern_len++] = '\n';
		illegal_with_f = 1;
#ifdef DEBUG
		saw_e = 1;
#endif
		break;
	
	    case 'f':
		show_only_accepted_files = 1;
		break;
		
	    case 'h':
		hdr_files = 1;
		break;
		
	    case 'i':
	    case 'y':
		ignore_case = 1;
		illegal_with_f = 1;
		break;
#if 0		
	    case 'l':
		follow_links = 1;
		illegal_with_f = 1;
		break;
#endif		
	    case 'n':
		line_numbers = 1;
		illegal_with_f = 1;
		break;
		
	    case 's':
		asm_files = 1;
		break;
	
	    case 't':
		all_text_files = 1;
		break;
		
	    case 'w':
		word_regexp = 1;
		illegal_with_f = 1;
		break;
		
	    case 257:
		esc = 1;
		break;
		
	    case 0:			    /* long options without a code */
		break;
	
	    default:
		usage(NULL, 2);
		break;
	} /* switch */
	
    if (show_help)
	usage(NULL, 0);
    
    if (show_only_accepted_files && illegal_with_f)
	usage("illegal options specified with -f (--accepted-files)", 2);
    
    if (follow_links && !(allfiles || all_text_files))
	usage("-l (--follow_links) only allowed with -a (--all, --any) or -t (--all-text, --any-text)", 2);
    
    if (all_text_files && allfiles)
	usage("-a (--all, --any) or -t (--all-text, --any-text) but not both", 2);
    
    if (!code_files && !forth_files && !hdr_files && !asm_files && !allfiles)
	all_text_files = 1;
    
    if (after_context < 0)
	after_context = default_context;
    if (before_context < 0)
	before_context = default_context;
    
    if (pattern) {
	if (pattern_len == 0)
	    usage("null pattern not allowed", 2);
	pattern[pattern_len] = 0;
    } else if (!show_only_accepted_files) {
	if (optind < argc) {
	    pattern = argv[optind++];
	} else
	    usage("no pattern", 2);
    } else
	pattern = "something other than a null ptr";
#if 0	
    if (argc - optind > 1)
	no_file = 0;
#endif
#ifdef DEBUG		
    if (debug_options)
	show_options(stderr, &argv[optind]);
#endif

    status = 0;
    
    if (optind < argc)
	status = search(&argv[optind]);
    else if (!show_only_accepted_files)
	status = search(&argv[optind]);
    else
	usage("pattern not allowed with -f (--accepted-files)", 2);
    
    exit(errseen ? 2 : status);
}
