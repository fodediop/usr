#!/usr/bin/env bash
#
# This script specifies the installation of the search command.  See
# accompaning man page for a complete description of this tool. It is
# invoked as follows:
#
#    source search_startup [--search=path] [--ln=off|path] [--sudo=path]
#
#	--search=path	Specify the pathname for the search tool, i.e., where
#			you put it.   If not specified then the environment
#			variable SEARCH_PATH is used.
#
#	--ln=off|path	The search tool may be placed anywhere.  If it is in
#			a directory which are ahead of /bin in the $PATH
#			command search hierarchy then specify 'off' (or 'no'
#   			or 'none') for the --ln option.
#
#			If the tool is in a directory not specified in $PATH,
#			then a symbolic links for search is created in the
#			specified path *directory* to the path specified
#			by --search (or to the one specified by SEARCH_PATH).
#
#			The default for --ln is the same as specifying
#			--ln /usr/local/bin.  In other words a symbolic link
#			is created in /usr/local/bin to the --search path.
#
#	--sudo=path	Path to the "sudo" tool which is used to create
#			symbolic links in the -ln path in directories owned
#                       by root.  If the --ln path is 'off' or a path for a
#                       directory that doesn't need the extra privleges (a
#                       directory like /usr/local/bin does require "sudo")
#                       then --sudo may be defined as null. The --sudo
#                       option defaults to using the SUDO environment
#                       variable.
#
#       An space may be used instead of '=' between the option and it's argument.
#	All options accepted with either one or two dashes.
#
# Finally, if the SEARCH_PATH environment variable already has a value prior to
# executing this script, they it will NOT be set here unless explicitly overridden
# by the script command line arguments.
#
# Ira L. Ruben
# Copyright Apple Computer, Inc. 2000-2001
# Last updated 5/30/01

##########################################################################################

#-----------------------------------------------------------------------------------#
#
# Set the following variable to where you placed the search tool.
#
search_path=./

#-----------------------------------------------------------------------------------#
#
# Defaults needed for operation of this script.
#
default_ln_path=~/bin				# default --ln path (where to put links)
#sudo_tool=$SUDO				# "sudo" tool for privleged ln's
sudo_tool=/usr/bin/sudo

#===================================================================================#

usage='usage: source search_startup" [--search=path] [--ln=off | path] [--sudo=path]'

null_string="`echo -e '\e0'`"			# a non-null "null" :-)

unset_vars="unset null_string arg next_arg prev_arg param	\
		  usage						\
		  search_path search_path1			\
      	  	  ln_path					\
      	  	  sudo_tool sudo_tool1	 			\
      	  	  default_ln_path				\
      		  unset_vars"
      
for arg; do
    param=
    case $next_arg in
    	-search | --search)
    	    search_path1="$arg"
 	    search_path1="${search_path1:=$null_string}"
 	    param=$search_path1
   	    next_arg=
    	    ;;
    	-ln | --ln)
   	    ln_path="$arg"
	    param=$ln_path
    	    next_arg=
    	    ;;
    	-sudo | --sudo)
   	    sudo_tool1="$arg"
 	    sudo_tool1="${sudo_tool1:=$null_string}"
	    param=$sudo_tool1
    	    next_arg=
    	    ;;
   	*)
    	    case $arg in
	    	-search=* | --search=*)
	    	    search_path1=`echo $arg | sed -e 's/-*search=//'`
	    	    search_path1="${search_path1:=$null_string}"
	     	    param=$search_path1
		    ;;
		-ln=* | --ln=*)
	    	    ln_path=`echo $arg | sed -e 's/-*ln=//'`
	    	    if [ "$ln_path" == "" ]; then
		    	echo "$usage"
		    	echo "-ln reqires 'off' or a non-null pathname"
		    	$unset_vars
		    	return 1
	    	    fi
	     	    param=$ln_path
		    ;;
	    	-sudo=* | --sudo=*)
	    	    sudo_tool1=`echo $arg | sed -e 's/-*sudo=//'`
	    	    sudo_tool1="${sudo_tool1:=$null_string}"
	     	    param=$sudo_tool1
		    ;;
	    	-*)
		    next_arg=$arg
		    ;;
	    	--*)
		    next_arg=$arg
		    ;;
	    	*)
	    	    echo "$usage"
	    	    echo "Unknown option $arg"
	    	    $unset_vars
	    	    return 1
	    	    ;;
   	    esac
    esac
    if [ "${param:0:1}" = "-" ]; then
    	echo "$usage"
    	echo "$prev_arg missing its parameter($arg seen as its parameter)"
    	$unset_vars
    	return 1
    fi
    prev_arg="$arg"
done

if [ "$next_arg" != "" ]; then
    echo "$usage"
    echo "Unknown option $next_arg"
    $unset_vars
    return 1
fi

#
# Existing environment setting overrides the script settings.
#
search_path="${SEARCH_PATH:-$search_path}"

#
# Explicit script parameters override everything
#
search_path="${search_path1:-$search_path}"

ln_path="${ln_path:=$default_ln_path}"
sudo_tool="${sudo_tool1:=$sudo_tool}"

#
# Couldn't used null strings for option values because {$x:-y} constructs require
# the 'x' to be non-null for the overrides to work.  Now we can revert the nulls
# for those options.
#
if [ "$sudo_tool" == "$null_string" ]; then
    sudo_tool=
fi

if false; then
    echo "search_path = \"$search_path\""
    echo "ln_path     = \"$ln_path\""
    echo "sudo_tool = \"$sudo_tool\""
    $unset_vars 
    return
fi

#-----------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------#
#
# Point the following macros to where search is stored.
#
export SEARCH_PATH="$search_path"

#
# Place symbolic links in $ln_path unless requested not to.  Even if we are requested
# we don't recreate a link if it's already there AND points to the same place specified
# by $SEARCH_PATH.
#
if [ "$ln_path" != "off" ] && [ "$ln_path" != "no" ] && [ "$ln_path" != "none" ]; then
    if [ ! -L "$ln_path"/search ] || \
       [ "`command ls -l "$ln_path"/search 2> /dev/null | sed -e 's/^.*[-> ]//'`" != "$SEARCH_PATH" ]; then
    	$sudo_tool /bin/rm -f "$ln_path"/search
    	$sudo_tool ln -s "$SEARCH_PATH" "$ln_path"/search
    fi
fi

$unset_vars

#-----------------------------------------------------------------------------------#
#
# For use with the .userstatup script
#
if [ "`type -t add_help`" = "function" ]; then
#
## Help display for the "myhelp" command defined in .userstartup.
#
search_help()
{
#                                            #                                             1
#        1        2        3        4        5        6         7       8        9         0
#2345678902345678902345678902345678902345678902345678902345678902345678902345678901234567890
cat <<EOF
search [options] [-e] PATTERN [FILE...]      Search project text files for pattern.
search [--esc] [-chsta] -f FILE...           Generate list of acceptable files.
                                               --help option for details.
EOF
}

add_help search_help	     # add to the list of help display functions
fi
