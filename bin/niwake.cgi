#!/bin/sh

# FILE: niwake.cgi
# AUTH: Soren Spies (sspies)
# DATE: 7 Sept 2001
# DESC: call ~public/bin/wakeonlan for remote (unbound) clients

# install this file into /Library/WebServer/CGI-Executables on a netinfo-
# bound web server and set $WAKESERVER on your (unbound or wrong-subnet)
# client machine to the name of the server.  This will enable niwakeonlan
# -http to wake the machines on the subnet containing WAKESERVER.

# there are better ways of doing 'really' 
# export REALLY=~appkit/bin/really
# we used ~appkit/bin/really before niwakeonlan used the broadcast addr

NIWAKE=~public/bin/niwakeonlan

echo "Content-Type: text/html

"

echo "calling $NIWAKE $1"

if $NIWAKE "$1"; then
	echo "niwake suceeded"
else
	echo "niwake failed"
fi
