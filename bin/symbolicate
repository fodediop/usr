#!/usr/bin/ruby   

#
# Symbolicate
# Translates non-symboled backtraces, samples etc into symboled ones using atos.
# Version 2, now in glorious widescreen RubyVision.
#
# Within Apple, documentation is in (~public/doc/symbolicate).
#
# Jamie Montgomerie, jmontgomerie@apple.com
#

class AddressConvertor
    def initialize(symboledBinary)
    	require 'pty'
    	
		#@fromAtos, @toAtos, @pid, fileName = PTY.spawn("/bin/sh -c \"atos -o \\\"/Applications/TextEdit.app/Contents/MacOS/TextEdit\\\"\"")

		# For some reason I can't figure out, RuntimeError exceptions are often
		# thrown if I just invoke atos directly, or even with "sh -c".  
		# I'm running it through an interactive shell instead, because that 
		# seems to be reliable.
		oldPrompt = ""
		# ruby appears to crash if you read or set an environment variable that doesn't exist. 
		if ENV.keys.include? "PS1" 
			oldPrompt = ENV["PS1"]
			ENV["PS1"] = "sh-2.05a$ "
		end
		@fromAtos, @toAtos, @pid, fileName = PTY.spawn("sh")
		if oldPrompt != ""
			ENV["PS1"] = oldPrompt
		end
	
		@toAtos.puts("stty -onlcr -echo echonl")
		@toAtos.puts("atos -o \"#{symboledBinary}\"")
		#@toAtos.puts("atos -o \"/Applications/Stickies.app/Contents/MacOS/Stickies\"")
		from = ""
		while !(from =~ /sh-/)
			from = @fromAtos.readline  # Eat the command prompt.
		end
		from = ""
		while !(from =~ /sh-/)
			from = @fromAtos.readline  # Eat the command prompt.
		end
	
		@toAtos.puts("0x00");		
		firstLine = ""
		while(!(firstLine =~ /.*0x00/)) 
			firstLine = @fromAtos.readline
			# If atos can't be found, we'll get a command prompt.
			if(firstLine =~ /sh-/)
				@toAtos.puts("exit")
				raise "Cannot run atos"
				return nil
			end
			
			# This is the form that atos' errors have.
			if(firstLine =~ /Error:\s*(.*)/)
				@toAtos.puts("exit")
				raise $1
				return nil
			end
		end
    end
    
    def convertAddress(address) 
    	@toAtos.puts(address)
    	@fromAtos.readline  # Eat the blank line.
    	return @fromAtos.readline.chomp
    end

	def convertAddressesInLine(line) 
		return line.gsub(/0[xX][0-9a-fA-F]+/) { |address| convertAddress(address) }
	end
end


def usage
	puts("Usage: symbolicate SYMBOLEDEXECUTABLE")
	puts("  Reads from stdin and tries to convert hex numbers to their symbols from the")
	puts("  symboled version of the executable SYMBOLEDEXECUTABLE.")
	puts("  Note that SYMBOLEDEXECUTABLE must have been build with degugging symbols.")
	puts("  See the documentation for more details and examples.")
end


if(ARGV.length != 1 || ARGV[0] == "-h" || ARGV[0] == "--help")
	usage
	exit
end

begin
	myAddressConvertor = AddressConvertor.new(ARGV[0])
	$stdin.each_line { |line| puts myAddressConvertor.convertAddressesInLine(line) }
# I should probably define my own exception class, but this works.
rescue RuntimeError => caughtException 
	puts("Error: " + caughtException)
end
