#!/bin/tcsh -f

# /etc/rsync-wrapper
# rsync-wrapper shell script.  Mike Bombich, Bowling Green State University
# This script is executed when the root user from a remote machine
# successfully authenticates with a public key.  The privileges
# of that user on this server are limited to the functionality of this script.
# This wrapper will verify that the client is sending an rsync command.
# If it is, then the original, unaltered command is run, if not, 
# an error is returned.

set log = "/Users/admin/rsync.out"

set command = ($SSH_ORIGINAL_COMMAND)
echo $command[1] > $log

#if ($?command) then
#
#else
#	echo "environment variable SSH_ORIGINAL_COMMAND not set"
#	exit 127
#endif

#set command = (rsync --server --sender)

# Make sure the original command is rsync
if ( "$command[1]" != "rsync" ) then
	echo "This wrapper only supports rsync"
	exit 127
endif

# Ensure that --server is on the command line, to enforce running
# rsync in server mode.

set ok = false
foreach arg ($command)
	echo "Arg:" "$arg" >> $log
	if ("$arg" == "--server") then
		set ok = true
	endif
end

# If we're OK, run the rsync server.
if ($ok == "true") then
	/usr/bin/$command
else
	echo "This does not appear to be a valid rsync request"
	exit 127
endif
