#!/usr/bin/perl
#
# findFile
# by Ronnie Misra (rgmisra)
# last updated 5/22/03
# Please contact me if you have any questions/comments.
#
# Uses slocate to quickly find what projects/packages a given file
# comes from. Indexes can be shared between users. However, only
# indexes for Jaguar and Panther are stored in ~public -- all other
# indexes will be stored by default in ~/.findFilePrivate.
#
# Notes on sharing indexes for private build trains:
#
# If you create a file called `~/.findfile', it will be consulted to
# find additional indexes.  If you'd like your generated indexes for
# some particular build trains to go to a specific directory, add that
# directory to `~/.findFile'. Then create a file called `trains' in
# the directory containing a list of build train names (such as
# `Jaguar', `Panther', etc.), one per line.  Optionally, you can also
# create a file called `desc' containing a brief description. If the
# directory is in netinfo, it can be shared between other users if
# they add it to their `~/.findFile' as well.

use strict;
use FileHandle;

my $FFLIB = "~public/lib/findFile";
my $SLOC = shellExpand("$FFLIB/slocatebz2");

my $RC = "~/.findFile";
my $PRIVATELIB = "~/.findFilePrivate";

sub shellExpand ($) {
  # uses the shell to expand ~, ~user in strings
  my $ret = `echo $_[0]`;
  chop $ret;
  return $ret;
}

my %fileHash;			# file -> package,package,...

sub addBom ($$) {
  # adds the list of files from a bom to the in memory hash
  my ($bom, $bname) = @_;
  if ( ! -f $bom ) {
      die "Can't access $bom; try again later.\n";
  }
  my @bomlist = split("\n", `lsbom -s $bom`);
  if ($?) {
      die "Failed to index $bom.\n";
  }
  foreach my $file (@bomlist) {
    $file =~ s/^\.//;		# remove leading dot
    if (!length($file)) {
      next;
    }				# this was just "."
    if ($fileHash{$file}) {
      $fileHash{$file} .= ", $bname";
    } else {
      $fileHash{$file} = $bname;
    }
  }
}

sub buildDB($) {
  # build DB for a given release
  my ($rel) = @_;
  $rel =~ /([^0-9]*)(.*)/;
  my ($train, $ver) = ($1, $2);
  my $dest = getDestLib($train);
  if ($dest ne $FFLIB) {
    print "$train looks like a private build train.\n";
    print "The resulting index will be stored in:\n";
    print "     $dest\n";
  }

  my $dir = "/Network/Servers/seaport/release/Software/$train/Updates/$rel";
  if (! -d $dir ) {
    die "Release $rel not found.\n";
  }

  print "Indexing will take a few minutes.\n";
  print "Would you like to proceed? [Y/n] ";
  my $answer = <STDIN>;
  if ($answer !~ /^y/i && $answer !~ /^\n/) {
    return;
  }

  chdir($dir);
  print "Getting Bom list\n";
  my @boms = split("\n", `find Boms PkgBoms ! -type d`);
  if ($?) {
      die "Can't get BOM list.\n";
  }
  my $tot = $#boms + 1;

  print "\n\n";

  my $i = 0;
  STDOUT->autoflush(1);
  foreach my $bom (@boms) {
    my $percent = int(100 * $i / $tot);
    $i++;
    my $bname = $bom;
    $bname =~ s|^.*/||;
    $bname =~ s/\.bom$/.pkg/;
    print "7[2A[1G[2KProcessing $bname\n";
    print "[2K$percent% of bom list done\n8";
    addBom($bom, $bname);
  }
  print "Indexing...\n";
  my $db = "/tmp/${rel}.ffdb";
  open F, "| $SLOC -U / -o $db";
  F->autoflush(1);

  foreach my $ent (sort(keys %fileHash)) {
    print F "$ent : $fileHash{$ent}\n";
  }
  close F;
  system("bzip2 -9 $db");
  system("mkdir $dest 2>/dev/null");
  system("mv ${db}.bz2 $dest");
  system("chmod a+r $dest/${rel}.ffdb.bz2");
}

my %libHash;			# train -> db lib dir
my %relHash;			# release -> db file

sub parseRC {
  # parse ~/.findFile - get list of releases and db lib dirs
  my ($print) = @_;
  my @libs = ($FFLIB, split("\n", `cat $RC 2>/dev/null`), $PRIVATELIB);
  # rc parsing must be done after FFLIB to make sure that the
  # public trains file doesn't override entries in ~/.findFile

  foreach my $dir (@libs) {
    my @trains = split("\n", `cat $dir/trains 2>/dev/null`);
    if ($#trains < 0) {
      @trains = ("unspecified");
    }				# unknown trains

    foreach my $train (@trains) {
      $libHash{$train} = $dir;
    }

    my @dbs = split("\n", `ls $dir 2>/dev/null | grep ffdb`);
    if ($#dbs < 0) {
      next;
    }

    if ($print) {
      my $desc = `cat $dir/desc 2>/dev/null`;
      chop $desc;
      if (length($desc) < 1) {
	$desc = "Private";
      }
      print "$desc : $dir\n";
      print "(Build Trains: " . join(", ", @trains) . ")\n";
      print "     ";
    }
    my $n = 0;
    foreach my $db (@dbs) {
      my $rel = $db;
      $rel =~ s/\..*//;
      if ($print) {
	print $rel;
	for (my $i = 0; $i < 18 - length($rel); $i++) {
	  print " ";
	}
	$n++;
	if ($n % 4 == 0) {
	  print "\n     ";
	}
      }
      $relHash{$rel} = "$dir/$db";
    }
    if ($print) {
      if ($n % 4 != 0) {
	print "\n";
      }
      print "\n";
    }
  }
  $libHash{"private"} = $PRIVATELIB; # make sure private DBs always go to ~
}

sub getDestLib($) {
  # returns the directory for dbs of a given train
  my $dest = $libHash{$_[0]};
  if (!defined($dest)) {
    $dest = $libHash{"private"};
  }
  return $dest;
}

sub usage {
  # print usage message and exit
  my $BASE = `basename $0`;
  chop $BASE;
  print "Usage: $BASE <release> <slocate arguments>\n";
  print "       $BASE <release> --rebuild\n";
  print << 'EOF';
     <release> can be:
        * a full release name (`Panther7A120')
        * the build version of an already indexed release (`7A120')
        * `-c' to indicate the current build
Valid slocate arguments:
     -n <num>           - Limit the amount of results shown to <num>.
     -i                 - Does a case insensitive search.
     <search string>    - Search the database for the given substring.
     -r <regexp>
     --regexp=<regexp>  - Search the database using a basic POSIX regular
                          expression.
(slocate is by Kevin Lindsay. http://www.geekreview.org/slocate/)

The following releases are already indexed:
EOF
  parseRC(1);			# tell parseRC to be verbose
  exit(0);
}

if (($#ARGV < 1) || ($ARGV[0] eq "--help")) {
  usage();
}

my ($rel, @slargs) = @ARGV;

parseRC();

if ($rel eq "-c") {
  # try to find a db for the current system
  if (`/usr/local/bin/sw_vers` =~ /BuildVersion:\s*(.*)\n/s) {
    $rel = $1;
  } elsif (`/usr/bin/sw_vers` =~ /BuildVersion:\s*(.*)\n/s) {
    $rel = $1;
  } else {
    die "Can't determine current version\n";
  }
}

if ($rel =~ /^[0-9]/) {
  my @rels = grep (/$rel\$/, (keys %relHash));
  if ($#rels < 0) {
    my $train = `~rc/bin/getTrainForBuild --quiet $rel`;
    chomp($train);
    if (-d "/Network/Servers/seaport/release/Software/$train") {
	$rel = "$train$rel";
    } else {
	die "Can't resolve version $rel.\n";
    }
  } elsif ($#rels > 0) {
    die "$rel is not unique:\n     " . join("\n     ", @rels) . "\n";
  } else {
      $rel = $rels[0];
  }
}

if (!defined($relHash{$rel}) || $slargs[0] eq "--rebuild") {
  if ($slargs[0] ne "--rebuild") {
    print "Release $rel is not indexed yet.\n";
  }
  buildDB($rel);		# ask user, then build the db
  parseRC();			# reload the RC file
  if (!defined($relHash{$rel})) { # we didn't rebuild the db!
    die "Indexing of $rel failed.\n";
  }
  if ($slargs[0] eq "--rebuild") {
    exit(0);
  }
}

# run slocate with our database
exec($SLOC, "-d", shellExpand($relHash{$rel}), @slargs) || die "exec slocate: $!\n";
