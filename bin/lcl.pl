#!/bin/env perl -d -w
use Getopt::Simple;

$lib_base = "/Library";
$agents = "$lib_base/LaunchAgents";
$daemons = "$lib_base/LaunchDaemons";

my($options) =
  {
      'help' =>
      {
	 'type'		=> '',
	 'env'		=> '-',
	 'default'	=> '',
	 #		'verbose'	=> '',	# Not needed on every key.
	 'order'		=> 1,
     	},
      'list' =>
      {
	 'type'		=> '=l',			# As per Getopt::Long.
	 'env'		=> 'list text',			# Help text.
	 'default'	=> 'l',	                        # In case $USER is undef.
	 'verbose'	=> 'List agents & daemons',
	 'order'		=> 3,				# Help text sort order.
     	},
      'password' =>
      {
	 'type'		=> '=s',
	 'env'		=> '-',
	 'default'	=> 'password',
	 'verbose'	=> 'Specify the password on the remote machine',
	 'order'		=> 4,
     	},
     };

my($option) = new Getopt::Simple;

if (! $option -> getOptions($options, "Usage: lcl.pl [options]") )
  {
       exit(-1);	# Failure.
      }

print "list: $option->{'switch'}{'list'}. \n";
print "password: $option->{'switch'}{'password'}. \n";

opendir(DIR, $agents) or die "can't opendir $agents: $!";
while (defined($file = readdir(DIR))) {
  print "$agents/$file\n"
}
closedir(DIR);

opendir(DIR, $daemons) or die "can't opendir $daemons: $!";
while (defined($file = readdir(DIR))) {
  print "$daemons/$file\n"
}
closedir(DIR);


