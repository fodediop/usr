#!/usr/bin/perl -w
use strict;

open(PS,"ps -auxjwww|");
my @processes=<PS>;
close PS;
if (grep(/iPhoto.app/,@processes)){
	print "iPhoto is running.  It is too dangerous to move the library.\n";
	print "Quitting $0 now.\n";
	exit;
}
my $pictures="$ENV{'HOME'}/Pictures";
if (-l "$pictures/iPhoto Library"){
	open(LS,"ls -l $pictures/iPhoto\\ Library|");
	my $currentlib=<LS>;
	close LS;
	$currentlib =~ s/.*-> //;
	chomp $currentlib;
	print "The current library is : $currentlib\n";
} else {
	if (-d "$pictures/iPhoto Library"){
		print "The current library has not been named.\n";
		print "Enter a one-word name for it: ";
		my $newname=<STDIN>;
		chomp $newname;
		unless ($newname){
			print "You chose no new name.  Exiting.\n";
			exit;
		}

		print "You have chosen to name the library \'$newname\'. Ok(yes/no)?\n";
		my $response=<STDIN>;
		if ($response =~ /yes/i){
			if (-d "$pictures/$newname"){
				print "A folder or library already exists with that name.  Exiting.\n";
				exit;
			}
			print "Now saving 'iPhoto Library' as \'$newname\'.\n";
			system("mv $pictures/iPhoto\\ Library $pictures/$newname");
		} else {
			exec($0);
		}
	}
}

my @locations=($pictures);
open(DF,"df -k|");
while(<DF>){
	next unless /Volumes/;
	my @parts=split;
	my $newlocation=pop(@parts);
	chomp($newlocation);
	push(@locations,$newlocation);
}

my @librarylist=();
while(@locations){
	my $location=shift(@locations);
	print "Searching for libraries in $location\n";
	open(FIND,"find -x $location  -maxdepth 4 -name 'Library.data' -print 2>/dev/null|");
	while(<FIND>){
		s|/Library.data||;
		chomp;
		push(@librarylist,$_);
	}
}
print "Available libraries are :\n";
my @printlist=@librarylist;
my $locationcounter=0;
while(@printlist){
	my $location=shift(@printlist);
	$locationcounter++;
	print "\t$locationcounter\t$location\n";
}
print "Enter the NUMBER of the library to use, or just return to do nothing: ";
my $newlib=<STDIN>;
chomp($newlib);
exit unless ($newlib =~ m/\d+/);
exit unless (-d $librarylist[$newlib-1]);
if (-l "$pictures/iPhoto Library"){
	unlink "$pictures/iPhoto Library";
}
if (-d "$pictures/iPhoto Library"){
	print "Sorry, current library was not yet named.\n";
	exit;
}
system("cd $pictures;ln -s $librarylist[$newlib-1] iPhoto\\ Library");
print "Library changed to $librarylist[$newlib-1]\n";
system("open /Applications/iPhoto.app");
print "To rename, duplicate or delete a library, go to the Pictures\n";
print "folder with the Finder and do it.\n";
