#!/bin/bash -x
pushd $HOME/factor
if [[ ! -L work ]] ; then
	rm -fr work
	ln -s ../FactorWork work
fi
popd
