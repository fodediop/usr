#!/bin/bash 
# Copy ownership and premissions from one folder to another for matching file names
SRC=$1
DST=$2

cp /dev/null /tmp/notfound
for file in $(find $SRC) ; do
    FILE_PATH=${file#$SRC/*}
    if [[ -e $DST/$FILE_PATH ]] ; then
        PERM=$(stat -f "%Op" ${file})
        USER=$(stat -f "%u" ${file})
        GROUP=$(stat -f "%g" ${file})
        chown $USER $DST/$FILE_PATH; chgrp $GROUP $DST/$FILE_PATH; chmod $PERM $DST/$FILE_PATH
    else
        echo "$SRC/$FILE_PATH" >> /tmp/notfound
    fi
done
