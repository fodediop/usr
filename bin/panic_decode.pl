#!/usr/bin/perl

# -----------------------------------------------------------------

# panic_decode
# author: Kiren Sekar (ksekar@apple.com)
# date: October, 2002

# Script automates kernel extension symbol generation (via kextload), 
# gdb setup, and backtrace/register decoding, resolving kernel panic 
# output to debug symbols.

# -----------------------------------------------------------------


# set up temporary files
# note that for gdb to continue decoding after resolving a "bad" 
# address, each command must be in a separate file.  (Fortunately 
# the backtraces are relatively shallow.)

mkdir("/tmp/bt_decode", 0777);
mkdir("/tmp/bt_decode/symbols", 0777);
mkdir("/tmp/bt_decode/scripts", 0777);
opendir(SCRIPTS, "/tmp/bt_decode/scripts");
@scripts = grep(!/^\.\.?$/,readdir(SCRIPTS));
closedir(SCRIPTS);
foreach $script (@scripts) {
    $script = "/tmp/bt_decode/scripts/" . $script;
}
unlink @scripts;
opendir(SYMBOLS, "/tmp/bt_decode/symbols");
@symbols = grep(!/^\.\.?$/,readdir(SYMBOLS));
closedir(SYMBOLS);
foreach $symbol (@symbols) {
    $symbol = "/tmp/bt_decode/symbols/" . $symbol;
}
unlink @symbols;
unlink "/tmp/bt_decode/gdb_output";
open(SCRIPT0, ">/tmp/bt_decode/scripts/s0");


# get main kernel symbol file
print "Enter path to mach kernel symbol file:\n";
$kpath = <STDIN>;
chop $kpath;

#need to add "\ " parsing for drag-and-drop from finder 
#while (! -e $kpath) { 
#    print "Symbol file ", $kpath, " invalid!  Please try again:\n";
#    $kpath = <STDIN>;
#    chop $kpath;
#}

#generate kernel module symbols

while (1) {
    print "Enter extension and address (ex. \"com.apple.driver.KeyLargoATA(1.0.9f1)@\Ox1e59b000\")\n",
    "RETURN if no more extensions to load:\n";
    $kextID = <STDIN>;
    chop $kextID;
    last unless $kextID;

    @fields = split(/\(.*\)/, $kextID);
    $kextID = join("", @fields);
    @fields = ();

    while (1) {
	print "Enter dependency and address (ex. \"com.apple.iokit.IOATAFamily(1.0.5f1)\@Ox1e56f000\") - (RETURN if none):\n";
	$newDep = <STDIN>;
	chop $newDep;
	last unless $newDep;
	@fields = split(/\(.*\)/, $newDep);
	$newDep = join("", @fields);
	push(@deps, $newDep);
    }
    print "Enter path to extension (relative paths OK):\n";
    $kextP = <STDIN>;
    chop $kextP;

#    while (! -e $kextP) {
#	print "kext ", $kextP, " invalid!  Please try again:\n";
#	$kextP = <STDIN>;
#	chop $kpath;
#    } 



   
    $loadCmd = "kextload -k " . $kpath . " -s " . "/tmp/bt_decode/symbols ";
    
    foreach $dep (@deps) {
	$loadCmd = $loadCmd . "-a " . $dep;
    }
    $loadCmd = $loadCmd . " -a " .$kextID . " " . $kextP;
    
#print "\n\n", $loadCmd, "\n\n";
    print "Generating symbol file...  ";
    system($loadCmd) && die "error generating kext symbols\n";
    print "done.\n";
}

# gdb setup (load symbols, etc.)

print SCRIPT0 "set print asm-demangle on\n";
print SCRIPT0 "file ", $kpath, "\n";

#for manually generated kext symbols...

#$v = "first";
#while (1) {
#    print "Enter path to ", $v, " kernel extension symbol file (RETURN if none):\n";
#    $path = <STDIN>;
#    chop $path;
#    last unless $path;
#    while (! -R $path) {
#	print "Symbol file invalid!  Please try again (RETURN if none):\n";
#	$path = <STDIN>;
#	chop $path;
#	last unless $path;
#    }
#    last unless $path;
#    print GDB_OUT "add-symbol-file ", $path, "\n";
#    $v = "next";
#}

#load script-generated kext symbols

opendir(SYMBOLS, "/tmp/bt_decode/symbols");
@symbols = grep(!/^\.\.?$/,readdir(SYMBOLS));
closedir(SYMBOLS);

foreach $sym (@symbols) {
    print SCRIPT0 "add-symbol-file /tmp/bt_decode/symbols/", $sym, "\n";
}

#decode actual bactrace

print "Enter backtrace addresses to decode, separated by whitespaces:\n";
$allAddrs = <STDIN>;
chop $allAddrs;
@addrs = split(/ /, $allAddrs);

$numScripts = 1;
foreach $addr (@addrs) {
    $fileName = ">/tmp/bt_decode/scripts/s" . $numScripts;
    open(CMD, $fileName) || die "error.";
    print CMD "x/i ", $addr, "-4\n";
    close(CMD);
    $numScripts = $numScripts + 1;
}


print "Enter the address of the PC (or leave blank):  ";
$pc = <STDIN>;
chop $pc;
if ($pc ) {
    $fileName = ">/tmp/bt_decode/scripts/s" . $numScripts;
    open(CMD, $fileName) || die "error.";    
    print CMD "echo *** PC: \n";
    print CMD "x/i " , $pc, "\n";
    close(CMD);
    $numScripts = $numScripts + 1;
}


print "Enter the address of the DAR (or leave blank):  ";
$dar = <STDIN>;
chop $dar;
if ($dar) {
    $fileName = ">/tmp/bt_decode/scripts/s" . $numScripts;
    open(CMD, $fileName) || die "error.";    
    print CMD "echo *** DAR: \n";
    print CMD "x/i " , $dar, "-4\n";
    close(CMD);
    $numScripts = $numScripts + 1;
}

print "Enter the address of the LR (or leave blank):  ";
$lr = <STDIN>;
chop $lr;
if ($lr) {
    $fileName = ">/tmp/bt_decode/scripts/s" . $numScripts;
    open(CMD, $fileName) || die "error.";    
    print CMD "echo *** LR: \n";
    print CMD "x/i " , $lr, "-4\n";
    close(CMD);
    $numScripts = $numScripts + 1;
}

# to quit GDB (batch mode should take care of this...)
$fileName = ">/tmp/bt_decode/scripts/s" . $numScripts;
open(CMD, $fileName) || die "error.";    
print CMD "quit\n";
close(CMD);

opendir(CMDS, "/tmp/bt_decode/scripts");
@cmds = grep(!/^\.\.?$/, readdir(CMDS));
$gdbcmd = "gdb -batch";

foreach $cmd (@cmds) {
    $gdbcmd = $gdbcmd . " -x /tmp/bt_decode/scripts/" . $cmd;
}

$gdbcmd = $gdbcmd . " >& /tmp/bt_decode/gdb_output";

unlink "/tmp/bt_decode/gdb_output";
print "Decoding backtrace... ";
system($gdbcmd) && die "Error running gdb.\n";
print "For full output, see /tmp/bt_decode/gdb_output.\n\n\n\n";

# strip out gdb crap, dump backtrace to terminal
open(LOG, "/tmp/bt_decode/gdb_output") || die "Error reading gdb output\n";
while (<LOG>) {
    if (/0x/) {
	print "$_";
    }
}




print "\n\n";





