#!/usr/bin/perl -w

require IO::Socket;

my $verbose = 0;
my (@cities) = ();
my $detailed = 0;
my $expand = 0;
my $delimited = 0;

my $arg;

while (1) {
	my $arg = shift(@ARGV);
	if (! defined $arg) {
		last;
	}
	if ($arg eq "-h") {
		usage();
	} elsif ($arg eq "-v") {
		$verbose = 1;
	} elsif ($arg eq "-city") {
		my $city = shift @ARGV;
		usage() unless defined $city;
		push(@cities, $city);
	} elsif ($arg eq "-detailed") {
		$detailed = 1;
	} elsif ($arg eq "-expand") {
		$expand = 1;
	} elsif ($arg eq "-delim") {
		$delimited = 1;
	} else {
		usage();
	}
}

my (@report) = &getTrafficReport();
my (@toPrint) = ();
my %maxes;

sub updateMaxes
{
	my $entry = shift;
	foreach (qw(incident time lognumber comment location city)) {
		my $newLen = length $entry->{$_};
		if (! defined $maxes{$_} || $maxes{$_} < $newLen) {
			$maxes{$_} = $newLen;
		}
	}
}

# Do the work...

foreach (sort { $a->{'city'} cmp $b->{'city'} } @report) {
	my $printIt = 0;

	if (! @cities) {
		$printIt = 1;
	} else {
		foreach my $city (@cities) {
			if ($_->{'city'} =~ m/$city/i) {
				$printIt = 1;
			}
		}
	}

	if ($printIt) {
		push(@toPrint, $_);
		updateMaxes($_);
	}
}

if (scalar @toPrint == 0) {
	print "No cities match " . join(", ", @cities) . "\n";
} else {
	my $format;

	if ($delimited) {
		foreach (qw(city time location comment)) {
			$format .= "%s|";
		}
	} else {
		foreach (qw(city time location comment)) {
			$format .= "%-$maxes{$_}.$maxes{$_}s   ";
		}
	}

	foreach (@toPrint) {
		printReportItem($format, $_);
	}
}

sub printReportItem
{
	my $format = shift;
	my $item = shift;
	printf($format,
		  $item->{'city'},
		  $item->{'time'},
		  $item->{'location'},
		  $item->{'comment'});

	if ($detailed) {
		my $detailData = getWebData("cad.chp.ca.gov", 80, "/ii.asp?Center=GGCC&LogNumber=$item->{'lognumber'}");

		my (@bundle) = split(/\r\n\r\n/, $detailData);

		my $data;
		foreach (@bundle) {
			if ($_ =~ m/<body/) {
				$data = $_;
				last;
			}
		}
		$data =~ s/<\/tr>/\n/ig;
		$data =~ s/<BR>/\n/ig;
		$data =~ s/\&nbsp\;/ /ig;

		my $result;
		my $dataLength = length $data;
		my $level = 0;
		
		for (my $ix = 0;  $ix < $dataLength;  $ix = $ix + 1) {
			my $ch = substr($data, $ix, 1);
			
			if ($ch eq "<") {
				$level += 1;
			} elsif ($ch eq ">") {
				$level -= 1;
			} elsif ($level == 0) {
				if ($ch ne "\r") {
					$result .= $ch;
				}
			}
		}

		print join("\n\t| ", split(/\n/, $result));

		print "\n\n";
	}

	print "\n";
}

sub usage
{
	print qq~
traffic: get chp traffic reports

  Usage: traffic [-h] [-v] [-city <name>]* [-detailed] [-expand]

	  -h: 			this help message:
	  -v: 			verbose mode
	  -city <name>:	name of the city to search for. Use quotes
		  				around names with spaces (eg, "San Jose")
	  -detailed:	obtain details on incidents (slow)
	  -expand:		expand CHP mnemonics

  By Steve Zellers, x4-1612
~;		  
	exit(0);
}

sub getWebData
{
	my ($host, $port, $resource) = @_;

	my $sock = IO::Socket::INET->new(PeerAddr => $host,
									 PeerPort => $port,
									 Proto    => 'tcp',
									 Timeout  => 60) || &failure("Can't open socket");
	$sock->autoflush;

	print $sock "GET $resource\r\n\r\n";

	my $buf = "";
	my $n;
	1 while $n = sysread($sock, $buf, 8*1024, length($buf));
	return undef unless defined($n);
	return $buf;
}

sub getTrafficReport()
{
	my $report = getWebData("cad.chp.ca.gov", 80, "/sa.asp?centerin=GGCC");

	&failure("Can't read the traffic report...") if (! defined $report);

	my (@rows) = split(/\<\/tr\>\<tr[^\>]+\>/i, $report);

	my $doneWithHeader = 0;
	
	my @report;

	foreach (@rows) {
		if ($doneWithHeader == 0) {
			if ($_ =~ m/<center>/i) {
				$doneWithHeader = 1;
			}
			next;
		}
		$_ =~ s/\<font[^\>]*\>//ig;
		$_ =~ s/\<\/font\>//ig;
		$_ =~ s/\&nbsp\;//ig;
		$_ =~ s/\<td[^\>]*\>//ig;
		
		if ($verbose) {
			print "\n\n" . '='x40 . "\n";
			print $_;
			print "\n" . '-'x40 . "\n";
		}
		
		my (@cols) = split(/\<\/TD\>/, $_);
		
		if (@cols > 0) {
			my %data;
			my $lognumber = undef;
			my $comment = "<none>";
			
			if ($cols[2] =~ m/LogNumber\=([a-zA-Z0-9]+)/) {
				$lognumber = $1;
			}
			
			if ($cols[2] =~ m/>([^>]+)</) {
				$comment = $1;
			}
			
			$data{'incident'} = $cols[0];
			$data{'time'} = $cols[1];
			$data{'lognumber'} = $lognumber;
			$data{'comment'} = cleanup($comment);
			$data{'location'} = cleanup($cols[3]);
			if ($expand) {
				$data{'location'} = expandMnemonics($data{'location'});
			}
			$data{'city'} = cleanup($cols[4]);
			
			push(@report, \%data);
		}
	}
	return (@report);
}
	
sub failure
{
	my $err = shift;
	die "An error occured: $err\n";
}

sub cleanup
{
	my $s = shift;
	if ($s =~ m/^[ \t]+(.*?)$/) {
		$s = $1;
	}
	return $s;
}

sub expandMnemonics
{
	my $s = shift;
	my %mnemonics = (
					 'NB'			=> "North Bound",
					 'SB'			=> "South Bound",
					 'EB'			=> "East Bound",
					 'WB'			=> "West Bound",
					 'JNO'		=> "Just North Of",
					 'JSO'		=> "Just South Of",
					 'JEO'		=> "Just East Of",
					 'JWO'		=> "Just West Of",
					 'E'			=> "East",
					 'W'			=> "West",
					 'N'			=> "North",
					 'S'			=> "South",
					 'NW'			=> "North West",
					 'SW'			=> "South West",
					 'NE'			=> "North East",
					 'SE'			=> "South East"
					 );

	my $result = "";
	foreach (split(/\s/, uc $s)) {
		if (defined $mnemonics{$_}) {
			$result .= " $mnemonics{$_}";
		} else {
			$result .= " $_";
		}
	}

	return $result;
}

exit 0;

