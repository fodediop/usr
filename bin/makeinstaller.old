#!/bin/sh

# FILE: make-installer
# AUTH: Byron Han (han) based on instructions by Jim Kateley
#	http mods and rewrite by Soren Spies (sspies)
# DATE: 14 Aug 2001
# DESC: Make a bootable partition for MacOSX install
# add progress

# view with tabstops = 4

# set up constants
CDSEEDDIR=PPCSeedCD		# cd.apple.com encodes the image type via directory name
CDDEVDIR=PPCDevCD
CDLOCDIR=PPCLocalCD
DISKTOOLSLEEP=2			# disktool -m never returns
CHEETAHHDIDSLEEP=2		# hdid returns before the mount is made
SYSXFOLDER=System/Library/CoreServices

# "programatic constants"
# DEVENTRYAWKBITS="{ if (NF<=2) next; else print \$1; }"
# MOUNTPOINTAWKBITS="{ if (NF<=2) next; else print substr(\$0, index(\$0,\"/Volumes\"), length(\$0)); }"
DEVENTRYAWKBITS="{ print \$1 }"
# MOUNTPOINTAWKBITS="{ print \$NF }"
MOUNTPOINTAWKBITS="{ print substr(\$0, index(\$0,\"/Volumes\"), length(\$0)); }"

PROGNAME=`basename "$0"`

# error function to clean up after ourselves
die() {
	[ "$1" ] && echo "$PROGNAME failed: $@" >&2
	[ -s "$errfile" ] && echo errors: && cat "$errfile"
	[ "$errfile" ] && rm "$errfile" 2> /dev/null
	[ "$tmpfile" ] && rm "$tmpfile" 2> /dev/null
	if [ "$deventry" ]; then 
		echo ejecting mounted image... 
		hdiutil eject `echo $deventry|sed 's!/dev/\(disk[0-9][0-9]*\)\(s[0-9][0-9]*\)*!\1!'` || echo "You'll need to unmount the mounted disk image before running this script again."
	fi
	exit 1
}

# -----------------------------------------------------------------------------
# argument processing

usage() {
	echo "Usage:	$PROGNAME <opts> <src-images-dir> <inst-vol>" >&2
	echo "	<src-images-dir> can be an http URL; supplied by httpmount even" >&2
	echo "	<opts> are any of:" >&2
	echo "		-reboot   - reboot to the new installer partition" >&2
	echo "		-nobless  - don't bless the installer partition" >&2
	echo "		-eraseUFS <finaldest> - reformat <finaldest> as UFS" >&2
	# eraseUFS gets around the fact that you can't erase-install UFS
	# when you are booted from an installer partition on the same drive
	echo "		-verify   - hdiutil verify the images; abort on failure" >&2
	echo "		-nolocal  - only use seed and developer images" >&2
	echo " 		-seedonly - only use the seed CD" >&2
}

# erasevol requires a newfs command and a volume name
erasevol() {
	[ $# -lt 2 ] && die "someone called erasevol() incorrectly"
	newfs_cmd="$1"
	delvol="$2"

	# add check for Apple_?FS :)
	delvoldev=`df "$delvol"|tail +2|awk "$DEVENTRYAWKBITS"`
	[ "$delvoldev" ] || {
		echo "couldn't find /dev entry for $delvol"
		return 1
	}
	delvolrdev=`echo $delvoldev|sed 's/disk/rdisk/'`
	df $delvoldev|grep "/$" && die "$delvol appears to be the boot volume!"
	# fix check if we ever don't mount bootvol at /

	# okay to erase this volume?
	echo -n "Okay to erase $delvol ($delvoldev) (y/n)? "
	read key
	[ "$key" != "y" ] && echo erase declined && return 1

	echo unmounting and erasing $delvol...
	if [ "$DEBUG" ]; then
		/usr/sbin/disktool -p `basename "$delvoldev"` 0 > $errfile
	else
		/usr/sbin/disktool -p `basename "$delvoldev"` 0 > /dev/null
	fi
	if df $delvol 2>&1 | grep $delvoldev > /dev/null; then
		echo "couldn't unmount $delvol (eject in Finder?)" 
		echo "these processes are to blame:"
		fstat -f "$delvol"
		return 1
	fi
	[ "$CHEETAH" ] && sleep $DISKTOOLSLEEP
	echo $newfs_cmd -v "`basename $delvol`" $delvolrdev
	$newfs_cmd -v "`basename $delvol`" $delvolrdev || newfsFailed=1
	/usr/sbin/disktool -m `basename "$delvoldev"` > /dev/null &

	# disktool badness -- never quits
	echo "sleeping since disktool -m doesn't return..."
	sleep $DISKTOOLSLEEP
	echo "killing disktool ($!)" && kill $!
	[ "$newfsFailed" ] && \
			echo "newfs of $delvoldev failed (is $delvol in use?)" && return 1
	return 0
}

if [ "$1" = "-h" ]; then
	usage
	exit
fi

# support :unselected packages (via env var)

# DiskImages-22 or better required; doesn't work on Cheetah
if [[ `sw_vers|grep BuildVersion|awk '{print $2}'` == 4K* ]]; then
	echo "WARNING: $PROGNAME required serious porting to Cheetah!"
	CHEETAH=1
	#die "doesn't work on Cheetah"
fi

while [ $# -gt 0 ]; do
	case "$1" in
		-debug)
			DEBUG=1;;

		-verify)
			VERIFYIMAGES=1;;

		-reboot)
			[ -x /usr/sbin/bless ] || die "-reboot is pointless on Cheetah; no /usr/sbin/bless"
			[ "$NOBLESS" ] && die "-reboot and -nobless don't play together"
			echo "WARNING: UFS erase-installs on the same device won't work."
			echo "Erase destination now for a UFS install."
			[ "$CHEETAH" ] || echo "(you might try the -eraseUFS option)"
			REBOOTATEND=1;;

		-nobless)
			[ "$REBOOTATEND" ] && die "-reboot and -nobless don't play together"
			NOBLESS=1;;

		-eraseUFS)
			[ "$2" ] || die need volume to erase
			[ "$CHEETAH" ] && echo "WARNING: eraseUFS may break Cheetah"

			vol="$2"	# following also protects '/' bootvol
			[ "$vol" = "/" ] && die "GAH, don't erase the boot volume"
			[[ $vol = */ ]] && vol=`echo $vol|sed 's!/$!!'`
			mount -t ufs | grep "$vol" > /dev/null || die "can only erase mounted UFS volumes"
			erasevol /sbin/newfs "$vol" || die "couldn't erase $2"
			shift	# eat up argument
			;;

		-nolocal)
			[ "$SEEDONLY" ] && echo "WARNING: -seedonly implies -nolocal"
			NOLOCAL=1;;

		-seedonly)
			[ "$NOLOCAL" ] && echo "WARNING: -seedonly implies -nolocal"
			SEEDONLY=1;;

		*)
			if [ "$rootimagedir" ]; then
				instvol="$1"
			else
				rootimagedir="$1"
			fi;;
	esac
	shift
done

if [ -z "$rootimagedir" -o -z "$instvol" ]; then
	echo "not enough arguments (don't forget <dest-vol>)" >&2
	usage
	exit 1
fi

# -----------------------------------------------------------------------------
# check on stuff and define listurls()

# have to be root
if [ `whoami` != root ]; then
        echo you need to be root to run $PROGNAME >&2
        exit 1
fi

# figure out which web-get program we have
WGET=`whence curl`
[ "$WGET" ] || WGET=`which wget`
echo using $WGET
[ "$WGET" ] || die "couldn't find curl or wget in your PATH"

if [[ $WGET = *curl ]]; then
    # curl detected
    WGETCATOPTS="-L -f"
    WGETDLOPTS="-f -C - -O"
else          
    # wget case
    WGETCATOPTS="-nv -O -"
    WGETDLOPTS="-c"
fi

# listurls function
listurls() {
	baseurl="$1"
	[[ "$baseurl" != */ ]] && baseurl="${baseurl}/"
	#echo baseurl: $baseurl >&2
	$WGET $WGETCATOPTS "$baseurl"|sed -n '
			/\[   \]/s!.*HREF="\([^"]*\).*$!'"$baseurl"'\1!p
			/\[DIR\]/s!.*HREF="/\([^"]*\).*$!'"$baseurl"'!p
			/\[DIR\]/s!.*HREF="\([^"]*\).*$!'"$baseurl"'\1!p'
}

# -----------------------------------------------------------------------------

if [ -w $HOME ]; then
	errfile="${TMP:-$HOME}/$PROGNAME.$$"
else
	errfile="${TMP:-/tmp}/$PROGNAME.$$"
fi

instvoldev=`df "$instvol"|tail +2|awk "$DEVENTRYAWKBITS"`
[ "$instvoldev" ] || die "couldn't find /dev entry for $instvol"

# check size of partition
if [ `df -k "$instvol"|tail +2|awk '{ print $2 }'` -lt 1217000 ]; then
	echo "* WARNING: $instvol might not be big enough (1.1+ gig recommended)"
fi

# find images (detect cd.apple.com-style layout)
if [[ "$rootimagedir" = http:* ]]; then
	if listurls "$rootimagedir/$CDSEEDDIR" 2>/dev/null|\
			grep \\.dmg\$ > /dev/null; then
		seedimage=`listurls "${rootimagedir}/$CDSEEDDIR" 2>>"$errfile"|grep dmg`
		devimage=`listurls "${rootimagedir}/$CDDEVDIR" 2>>"$errfile"|grep dmg`
		locimage=`listurls "${rootimagedir}/$CDLOCDIR" 2>>"$errfile"|grep dmg`
	else
		echo "web server doesn't look like cd.apple.com"
		listurls "${rootimagedir}/$CDSEEDDIR" 2>>"$errfile"
		seedimage=`listurls "$rootimagedir" 2>>"$errfile"|grep Seed`
		devimage=`listurls "$rootimagedir" 2>>"$errfile"|grep Dev`
		locimage=`listurls "$rootimagedir" 2>>"$errfile"|grep Loc`
	fi
else
	if [ -d "$rootimagedir/$CDSEEDDIR" ]; then
		echo "detected a cd.apple.com-like layout"
		seedimage=`ls "$rootimagedir/$CDSEEDDIR/"*.dmg`
		devimage=`ls "$rootimagedir/$CDDEVDIR/"*.dmg`
		locimage=`ls "$rootimagedir/$CDLOCDIR/"*.dmg`
	else
		seedimage=`ls "$rootimagedir/"*Seed*dmg`
		devimage=`ls "$rootimagedir/"*Dev*dmg`
		locimage=`ls "$rootimagedir/"*Loc*dmg`
	fi
fi

echo Using images:
[ "$seedimage" ] || die "couldn't find seed image in $rootimagedir"
echo $seedimage
if ! [ "$SEEDONLY" ]; then
	[ "$devimage" ] || die "couldn't find dev image in $rootimagedir"
	echo $devimage
	if ! [ "$NOLOCAL" ]; then
		[ "$locimage" ] || die "couldn't find local pkgs image $rootimagedir"
		echo $locimage
	fi
fi

rm "$errfile" 2> /dev/null		# no longer needed

# we're going to erase that volume you gave us
erasevol /sbin/newfs_hfs $instvol 2> "$errfile" || \
		die "partition required to turn into an installer"

if [ "$VERIFYIMAGES" ]; then
	echo verifying images...
	[ "$seedimage" ]&&hdiutil verify "$seedimage"||die "seed didn't verify"
	[ "$devimage" ]&&hdiutil verify "$devimage"||die "dev pkgs didn't verify"
	[ "$locimage" ]&&hdiutil verify "$locimage"||die "local pkgs didn't verify"
fi

# set up relative paths
tgtpackagesdir="$instvol/System/Installation/Packages"
tgtmpkglist="$tgtpackagesdir/OSInstall.mpkg/Contents/Resources/OSInstall.list"

# -----------------------------------------------------------------------------
# copy over base install packages
startdir="`pwd`"
tmpfile=/var/run/hdid.out

echo "Mounting $seedimage"
hdid "$seedimage" > "$tmpfile" || die "couldn't mount seed image"
[ "$CHEETAH" ] && sleep $CHEETAHHDIDSLEEP	# hack for Cheetah to make the mount

deventry=`head -1 "$tmpfile"|awk "$DEVENTRYAWKBITS"`
# df|grep ^${deventry}
mountpoint=`df|grep ^${deventry}s|awk "$MOUNTPOINTAWKBITS"`
deventry=`df "$mountpoint"|tail +2|awk "$DEVENTRYAWKBITS"`		# Cheetah-compat

/sbin/mount -t hfs -ur -o perm "$deventry" "$mountpoint" || die "can't update mount (deventry: $deventry; mountpoint: $mountpoint; tmpfile: `cat $tmpfile`)"

echo "Copying base install packages"
time ditto -v "$mountpoint" "$instvol" || die ditto of seed CD failed
ditto -rsrcFork "$mountpoint/$SYSXFOLDER/BootX" "$instvol/$SYSXFOLDER/BootX" ||\
		die could not recopy BootX file
# /Developer/Tools/SetFile -t tbxi -c chrp "$instvol/System/Library/CoreServices/BootX" || die SetFile failed
hdiutil eject $deventry > /dev/null
unset deventry

# -----------------------------------------------------------------------------
# copy over the various packages
{ if ! [ "$SEEDONLY" ]; then
	echo $devimage
	if ! [ "$NOLOCAL" ]; then
		echo $locimage
	fi
fi
} |
while read image; do
	echo "Mounting $image"
	hdid "$image" > "$tmpfile" || die "couldn't mount $image"
	[ "$CHEETAH" ] && sleep 2		# hack for Cheetah to make the mount

	deventry=`head -1 "$tmpfile"|awk "$DEVENTRYAWKBITS"`
	mountpoint=`df|grep ^${deventry}s|awk "$MOUNTPOINTAWKBITS"`
	deventry=`df "$mountpoint"|tail +2|awk "$DEVENTRYAWKBITS"`	# :P Cheetah

	echo "Copying packages (and updating mpkg)..."
	cd "$mountpoint" || die "$image isn't mounted"
	cd Packages 2> /dev/null # might fail; fine (eg. loc pkgs)
	ls -d *.pkg|while read pkg; do
		#echo processing $pkg
		time ditto -v $pkg "$tgtpackagesdir/$pkg" || die ditto of $image data failed
		echo $pkg >> "$tgtmpkglist" || die "image missing files?"
	done
	cd "$startdir"
	hdiutil eject $deventry > /dev/null
	unset deventry
done

if [ -z "$NOBLESS" -a -x /usr/sbin/bless ]; then
	echo blessing install partition
	/usr/sbin/bless -folder "$instvol/$SYSXFOLDER" -setOF || die "bless failed ... install partition probably unbootable"

	# only makes sense to reboot if you have blessed
	if [ "$REBOOTATEND" ]; then
		echo rebooting as requested...
		reboot
	else
		echo "WARNING: UFS erase-installs on the same device won't work."
		echo "Erase destination now for a UFS install."
		[ "$CHEETAH" ] || echo "(you might try the -eraseUFS option)"
		echo "All done; you can reboot to the installer now"
		echo 
	fi
else
	echo "Please select $instvol as your Startup Disk and reboot."
	echo 
fi
