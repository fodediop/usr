cat << EOFEOFEOF  | awk 'BEGIN { i=0 } /^.+/{ print "property(readwrite, retain) " $1, $2 ; split($2, a, ";"); name[i]=a[1];  i++ } END { printf("@synthesize "); for (w=0; w < FNR-2; w++) {printf("%s, ", name[w]) } printf("%s;\n", name[FNR-2]) } '
	MediaObject*	theMedia;
	MediaDrive*		drive;
	NSArray*		volumes;

EOFEOFEOF
