'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tklib/tklib/modules/plotchart/plotchart.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2004 Arjen Markus <arjenmarkus@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.1.1.1 2001/11/07 20:51:21 hobbs Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "Plotchart" n 0.9 plotchart "Plotchart"
.BS
.SH "NAME"
Plotchart \- Simple plotting and charting package
.SH "SYNOPSIS"
package require \fBTcl  ?8.3?\fR
.sp
package require \fBPlotchart  ?0.9?\fR
.sp
\fB::Plotchart::createXYPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR\fR
.sp
\fB::Plotchart::createStripchart\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR\fR
.sp
\fB::Plotchart::createPolarPlot\fR \fIw\fR \fIradius_data\fR\fR
.sp
\fB::Plotchart::createIsometricPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR \fIstepsize\fR\fR
.sp
\fB::Plotchart::create3DPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR \fIzaxis\fR\fR
.sp
\fB::Plotchart::createPiechart\fR \fIw\fR\fR
.sp
\fB::Plotchart::createBarchart\fR \fIw\fR \fIxlabels\fR \fIyaxis\fR \fInoseries\fR\fR
.sp
\fB::Plotchart::createHorizontalBarchart\fR \fIw\fR \fIxlabels\fR \fIyaxis\fR \fInoseries\fR\fR
.sp
\fB::Plotchart::createTimechart\fR \fIw\fR \fItime_begin\fR \fItime_end\fR \fInoitems\fR\fR
.sp
\fB$anyplot\fR title \fItext\fR\fR
.sp
\fB$anyplot\fR saveplot \fIfilename\fR\fR
.sp
\fB$anyplot\fR xtext \fItext\fR\fR
.sp
\fB$anyplot\fR ytext \fItext\fR\fR
.sp
\fB$anyplot\fR xconfig \fB-option\fR \fIvalue\fR ...\fR
.sp
\fB$anyplot\fR yconfig \fB-option\fR \fIvalue\fR ...\fR
.sp
\fB$xyplot\fR plot \fIseries\fR \fIxcrd\fR \fIycrd\fR\fR
.sp
\fB$polarplot\fR plot \fIseries\fR \fIradius\fR \fIangle\fR\fR
.sp
\fB$plot3d\fR plotfunc \fIfunction\fR\fR
.sp
\fB$plot3d\fR gridsize \fInxcells\fR \fInycells\fR\fR
.sp
\fB$plot3d\fR plotdata \fIdata\fR\fR
.sp
\fB$plot3d\fR colours \fIfill\fR \fIborder\fR\fR
.sp
\fB$xyplot\fR dataconfig \fIseries\fR \fB-option\fR \fIvalue\fR ...\fR
.sp
\fB$pie\fR plot \fIdata\fR\fR
.sp
\fB$pie\fR colours \fIcolour1\fR \fIcolour2\fR ...\fR
.sp
\fB$barchart\fR plot \fIseries\fR \fIydata\fR \fIcolour\fR\fR
.sp
\fB$barchart\fR plot \fIseries\fR \fIxdata\fR \fIcolour\fR\fR
.sp
\fB$timechart\fR period \fItext\fR \fItime_begin\fR \fItime_end\fR \fIcolour\fR\fR
.sp
\fB$timechart\fR milestone \fItext\fR \fItime\fR \fIcolour\fR\fR
.sp
\fB$timechart\fR vertline \fItext\fR \fItime\fR\fR
.sp
\fB$isoplot\fR plot rectangle \fIx1\fR \fIy1\fR \fIx2\fR \fIy2\fR \fIcolour\fR\fR
.sp
\fB$isoplot\fR plot filled-rectangle \fIx1\fR \fIy1\fR \fIx2\fR \fIy2\fR \fIcolour\fR\fR
.sp
\fB$isoplot\fR plot circle \fIxc\fR \fIyc\fR \fIradius\fR \fIcolour\fR\fR
.sp
\fB$isoplot\fR plot filled-circle \fIxc\fR \fIyc\fR \fIradius\fR \fIcolour\fR\fR
.sp
\fB::Plotchart::viewPort\fR \fIw\fR \fIpxmin\fR \fIpymin\fR \fIpxmax\fR \fIpymax\fR\fR
.sp
\fB::Plotchart::worldCoordinates\fR \fIw\fR \fIxmin\fR \fIymin\fR \fIxmax\fR \fIymax\fR\fR
.sp
\fB::Plotchart::world3DCoordinates\fR \fIw\fR \fIxmin\fR \fIymin\fR \fIzmin\fR \fIxmax\fR \fIymax\fR \fIzmax\fR\fR
.sp
\fB::Plotchart::coordsToPixel\fR \fIw\fR \fIx\fR \fIy\fR\fR
.sp
\fB::Plotchart::coords3DToPixel\fR \fIw\fR \fIx\fR \fIy\fR \fIz\fR\fR
.sp
\fB::Plotchart::polarCoordinates\fR \fIw\fR \fIradmax\fR\fR
.sp
\fB::Plotchart::polarToPixel\fR \fIw\fR \fIrad\fR \fIphi\fR\fR
.sp
\fB::Plotchart::pixelToCoords\fR \fIw\fR \fIx\fR \fIy\fR\fR
.sp
\fB::Plotchart::determineScale\fR \fIxmin\fR \fIxmax\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
Plotchart is a Tcl-only package that focuses on the easy creation of
xy-plots, barcharts and other common types of graphical presentations.
The emphasis is on ease of use, rather than flexibility. The procedures
that create a plot use the entire canvas window, making the layout
of the plot completely automatic.
.PP
This results in the creation of an xy-plot in, say, ten lines of code:
.PP
.nf
    package require Plotchart

    canvas .c -background white -width 400 -height 200
    pack   .c -fill both

    #
    # Create the plot with its x- and y-axes
    #
    set s [::Plotchart::createXYPlot .c {0.0 100.0 10.0} {0.0 100.0 20.0}]

    foreach {x y} {0.0 32.0 10.0 50.0 25.0 60.0 78.0 11.0 } {
        $s plot series1 $x $y
    }

    $s title "Data series"
.fi
.PP
A drawback of the package might be that it does not do any data
management. So if the canvas that holds the plot is to be resized, the
whole plot must be redrawn.
The advantage, though, is that it offers a number of plot and chart
types:
.IP \(bu
XY-plots like the one shown above with any number of data series.
.IP \(bu
Stripcharts, a kind of XY-plots where the horizontal axis is adjusted
automatically. The result is a kind of sliding window on the data
series.
.IP \(bu
Polar plots, where the coordinates are polar instead of cartesian.
.IP \(bu
Isometric plots, where the scale of the coordinates in the two
directions is always the same, i.e. a circle in world coordinates
appears as a circle on the screen.
.sp
You can zoom in and out, as well as pan with these plots (\fINote:\fR
this works best if no axes are drawn, the zooming and panning routines
do not distinguish the axes), using the mouse buttons with the control
key and the arrow keys with the control key.
.IP \(bu
Piecharts, with automatic scaling to indicate the proportions.
.IP \(bu
Barcharts, with either vertical or horizontal bars, stacked bars or
bars side by side.
.IP \(bu
Timecharts, where bars indicate a time period and milestones or other
important moments in time are represented by triangles.
.IP \(bu
3D plots (both for displaying surfaces and 3D bars)
.SH "PLOT CREATION COMMANDS"
You create the plot or chart with one single command and then fill the
plot with data:
.TP
\fB::Plotchart::createXYPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR\fR
Create a new xy-plot.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIxaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the x-axis, in this order.
.TP
\fIyaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the y-axis, in this order.
.RE
.sp
.TP
\fB::Plotchart::createStripchart\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR\fR
Create a new strip chart. The only difference to a regular XY plot is
that the x-axis will be automatically adjusted when the x-coordinate
of a new point exceeds the maximum.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIxaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the x-axis, in this order.
.TP
\fIyaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the y-axis, in this order.
.RE
.sp
.TP
\fB::Plotchart::createPolarPlot\fR \fIw\fR \fIradius_data\fR\fR
Create a new polar plot.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIradius_data\fR list (in)
A 2-element list containing maximum radius and stepsize for the radial
axis, in this order.
.RE
.sp
.TP
\fB::Plotchart::createIsometricPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR \fIstepsize\fR\fR
Create a new isometric plot, where the vertical and the horizontal
coordinates are scaled so that a circle will truly appear as a circle.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIxaxis\fR list (in)
A 2-element list containing minimum, and maximum for the x-axis, in this order.
.TP
\fIyaxis\fR list (in)
A 2-element list containing minimum, and maximum for the y-axis, in this order.
.TP
\fIstepsize\fR float|\fBnoaxes\fR (in)
Either the stepsize used by both axes or the keyword \fBnoaxes\fR to
signal the plot that it should use the full area of the widget, to not
draw any of the axes.
.RE
.sp
.TP
\fB::Plotchart::create3DPlot\fR \fIw\fR \fIxaxis\fR \fIyaxis\fR \fIzaxis\fR\fR
Create a new 3D plot.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIxaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the x-axis, in this order.
.TP
\fIyaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the y-axis, in this order.
.TP
\fIzaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the z-axis, in this order.
.RE
.sp
.TP
\fB::Plotchart::createPiechart\fR \fIw\fR\fR
Create a new piechart.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.RE
.sp
.TP
\fB::Plotchart::createBarchart\fR \fIw\fR \fIxlabels\fR \fIyaxis\fR \fInoseries\fR\fR
Create a new barchart with vertical bars. The horizontal axis will
display the labels contained in the argument \fIxlabels\fR. The number
of series given by \fInoseries\fR determines both the width of the
bars, and the way the series will be drawn.
.sp
If the keyword \fBstacked\fR was specified the series will be drawn
stacked on top of each other. Otherwise each series that is drawn will
be drawn shifted to the right.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIxlabels\fR list (in)
List of labels for the x-axis. Its length also determines the number of
bars that will be plotted per series.
.TP
\fIyaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the y-axis, in this order.
.TP
\fInoseries\fR int|\fBstacked\fR (in)
The number of data series that will be plotted. This has to be an
integer number greater than zero (if \fBstacked\fR is not used).
.RE
.sp
.TP
\fB::Plotchart::createHorizontalBarchart\fR \fIw\fR \fIxlabels\fR \fIyaxis\fR \fInoseries\fR\fR
Create a new barchart with horizontal bars. The vertical axis will
display the labels contained in the argument \fIylabels\fR. The number
of series given by \fInoseries\fR determines both the width of the
bars, and the way the series will be drawn.
.sp
If the keyword \fBstacked\fR was specified the series will be drawn
stacked from left to right. Otherwise each series that is drawn will
be drawn shifted upward.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fIylabels\fR list (in)
List of labels for the y-axis. Its length also determines the number of
bars that will be plotted per series.
.TP
\fIyaxis\fR list (in)
A 3-element list containing minimum, maximum and stepsize for the x-axis, in this order.
.TP
\fInoseries\fR int|\fBstacked\fR (in)
The number of data series that will be plotted. This has to be an
integer number greater than zero (if \fBstacked\fR is not used).
.RE
.sp
.TP
\fB::Plotchart::createTimechart\fR \fIw\fR \fItime_begin\fR \fItime_end\fR \fInoitems\fR\fR
Create a new timechart.
The time axis (= x-axis) goes from \fItime_begin\fR to \fItime_end\fR,
and the vertical spacing is determined by the number of items to plot.
.RS
.TP
\fIw\fR widget (in)
Name of the \fIexisting\fR canvas widget to hold the plot.
.TP
\fItime_begin\fR string (in)
The start time given in a form that is recognised by the \fBclock scan\fR
command (e.g. "1 january 2004").
.TP
\fItime_end\fR string (in)
The end time given in a form that is recognised by the \fBclock scan\fR
command (e.g. "1 january 2004").
.TP
\fInoitems\fR int (in)
Expected/maximum number of items. This determines the vertical
spacing.
.RE
.SH "PLOT METHODS"
Each of the creation commands explained in the last section returns
the name of a new object command that can be used to manipulate the
plot or chart. The subcommands available to a chart command depend on
the type of the chart.
.PP
General subcommands for all types of charts. $anyplot is the command
returned by the creation command:
.TP
\fB$anyplot\fR title \fItext\fR\fR
Specify the title of the whole chart.
.RS
.TP
\fItext\fR string (in)
The text of the title to be drawn.
.RE
.sp
.TP
\fB$anyplot\fR saveplot \fIfilename\fR\fR
Draws the plot into a file, using PostScript.
.RS
.TP
\fIfilename\fR string (in)
Contain the path name of the file to write the plot to.
.RE
.sp
.TP
\fB$anyplot\fR xtext \fItext\fR\fR
Specify the title of the x-axis, for those plots that have a straight
x-axis.
.RS
.TP
\fItext\fR string (in)
The text of the x-axis label to be drawn.
.RE
.sp
.TP
\fB$anyplot\fR ytext \fItext\fR\fR
Specify the title of the y-axis, for those plots that have a straight
y-axis.
.RS
.TP
\fItext\fR string (in)
The text of the y-axis label to be drawn.
.RE
.sp
.TP
\fB$anyplot\fR xconfig \fB-option\fR \fIvalue\fR ...\fR
Set one or more configuration parameters for the x-axis.
The following options are known:
.RS
.TP
\fBformat\fR fmt
The format for the numbers along the axis.
.TP
\fBticklength\fR length
The length of the tickmarks (in pixels).
.TP
\fBticklines\fR boolean
Whether to draw ticklines (\fBtrue\fR) or not (\fBfalse\fR).
.TP
\fBscale\fR scale_data
New scale data for the axis, i.e. a 3-element list containing minimum,
maximum and stepsize for the axis, in this order.
.sp
\fIBeware:\fR Setting this option will clear all data from the plot.
.RE
.sp
.TP
\fB$anyplot\fR yconfig \fB-option\fR \fIvalue\fR ...\fR
Set one or more configuration parameters for the y-axis. This method
accepts the same options and values as the method \fBxconfig\fR.
.PP
\fINote:\fR The commands \fBxconfig\fR and \fByconfig\fR are
currently implemented only for XY-plots
and only the option \fB-format\fR has any effect.
.PP
For \fIxy plots\fR and \fIstripcharts\fR:
.TP
\fB$xyplot\fR plot \fIseries\fR \fIxcrd\fR \fIycrd\fR\fR
Add a data point to the plot.
.RS
.TP
\fIseries\fR string (in)
Name of the data series the new point belongs to.
.TP
\fIxcrd\fR float (in)
X-coordinate of the new point.
.TP
\fIycrd\fR float (in)
Y-coordinate of the new point.
.RE
.PP
For \fIpolar plots\fR:
.TP
\fB$polarplot\fR plot \fIseries\fR \fIradius\fR \fIangle\fR\fR
Add a data point to the polar plot.
.RS
.TP
\fIseries\fR string (in)
Name of the data series the new point belongs to.
.TP
\fIradius\fR float (in)
Radial coordinate of the new point.
.TP
\fIangle\fR float (in)
Angular coordinate of the new point (in degrees).
.RE
.PP
For \fI3D plots\fR:
.TP
\fB$plot3d\fR plotfunc \fIfunction\fR\fR
Plot a function defined over two variables \fBx\fR and \fBy\fR.
The resolution is determined by the set grid sizes (see the method
\fBgridsize\fR for more information).
.RS
.TP
\fIfunction\fR string (in)
Name of the procedure that calculates the z-value for the given x and
y coordinates. The procedure has to accept two float arguments (x is
first argument, y is second) and return a floating-point value.
.RE
.sp
.TP
\fB$plot3d\fR gridsize \fInxcells\fR \fInycells\fR\fR
Set the grid size in the two directions. Together they determine how
many polygons will be drawn for a function plot.
.RS
.TP
\fInxcells\fR int (in)
Number of grid cells in x direction. Has to be an integer number
greater than zero.
.TP
\fInycells\fR int (in)
Number of grid cells in y direction. Has to be an integer number
greater than zero.
.RE
.sp
.TP
\fB$plot3d\fR plotdata \fIdata\fR\fR
Plot a matrix of data.
.RS
.TP
\fIdata\fR list (in)
The data to be plotted. The data has to be provided as a nested list
with 2 levels. The outer list contains rows, drawn in y-direction, and
each row is a list whose elements are drawn in x-direction, for the
columns. Example:
.sp
.nf
    set data {
    {1.0 2.0 3.0}
    {4.0 5.0 6.0}
    }
.fi
.RE
.sp
.TP
\fB$plot3d\fR colours \fIfill\fR \fIborder\fR\fR
Configure the colours to use for polygon borders and inner area.
.RS
.TP
\fIfill\fR color (in)
The colour to use for filling the polygons.
.TP
\fIborder\fR color (in)
The colour to use for the border of the polygons.
.RE
.PP
For \fIxy plots\fR, \fIstripcharts\fR and \fIpolar plots\fR:
.TP
\fB$xyplot\fR dataconfig \fIseries\fR \fB-option\fR \fIvalue\fR ...\fR
Set the value for one or more options regarding the drawing of data of
a specific series.
.RS
.TP
\fIseries\fR string (in)
Name of the data series whose configuration we are changing.
.RE
.sp
The following option are known:
.RS
.TP
\fBcolour\fR c
.TP
\fBcolor\fR c
The colour to be used when drawing the data series.
.TP
\fBtype\fR enum
The drawing mode chosen for the series.
This can be one of \fBline\fR, \fBsymbol\fR, or \fBboth\fR.
.TP
\fBsymbol\fR enum
What kind of symbol to draw. The value of this option is ignored when
the drawing mode \fBline\fR was chosen. This can be one of
\fBplus\fR, \fBcross\fR, \fBcircle\fR, \fBup\fR (triangle
pointing up), \fBdown\fR (triangle pointing down), \fBdot\fR
(filled circle), \fBupfilled\fR or \fBdownfilled\fR (filled
triangles).
.RE
.PP
For \fIpiecharts\fR:
.TP
\fB$pie\fR plot \fIdata\fR\fR
Fill a piechart.
.RS
.TP
\fIdata\fR list (in)
A list of pairs (labels and values). The values determine the relative
size of the circle segments. The labels are drawn beside the circle.
.RE
.TP
\fB$pie\fR colours \fIcolour1\fR \fIcolour2\fR ...\fR
Set the colours to be used.
.RS
.TP
\fIcolour1\fR color (in)
The first colour.
.TP
\fIcolour2\fR color (in)
The second colour, and so on.
.RE
.PP
For \fIvertical barcharts\fR:
.TP
\fB$barchart\fR plot \fIseries\fR \fIydata\fR \fIcolour\fR\fR
Add a data series to a barchart.
.RS
.TP
\fIseries\fR string (in)
Name of the series the values belong to.
.TP
\fIydata\fR list (in)
A list of values, one for each x-axis label.
.TP
\fIcolour\fR color (in)
The colour of the bars.
.RE
.PP
For \fIhorizontal barcharts\fR:
.TP
\fB$barchart\fR plot \fIseries\fR \fIxdata\fR \fIcolour\fR\fR
Add a data series to a barchart.
.RS
.TP
\fIseries\fR string (in)
Name of the series the values belong to.
.TP
\fIxdata\fR list (in)
A list of values, one for each y-axis label.
.TP
\fIcolour\fR color (in)
The colour of the bars.
.RE
.PP
For \fItimecharts\fR:
.TP
\fB$timechart\fR period \fItext\fR \fItime_begin\fR \fItime_end\fR \fIcolour\fR\fR
Add a time period to the chart.
.RS
.TP
\fItext\fR string (in)
The text describing the period.
.TP
\fItime_begin\fR string (in)
Start time of the period.
.TP
\fItime_end\fR string (in)
Stop time of the period.
.TP
\fIcolour\fR color (in)
The colour of the bar (defaults to black).
.RE
.sp
.TP
\fB$timechart\fR milestone \fItext\fR \fItime\fR \fIcolour\fR\fR
Add a \fImilestone\fR (represented as an point-down triangle) to the
chart.
.RS
.TP
\fItext\fR string (in)
The text describing the milestone.
.TP
\fItime\fR string (in)
Time at which the milestone must be positioned.
.TP
\fIcolour\fR color (in)
The colour of the triangle (defaults to black).
.RE
.sp
.TP
\fB$timechart\fR vertline \fItext\fR \fItime\fR\fR
Add a vertical line (to indicate the start of the month for instance)
to the chart.
.RS
.TP
\fItext\fR string (in)
The text appearing at the top (an abbreviation of the
date/time for instance).
.TP
\fItime\fR string (in)
Time at which the line must be positioned.
.RE
.PP
For \fIisometric plots\fR (to be extended):
.TP
\fB$isoplot\fR plot rectangle \fIx1\fR \fIy1\fR \fIx2\fR \fIy2\fR \fIcolour\fR\fR
Plot the outlines of a rectangle.
.RS
.TP
\fIx1\fR float (in)
Minimum x coordinate of the rectangle to be drawn.
.TP
\fIy1\fR float (in)
Minimum y coordinate of the rectangle.
.TP
\fIx2\fR float (in)
Maximum x coordinate of the rectangle to be drawn.
.TP
\fIy2\fR float (in)
Maximum y coordinate of the rectangle.
.TP
\fIcolour\fR color (in)
The colour of the rectangle.
.RE
.sp
.TP
\fB$isoplot\fR plot filled-rectangle \fIx1\fR \fIy1\fR \fIx2\fR \fIy2\fR \fIcolour\fR\fR
Plot a rectangle filled with the given colour.
.RS
.TP
\fIx1\fR float (in)
Minimum x coordinate of the rectangle to be drawn.
.TP
\fIy1\fR float (in)
Minimum y coordinate of the rectangle.
.TP
\fIx2\fR float (in)
Maximum x coordinate of the rectangle to be drawn.
.TP
\fIy2\fR float (in)
Maximum y coordinate of the rectangle.
.TP
\fIcolour\fR color (in)
The colour of the rectangle.
.RE
.sp
.TP
\fB$isoplot\fR plot circle \fIxc\fR \fIyc\fR \fIradius\fR \fIcolour\fR\fR
Plot the outline of a circle.
.RS
.TP
\fIxc\fR float (in)
X coordinate of the circle's centre.
.TP
\fIyc\fR float (in)
Y coordinate of the circle's centre.
.TP
\fIcolour\fR color (in)
The colour of the circle.
.RE
.sp
.TP
\fB$isoplot\fR plot filled-circle \fIxc\fR \fIyc\fR \fIradius\fR \fIcolour\fR\fR
Plot a circle filled with the given colour.
.RS
.TP
\fIxc\fR float (in)
X coordinate of the circle's centre.
.TP
\fIyc\fR float (in)
Y coordinate of the circle's centre.
.TP
\fIcolour\fR color (in)
The colour of the circle.
.RE
.PP
There are a number of public procedures that may be useful in specific
situations: \fIPro memorie\fR.
.SH "COORDINATE TRANSFORMATIONS"
Besides the commands that deal with the plots and charts directly,
there are a number of commands that can be used to convert world
coordinates to pixels and vice versa.
These include:
.TP
\fB::Plotchart::viewPort\fR \fIw\fR \fIpxmin\fR \fIpymin\fR \fIpxmax\fR \fIpymax\fR\fR
Set the viewport for window \fIw\fR. Should be used in cooperation
with \fB::Plotchart::worldCoordinates\fR.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIpxmin\fR float (in)
Left-most pixel coordinate.
.TP
\fIpymin\fR float (in)
Top-most pixel coordinate (remember: the vertical pixel coordinate
starts with 0 at the top!).
.TP
\fIpxmax\fR float (in)
Right-most pixel coordinate.
.TP
\fIpymax\fR float (in)
Bottom-most pixel coordinate.
.RE
.sp
.TP
\fB::Plotchart::worldCoordinates\fR \fIw\fR \fIxmin\fR \fIymin\fR \fIxmax\fR \fIymax\fR\fR
Set the extreme world coordinates for window \fIw\fR. The world
coordinates need not be in ascending order (i.e. xmin can be larger
than xmax, so that a reversal of the x-axis is achieved).
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIxmin\fR float (in)
X-coordinate to be mapped to left side of viewport.
.TP
\fIymin\fR float (in)
Y-coordinate to be mapped to bottom of viewport.
.TP
\fIxmax\fR float (in)
X-coordinate to be mapped to right side of viewport.
.TP
\fIymax\fR float (in)
Y-coordinate to be mapped to top side of viewport.
.RE
.sp
.TP
\fB::Plotchart::world3DCoordinates\fR \fIw\fR \fIxmin\fR \fIymin\fR \fIzmin\fR \fIxmax\fR \fIymax\fR \fIzmax\fR\fR
Set the extreme three-dimensional world coordinates for window
\fIw\fR. The world coordinates need not be in ascending order (i.e. xmin
can be larger than xmax, so that a reversal of the x-axis is
achieved).
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIxmin\fR float (in)
X-coordinate to be mapped to front side of the 3D viewport.
.TP
\fIymin\fR float (in)
Y-coordinate to be mapped to left side of the viewport.
.TP
\fIzmin\fR float (in)
Z-coordinate to be mapped to bottom of viewport.
.TP
\fIxmax\fR float (in)
X-coordinate to be mapped to back side of viewport.
.TP
\fIymax\fR float (in)
Y-coordinate to be mapped to right side of viewport.
.TP
\fIzmax\fR float (in)
Z-coordinate to be mapped to top side of viewport.
.RE
.sp
.TP
\fB::Plotchart::coordsToPixel\fR \fIw\fR \fIx\fR \fIy\fR\fR
Return a list of pixel coordinates valid for the given window.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIx\fR float (in)
X-coordinate to be mapped.
.TP
\fIy\fR float (in)
Y-coordinate to be mapped.
.RE
.sp
.TP
\fB::Plotchart::coords3DToPixel\fR \fIw\fR \fIx\fR \fIy\fR \fIz\fR\fR
Return a list of pixel coordinates valid for the given window.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIx\fR float (in)
X-coordinate to be mapped.
.TP
\fIy\fR float (in)
Y-coordinate to be mapped.
.TP
\fIy\fR float (in)
Z-coordinate to be mapped.
.RE
.sp
.TP
\fB::Plotchart::polarCoordinates\fR \fIw\fR \fIradmax\fR\fR
Set the extreme polar coordinates for window \fIw\fR. The angle always
runs from 0 to 360 degrees and the radius starts at 0. Hence you only
need to give the maximum radius.
\fINote:\fR If the viewport is not square, this procedure will not
adjust the extremes, so that would result in an elliptical plot. The
creation routine for a polar plot always determines a square viewport.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIradmax\fR float (in)
Maximum radius.
.RE
.sp
.TP
\fB::Plotchart::polarToPixel\fR \fIw\fR \fIrad\fR \fIphi\fR\fR
Wrapper for a call to \fB::Plotchart::coordsToPixel\fR, which assumes
the world coordinates and viewport are set appropriately. Converts
polar coordinates to pixel coordinates.
\fINote:\fR To be useful it should be accompanied by a matching
\fB::Plotchart::worldCoordinates\fR procedure. This is automatically
taken care of in the creation routine for polar plots.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIrad\fR float (in)
Radius of the point.
.TP
\fIphi\fR float (in)
Angle to the positive x-axis.
.RE
.sp
.TP
\fB::Plotchart::pixelToCoords\fR \fIw\fR \fIx\fR \fIy\fR\fR
Return a list of world coordinates valid for the given window.
.RS
.TP
\fIw\fR widget (in)
Name of the window (canvas widget) in question.
.TP
\fIx\fR float (in)
X-pixel to be mapped.
.TP
\fIy\fR float (in)
Y-pixel to be mapped.
.RE
.PP
Furthermore there is a routine to determine "pretty" numbers for use
with an axis:
.TP
\fB::Plotchart::determineScale\fR \fIxmin\fR \fIxmax\fR\fR
Determine "pretty" numbers from the given range and return a list
containing the minimum, maximum and stepsize that can be used for a
(linear) axis.
.RS
.TP
\fIxmin\fR float (in)
Rough minimum value for the scaling
.TP
\fIxmax\fR float (in)
Rough maximum value for the scaling.
.RE
.SH "OTHER OUTPUT FORMATS"
Besides output to the canvas on screen, the module is capable, via
\fBcanvas postscript\fR, of producing PostScript files. One may wonder
whether it is possible to extend this set of output formats and the
answer is "yes". This section tries to sum up the aspects of using this
module for another sort of output.
.PP
One way you can create output files in a different format, is by
examining the contents of the canvas after everything has been drawn and
render that contents in the right form. This is probably the easiest
way, as it involves nothing more than the re-creation of all the
elements in the plot that are already there.
.PP
The drawback of that method is that you need to have a display, which is
not always the case if you run a CGI server or something like that.
.PP
An alternative is to emulate the canvas command. For this to work, you
need to know which canvas subcommands are used and what for. Obviously,
the \fIcreate\fR subcommand is used to create the lines, texts and
other items. But also the \fIraise\fR and \fIlower\fR subcommands are
used, because with these the module can influence the drawing order -
important to simulate a clipping rectangle around the axes. (The routine
DrawMask is responsible for this - if the output format supports proper
clipping areas, then a redefinition of this routine might just solve
this).
.PP
Furthermore, the module uses the \fIcget\fR subcommand to find out the
sizes of the canvas. A more mundane aspect of this is that the module
currently assumes that the text is 14 pixels high and that 80 pixels in
width suffice for the axis' labels. No "hook" is provided to customise
this.
.PP
In summary:
.IP \(bu
Emulate the \fIcreate\fR subcommand to create all the items in the
correct format
.IP \(bu
Emulate the \fIcget\fR subcommand for the options -width and -height to
allow the correct calculation of the rectangle's position and size
.IP \(bu
Solve the problem of \fIraising\fR and \fIlowering\fR the items so
that they are properly clipped, for instance by redefining the
routine DrawMask.
.IP \(bu
Take care of the currently fixed text size properties
.SH "ROOM FOR IMPROVEMENT"
In this version there are a lot of things that still need to
be implemented:
.IP \(bu
General options like legends and text to the axes.
.IP \(bu
More robust handling of incorrect calls (right now the procedures may
fail when called incorrectly):
.RS
.IP \(bu
The axis drawing routines can not handle inverse axes right now.
.IP \(bu
If the user provides an invalid date/time string, the routines simply
throw an error.
.RE
.SH "TODO - SOME PRIVATE NOTES"
I have the following wishlist:
.IP \(bu
Isometric plots - allow new items to be implemented easily.
.IP \(bu
Add support for histograms where the independent axis is numerical.
.IP \(bu
A general 3D viewer - emphasis on geometry, not a ray-tracer.
.SH "KEYWORDS"
3D bars, 3D surfaces, bar charts, charts, coordinate transformations, coordinates, graphical presentation, isometric plots, pie charts, plotting, polar plots, strip charts, time charts, xy-plots
.SH "COPYRIGHT"
.nf
Copyright (c) 2004 Arjen Markus <arjenmarkus@users.sourceforge.net>
.fi