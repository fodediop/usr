'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/memchan/memchan/doc/zero.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2004 Pat Thoyts <patthoyts@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools
.so man.macros
.TH "zero" n 2.2  "Memory channels"
.BS
.SH "NAME"
zero \- Create and manipulate zero channels
.SH "SYNOPSIS"
package require \fBTcl \fR
.sp
package require \fBmemchan \fR
.sp
\fBzero\fR \fR
.sp
.BE
.SH "DESCRIPTION"
.PP
The command described here is only available in a not-yet released
version of the package. Use the CVS to get the sources.
.TP
\fBzero\fR \fR
creates a zero channel which absorbs everything written into
it. Reading from a zero channel will return the requested number of null
bytes. These channels are essentially Tcl-specific variants of the
zero device for unixoid operating systems (/dev/zero). Transfering the
generated channel between interpreters is possible but does not make
much sense.
.SH "OPTIONS"
Memory channels created by \fBzero\fR provide one additional option to
set or query.
.TP
\fI-delay ?milliseconds?\fR
A \fBzero\fR channel is always writable and readable. This means
that all \fBfileevent\fR-handlers will fire continuously.  To
avoid starvation of other event sources the events raised by this
channel type have a configurable delay. This option is set in
milliseconds and defaults to 5.
.SH "SEE ALSO"
fifo, fifo2, memchan, null, random
.SH "KEYWORDS"
channel, i/o, in-memory channel, null, zero
.SH "COPYRIGHT"
.nf
Copyright (c) 2004 Pat Thoyts <patthoyts@users.sourceforge.net>
.fi