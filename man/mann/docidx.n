'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/doctools/docidx.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "docidx" n 1.0 doctools "Documentation tools"
.BS
.SH "NAME"
docidx \- Create and manipulate docidx converter objects
.SH "SYNOPSIS"
package require \fBTcl  8.2\fR
.sp
package require \fBdoctools::idx  ?1.0?\fR
.sp
\fB::doctools::idx::new\fR \fIobjectName\fR ?\fIoption value\fR...?\fR
.sp
\fB::doctools::idx::help\fR \fR
.sp
\fB::doctools::idx::search\fR \fIpath\fR\fR
.sp
\fBobjectName\fR \fIoption\fR ?\fIarg arg ...\fR?\fR
.sp
\fIobjectName\fR \fBconfigure\fR\fR
.sp
\fIobjectName\fR \fBconfigure\fR \fIoption\fR\fR
.sp
\fIobjectName\fR \fBconfigure\fR \fIoption value\fR...\fR
.sp
\fIobjectName\fR \fBcget\fR \fIoption\fR\fR
.sp
\fIobjectName\fR \fBdestroy\fR\fR
.sp
\fIobjectName\fR \fBformat\fR \fItext\fR\fR
.sp
\fIobjectName\fR \fBsearch\fR \fIpath\fR\fR
.sp
\fIobjectName\fR \fBwarnings\fR\fR
.sp
.BE
.SH "DESCRIPTION"
This package provides objects which can be used to convert text
written in the doctoc format as specified in \fBdocidx_fmt\fR into
any output format X, assuming that a formatting engine for X is
available and provides the interface specified in \fBdocidx_api\fR.
.SH "API"
.TP
\fB::doctools::idx::new\fR \fIobjectName\fR ?\fIoption value\fR...?\fR
Creates a new docidx object with an associated global Tcl command
whose name is \fIobjectName\fR. This command is explained in full
detail in the sections \fBOBJECT COMMAND\fR and
\fBOBJECT METHODS\fR.
.sp
The list of options and values coming after the name of the object is
used to set the initial configuration of the object.
.TP
\fB::doctools::idx::help\fR \fR
This is a pure convenience command for applications which want to
provide their user with a reminder of the available formatting
commands and their meanings. It returns a string containing a standard
help for this purpose.
.TP
\fB::doctools::idx::search\fR \fIpath\fR\fR
Whenever the package has to map the name of a format to the file
containing the code for its formatting engine it will search the file
in a number of directories. Three such directories are declared by the
package itself.
.sp
However the list is extensible by the user of the package and the
command above is the means to do so. When given a \fIpath\fR to an
existing and readable directory it will prepend that directory to the
existing list. This means that the path added last is searched through
first.
.sp
An error will be thrown if the \fIpath\fR either does not excist, is
not a directory, or is not readable.
.SH "OBJECT COMMAND"
All commands created by \fB::doctools::idx::new\fR have the following
general form and may be used to invoke various operations on the
object they are associated with.
.TP
\fBobjectName\fR \fIoption\fR ?\fIarg arg ...\fR?\fR
The \fIoption\fR and its \fIarg\fRs determine the exact behavior of
the command. See section \fBOBJECT METHODS\fR for more
explanations.
.SH "OBJECT METHODS"
.TP
\fIobjectName\fR \fBconfigure\fR\fR
When called without argument this method returns a list of all known
options and their current values.
.TP
\fIobjectName\fR \fBconfigure\fR \fIoption\fR\fR
When called with a single argument this method behaves like
\fBcget\fR.
.TP
\fIobjectName\fR \fBconfigure\fR \fIoption value\fR...\fR
When called with more than one argument the method reconfigures the
object using the \fIoption\fRs and \fIvalue\fRs given to it.
.sp
The legal configuration options are described in section
\fBOBJECT CONFIGURATION\fR.
.TP
\fIobjectName\fR \fBcget\fR \fIoption\fR\fR
This method expects a legal configuration option as argument and
returns the current value of that option for the object the method was
invoked for.
.sp
The legal configuration options are described in section
\fBOBJECT CONFIGURATION\fR.
.TP
\fIobjectName\fR \fBdestroy\fR\fR
Destroys the object it is invoked for.
.TP
\fIobjectName\fR \fBformat\fR \fItext\fR\fR
Takes the \fItext\fR and runs it through the configured formatting
engine. The resulting string is returned as the result of this
method. An error will be thrown if no \fB-format\fR was configured
for the object.
.sp
The method assumes that the \fItext\fR is in docidx format as
specified in \fBdtformat(n)\fR. Errors will be thrown otherwise.
.TP
\fIobjectName\fR \fBsearch\fR \fIpath\fR\fR
This method extends the per-object list of paths searched for
formatting engines. See also \fB::doctools::idx::search\fR on how to extend
the global (per-package) list of paths.
.sp
The path entered last is searched through first.
.TP
\fIobjectName\fR \fBwarnings\fR\fR
Returns a list containing all the warnings generated by the engine
during the last invocation of method \fBformat\fR.
.SH "OBJECT CONFIGURATION"
All docidx objects understand the following configuration options:
.TP
\fB-file\fR \fIfile\fR
The argument of this option is stored in the object and can be
retrieved by the formatting engine via the command \fBdt_file\fR (see
\fBdtformatter(n)\fR). Its default value is the empty string.
.sp
It will be interpreted as the name of the file containing the text
currently processed by the engine.
.TP
\fB-module\fR \fItext\fR
The argument of this option is stored in the object and can be
retrieved by the formatting engine via the command \fBdt_module\fR
(see \fBdtformatter(n)\fR). Its default value is the empty string.
.sp
It will be interpreted as the name of the module the file containing
the text currently processed by the engine belongs to.
.TP
\fB-format\fR \fItext\fR
The argument of this option specifies the format and thus the engine
to use when converting text via \fBformat\fR. Its default value is
the empty string. No formatting is possible if this
option is not set at least once.
.sp
The package will immediately try to map the name of the format to a
file containing the implementation of the engine for that format. An
error will be thrown if this mapping fails and a previously configured
format is left untouched.
.sp
Section \fBFORMAT MAPPING\fR explains how
the package looks for engine implementations.
.TP
\fB-deprecated\fR \fIboolean\fR
This option is a flag. If set the object will generate warnings when
formatting a text containing the deprecated markup command \fBstrong\fR
Its default value is \fBFALSE\fR. In other words, no warnings will
be generated.
.SH "FORMAT MAPPING"
When trying to map a format name \fIfoo\fR to the file containing
the implementation of formatting engine for \fIfoo\fR the package
will perform the following algorithm:
.IP [1]
If \fIfoo\fR is the name of an existing file this file is directly
taken as the implementation.
.IP [2]
If not, the list of per-object search paths is searched. For each
directory in the list the package checks if that directory contains a
file "\fIfmt.\fIfoo\fR\fR". If yes, that file is taken as the
implementation.
.sp
This list of paths is initially empty and can be extended through the
object method \fBsearch\fR.
.IP [3]
If not, the list of global (package) paths is searched. For each
directory in the list the package checks if that directory contains a
file "\fIidx.\fIfoo\fR\fR". If yes, that file is taken as the
implementation.
.sp
This list of paths contains initially one path and can be extended
through the command \fB::doctools::idx::search\fR.
.sp
The initial (standard) path is the sub directory "\fImpformats\fR" of
the directory the package itself is located in. In other words, if the
package implementation "\fIdocidx.tcl\fR" is installed in the
directory "\fI/usr/local/lib/tcllib/doctools\fR" then it will by
default search the directory
"\fI/usr/local/lib/tcllib/doctools/mpformats\fR" for format
implementations.
.IP [4]
The mapping fails.
.SH "ENGINES"
The package comes with the following predefined formatting engines
.TP
html
This engine generates HTML markup, for processing by web browsers and
the like.
.TP
latex
This engine generates output suitable for the \fBlatex\fR text
processor coming out of the TeX world.
.TP
list
This engine retrieves version, section and title of the manpage from
the document. As such it can be used to generate a directory listing
for a set of manpages.
.TP
nroff
This engine generates nroff output, for processing by \fBnroff\fR,
or \fBgroff\fR. The result will be standard man pages as they are
known in the unix world.
.TP
null
This engine generates no outout at all. This can be used if one just
wants to validate some input.
.TP
tmml
This engine generates TMML markup as specified by Joe English. The Tcl
Manpage Markup Language is a derivate of XML.
.TP
wiki
This engine generates Wiki markup as understood by Jean Claude
Wippler's \fBwikit\fR application.
.SH "SEE ALSO"
docidx_api, docidx_fmt
.SH "KEYWORDS"
HTML, TMML, conversion, documentation, index, manpage, markup, nroff, table of contents, toc
.SH "COPYRIGHT"
.nf
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi