'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/inifile/ini.man' by tcllib/doctools with format 'nroff'
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "inifile" n 0.1 inifile "Parsing of Windows INI files"
.BS
.SH "NAME"
inifile \- Parsing of Windows INI files
.SH "SYNOPSIS"
package require \fBTcl  8.2\fR
.sp
package require \fBinifile  ?0.1?\fR
.sp
\fB::ini::open\fR \fIfile\fR ?\fImode\fR?\fR
.sp
\fB::ini::close\fR \fIini\fR\fR
.sp
\fB::ini::commit\fR \fIini\fR\fR
.sp
\fB::ini::revert\fR \fIini\fR\fR
.sp
\fB::ini::filename\fR \fIini\fR\fR
.sp
\fB::ini::sections\fR \fIini\fR\fR
.sp
\fB::ini::keys\fR \fIini\fR \fIsection\fR\fR
.sp
\fB::ini::get\fR \fIini\fR \fIsection\fR\fR
.sp
\fB::ini::exists\fR \fIini\fR \fIsection\fR ?\fIkey\fR?\fR
.sp
\fB::ini::value\fR \fIini\fR \fIsection\fR \fIkey\fR\fR
.sp
\fB::ini::set\fR \fIini\fR \fIsection\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fB::ini::delete\fR \fIini\fR \fIsection\fR ?\fIkey\fR?\fR
.sp
\fB::ini::comment\fR \fIini\fR \fIsection\fR ?\fIkey\fR? ?\fItext\fR...?\fR
.sp
.BE
.SH "DESCRIPTION"
This package provides an interface for easy manipulation of Windows INI files.
.PP
.TP
\fB::ini::open\fR \fIfile\fR ?\fImode\fR?\fR
Opens an INI file and returns a handle that is used by other commands.
Mode has the same types as the \fBopen\fR command. The default mode is \fBr+\fR.
.TP
\fB::ini::close\fR \fIini\fR\fR
Close the specified handle. If any changes were made and not written by
\fBcommit\fR they are lost.
.TP
\fB::ini::commit\fR \fIini\fR\fR
Writes the file and all changes to disk. The sections are written in
arbitrary order. The keys in a section are written in alphabetical
order.
.TP
\fB::ini::revert\fR \fIini\fR\fR
Rolls all changes made to the inifile object back to the last
committed state.
.TP
\fB::ini::filename\fR \fIini\fR\fR
Returns the name of the file the \fIini\fR object is associated with.
.TP
\fB::ini::sections\fR \fIini\fR\fR
Returns a list of all the names of the existing sections in the file handle
specified.
.TP
\fB::ini::keys\fR \fIini\fR \fIsection\fR\fR
Returns a list of all they key names in the section and file specified.
.TP
\fB::ini::get\fR \fIini\fR \fIsection\fR\fR
Returns a list of key value pairs that exist in the section and file specified.
.TP
\fB::ini::exists\fR \fIini\fR \fIsection\fR ?\fIkey\fR?\fR
Returns a boolean value indicating the existance of the specified section as a
whole or the specified key within that section.
.TP
\fB::ini::value\fR \fIini\fR \fIsection\fR \fIkey\fR\fR
Returns the value of the named key from the specified section.
.TP
\fB::ini::set\fR \fIini\fR \fIsection\fR \fIkey\fR \fIvalue\fR\fR
Sets the value of the key in the specified section. If the section does not
exist then a new one is created.
.TP
\fB::ini::delete\fR \fIini\fR \fIsection\fR ?\fIkey\fR?\fR
Removes the key or the entire section and all its keys.
.TP
\fB::ini::comment\fR \fIini\fR \fIsection\fR ?\fIkey\fR? ?\fItext\fR...?\fR
Reads and modifies comments for sections and keys.