'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/memchan/memchan/doc/null.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools
.so man.macros
.TH "null" n 2.2  "Memory channels"
.BS
.SH "NAME"
null \- Create and manipulate null channels
.SH "SYNOPSIS"
package require \fBTcl \fR
.sp
package require \fBmemchan \fR
.sp
\fBnull\fR \fR
.sp
.BE
.SH "DESCRIPTION"
.PP
The command described here is only available in a not-yet released
version of the package. Use the CVS to get the sources.
.TP
\fBnull\fR \fR
creates a null channel which absorbs everything written into
it. Reading from it is not possible, or rather will always return zero
bytes. These channels are essentially Tcl-specific variants of the
null device for unixoid operating systems (/dev/null). Transfering the
generated channel between interpreters is possible but does not make
much sense.
.SH "OPTIONS"
Memory channels created by \fBnull\fR provide one additional option to
set or query.
.TP
\fI-delay ?milliseconds?\fR
A \fBnull\fR channel is always writable and readable. This means
that all \fBfileevent\fR-handlers will fire continuously.  To
avoid starvation of other event sources the events raised by this
channel type have a configurable delay. This option is set in
milliseconds and defaults to 5.
.PP
A null channel is always writable and never readable. This means that a
writable \fBfileevent\fR-handler will fire continuously and a readable
\fBfileevent\fR-handler never at all. The exception to the latter is
only the destruction of the channel which will cause the delivery of
an eof event to a readable handler.
.SH "SEE ALSO"
fifo, fifo2, memchan, random, zero
.SH "KEYWORDS"
channel, i/o, in-memory channel, null
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi