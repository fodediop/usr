'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/memchan/memchan/doc/random.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2004 Pat Thoyts <patthoyts@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools
.so man.macros
.TH "random" n 2.2  "Memory channels"
.BS
.SH "NAME"
random \- Create and manipulate randomizer channels
.SH "SYNOPSIS"
package require \fBTcl \fR
.sp
package require \fBmemchan \fR
.sp
\fBrandom\fR \fR
.sp
.BE
.SH "DESCRIPTION"
.TP
\fBrandom\fR \fR
creates a random channel which absorbs everything written into it and
uses it as a seed for a random number generator. This generator is
used to create a random sequence of bytes when reading from the
channel. It is not possible to seek the channel.
.SH "OPTIONS"
Memory channels created by \fBrandom\fR provide one additional option to
set or query.
.TP
\fI-delay ?milliseconds?\fR
A \fBrandom\fR channel is always writable and readable. This means
that all \fBfileevent\fR-handlers will fire continuously.  To
avoid starvation of other event sources the events raised by this
channel type have a configurable delay. This option is set in
milliseconds and defaults to 5.
.SH "SEE ALSO"
fifo, fifo2, memchan, null, zero
.SH "KEYWORDS"
channel, i/o, in-memory channel, random
.SH "COPYRIGHT"
.nf
Copyright (c) 2004 Pat Thoyts <patthoyts@users.sourceforge.net>
.fi