'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/trf/trf/doc/md5crypt.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools = trf.inc
.so man.macros
.TH "md5crypt" n 2.1p2  "Trf transformer commands"
.BS
.SH "NAME"
md5crypt \- Password hashing based on "md5"
'\" -*- tcl -*- doctools = trf_header.inc
.SH "SYNOPSIS"
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1p2?\fR
.sp
\fBmd5crypt\fR \fIpassword\fR \fIsalt\fR\fR
.sp
.BE
.SH "DESCRIPTION"
The command \fBmd5crypt\fR is for the encryption of passwords and uses
\fImd5\fR as hash algorithm. An alternative command for the same
function, but based on the older \fBcrypt(3)\fR hash function is
\fBcrypt\fR.
.PP
.TP
\fBmd5crypt\fR \fIpassword\fR \fIsalt\fR\fR
Encrypts the \fIpassword\fR using the specified \fIsalt\fR and returns
the generated hash value as the result of the command.
.SH "SEE ALSO"
crypt, trf-intro
.SH "KEYWORDS"
authentication, crypt, hash, hashing, mac, md5, message digest, password
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi