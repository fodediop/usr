'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/doctools/docidx_fmt.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "docidx_fmt" n 1.0 doctools "Documentation tools"
.BS
.SH "NAME"
docidx_fmt \- Specification of simple tcl markup for an index
.SH "SYNOPSIS"
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
.sp
\fBvset\fR \fIvarname\fR\fR
.sp
\fBinclude\fR \fIfilename\fR\fR
.sp
\fBcomment\fR \fItext\fR\fR
.sp
\fBlb\fR \fR
.sp
\fBrb\fR \fR
.sp
\fBindex_begin\fR \fItext\fR \fItitle\fR\fR
.sp
\fBindex_end\fR \fR
.sp
\fBkey\fR \fItext\fR\fR
.sp
\fBmanpage\fR \fIfile\fR \fIlabel\fR\fR
.sp
\fBurl\fR \fIurl\fR \fIlabel\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
This manpage specifies a documentation format for indices. It is
intended to complement both the \fBdoctools\fR format for writing
manpages and the \fBdoctoc\fR format for writing tables of
contents. See \fBdoctools_fmt\fR and \fBdoctoc_fmt\fR for the
specification of these two formats.
.PP
This format is called \fBdocidx\fR.
It provides all the necessary commands to write an index for a group
of manpages.
Like for the \fBdoctools\fR and \fBdoctoc\fR formats a package
is provided implementing a generic framework for the conversion of
\fBdocidx\fR to a number of different output formats, like HTML,
TMML, nroff, LaTeX, etc.
The package is called \fBdoctools::idx\fR, its documentation can
be found in \fBdocidx\fR.
People wishing to write a formatting engine for the conversion of
\fBdocidx\fR into a new output format have to read
\fBdocidx_api\fR. This manpage will explain the interface between
the generic package and such engines.
.SH "OVERVIEW"
\fBdocidx\fR is similar to LaTex in that it consists primarily of
text, with markup commands embedded into it. The format used to mark
something as command is different from LaTeX however. All text between
matching pairs of [ and ] is a command, possibly with
arguments. Note that both brackets have to be on the same line for a
command to be recognized.
.PP
In this format plain text is not allowed, except for whitespace, which
can be used to separate the formatting commands described in the next
section (\fBFORMATTING COMMANDS\fR).
.SH "FORMATTING COMMANDS"
First a number of generic commands useable anywhere in a
\fBdocidx\fR file.
.TP
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
Sets the formatter variable \fIvarname\fR to the specified
\fIvalue\fR. Returns the empty string.
.TP
\fBvset\fR \fIvarname\fR\fR
Returns the value associated with the formatter variable
\fIvarname\fR.
.TP
\fBinclude\fR \fIfilename\fR\fR
Instructs the system to insert the expanded contents of the file named
\fIfilename\fR in its own place.
.TP
\fBcomment\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a comment.
Commands to insert special plain text. These bracket commands are
necessary as plain brackets are used to denote the beginnings and
endings of the formatting commands and thus cannot be used as normal
characters anymore.
.TP
\fBlb\fR \fR
Introduces a left bracket into the output.
.TP
\fBrb\fR \fR
Introduces a right bracket into the output.
And now the relevant markup commands.
.TP
\fBindex_begin\fR \fItext\fR \fItitle\fR\fR
This command starts an index. It has to be the very first
\fImarkup\fR command in a \fBdocidx\fR file. Plain text is not
allowed to come before this command. Only the generic commands (see
above: \fBvset\fR, \fBinclude\fR, \fBcomment\fR) can be used before
it.
.sp
The \fItext\fR argument provides a label for the whole group of
manpages the index refers to. Often this is the name of the package
(or extension) the manpages belong to.
.sp
The \fItitle\fR argument provides the title for the index.
.sp
Each index has to contain at least one \fBkey\fR.
.TP
\fBindex_end\fR \fR
This command closes an index. Nothing is allowed to follow it.
.TP
\fBkey\fR \fItext\fR\fR
This commands starts the list of manpages and other entities which
refer to the keyword named by the argument \fItext\fR.
.sp
Each key section has to contain at least one index element, either
\fBmanpage\fR or \fBurl\fR.
.TP
\fBmanpage\fR \fIfile\fR \fIlabel\fR\fR
This command describes an individual index element. Each such element
belongs to the last occurence of a \fBkey\fR command coming before the
index.
.sp
The \fIfile\fR argument refers to the file containing the actual
manpage refering to that key. The second argument is used to label the
reference.
.sp
To preserve convertibility of this format to various output formats
the filename argument \fIfile\fR is considered to contain a symbolic
name. The actual name of the file will be inserted by the formatting
engine used to convert the input, based on a mapping from symbolic to
actual names given to it.
.TP
\fBurl\fR \fIurl\fR \fIlabel\fR\fR
This is the second command to describe an index element. The
association to the key it belongs to is done in the same way as for
the \fBmanpage\fR command. The first however is not the symbolic name
of the file refering to that key, but an url describing the exact
location of the document indexed here.
.SH "NOTES"
.IP [1]
Using an appropriate formatting engine and some glue code it is
possible to automatically generate a document in \fBdocidx\fR
format from a collection of manpages in \fBdoctools\fR format.
.SH "EXAMPLE"
As an example an index for all manpages belonging to this module
(doctools) of package \fBtcllib\fR.
.PP
.nf
[index_begin tcllib/doctools {Documentation tools}]
 [key HTML]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key TMML]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key conversion]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key documentation]
  [manpage doctools]
  [manpage dtformatter]
 [key index]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtocformat]
 [key interface]
  [manpage didxengine]
  [manpage dtformatter]
  [manpage dtocengine]
 [key manpage]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key markup]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key nroff]
  [manpage didxengine]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtformatter]
  [manpage dtocengine]
  [manpage dtocformat]
  [manpage mpexpand]
 [key {table of contents}]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtocengine]
  [manpage dtocformat]
 [key toc]
  [manpage didxformat]
  [manpage doctools]
  [manpage dtformat]
  [manpage dtocengine]
  [manpage dtocformat]
[index_end]
.fi
.SH "SEE ALSO"
docidx, docidx_api, doctoc_fmt, doctools_fmt
.SH "KEYWORDS"
HTML, LaTeX, TMML, generic markup, index, keywords, markup, nroff
.SH "COPYRIGHT"
.nf
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi