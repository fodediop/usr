'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/trf/trf/doc/transform.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools = trf.inc
.so man.macros
.TH "transform" n 2.1p2  "Trf transformer commands"
.BS
.SH "NAME"
transform \- Tcl level transformations
'\" -*- tcl -*- doctools = trf_header.inc
.SH "SYNOPSIS"
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1p2?\fR
.sp
\fBtransform\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
.sp
\fBcallback\fR \fIoperation\fR \fIdata\fR\fR
.sp
.BE
.SH "DESCRIPTION"
The command \fBtransform\fR reflects the API for a stack channel
transformation into the tcl level, thus enabling the writing of
transformations in tcl.
.PP
.TP
\fBtransform\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
.RS
.TP
\fB-mode\fR \fBread\fR|\fBwrite\fR
This option is accepted by the command if and only if it is used in
\fIimmediate\fR mode.  See section
\fBIMMEDIATE versus ATTACHED\fR for an explanation of the term.
.sp
The argument value specifies whether to run the read or the write part
of the transformation specified via option \fB-command\fR on the
immediate data.
.sp
Beyond the argument values listed above all unique abbreviations are
recognized too.
.TP
\fB-command\fR \fIcmd\fR
This option has to be present and is always understood. Its argument
is a command prefix. This command prefix will be called by internally
whenever some operation of the transformation has to be executed. An
empty \fIcmd\fR is not allowed.
.sp
The exact nature of the various possible calls and their expected
results is described later, in section \fBCALLBACK API\fR.
'\" -*- tcl -*- doctools = common_options.inc
.TP
\fB-attach\fR \fIchannel\fR
The presence/absence of this option determines the main operation mode
of the transformation.
.sp
If present the transformation will be stacked onto the \fIchannel\fR
whose handle was given to the option and run in \fIattached\fR
mode. More about this in section \fBIMMEDIATE versus ATTACHED\fR.
.sp
If the option is absent the transformation is used in \fIimmediate\fR
mode and the options listed below are recognized. More about this in
section \fBIMMEDIATE versus ATTACHED\fR.
.TP
\fB-in\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the data
to transform has to be read from.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the data to transform is expected as the last argument to the
transformation.
.TP
\fB-out\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the
generated transformation result is written to.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the generated data is returned as the result of the command
itself.
.RE
'\" -*- tcl -*- doctools = common_sections.inc
.SH "IMMEDIATE versus ATTACHED"
The transformation distinguishes between two main ways of using
it. These are the \fIimmediate\fR and \fIattached\fR operation
modes.
.PP
For the \fIattached\fR mode the option \fB-attach\fR is used to
associate the transformation with an existing channel. During the
execution of the command no transformation is performed, instead the
channel is changed in such a way, that from then on all data written
to or read from it passes through the transformation and is modified
by it according to the definition above.
This attachment can be revoked by executing the command \fBunstack\fR
for the chosen channel. This is the only way to do this at the Tcl
level.
.PP
In the second mode, which can be detected by the absence of option
\fB-attach\fR, the transformation immediately takes data from
either its commandline or a channel, transforms it, and returns the
result either as result of the command, or writes it into a channel.
The mode is named after the immediate nature of its execution.
.PP
Where the data is taken from, and delivered to, is governed by the
presence and absence of the options \fB-in\fR and \fB-out\fR.
It should be noted that this ability to immediately read from and/or
write to a channel is an historic artifact which was introduced at the
beginning of Trf's life when Tcl version 7.6 was current as this and
earlier versions have trouble to deal with \\0 characters embedded into
either input or output.
.SH "CALLBACK API"
Here we describe the API of the callback command implementing the
actual transformation.
.PP
.TP
\fBcallback\fR \fIoperation\fR \fIdata\fR\fR
The callback is always called with two arguments, first an operation
code followed by data. The latter will be empty for some operations.
.sp
The known operations are listed below, together with an explanation of
the arguments, what is expected of them, and how their results are
handled.
.RS
.TP
\fBcreate/write\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
This is the first operation executed for the write side of the
transformation. It has to initialize the internals of this part of the
transformation and ready it for future calls.
.TP
\fBdelete/write\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
This is the last operation executed for the write side of the
transformation. It has to shutdown the internals of this part of the
transformation and release any resources which were acquired over the
lifetime of the transformation.
.TP
\fBwrite\fR
The operation is called whenever data is written to the channel.
.sp
At the time of the call the argument \fIdata\fR will contain the bytes
to transform. The result of the call is taken as the result of the
transformation and handed to the next stage down in the stack of
transformation associated with the channel.
.sp
This operation has to transform the contents of \fIdata\fR, using
whatever data was left over from the last call of the operation. The
transformation is allowed to buffer incomplete data.
.TP
\fBflush/write\fR
When called \fIdata\fR is empty. The operation has to transform any
incomplete data it has buffered internally on the write side. The
result of the call is taken as the result of the transformation and
handed to the next stage down in the stack of transformation
associated with the channel.
.TP
\fBclear/write\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
The write side of the transformation has to clear its internal
buffers. This operation is called when the user seeks on the channel,
thus invalidating any incomplete transformation.
.TP
\fBcreate/read\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
This is the first operation executed for the read side of the
transformation. It has to initialize the internals of this part of the
transformation and ready it for future calls.
.TP
\fBdelete/read\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
This is the last operation executed for the write side of the
transformation. It has to shutdown the internals of this part of the
transformation and release any resources which were acquired over the
lifetime of the transformation.
.TP
\fBread\fR
The operation is called whenever data is read from the channel.
.sp
At the time of the call the argument \fIdata\fR will contain the bytes
to transform. The result of the call is taken as the result of the
transformation and posted to the next stage up in the stack of
transformation associated with the channel.
.sp
This operation has to transform the contents of \fIdata\fR, using
whatever data was left over from the last call of the operation. The
transformation is allowed to buffer incomplete data.
.TP
\fBflush/read\fR
When called \fIdata\fR is empty. The operation has to transform any
incomplete data it has buffered internally on the read side. The
result of the call is taken as the result of the transformation and
posted to the next stage up in the stack of transformation associated
with the channel.
.TP
\fBclear/read\fR
When called \fIdata\fR is empty. The result of the call is ignored.
.sp
The read side of the transformation has to clear its internal
buffers. This operation is called when the user seeks on the channel,
thus invalidating any incomplete transformation.
.TP
\fBquery/maxRead\fR
When called \fIdata\fR is empty. The result of the call is interpreted
as integer number. This operation is used by the generic layer to
determine if the transformation establishes a limit on the number of
bytes it (the generic layer) is allowed read from the transformations
lower in the stack. A negative result unsets any limit.
.sp
This has to be used if a transformation employs some kind of
end-of-data marker. We cannot allow the generic layer to overshoot
this marker because any data read after it cannot be stuffed back into
the core buffers, causing the I/O system to loose data if the
transformation is unstacked after it recognized the end of its
data. This is a limitation of the I/O system in the tcl core.
.sp
Returning a positive value will cause the I/O system to slow down, but
also ensures that no data is lost.
.sp
Two examples for such transformations are the data decompressors for
\fBzip\fR and \fBbz2\fR. They use the C-level equivalent of this
operation to prevent the overshooting.
.RE
.SH "SEE ALSO"
trf-intro
.SH "KEYWORDS"
general transform
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi