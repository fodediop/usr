'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/fileutil/fileutil.man' by tcllib/doctools with format 'nroff'
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "fileutil" n 1.6.1 fileutil "file utilities"
.BS
.SH "NAME"
fileutil \- Procedures implementing some file utilities
.SH "SYNOPSIS"
package require \fBTcl  8\fR
.sp
package require \fBfileutil  ?1.6.1?\fR
.sp
\fB::fileutil::cat\fR \fIfiles\fR\fR
.sp
\fB::fileutil::fileType\fR \fIfilename\fR\fR
.sp
\fB::fileutil::find\fR ?\fIbasedir\fR ?\fIfiltercmd\fR??\fR
.sp
\fB::fileutil::findByPattern\fR \fIbasedir\fR ?\fB-regexp\fR|\fB-glob\fR? ?\fB--\fR? \fIpatterns\fR\fR
.sp
\fB::fileutil::foreachLine\fR \fIvar filename cmd\fR\fR
.sp
\fB::fileutil::grep\fR \fIpattern\fR ?\fIfiles\fR?\fR
.sp
\fB::fileutil::install\fR ?\fB-m\fR \fImode\fR? \fIsource\fR \fIdestination\fR\fR
.sp
\fB::fileutil::stripN\fR \fIpath\fR \fIn\fR\fR
.sp
\fB::fileutil::stripPwd\fR \fIpath\fR\fR
.sp
\fB::fileutil::touch\fR ?\fB-a\fR? ?\fB-c\fR? ?\fB-m\fR? ?\fB-r\fR \fIref_file\fR? ?\fB-t\fR \fItime\fR? \fIfilename\fR ?\fI...\fR?\fR
.sp
\fB::fileutil::tempdir\fR \fR
.sp
\fB::fileutil::tempfile\fR ?\fIprefix\fR?\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
This package provides implementations of standard unix utilities.
.TP
\fB::fileutil::cat\fR \fIfiles\fR\fR
A tcl implementation of the UNIX \fBcat\fR command.  Returns the
contents of the specified file(s). The arguments are files to read.
If there are problems reading any of the files, an error will occur,
and no data will be returned.
.TP
\fB::fileutil::fileType\fR \fIfilename\fR\fR
An implementation of the UNIX \fBfile\fR command, which uses
various heuristics to guess the type of a file.  Returns a list
specifying as much type information as can be determined about the
file, from most general (eg, "binary" or "text") to most specific (eg,
"gif").  For example, the return value for a GIF file would be "binary
graphic gif".  The command will detect the following types of files:
directory, empty, binary, text, script (with interpreter), executable
elf, graphic gif, graphic jpeg, graphic png, graphic tiff, html,
xml (with doctype if available), message pgp, binary pdf, text ps,
text eps, binary gravity_wave_data_frame, compressed bzip,
compressed gzip, and link.
.TP
\fB::fileutil::find\fR ?\fIbasedir\fR ?\fIfiltercmd\fR??\fR
An implementation of the unix command \fBfind\fR. Adapted from the
Tcler's Wiki. Takes at most two arguments, the path to the directory
to start searching from and a command to use to evaluate interest in
each file. The path defaults to "\fI.\fR", i.e. the current
directory. The command defaults to the empty string, which means that
all files are of interest. The command takes care \fInot\fR to
loose itself in infinite loops upon encountering circular link
structures.  The result of the command is a list containing the paths
to the interesting files.
.TP
\fB::fileutil::findByPattern\fR \fIbasedir\fR ?\fB-regexp\fR|\fB-glob\fR? ?\fB--\fR? \fIpatterns\fR\fR
This command is based upon the \fBTclX\fR command
\fBrecursive_glob\fR, except that it doesn't allow recursion over more
than one directory at a time. It uses \fB::fileutil::find\fR
internally and is thus able to and does follow symbolic links,
something the \fBTclX\fR command does not do. First argument is
the directory to start the search in, second argument is a list of
\fIpatterns\fR. The command returns a list of all files reachable
through \fIbasedir\fR whose names match at least one of the
patterns. The options before the pattern-list determine the style of
matching, either regexp or glob. glob-style matching is the default if
no options are given. Usage of the option \fB--\fR stops option
processing. This allows the use of a leading '-' in the patterns.
.TP
\fB::fileutil::foreachLine\fR \fIvar filename cmd\fR\fR
The command reads the file \fIfilename\fR and executes the script
\fIcmd\fR for every line in the file. During the execution of the
script the variable \fIvar\fR is set to the contents of the current
line. The return value of this command is the result of the last
invocation of the script \fIcmd\fR or the empty string if the file was
empty.
.TP
\fB::fileutil::grep\fR \fIpattern\fR ?\fIfiles\fR?\fR
Implementation of \fBgrep\fR. Adapted from the Tcler's Wiki. The
first argument defines the \fIpattern\fR to search for. This is
followed by a list of \fIfiles\fR to search through. The list is
optional and \fBstdin\fR will be used if it is missing. The result
of the procedures is a list containing the matches. Each match is a
single element of the list and contains filename, number and contents
of the matching line, separated by a colons.
.TP
\fB::fileutil::install\fR ?\fB-m\fR \fImode\fR? \fIsource\fR \fIdestination\fR\fR
The \fBinstall\fR command is similar in functionality to the \fBinstall\fR
command found on many unix systems, or the shell script
distributed with many source distributions (unix/install-sh in the Tcl
sources, for example).  It copies \fIsource\fR, which can be either a
file or directory to \fIdestination\fR, which should be a directory,
unless \fIsource\fR is also a single file.  The ?-m? option lets
the user specify a unix-style mode (either octal or symbolic - see
\fBfile attributes\fR.
.TP
\fB::fileutil::stripN\fR \fIpath\fR \fIn\fR\fR
Removes the first \fIn\fR elements from the specified \fIpath\fR and
returns the modified path. If \fIn\fR is greater than the number of
components in \fIpath\fR an empty string is returned.
.TP
\fB::fileutil::stripPwd\fR \fIpath\fR\fR
If the \fIpath\fR is inside of the directory returned by
[\fBpwd\fR] (or the current working directory itself) it is made
relative to that directory. In other words, the current working
directory is stripped from the \fIpath\fR.  The possibly modified path
is returned as the result of the command. If the current working
directory itself was specified for \fIpath\fR the result is the string
"\fB.\fR".
.TP
\fB::fileutil::touch\fR ?\fB-a\fR? ?\fB-c\fR? ?\fB-m\fR? ?\fB-r\fR \fIref_file\fR? ?\fB-t\fR \fItime\fR? \fIfilename\fR ?\fI...\fR?\fR
Implementation of \fBtouch\fR. Alter the atime and mtime of the
specified files. If \fB-c\fR, do not create files if they do not
already exist. If \fB-r\fR, use the atime and mtime from
\fIref_file\fR. If \fB-t\fR, use the integer clock value
\fItime\fR. It is illegal to specify both \fB-r\fR and
\fB-t\fR. If \fB-a\fR, only change the atime. If \fB-m\fR,
only change the mtime.
.sp
\fIThis command is not available for Tcl versions less than 8.3.\fR
.TP
\fB::fileutil::tempdir\fR \fR
The command returns the path of a directory where the caller can
place temporary files, such as "\fI/tmp\fR" on Unix systems. The
algorithm we use to find the correct directory is as follows:
.RS
.IP [1]
The directory named in the TMPDIR environment variable.
.IP [2]
The directory named in the TEMP environment variable.
.IP [3]
The directory named in the TMP environment variable.
.IP [4]
A platform specific location:
.RS
.TP
Windows
"\fIC:\\TEMP\fR", "\fIC:\\TMP\fR", "\fI\\TEMP\fR",
and "\fI\\TMP\fR" are tried in that order.
.TP
(classic) Macintosh
The TRASH_FOLDER environment variable is used.  This is most likely
not correct.
.TP
Unix
The directories "\fI/tmp\fR", "\fI/var/tmp\fR", and "\fI/usr/tmp\fR" are
tried in that order.
.RE
.RE
.sp
The algorithm utilized is that used in the Python standard library.
.TP
\fB::fileutil::tempfile\fR ?\fIprefix\fR?\fR
The command generates a temporary file name suitable for writing to,
and the associated file.  The file name will be unique, and the file
will be writable and contained in the appropriate system specific temp
directory. The name of the file will be returned as the result of the
command.
.sp
The code was taken from \fIhttp://wiki.tcl.tk/772\fR, attributed to
Igor Volobouev and anon.
.SH "KEYWORDS"
file utilities, grep, temp file, touch, type