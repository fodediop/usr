'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/math/statistics.man' by tcllib/doctools with format 'nroff'
'\"
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "math::statistics" n 0.1.1 math "Math"
.BS
.SH "NAME"
math::statistics \- Basic statistical functions and procedures
.SH "SYNOPSIS"
package require \fBTcl  8\fR
.sp
package require \fBmath::statistics  0.1.1\fR
.sp
\fB::math::statistics::mean\fR \fIdata\fR\fR
.sp
\fB::math::statistics::min\fR \fIdata\fR\fR
.sp
\fB::math::statistics::max\fR \fIdata\fR\fR
.sp
\fB::math::statistics::number\fR \fIdata\fR\fR
.sp
\fB::math::statistics::stdev\fR \fIdata\fR\fR
.sp
\fB::math::statistics::var\fR \fIdata\fR\fR
.sp
\fB::math::statistics::basic-stats\fR \fIdata\fR\fR
.sp
\fB::math::statistics::histogram\fR \fIlimits\fR \fIvalues\fR\fR
.sp
\fB::math::statistics::corr\fR \fIdata1\fR \fIdata2\fR\fR
.sp
\fB::math::statistics::interval-mean-stdev\fR \fIdata\fR \fIconfidence\fR\fR
.sp
\fB::math::statistics::t-test-mean\fR \fIdata\fR \fIest_mean\fR \fIest_stdev\fR \fIconfidence\fR\fR
.sp
\fB::math::statistics::quantiles\fR \fIdata\fR \fIconfidence\fR\fR
.sp
\fB::math::statistics::quantiles\fR \fIlimits\fR \fIcounts\fR \fIconfidence\fR\fR
.sp
\fB::math::statistics::autocorr\fR \fIdata\fR\fR
.sp
\fB::math::statistics::crosscorr\fR \fIdata1\fR \fIdata2\fR\fR
.sp
\fB::math::statistics::mean-histogram-limits\fR \fImean\fR \fIstdev\fR \fInumber\fR\fR
.sp
\fB::math::statistics::minmax-histogram-limits\fR \fImin\fR \fImax\fR \fInumber\fR\fR
.sp
\fB::math::statistics::pdf-normal\fR \fImean\fR \fIstdev\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::pdf-exponential\fR \fImean\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::pdf-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::cdf-normal\fR \fImean\fR \fIstdev\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::cdf-exponential\fR \fImean\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::cdf-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::cdf-students-t\fR \fIdegrees\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::random-normal\fR \fImean\fR \fIstdev\fR \fInumber\fR\fR
.sp
\fB::math::statistics::random-exponential\fR \fImean\fR \fInumber\fR\fR
.sp
\fB::math::statistics::random-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
.sp
\fB::math::statistics::histogram-uniform\fR \fIxmin\fR \fIxmax\fR \fIlimits\fR \fInumber\fR\fR
.sp
\fB::math::statistics::filter\fR \fIvarname\fR \fIdata\fR \fIexpression\fR\fR
.sp
\fB::math::statistics::map\fR \fIvarname\fR \fIdata\fR \fIexpression\fR\fR
.sp
\fB::math::statistics::samplescount\fR \fIvarname\fR \fIlist\fR \fIexpression\fR\fR
.sp
\fB::math::statistics::subdivide\fR \fR
.sp
\fB::math::statistics::plot-scale\fR \fIcanvas\fR \fIxmin\fR \fIxmax\fR \fIymin\fR \fIymax\fR\fR
.sp
\fB::math::statistics::plot-xydata\fR \fIcanvas\fR \fIxdata\fR \fIydata\fR \fItag\fR\fR
.sp
\fB::math::statistics::plot-xyline\fR \fIcanvas\fR \fIxdata\fR \fIydata\fR \fItag\fR\fR
.sp
\fB::math::statistics::plot-tdata\fR \fIcanvas\fR \fItdata\fR \fItag\fR\fR
.sp
\fB::math::statistics::plot-tline\fR \fIcanvas\fR \fItdata\fR \fItag\fR\fR
.sp
\fB::math::statistics::plot-histogram\fR \fIcanvas\fR \fIcounts\fR \fIlimits\fR \fItag\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
The \fBmath::statistics\fR package contains functions and procedures for
basic statistical data analysis, such as:
.IP \(bu
Descriptive statistical parameters (mean, minimum, maximum, standard
deviation)
.IP \(bu
Estimates of the distribution in the form of histograms and quantiles
.IP \(bu
Basic testing of hypotheses
.IP \(bu
Probability and cumulative density functions
It is meant to help in developing data analysis applications or doing
ad hoc data analysis, it is not in itself a full application, nor is it
intended to rival with full (non-)commercial statistical packages.
.PP
The purpose of this document is to describe the implemented procedures
and provide some examples of their usage. As there is ample literature
on the algorithms involved, we refer to relevant text books for more
explanations.
The package contains a fairly large number of public procedures. They
can be distinguished in three sets: general procedures, procedures
that deal with specific statistical distributions, list procedures to
select or transform data and simple plotting procedures (these require
Tk).
\fINote:\fR The data that need to be analyzed are always contained in a
simple list. Missing values are represented as empty list elements.
.SH "GENERAL PROCEDURES"
The general statistical procedures are:
.TP
\fB::math::statistics::mean\fR \fIdata\fR\fR
Determine the \fImean\fR value of the given list of data.
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::min\fR \fIdata\fR\fR
Determine the \fIminimum\fR value of the given list of data.
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::max\fR \fIdata\fR\fR
Determine the \fImaximum\fR value of the given list of data.
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::number\fR \fIdata\fR\fR
Determine the \fInumber\fR of non-missing data in the given list
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::stdev\fR \fIdata\fR\fR
Determine the \fIstandard deviation\fR of the data in the given list
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::var\fR \fIdata\fR\fR
Determine the \fIvariance\fR of the data in the given list
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::basic-stats\fR \fIdata\fR\fR
Determine a list of all the descriptive parameters: mean, minimum,
maximum, number of data, standard deviation and variance.
.sp
(This routine is called whenever either or all of the basic statistical
parameters are required. Hence all calculations are done and the
relevant values are returned.)
.sp
\fIdata\fR - List of data
.sp
.TP
\fB::math::statistics::histogram\fR \fIlimits\fR \fIvalues\fR\fR
Determine histogram information for the given list of data. Returns a
list consisting of the number of values that fall into each interval.
(The first interval consists of all values lower than the first limit,
the last interval consists of all values greater than the last limit.
There is one more interval than there are limits.)
.sp
\fIlimits\fR - List of upper limits (in ascending order) for the
intervals of the histogram.
.sp
\fIvalues\fR - List of data
.sp
.TP
\fB::math::statistics::corr\fR \fIdata1\fR \fIdata2\fR\fR
Determine the correlation coefficient between two sets of data.
.sp
\fIdata1\fR - First list of data
.sp
\fIdata2\fR - Second list of data
.sp
.TP
\fB::math::statistics::interval-mean-stdev\fR \fIdata\fR \fIconfidence\fR\fR
Return the interval containing the mean value and one
containing the standard deviation with a certain
level of confidence (assuming a normal distribution)
.sp
\fIdata\fR - List of raw data values (small sample)
.sp
\fIconfidence\fR - Confidence level (0.95 or 0.99 for instance)
.sp
.TP
\fB::math::statistics::t-test-mean\fR \fIdata\fR \fIest_mean\fR \fIest_stdev\fR \fIconfidence\fR\fR
Test whether the mean value of a sample is in accordance with the
estimated normal distribution with a certain level of confidence.
Returns 1 if the test succeeds or 0 if the mean is unlikely to fit
the given distribution.
.sp
\fIdata\fR - List of raw data values (small sample)
.sp
\fIest_mean\fR - Estimated mean of the distribution
.sp
\fIest_stdev\fR - Estimated stdev of the distribution
.sp
\fIconfidence\fR - Confidence level (0.95 or 0.99 for instance)
.sp
.TP
\fB::math::statistics::quantiles\fR \fIdata\fR \fIconfidence\fR\fR
Return the quantiles for a given set of data
.sp
\fIdata\fR - List of raw data values
.sp
\fIconfidence\fR - Confidence level (0.95 or 0.99 for instance)
.sp
.TP
\fB::math::statistics::quantiles\fR \fIlimits\fR \fIcounts\fR \fIconfidence\fR\fR
Return the quantiles based on histogram information (alternative to the
call with two arguments)
.sp
\fIlimits\fR - List of upper limits from histogram
.sp
\fIcounts\fR - List of counts for for each interval in histogram
.sp
\fIconfidence\fR -  Confidence level (0.95 or 0.99 for instance)
.sp
.TP
\fB::math::statistics::autocorr\fR \fIdata\fR\fR
Return the autocorrelation function as a list of values (assuming
equidistance between samples, about 1/2 of the number of raw data)
.sp
The correlation is determined in such a way that the first value is
always 1 and all others are equal to or smaller than 1. The number of
values involved will diminish as the "time" (the index in the list of
returned values) increases
.sp
\fIdata\fR - Raw data for which the autocorrelation must be determined
.sp
.TP
\fB::math::statistics::crosscorr\fR \fIdata1\fR \fIdata2\fR\fR
Return the cross-correlation function as a list of values (assuming
equidistance between samples, about 1/2 of the number of raw data)
.sp
The correlation is determined in such a way that the values can never
exceed 1 in magnitude. The number of values involved will diminish
as the "time" (the index in the list of returned values) increases.
.sp
\fIdata1\fR - First list of data
.sp
\fIdata2\fR - Second list of data
.sp
.TP
\fB::math::statistics::mean-histogram-limits\fR \fImean\fR \fIstdev\fR \fInumber\fR\fR
Determine reasonable limits based on mean and standard deviation
for a histogram
.sp
Convenience function - the result is suitable for the histogram function.
.sp
\fImean\fR - Mean of the data
.sp
\fIstdev\fR - Standard deviation
.sp
\fInumber\fR - Number of limits to generate (defaults to 8)
.sp
.TP
\fB::math::statistics::minmax-histogram-limits\fR \fImin\fR \fImax\fR \fInumber\fR\fR
Determine reasonable limits based on a minimum and maximum for a histogram
.sp
Convenience function - the result is suitable for the histogram function.
.sp
\fImin\fR - Expected minimum
.sp
\fImax\fR - Expected maximum
.sp
\fInumber\fR - Number of limits to generate (defaults to 8)
.sp
.SH "STATISTICAL DISTRIBUTIONS"
In the literature a large number of probability distributions can be
found. The statistics package supports:
.IP \(bu
The normal or Gaussian distribution
.IP \(bu
The uniform distribution - equal probability for all data within a given
interval
.IP \(bu
The exponential distribution - useful as a model for certain
extreme-value distributions.
.IP \(bu
PM - binomial, Poisson, chi-squared, student's T, F.
In principle for each distribution one has procedures for:
.IP \(bu
The probability density (pdf-*)
.IP \(bu
The cumulative density (cdf-*)
.IP \(bu
Quantiles for the given distribution (quantiles-*)
.IP \(bu
Histograms for the given distribution (histogram-*)
.IP \(bu
List of random values with the given distribution (random-*)
The following procedures have been implemented:
.TP
\fB::math::statistics::pdf-normal\fR \fImean\fR \fIstdev\fR \fIvalue\fR\fR
Return the probability of a given value for a normal distribution with
given mean and standard deviation.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fIstdev\fR - Standard deviation of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::pdf-exponential\fR \fImean\fR \fIvalue\fR\fR
Return the probability of a given value for an exponential
distribution with given mean.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::pdf-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
Return the probability of a given value for a uniform
distribution with given extremes.
.sp
\fIxmin\fR - Minimum value of the distribution
.sp
\fIxmin\fR - Maximum value of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::cdf-normal\fR \fImean\fR \fIstdev\fR \fIvalue\fR\fR
Return the cumulative probability of a given value for a normal
distribution with given mean and standard deviation, that is the
probability for values up to the given one.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fIstdev\fR - Standard deviation of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::cdf-exponential\fR \fImean\fR \fIvalue\fR\fR
Return the cumulative probability of a given value for an exponential
distribution with given mean.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::cdf-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
Return the cumulative probability of a given value for a uniform
distribution with given extremes.
.sp
\fIxmin\fR - Minimum value of the distribution
.sp
\fIxmin\fR - Maximum value of the distribution
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::cdf-students-t\fR \fIdegrees\fR \fIvalue\fR\fR
Return the cumulative probability of a given value for a Student's t
distribution with given number of degrees.
.sp
\fIdegrees\fR - Number of degrees of freedom
.sp
\fIvalue\fR - Value for which the probability is required
.sp
.TP
\fB::math::statistics::random-normal\fR \fImean\fR \fIstdev\fR \fInumber\fR\fR
Return a list of "number" random values satisfying a normal
distribution with given mean and standard deviation.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fIstdev\fR - Standard deviation of the distribution
.sp
\fInumber\fR - Number of values to be returned
.sp
.TP
\fB::math::statistics::random-exponential\fR \fImean\fR \fInumber\fR\fR
Return a list of "number" random values satisfying an exponential
distribution with given mean.
.sp
\fImean\fR - Mean value of the distribution
.sp
\fInumber\fR - Number of values to be returned
.sp
.TP
\fB::math::statistics::random-uniform\fR \fIxmin\fR \fIxmax\fR \fIvalue\fR\fR
Return a list of "number" random values satisfying a uniform
distribution with given extremes.
.sp
\fIxmin\fR - Minimum value of the distribution
.sp
\fIxmin\fR - Maximum value of the distribution
.sp
\fInumber\fR - Number of values to be returned
.sp
.TP
\fB::math::statistics::histogram-uniform\fR \fIxmin\fR \fIxmax\fR \fIlimits\fR \fInumber\fR\fR
Return the expected histogram for a uniform distribution.
.sp
\fIxmin\fR - Minimum value of the distribution
.sp
\fIxmax\fR - Maximum value of the distribution
.sp
\fIlimits\fR - Upper limits for the buckets in the histogram
.sp
\fInumber\fR - Total number of "observations" in the histogram
.sp
TO DO: more function descriptions to be added
.SH "DATA MANIPULATION"
The data manipulation procedures act on lists or lists of lists:
.TP
\fB::math::statistics::filter\fR \fIvarname\fR \fIdata\fR \fIexpression\fR\fR
Return a list consisting of the data for which the logical
expression is true (this command works analogously to the command \fBforeach\fR).
.sp
\fIvarname\fR - Name of the variable used in the expression
.sp
\fIdata\fR - List of data
.sp
\fIexpression\fR - Logical expression using the variable name
.sp
.TP
\fB::math::statistics::map\fR \fIvarname\fR \fIdata\fR \fIexpression\fR\fR
Return a list consisting of the data that are transformed via the
expression.
.sp
\fIvarname\fR - Name of the variable used in the expression
.sp
\fIdata\fR - List of data
.sp
\fIexpression\fR - Expression to be used to transform (map) the data
.sp
.TP
\fB::math::statistics::samplescount\fR \fIvarname\fR \fIlist\fR \fIexpression\fR\fR
Return a list consisting of the \fIcounts\fR of all data in the
sublists of the "list" argument for which the expression is true.
.sp
\fIvarname\fR - Name of the variable used in the expression
.sp
\fIdata\fR - List of sublists, each containing the data
.sp
\fIexpression\fR - Logical expression to test the data (defaults to
"true").
.sp
.TP
\fB::math::statistics::subdivide\fR \fR
Routine \fIPM\fR - not implemented yet
.SH "PLOT PROCEDURES"
The following simple plotting procedures are available:
.TP
\fB::math::statistics::plot-scale\fR \fIcanvas\fR \fIxmin\fR \fIxmax\fR \fIymin\fR \fIymax\fR\fR
Set the scale for a plot in the given canvas. All plot routines expect
this function to be called first. There is no automatic scaling
provided.
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fIxmin\fR - Minimum x value
.sp
\fIxmax\fR - Maximum x value
.sp
\fIymin\fR - Minimum y value
.sp
\fIymax\fR - Maximum y value
.sp
.TP
\fB::math::statistics::plot-xydata\fR \fIcanvas\fR \fIxdata\fR \fIydata\fR \fItag\fR\fR
Create a simple XY plot in the given canvas - the data are
shown as a collection of dots. The tag can be used to manipulate the
appearance.
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fIxdata\fR - Series of independent data
.sp
\fIydata\fR - Series of dependent data
.sp
\fItag\fR - Tag to give to the plotted data (defaults to xyplot)
.sp
.TP
\fB::math::statistics::plot-xyline\fR \fIcanvas\fR \fIxdata\fR \fIydata\fR \fItag\fR\fR
Create a simple XY plot in the given canvas - the data are
shown as a line through the data points. The tag can be used to
manipulate the appearance.
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fIxdata\fR - Series of independent data
.sp
\fIydata\fR - Series of dependent data
.sp
\fItag\fR - Tag to give to the plotted data (defaults to xyplot)
.sp
.TP
\fB::math::statistics::plot-tdata\fR \fIcanvas\fR \fItdata\fR \fItag\fR\fR
Create a simple XY plot in the given canvas - the data are
shown as a collection of dots. The horizontal coordinate is equal to the
index. The tag can be used to manipulate the appearance.
This type of presentation is suitable for autocorrelation functions for
instance or for inspecting the time-dependent behaviour.
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fItdata\fR - Series of dependent data
.sp
\fItag\fR - Tag to give to the plotted data (defaults to xyplot)
.sp
.TP
\fB::math::statistics::plot-tline\fR \fIcanvas\fR \fItdata\fR \fItag\fR\fR
Create a simple XY plot in the given canvas - the data are
shown as a line. See plot-tdata for an explanation.
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fItdata\fR - Series of dependent data
.sp
\fItag\fR - Tag to give to the plotted data (defaults to xyplot)
.sp
.TP
\fB::math::statistics::plot-histogram\fR \fIcanvas\fR \fIcounts\fR \fIlimits\fR \fItag\fR\fR
Create a simple histogram in the given canvas
.sp
\fIcanvas\fR - Canvas widget to use
.sp
\fIcounts\fR - Series of bucket counts
.sp
\fIlimits\fR - Series of upper limits for the buckets
.sp
\fItag\fR - Tag to give to the plotted data (defaults to xyplot)
.sp
.SH "THINGS TO DO"
The following procedures are yet to be implemented:
.IP \(bu
F-test-stdev
.IP \(bu
interval-mean-stdev
.IP \(bu
histogram-normal
.IP \(bu
histogram-exponential
.IP \(bu
test-histogram
.IP \(bu
linear-model
.IP \(bu
linear-residuals
.IP \(bu
test-corr
.IP \(bu
quantiles-*
.IP \(bu
fourier-coeffs
.IP \(bu
fourier-residuals
.IP \(bu
onepar-function-fit
.IP \(bu
onepar-function-residuals
.IP \(bu
plot-linear-model
.IP \(bu
subdivide
.SH "EXAMPLES"
The code below is a small example of how you can examine a set of
data:
.PP
.nf

# Simple example:
# - Generate data (as a cheap way of getting some)
# - Perform statistical analysis to describe the data
#
package require math::statistics

#
# Two auxiliary procs
#
proc pause {time} {
   set wait 0
   after [expr {$time*1000}] {set ::wait 1}
   vwait wait
}

proc print-histogram {counts limits} {
   foreach count $counts limit $limits {
      if { $limit != {} } {
         puts [format "<%12.4g\\t%d" $limit $count]
         set prev_limit $limit
      } else {
         puts [format ">%12.4g\\t%d" $prev_limit $count]
      }
   }
}

#
# Our source of arbitrary data
#
proc generateData { data1 data2 } {
   upvar 1 $data1 _data1
   upvar 1 $data2 _data2

   set d1 0.0
   set d2 0.0
   for { set i 0 } { $i < 100 } { incr i } {
      set d1 [expr {10.0-2.0*cos(2.0*3.1415926*$i/24.0)+3.5*rand()}]
      set d2 [expr {0.7*$d2+0.3*$d1+0.7*rand()}]
      lappend _data1 $d1
      lappend _data2 $d2
   }
   return {}
}

#
# The analysis session
#
package require Tk
console show
canvas .plot1
canvas .plot2
pack   .plot1 .plot2 -fill both -side top

generateData data1 data2

puts "Basic statistics:"
set b1 [::math::statistics::basic-stats $data1]
set b2 [::math::statistics::basic-stats $data2]
foreach label {mean min max number stdev var} v1 $b1 v2 $b2 {
   puts "$label\\t$v1\\t$v2"
}
puts "Plot the data as function of \\"time\\" and against each other"
::math::statistics::plot-scale .plot1  0 100  0 20
::math::statistics::plot-scale .plot2  0 20   0 20
::math::statistics::plot-tline .plot1 $data1
::math::statistics::plot-tline .plot1 $data2
::math::statistics::plot-xydata .plot2 $data1 $data2

puts "Correlation coefficient:"
puts [::math::statistics::corr $data1 $data2]

pause 2
puts "Plot histograms"
.plot2 delete all
::math::statistics::plot-scale .plot2  0 20 0 100
set limits         [::math::statistics::minmax-histogram-limits 7 16]
set histogram_data [::math::statistics::histogram $limits $data1]
::math::statistics::plot-histogram .plot2 $histogram_data $limits

puts "First series:"
print-histogram $histogram_data $limits

pause 2
set limits         [::math::statistics::minmax-histogram-limits 0 15 10]
set histogram_data [::math::statistics::histogram $limits $data2]
::math::statistics::plot-histogram .plot2 $histogram_data $limits d2
.plot2 itemconfigure d2 -fill red

puts "Second series:"
print-histogram $histogram_data $limits

puts "Autocorrelation function:"
set  autoc [::math::statistics::autocorr $data1]
puts [::math::statistics::map $autoc {[format "%.2f" $x]}]
puts "Cross-correlation function:"
set  crossc [::math::statistics::crosscorr $data1 $data2]
puts [::math::statistics::map $crossc {[format "%.2f" $x]}]

::math::statistics::plot-scale .plot1  0 100 -1  4
::math::statistics::plot-tline .plot1  $autoc "autoc"
::math::statistics::plot-tline .plot1  $crossc "crossc"
.plot1 itemconfigure autoc  -fill green
.plot1 itemconfigure crossc -fill yellow

puts "Quantiles: 0.1, 0.2, 0.5, 0.8, 0.9"
puts "First:  [::math::statistics::quantiles $data1 {0.1 0.2 0.5 0.8 0.9}]"
puts "Second: [::math::statistics::quantiles $data2 {0.1 0.2 0.5 0.8 0.9}]"

.fi
If you run this example, then the following should be clear:
.IP \(bu
There is a strong correlation between two time series, as displayed by
the raw data and especially by the correlation functions.
.IP \(bu
Both time series show a significant periodic component
.IP \(bu
The histograms are not very useful in identifying the nature of the time
series - they do not show the periodic nature.
.SH "KEYWORDS"
data analysis, mathematics, statistics