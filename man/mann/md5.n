'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/trf/trf/doc/md5.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools = digest.inc
'\" -*- tcl -*- doctools = digest_header.inc
'\" -*- tcl -*- doctools = trf.inc
.so man.macros
.TH "md5" n 2.1p2  "Trf transformer commands"
.BS
.SH "NAME"
md5 \- Message digest "md5"
'\" -*- tcl -*- doctools = trf_header.inc
.SH "SYNOPSIS"
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1p2?\fR
.sp
\fBmd5\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
.sp
.BE
.SH "DESCRIPTION"
The command \fBmd5\fR is one of several message digests
provided by the package \fBtrf\fR. See \fBtrf-intro\fR for an
overview of the whole package.
.PP
.TP
\fBmd5\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
The options listed below are understood by the digest if and only if
the digest is \fIattached\fR to a channel.
See section \fBIMMEDIATE versus ATTACHED\fR for an explanation
of the term \fIattached\fR.
.RS
'\" -*- tcl -*- doctools = digest_options.inc
.TP
\fB-mode\fR \fBabsorb\fR|\fBwrite\fR|\fBtransparent\fR
This option has to be present. The specified argument determines the
behaviour of the digest in \fIattached\fR mode.
.sp
Beyond the argument values listed above all unique abbreviations are
recognized too. Their meaning is explained below:
.RS
.TP
\fBabsorb\fR
All data written to the channel is used to calculate the value of the
message digest and then passed unchanged to the next level in the
stack of transformations for the channel the digest is attached to.
When the channel is closed the completed digest is written out too,
essentially attaching the vlaue of the diggest after the information
actually written to the channel.
.sp
When reading from the channel a value for the digest is computed too,
and after closing of the channel compared to the digest which was
attached, i.e. came behind the actual data.
The option \fB-matchflag\fR has to be specified so that the digest
knows where to store the result of said comparison. This result is a
string and either "\fBok\fR", or "\fBfailed\fR".
.TP
\fBwrite\fR
All data read from or written to the channel the digest is attached to
is ignored and thrown away. Only a value for the digest of the data is
computed.
When the channel is closed the computed values are stored as ordered
through the options \fB-write-destination\fR, \fB-write-type\fR,
\fB-read-destination\fR, and \fB-read-type\fR.
.TP
\fBtransparent\fR
This mode is a mixture of both \fBabsorb\fR and \fBwrite\fR
modes. As for \fBabsorb\fR all data, read or written, passes through
the digest unchanged. The generated values for the digest however are
handled in the same way as for \fBwrite\fR.
.RE
.sp
.TP
\fB-matchflag\fR \fIvarname\fR
This option can be used if and only if the option "\fB-mode\fR
\fBabsorb\fR" is present. In that situation the argument is the name
of a global or namespaced variable. The digest will write the result
of comparing two digest values into this variable. The option will be
ignored if the channel is write-only, because in that case there will
be no comparison of digest values.
.TP
\fB-write-type\fR \fBvariable\fR|\fBchannel\fR
This option can be used for digests in mode \fBwrite\fR or
\fBtransparent\fR. Beyond the values listed above all their unique
abbreviations are also allowed as argument values.
The option determines the type of the argument to option
\fB-write-destination\fR. It defaults to \fBvariable\fR.
.TP
\fB-read-type\fR \fBvariable\fR|\fBchannel\fR
Like option \fB-write-type\fR, but for option \fB-read-destination\fR.
.TP
\fB-write-destination\fR \fIdata\fR
This option can be used for digests in mode \fBwrite\fR or
\fBtransparent\fR.
The value \fIdata\fR is either the name of a global (or namespaced)
variable or the handle of a writable channel, dependent on the value
of option \fB-write-type\fR. The message digest computed for data
written to the attached channel is written into it after the attached
channel was closed.
The option is ignored if the channel is read-only.
.sp
\fINote\fR that using a variable may yield incorrect results under
tcl 7.6, due to embedded \\0's.
.TP
\fB-read-destination\fR \fIdata\fR
This option can be used for digests in mode \fBwrite\fR or
\fBtransparent\fR.
The value \fIdata\fR is either the name of a global (or namespaced)
variable or the handle of a writable channel, dependent on the value
of option \fB-read-type\fR. The message digest computed for data
read from the attached channel is written into it after the attached
channel was closed.
The option is ignored if the channel is write-only.
.sp
\fINote\fR that using a variable may yield incorrect results under
tcl 7.6, due to embedded \\0's.
.RE
.sp
The options listed below are always understood by the digest,
\fIattached\fR versus \fIimmediate\fR does not matter. See section
\fBIMMEDIATE versus ATTACHED\fR for explanations of these two
terms.
.RS
'\" -*- tcl -*- doctools = common_options.inc
.TP
\fB-attach\fR \fIchannel\fR
The presence/absence of this option determines the main operation mode
of the transformation.
.sp
If present the transformation will be stacked onto the \fIchannel\fR
whose handle was given to the option and run in \fIattached\fR
mode. More about this in section \fBIMMEDIATE versus ATTACHED\fR.
.sp
If the option is absent the transformation is used in \fIimmediate\fR
mode and the options listed below are recognized. More about this in
section \fBIMMEDIATE versus ATTACHED\fR.
.TP
\fB-in\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the data
to transform has to be read from.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the data to transform is expected as the last argument to the
transformation.
.TP
\fB-out\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the
generated transformation result is written to.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the generated data is returned as the result of the command
itself.
.RE
'\" -*- tcl -*- doctools = digest_footer.inc
'\" -*- tcl -*- doctools = common_sections.inc
.SH "IMMEDIATE versus ATTACHED"
The transformation distinguishes between two main ways of using
it. These are the \fIimmediate\fR and \fIattached\fR operation
modes.
.PP
For the \fIattached\fR mode the option \fB-attach\fR is used to
associate the transformation with an existing channel. During the
execution of the command no transformation is performed, instead the
channel is changed in such a way, that from then on all data written
to or read from it passes through the transformation and is modified
by it according to the definition above.
This attachment can be revoked by executing the command \fBunstack\fR
for the chosen channel. This is the only way to do this at the Tcl
level.
.PP
In the second mode, which can be detected by the absence of option
\fB-attach\fR, the transformation immediately takes data from
either its commandline or a channel, transforms it, and returns the
result either as result of the command, or writes it into a channel.
The mode is named after the immediate nature of its execution.
.PP
Where the data is taken from, and delivered to, is governed by the
presence and absence of the options \fB-in\fR and \fB-out\fR.
It should be noted that this ability to immediately read from and/or
write to a channel is an historic artifact which was introduced at the
beginning of Trf's life when Tcl version 7.6 was current as this and
earlier versions have trouble to deal with \\0 characters embedded into
either input or output.
.SH "SEE ALSO"
adler, crc, crc-zlib, haval, md2, md5, md5_otp, ripemd-128, ripemd-160, sha, sha1, sha1_otp, trf-intro
.SH "KEYWORDS"
authentication, hash, hashing, mac, md5, message digest
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi