'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/log/logger.man' by tcllib/doctools with format 'nroff'
'\"
'\" -*- tcl -*- doctools manpage
'\" $Id: logger.man,v 1.8.2.1 2004/05/27 03:47:22 andreas_kupries Exp $
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "logger" n 0.3.1 log "Object Oriented logging facility"
.BS
.SH "NAME"
logger \- System to control logging of events.
.SH "SYNOPSIS"
package require \fBTcl  8.2\fR
.sp
package require \fBlogger  ?0.3.1?\fR
.sp
\fBlogger::init\fR \fIservice\fR\fR
.sp
\fBlogger::services\fR \fR
.sp
\fBlogger::enable\fR \fIlevel\fR\fR
.sp
\fBlogger::disable\fR \fIlevel\fR\fR
.sp
\fBlogger::levels\fR \fR
.sp
\fB${log}::debug\fR \fImessage\fR\fR
.sp
\fB${log}::info\fR \fImessage\fR\fR
.sp
\fB${log}::notice\fR \fImessage\fR\fR
.sp
\fB${log}::warn\fR \fImessage\fR\fR
.sp
\fB${log}::error\fR \fImessage\fR\fR
.sp
\fB${log}::critical\fR \fImessage\fR\fR
.sp
\fB${log}::setlevel\fR \fIlevel\fR\fR
.sp
\fB${log}::enable\fR \fIlevel\fR\fR
.sp
\fB${log}::disable\fR \fIlevel\fR\fR
.sp
\fB${log}::logproc\fR \fIlevel\fR \fIcommand\fR\fR
.sp
\fB${log}::logproc\fR \fIlevel\fR \fIargname\fR \fIbody\fR\fR
.sp
\fB${log}::services\fR \fR
.sp
\fB${log}::currentloglevel\fR \fR
.sp
\fB${log}::delproc\fR \fIcommand\fR\fR
.sp
\fB${log}::delete\fR \fR
.sp
.BE
.SH "DESCRIPTION"
.PP
The \fBlogger\fR package provides a flexible system for logging messages
from different services, at priority levels, with different commands.
.PP
To begin using the logger package, we do the following:
.PP
.nf
    package require logger
    set log [logger::init myservice]
    ${log}::notice "Initialized myservice logging"

    ... code ...

    ${log}::notice "Ending myservice logging"
    ${log}::delete
.fi
.PP
In the above code, after the package is loaded, the following things
happen:
.TP
\fBlogger::init\fR \fIservice\fR\fR
Initializes the service \fIservice\fR for logging.  The service names
are actually Tcl namespace names, so they are seperated with '::'.
When a logger service is initialized, it "inherits" properties from its
parents.  For instance, if there were a service \fIfoo\fR, and
we did a \fBlogger::init\fR \fIfoo::bar\fR (to create a \fIbar\fR
service underneath \fIfoo\fR), \fIbar\fR would copy the current
configuration of the \fIfoo\fR service, although it would of
course, also be possible to then seperately configure \fIbar\fR.
If a logger service is initialized and the parent does not yet exist, the
parent is also created.
.TP
\fBlogger::services\fR \fR
Returns a list of all the available services.
.TP
\fBlogger::enable\fR \fIlevel\fR\fR
Globally enables logging at and "above" the given level.  Levels are
\fBdebug\fR, \fBinfo\fR, \fBnotice\fR, \fBwarn\fR, \fBerror\fR,
\fBcritical\fR.
.TP
\fBlogger::disable\fR \fIlevel\fR\fR
Globally disables logging at and "below" the given level.  Levels are
those listed above.
.TP
\fBlogger::levels\fR \fR
Returns a list of the available log levels (also listed above under \fBenable\fR).
.TP
\fB${log}::debug\fR \fImessage\fR\fR
.TP
\fB${log}::info\fR \fImessage\fR\fR
.TP
\fB${log}::notice\fR \fImessage\fR\fR
.TP
\fB${log}::warn\fR \fImessage\fR\fR
.TP
\fB${log}::error\fR \fImessage\fR\fR
.TP
\fB${log}::critical\fR \fImessage\fR\fR
These are the commands called to actually log a message about an
event.  \fB${log}\fR is the variable obtained from \fBlogger::init\fR.
.TP
\fB${log}::setlevel\fR \fIlevel\fR\fR
Enable logging, in the service referenced by \fB${log}\fR, and its
children, at or above the level specified, and disable logging below
it.
.TP
\fB${log}::enable\fR \fIlevel\fR\fR
Enable logging, in the service referenced by \fB${log}\fR, and its
children, at and above the level specified.  Note that this does \fInot\fR disable logging below this level, so you should probably use
\fBsetlevel\fR instead.
.TP
\fB${log}::disable\fR \fIlevel\fR\fR
Disable logging, in the service referenced by \fB${log}\fR, and its
children, at and below the level specified. Note that this does \fInot\fR enable logging above this level,
so you should probably use \fBsetlevel\fR instead.
Disabling the loglevel \fBcritical\fR switches logging off for the service and its children.
.TP
\fB${log}::logproc\fR \fIlevel\fR \fIcommand\fR\fR
.TP
\fB${log}::logproc\fR \fIlevel\fR \fIargname\fR \fIbody\fR\fR
This command comes in two forms - the second, older one is deprecated
and may be removed from future versions of the logger package.  The
current version takes one argument, a command to be executed when the
level is called.  The callback command takes on argument, the text to
be logged.
\fBlogproc\fR specifies which command will perform the actual logging
for a given level.  The logger package ships with default commands for
all log levels, but with \fBlogproc\fR it is possible to replace them
with custom code.  This would let you send your logs over the network,
to a database, or anything else.  For example:
.nf
    proc logtoserver {txt} {
        variable socket
        puts $socket "Notice: $txt"
    }

    ${log}::logproc notice logtoserver
.fi
.TP
\fB${log}::services\fR \fR
Returns a list of the registered logging services which are children of this service.
.TP
\fB${log}::currentloglevel\fR \fR
Returns the currently enabled log level for this service. If no logging is enabled returns \fBnone\fR.
.TP
\fB${log}::delproc\fR \fIcommand\fR\fR
Set the script to call when the log instance in question is deleted.
For example:
.nf
    ${log}::delproc [list closesock $logsock]
.fi
.TP
\fB${log}::delete\fR \fR
This command deletes a particular logging service, and its children.
You must call this to clean up the resources used by a service.
.SH "IMPLEMENTATION"
The logger package is implemented in such a way as to optimize (for
Tcl 8.4 and newer) log procedures which are disabled.  They are
aliased to a proc which has no body, which is compiled to a no op in
bytecode.  This should make the peformance hit minimal.  If you really
want to pull out all the stops, you can replace the ${log} token in
your code with the actual namespace and command (${log}::warn becomes
::logger::tree::myservice::warn), so that no variable lookup is done.
This puts the performance of disabled logger commands very close to no
logging at all.
.PP
The "object orientation" is done through a hierarchy of namespaces.
Using an actual object oriented system would probably be a better way
of doing things, or at least provide for a cleaner implementation.
.PP
The service "object orientation" is done with namespaces.
.SH "KEYWORDS"
log, log level, logger, service