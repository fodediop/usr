'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/doctools/doctools_fmt.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2002 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\" Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "doctools_fmt" n 1.0 doctools "Documentation tools"
.BS
.SH "NAME"
doctools_fmt \- Specification of simple tcl markup for manpages
.SH "SYNOPSIS"
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
.sp
\fBvset\fR \fIvarname\fR\fR
.sp
\fBinclude\fR \fIfilename\fR\fR
.sp
\fBmanpage_begin\fR \fIcommand\fR \fIsection\fR \fIversion\fR\fR
.sp
\fBmanpage_end\fR \fR
.sp
\fBmoddesc\fR \fIdesc\fR\fR
.sp
\fBtitledesc\fR \fIdesc\fR\fR
.sp
\fBcopyright\fR \fItext\fR\fR
.sp
\fBdescription\fR \fR
.sp
\fBrequire\fR \fIpkg\fR ?\fIversion\fR?\fR
.sp
\fBsection\fR \fIname\fR\fR
.sp
\fBpara\fR \fR
.sp
\fBsee_also\fR \fIargs\fR\fR
.sp
\fBkeywords\fR \fIargs\fR\fR
.sp
\fBarg\fR \fItext\fR\fR
.sp
\fBcmd\fR \fItext\fR\fR
.sp
\fBopt\fR \fItext\fR\fR
.sp
\fBemph\fR \fItext\fR\fR
.sp
\fBstrong\fR \fItext\fR\fR
.sp
\fBcomment\fR \fItext\fR\fR
.sp
\fBsectref\fR \fItext\fR\fR
.sp
\fBsyscmd\fR \fItext\fR\fR
.sp
\fBmethod\fR \fItext\fR\fR
.sp
\fBoption\fR \fItext\fR\fR
.sp
\fBwidget\fR \fItext\fR\fR
.sp
\fBfun\fR \fItext\fR\fR
.sp
\fBtype\fR \fItext\fR\fR
.sp
\fBpackage\fR \fItext\fR\fR
.sp
\fBclass\fR \fItext\fR\fR
.sp
\fBvar\fR \fItext\fR\fR
.sp
\fBfile\fR \fItext\fR\fR
.sp
\fBuri\fR \fItext\fR\fR
.sp
\fBterm\fR \fItext\fR\fR
.sp
\fBconst\fR \fItext\fR\fR
.sp
\fBnl\fR \fR
.sp
\fBlb\fR \fR
.sp
\fBrb\fR \fR
.sp
\fBexample_begin\fR \fR
.sp
\fBexample_end\fR \fR
.sp
\fBexample\fR \fItext\fR\fR
.sp
\fBlist_begin\fR \fIwhat\fR\fR
.sp
\fBlist_end\fR \fR
.sp
\fBbullet\fR \fR
.sp
\fBenum\fR \fR
.sp
\fBlst_item\fR \fItext\fR\fR
.sp
\fBcall\fR \fIargs\fR\fR
.sp
\fBarg_def\fR \fItype\fR \fIname\fR ?\fImode\fR?\fR
.sp
\fBopt_def\fR \fIname\fR ?\fIarg\fR?\fR
.sp
\fBcmd_def\fR \fIcommand\fR\fR
.sp
\fBtkoption_def\fR \fIname\fR \fIdbname\fR \fIdbclass\fR\fR
.sp
\fBusage\fR \fIargs\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
This manpage specifies a documentation format for manpages. It is
intended to complement both the \fBdoctoc\fR format for writing
tables of contents and the \fBdocidx\fR format for writing indices.
See \fBdoctoc_fmt\fR and \fBdocidx_fmt\fR for the specification
of these two formats.
.PP
This format is called \fBdoctools\fR.
It provides all the necessary commands to write manpages.
Like for the \fBdoctoc\fR and \fBdocidx\fR formats a package is
provided implementing a generic framework for the conversion of
\fBdoctools\fR to a number of different output formats, like HTML,
TMML, nroff, LaTeX, etc.
The package is called \fBdoctools\fR, its documentation can be
found in \fBdoctools\fR.
People wishing to write a formatting engine for the conversion of
\fBdoctools\fR into a new output format have to read
\fBdoctools_api\fR. This manpage will explain the interface between
the generic package and such engines.
.SH "OVERVIEW"
\fBdoctoc\fR is similar to LaTex in that it consists primarily of
text, with markup commands embedded into it. The format used to mark
something as command is different from LaTeX however. All text between
matching pairs of [ and ] is a command, possibly with
arguments. Note that both brackets have to be on the same line for a
command to be recognized.
.PP
In contrast to both \fBdoctoc\fR and \fBdocidx\fR this format
does allow plain text beyond white space. This plain text will be the
contents of the described manpage.
.SH "FORMATTING COMMANDS"
.IP \(bu
The main commands are \fBmanpage_begin\fR, \fBmanpage_end\fR,
\fBmoddesc\fR, \fBtitledesc\fR, and \fBdescription\fR. Four of these
five are required for a manpage. The optional command is
\fBtitledesc\fR. The first two are the first and last commands in a
manpage. Neither text nor other commands may precede
\fBmanpage_begin\fR nor follow \fBmanpage_end\fR.  The command
\fBdescription\fR separates header and body of the manpage and may not
be omitted.
.sp
The remaining commands (\fBmoddesc\fR and \fBtitledesc\fR) provide
one-line descriptions of module and specific title respectively.
.IP \(bu
The only text allowed between \fBmanpage_begin\fR and
\fBdescription\fR is the command \fBrequire\fR. Other commands or
normal text are not permitted. \fBrequire\fR is used to list the
packages the described command(s) depend(s) on for its operation. This
list can be empty.
.IP \(bu
After \fBdescription\fR text and all other commands are allowed. The
text can be separated into highlevel blocks using named
\fBsection\fRs.  Each block can be further divided into paragraphs via
\fBpara\fR.
.IP \(bu
The commands \fBsee_also\fR and \fBkeywords\fR define whole sections
named \fISEE ALSO\fR and \fIKEYWORDS\fR. They can occur everywhere
in the manpage but making them the last section is the usual thing to
do. They can be omitted.
.IP \(bu
There are five commands available to markup words, \fBarg\fR,
\fBcmd\fR, \fBopt\fR, \fBemph\fR and \fBstrong\fR. The first three are
used to mark words as \fIcommand arguments\fR, as
\fIcommand names\fR and as \fIoptional\fR. The other two are
visual markup to emphasize words. The term \fIwords\fR is used in a
loose sense here, i.e application of the commands to a sequence of
words is entirely possible, if they are properly quoted. \fINote\fR
that usage of \fBstrong\fR is discouraged as this command is
deprecated and only present for backwards compatibility
.IP \(bu
Another set of commands is available to construct (possibly nested)
lists. These are \fBlist_begin\fR, \fBlist_end\fR, \fBlst_item\fR,
\fBbullet\fR, \fBenum\fR, \fBcall\fR, \fBarg_def\fR, \fBopt_def\fR,
\fBcmd_def\fR, and \fBtkoption_def\fR. The first two of these begin
and end a list respectively.
.sp
The argument to the first command denotes the type of the list. The
allowed values and their associated item command are explained later,
in the section detailing the \fBCommands\fR.
.sp
The other commands start list items and each can be used only inside a
list of their type. In other words, \fBbullet\fR is allowed in
bulletted lists but nowhere else, \fBenum\fR in enumerated lists and
\fBlst_item\fR and \fBcall\fR are for definition lists. These two
commands also have some text directly associated with the item
although the major bulk of the item is the text following the item
until the next list command.
.sp
The last list command, \fBcall\fR is special. It is used to describe
the syntax of a command and its arguments. It should not only cause
the appropriate markup of a list item at its place but also add the
syntax to the table of contents (synopsis) if supported by the output
format in question. nroff and HTML for example do. A format focused on
logical markup, like TMML, may not.
.IP \(bu
The command \fBusage\fR is similar to \fBcall\fR in that it adds the
syntax to the table of contents (synopsis) if supported by the output
format. Unlike \fBcall\fR,  this command doesn't add any text to the
output as a direct result of the command. Thus, it can be used
anywhere within the document to add usage information. Typically it is
used near the top of the document, in cases where it is not desireable
to use \fBcall\fR elsewhere in the document, or where additional usage
information is desired (e.g.: to document a "package require" command).
.SH "Commands"
.TP
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
Sets the formatter variable \fIvarname\fR to the specified
\fIvalue\fR. Returns the empty string.
.TP
\fBvset\fR \fIvarname\fR\fR
Returns the value associated with the formatter variable
\fIvarname\fR.
.TP
\fBinclude\fR \fIfilename\fR\fR
Reads the file named \fIfilename\fR, runs it through the expansion
process and returns the expanded result.
.TP
\fBmanpage_begin\fR \fIcommand\fR \fIsection\fR \fIversion\fR\fR
This command begins a manpage. Nothing is allowed to precede
it. Arguments are the name of the command described by the manpage,
the section of the manpages this manpages lives in, and the version of
the module containing the command. All have to fit on one line.
.TP
\fBmanpage_end\fR \fR
This command closes a manpage. Nothing is allowed to follow it.
.TP
\fBmoddesc\fR \fIdesc\fR\fR
This command is required and comes after \fBmanpage_begin\fR, but
before either \fBrequire\fR or \fBdescription\fR. Its argument
provides a one-line description of the module described by the manpage.
.TP
\fBtitledesc\fR \fIdesc\fR\fR
This command is optional and comes after \fBmanpage_begin\fR, but
before either \fBrequire\fR or \fBdescription\fR. Its argument
provides a one-line expansion of the title for the manpage. If this
command is not used the manpage processor has to use information from
\fBmoddesc\fR instead.
.TP
\fBcopyright\fR \fItext\fR\fR
This command is optional and comes after \fBmanpage_begin\fR, but
before either \fBrequire\fR or \fBdescription\fR. Its argument
declares the copyright assignment for the manpage. When invoked more
than once the assignments are accumulated.
.sp
A doctools processor is allowed to provide auch information too, but a
formatting engine has to give the accumulated arguments of this
command precedence over the data coming from the processor.
.TP
\fBdescription\fR \fR
This command separates the header part of the manpage from the main
body. Only \fBrequire\fR, \fBmoddesc\fR, or \fBtitledesc\fR may
precede it.
.TP
\fBrequire\fR \fIpkg\fR ?\fIversion\fR?\fR
May occur only between \fBmanpage_begin\fR and \fBdescription\fR. Is
used to list the packages which are required for the described command
to be operational.
.TP
\fBsection\fR \fIname\fR\fR
Used to structure the body of the manpage into named sections. This
command is not allowed inside of a list or example. It implicitly
closes the last \fBpara\fRgraph before the command and also implicitly
opens the first paragraph of the new section.
.TP
\fBpara\fR \fR
Used to structure sections into paragraphs. Must not be used inside of
a list or example.
.TP
\fBsee_also\fR \fIargs\fR\fR
Creates a section \fISEE ALSO\fR containing the arguments as
cross-references. Must not be used inside of a list or example.
.TP
\fBkeywords\fR \fIargs\fR\fR
Creates a section \fIKEYWORDS\fR containing the arguments as words
indexing the manpage. Must not be used inside of a list or example.
.TP
\fBarg\fR \fItext\fR\fR
Declares that the marked \fItext\fR is the name of a command argument.
.TP
\fBcmd\fR \fItext\fR\fR
Declares that the marked \fItext\fR is the name of a command.
.TP
\fBopt\fR \fItext\fR\fR
Declares that the marked \fItext\fR is something optional. Most often used
in conjunction with \fBarg\fR to denote optional command arguments.
.TP
\fBemph\fR \fItext\fR\fR
Emphasize the \fItext\fR.
.TP
\fBstrong\fR \fItext\fR\fR
Emphasize the \fItext\fR. Same as \fBemph\fR. Usage is
discouraged. The command is deprecated and present only for backward
compatibility.
.TP
\fBcomment\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a comment.
.TP
\fBsectref\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a section reference.
.TP
\fBsyscmd\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a system command.
.TP
\fBmethod\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a object method.
.TP
\fBoption\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a option.
.TP
\fBwidget\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a widget.
.TP
\fBfun\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a function.
.TP
\fBtype\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a data type.
.TP
\fBpackage\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a package.
.TP
\fBclass\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a class.
.TP
\fBvar\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a variable.
.TP
\fBfile\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a file .
.TP
\fBuri\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a uri.
.TP
\fBterm\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a unspecific terminology.
.TP
\fBconst\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a constant value.
.TP
\fBnl\fR \fR
Vertical space to separate text without breaking it into a new
paragraph.
.TP
\fBlb\fR \fR
Introduces a left bracket into the output.
.TP
\fBrb\fR \fR
Introduces a right bracket into the output. The bracket commands are
necessary as plain brackets are used to denote the beginnings and
endings of the formatting commands.
.TP
\fBexample_begin\fR \fR
Formats subsequent text as a code sample:
line breaks, spaces, and tabs are preserved and,
where appropriate, text is presented in a fixed-width font.
.TP
\fBexample_end\fR \fR
End of a code sample block.
.TP
\fBexample\fR \fItext\fR\fR
Formats \fItext\fR as a multi-line block of sample code.
\fItext\fR should be enclosed in braces.
.TP
\fBlist_begin\fR \fIwhat\fR\fR
Starts new list of type \fIwhat\fR. The allowed types (and their
associated item commands) are:
.RS
.TP
\fIbullet\fR
\fBbullet\fR
.TP
\fIenum\fR
\fBenum\fR
.TP
\fIdefinitions\fR
\fBlst_item\fR and \fBcall\fR
.TP
\fIarg\fR
\fBarg_def\fR
.TP
\fIcmd\fR
\fBcmd_def\fR
.TP
\fIopt\fR
\fBopt_def\fR
.TP
\fItkoption\fR
\fBtkoption_def\fR
.RE
.TP
\fBlist_end\fR \fR
Ends the list opened by the last \fBlist_begin\fR.
.TP
\fBbullet\fR \fR
Starts a new item in a bulletted list.
.TP
\fBenum\fR \fR
Starts a new item in an enumerated list.
.TP
\fBlst_item\fR \fItext\fR\fR
Starts a new item in a definition list. The argument is the term to be
defined.
.TP
\fBcall\fR \fIargs\fR\fR
Starts a new item in a definition list, but the term defined by it is
a command and its arguments.
.TP
\fBarg_def\fR \fItype\fR \fIname\fR ?\fImode\fR?\fR
Starts a new item in an argument list. Specifies the data-\fItype\fR
of the described argument, its \fIname\fR and possibly its
i/o-\fImode\fR.
.TP
\fBopt_def\fR \fIname\fR ?\fIarg\fR?\fR
Starts a new item in an option list. Specifies the \fIname\fR of the
option and possible (i.e. optional) \fIarg\fRuments.
.TP
\fBcmd_def\fR \fIcommand\fR\fR
Starts a new item in a command list. Specifies the name of the
\fIcommand\fR.
.TP
\fBtkoption_def\fR \fIname\fR \fIdbname\fR \fIdbclass\fR\fR
Starts a new item in a widget option list.  Specifies the \fIname\fR
of the option, i.e.  the name used in scripts, name used by the option
database, and the class (type) of the option.
.TP
\fBusage\fR \fIargs\fR\fR
Defines a term to be used in the table of contents or synopsis section,
depending on the format. This command is \fIsilent\fR, as it doesn't
add any text to the output as a direct result of the call. It merely
defines data to appear in another section.
.SH "EXAMPLE"
The tcl sources of this manpage can serve as an example for all of the
markup described by it. Almost every possible construct (with the
exception of \fBrequire\fR) is used here.
.SH "SEE ALSO"
docidx_fmt, doctoc_fmt, doctools, doctools_api
.SH "KEYWORDS"
HTML, LaTeX, TMML, generic markup, manpage, markup, nroff
.SH "COPYRIGHT"
.nf
Copyright (c) 2002 Andreas Kupries <andreas_kupries@users.sourceforge.net>
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi