'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/doctools/doctoc_fmt.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "doctoc_fmt" n 1.0 doctools "Documentation tools"
.BS
.SH "NAME"
doctoc_fmt \- Specification of simple tcl markup for table of contents
.SH "SYNOPSIS"
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
.sp
\fBvset\fR \fIvarname\fR\fR
.sp
\fBinclude\fR \fIfilename\fR\fR
.sp
\fBcomment\fR \fItext\fR\fR
.sp
\fBlb\fR \fR
.sp
\fBrb\fR \fR
.sp
\fBtoc_begin\fR \fItext\fR \fItitle\fR\fR
.sp
\fBtoc_end\fR \fR
.sp
\fBdivision_start\fR \fItext\fR\fR
.sp
\fBdivision_end\fR \fR
.sp
\fBitem\fR \fIfile\fR \fIlabel\fR \fIdesc\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
This manpage specifies a documentation format for tables of
contents. It is intended to complement both the \fBdoctools\fR
format for writing manpages and the \fBdocidx\fR format for writing
indices.  See \fBdoctools_fmt\fR and \fBdocidx_fmt\fR for the
specification of these two formats
.PP
This format is called \fBdoctoc\fR.
It provides all the necessary commands to write a table of contents
for a group of manpages. It is simpler than TMML, but convertible into
it.
Like for the \fBdoctools\fR and \fBdocidx\fR formats a package
is provided implementing a generic framework for the conversion of
\fBdoctoc\fR to a number of different output formats, like HTML,
TMML, nroff, LaTeX, etc.
The package is called \fBdoctools::toc\fR, its documentation can
be found in \fBdoctoc\fR.
People wishing to write a formatting engine for the conversion of
\fBdoctoc\fR into a new output format have to read
\fBdoctoc_api\fR. This manpage will explain the interface between
the generic package and such engines.
.SH "OVERVIEW"
\fBdoctoc\fR is similar to LaTex in that it consists primarily of
text, with markup commands embedded into it. The format used to mark
something as command is different from LaTeX however. All text between
matching pairs of [ and ] is a command, possibly with
arguments. Note that both brackets have to be on the same line for a
command to be recognized.
.PP
In this format plain text is not allowed, except for whitespace, which
can be used to separate the formatting commands described in the next
section (\fBFORMATTING COMMANDS\fR).
.SH "FORMATTING COMMANDS"
First a number of generic commands useable anywhere in a
\fBdoctoc\fR file.
.TP
\fBvset\fR \fIvarname\fR \fIvalue\fR\fR
Sets the formatter variable \fIvarname\fR to the specified
\fIvalue\fR. Returns the empty string.
.TP
\fBvset\fR \fIvarname\fR\fR
Returns the value associated with the formatter variable
\fIvarname\fR.
.TP
\fBinclude\fR \fIfilename\fR\fR
Instructs the system to insert the expanded contents of the file named
\fIfilename\fR in its own place.
.TP
\fBcomment\fR \fItext\fR\fR
Declares that the marked \fItext\fR is a comment.
Commands to insert special plain text. These bracket commands are
necessary as plain brackets are used to denote the beginnings and
endings of the formatting commands and thus cannot be used as normal
characters anymore.
.TP
\fBlb\fR \fR
Introduces a left bracket into the output.
.TP
\fBrb\fR \fR
Introduces a right bracket into the output.
And now the relevant markup commands.
.TP
\fBtoc_begin\fR \fItext\fR \fItitle\fR\fR
This command starts a table of contents. It has to be the very first
\fImarkup\fR command in a \fBdoctoc\fR file. Plain text is not
allowed to come before this command. Only the generic commands (see
above: \fBvset\fR, \fBinclude\fR, \fBcomment\fR) can be used before
it.
.sp
The \fItext\fR argument provides a label for the whole group of
manpages listed in the table of contents. Often this is the name of
the package (or extension) the manpages belong to.
.sp
The \fItitle\fR argument provides the title for the whole table of
contents.
.sp
The table of contents has to contain at least either one toc element
(\fBitem\fR) or one division.
.TP
\fBtoc_end\fR \fR
This command closes a table of contents. Nothing is allowed to follow
it.
.TP
\fBdivision_start\fR \fItext\fR\fR
This command and its counterpart \fBdivision_end\fR can be used to give
the table of contents additional structure.
.sp
Each division starts with \fBdivision_start\fR, is ended by \fBdivision_end\fR
and has a title provided through the argument \fItitle\fR. The
contents of a division are like for the whole table of contents,
i.e. a series of either toc elements or divisions. The latter means
that divisions can be nested.
.sp
The division has to contain at least either one toc element
(\fBitem\fR) or one division.
.TP
\fBdivision_end\fR \fR
This command closes a toc division. See \fBdivision_start\fR above for
the detailed explanation.
.TP
\fBitem\fR \fIfile\fR \fIlabel\fR \fIdesc\fR\fR
This command describes an individual toc element. The \fIfile\fR
argument refers to the file containing the actual manpage, and the
\fIdesc\fR provides a short descriptive text of that manpage. The
argument \fIlabel\fR can be used by engines supporting hyperlinks to
give the link a nice text (instead of the symbolic filename).
.sp
To preserve convertibility of this format to various output formats
the filename argument is considered a symbolic name. The actual name
of the file will be inserted by the formatting engine used to convert
the input, based on a mapping from symbolic to actual names given to
it.
.SH "NOTES"
.IP [1]
The commands for the \fBdoctoc\fR format are closely modeled on the
TMML tags used for describing collections of manpages.
.IP [2]
Using an appropriate formatting engine and some glue code it is
possible to automatically generate a document in \fBdoctoc\fR
format from a collection of manpages in \fBdoctools\fR format.
.SH "EXAMPLE"
As an example a table of contents for all manpages belonging to this
module (doctools) of package \fBtcllib\fR.
.PP
.nf
[toc_begin tcllib/doctools {Documentation tools}]
[division_start {Basic format}]
[item dtformat.man    {doctools format specification}]
[item dtformatter.man {doctools engine interface}]
[item doctools.man    {Package to handle doctools input and engines}]
[division_end]
[division_start {Table of Contents}]
[item dtocformat.man    {doctoc format specification}]
[item dtocformatter.man {doctoc engine interface}]
[item doctoc.man        {Package to handle doctoc input and engines}]
[division_end]
[division_start {Indices}]
[item dtidxformat.man    {docindex format specification}]
[item dtidxformatter.man {docindex engine interface}]
[item docindex.man       {Package to handle docindex input and engines}]
[division_end]
[toc_end]
.fi
.SH "SEE ALSO"
docidx_fmt, doctoc, doctoc_api, doctools_fmt
.SH "KEYWORDS"
HTML, LaTeX, TMML, generic markup, markup, nroff, table of contents, toc
.SH "COPYRIGHT"
.nf
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi