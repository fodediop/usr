'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/doctools/docidx_api.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "docidx_api" n 1.0 doctools "Documentation tools"
.BS
.SH "NAME"
docidx_api \- Interface specification for index formatting code
.SH "DESCRIPTION"
.PP
This manpage specifies the interface between formatting engines for
data in the \fBdocidx\fR format as specified in
\fBdocidx_fmt\fR, and \fBdoctools::idx\fR, the package for the
generic handling of such data, as described in \fBdocidx\fR.
.PP
Each formatting engine has to implement the conversion of input in
\fBdocidx\fR format to one particular output format as chosen by
the author of the formatting engine.
.SH "INTERFACE"
Each formatting engine has to provide
.IP [1]
Implementations of all the formatting commands as specified in
\fBdocidx_fmt\fR, using the defined names, but prefixed with the
string \fBfmt_\fR. The sole exceptions to this are the formatting
commands \fBvset\fR and \fBinclude\fR. These two commands are
processed by the generic layer and will never be seen by the
formatting engine.
.IP [2]
and additionally implementations for
.RS
.TP
\fBidx_numpasses\fR
This command is called immediately after the formatter is loaded and
has to return the number of passes required by this formatter to
process a manpage. This information has to be an integer number
greater or equal to one.
.TP
\fBidx_initialize\fR
This command is called at the beginning of every conversion run and is
responsible for initializing the general state of the formatting
engine.
.TP
\fBidx_setup\fR \fIn\fR
This command is called at the beginning of each pass over the input
and is given the id of the current pass as its first argument. It is
responsible for setting up the internal state of the formatting for
this particular pass.
.TP
\fBidx_postprocess\fR \fItext\fR
This command is called immediately after the last pass, with the
expansion result of that pass as argument, and can do any last-ditch
modifications of the generated result.  Its result will be the final
result of the conversion.
.sp
Most formats will use \fIidentity\fR here.
.TP
\fBidx_shutdown\fR
This command is called at the end of every conversion run and is
responsible for cleaning up of all the state in the formatting engine.
.TP
\fBfmt_plain_text\fR \fItext\fR
This command is called for any plain text encountered by the processor
in the input and can do any special processing required for plain
text. Its result is the string written into the expansion.
.sp
Most formats will use \fIidentity\fR here.
.TP
\fBidx_listvariables\fR
The command is called after loading a formatting engine to determine
which parameters are supported by that engine. The return value is a
list containing the names of these parameters.
.TP
\fBidx_varset\fR \fIvarname\fR \fItext\fR
The command is called by the generic layer to set the value of an
engine specific parameter. The parameter to change is specified by
\fIvarname\fR, and the value to set is given in \fItext\fR.
.sp
The command will throw an error if an unknown \fIvarname\fR is
used. Only the names returned by \fBidx_listvariables\fR are
considered known.
.RE
.PP
The tcl code of a formatting engine implementing all of the above can
make the following assumptions about its environment
.IP [1]
It has full access to its own safe interpreter.  In other words, the
engine cannot damage the other parts of the processor, nor can it
damage the filesystem.
.IP [2]
The surrounding system provides the engine with the following
commands:
.RS
.TP
Doctools commands
.RS
.TP
\fBdt_format\fR
Returns the name of format loaded into the engine
.TP
\fBdt_fmap\fR \fIfname\fR
Returns the actual name to use in the output in place of the symbolic
filename \fIfname\fR.
.TP
\fBdt_source\fR \fIfile\fR
This command allows the engine to load additional tcl code. The file
being loaded has to be in the same directory as the file the format
engine was loaded from. Any path specified for \fIfile\fR is ignored.
.RE
.TP
Expander commands
All of the commands below are methods of the expander object (without
the prefix \fBex_\fR) handling the input. Their arguments and
results are described in \fBexpander(n)\fR.
.RS
.TP
\fBex_cappend\fR
.TP
\fBex_cget\fR
.TP
\fBex_cis\fR
.TP
\fBex_cname\fR
.TP
\fBex_cpop\fR
.TP
\fBex_cpush\fR
.TP
\fBex_cset\fR
.TP
\fBex_lb\fR
.TP
\fBex_rb\fR
.RE
.TP
_idx_common.tcl commands
Any engine loading (\fBdt_source\fR) the file "\fI_idx_common.tcl\fR" has
default implementations of the \fBidx_\fR commands explicitly
listed in this document and of \fBfmt_plaint_text\fR.
.RE
.SH "SEE ALSO"
docidx, docidx_fmt
.SH "KEYWORDS"
HTML, LaTeX, TMML, generic markup, index, keywords, markup, nroff
.SH "COPYRIGHT"
.nf
Copyright (c) 2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi