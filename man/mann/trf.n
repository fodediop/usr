'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/trf/trf/doc/trf.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools
'\" -*- tcl -*- doctools = trf.inc
.so man.macros
.TH "trf-intro" n 2.1p2  "Trf transformer commands"
.BS
.SH "NAME"
trf-intro \- Introduction to Trf
'\" -*- tcl -*- doctools = trf_header.inc
.SH "SYNOPSIS"
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1p2?\fR
.sp
.BE
.SH "DESCRIPTION"
The package \fBTrf\fR provides a number of commands which take
data and transform them in various ways.
.SH "BACKGROUND"
The implementation of Trf began as proof-of-concept of the validity
and usefulness of the "stacked channel" patches to the core. These
patches allow the writing of extensions to the generic I/O system of
the core which are able to intercept all read/write operations on
designated channels, thus giving it the ability to transform the data
flowing through these channels as desired.
.PP
This allows things like transparent encryption, compression, charset
recoding, etc.
.PP
Since version 8.2 of the tcl core the aforementioned patches are part
of the tcl core itself, changing the status of \fBtrf\fR from
"extension requiring core patches" to "normal extension".
.PP
Other packages built upon either the stackd channels directly, or Trf
are:
.IP [1]
\fBTrfCrypt\fR, by myself, contains various encryption systems
.IP [2]
\fBTLS\fR, an SSL/TLS implementation by Matt Newman.
.IP [3]
\fBTcl MIME\fR by Marshall Rose.
.SH "API"
The commands provide by \fBtrf\fR can be placed into the three
categories listed below. Note that all commands are added to the
global namespace.
.TP
\fIEncodings\fR
The encoding commands either take some data and return the same data
in encoded form, or take encoded data and return a decoded result.
.RS
.IP [1]
\fBoct\fR
.IP [2]
\fBhex\fR
.IP [3]
\fBoct\fR
.IP [4]
\fBbase64\fR
.IP [5]
\fBuuencode\fR
.IP [6]
\fBascii85\fR
.IP [7]
\fBotp_words\fR
.IP [8]
\fBquoted-printable\fR
.RE
.sp
.TP
\fIMessage Digests\fR
The second category are message digests in general, simple ones like
\fBcrc\fR, and cryptographically strong algorithms like \fBmd5\fR.
.RS
.IP [1]
\fBcrc-zlib\fR
.IP [2]
\fBcrc\fR
.IP [3]
\fBadler\fR
.IP [4]
\fBmd2\fR
.IP [5]
\fBmd5\fR
.IP [6]
\fBmd5_otp\fR
.IP [7]
\fBsha\fR
.IP [8]
\fBsha1\fR
.IP [9]
\fBsha1_otp\fR
.IP [10]
\fBhaval\fR
.IP [11]
\fBripemd-160\fR
.IP [12]
\fBripemd-128\fR
.RE
.sp
.TP
Miscellaneous
At last a number of commands not readily placed into categories
providing password crypting, general transformations, data
compression, error correction and others.
.RS
.IP [1]
\fBcrypt\fR
.IP [2]
\fBmd5crypt\fR
.IP [3]
\fBtransform\fR
.IP [4]
\fBrs_ecc\fR
.IP [5]
\fBzip\fR
.IP [6]
\fBbz2\fR
.IP [7]
\fBunstack\fR
.RE
.SH "SEE ALSO"
adler, ascii85, base64, bz2, crc, crc-zlib, crypt, haval, hex, md2, md5, md5_otp, md5crypt, oct, otp_words, quoted-printable, ripemd-128, ripemd-160, rs_ecc, sha, sha1, sha1_otp, transform, uuencode, zip
.SH "KEYWORDS"
compression, encoding, error correction, message digest, transformation
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi