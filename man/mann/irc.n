'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/irc/irc.man' by tcllib/doctools with format 'nroff'
'\"
'\" -*- tcl -*- doctools manpage
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "irc" n 0.4 irc "Low Level Tcl IRC Interface"
.BS
.SH "NAME"
irc \- Create IRC connection and interface.
.SH "SYNOPSIS"
package require \fBTcl \fR
.sp
package require \fBirc  ?0.4?\fR
.sp
\fB::irc::config\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fB::irc::connection\fR \fR
.sp
\fB::irc::connections\fR \fR
.sp
\fB::irc::destroy\fR \fInet\fR\fR
.sp
\fInet\fR \fBregisterevent\fR \fIevent\fR \fIscript\fR\fR
.sp
\fInet\fR \fBgetevent\fR \fIevent\fR \fIscript\fR\fR
.sp
\fInet\fR \fBeventexists\fR \fIevent\fR \fIscript\fR\fR
.sp
\fInet\fR \fBconnect\fR \fIhostname\fR ?\fIport\fR?\fR
.sp
\fInet\fR \fBconnected\fR\fR
.sp
\fInet\fR \fBsockname\fR\fR
.sp
\fInet\fR \fBpeername\fR\fR
.sp
\fInet\fR \fBuser\fR \fIusername\fR \fIlocalhostname\fR \fIlocaldomainname\fR \fIuserinfo\fR\fR
.sp
\fInet\fR \fBnick\fR \fInick\fR\fR
.sp
\fInet\fR \fBping\fR \fItarget\fR\fR
.sp
\fInet\fR \fBserverping\fR\fR
.sp
\fInet\fR \fBjoin\fR \fIchannel\fR ?\fIkey\fR?\fR
.sp
\fInet\fR \fBpart\fR \fIchannel\fR ?\fImessage\fR?\fR
.sp
\fInet\fR \fBquit\fR ?\fImessage\fR?\fR
.sp
\fInet\fR \fBprivmsg\fR \fItarget\fR \fImessage\fR\fR
.sp
\fInet\fR \fBnotice\fR \fItarget\fR \fImessage\fR\fR
.sp
\fInet\fR \fBctcp\fR \fItarget\fR \fImessage\fR\fR
.sp
\fInet\fR \fBkick\fR \fIchannel\fR \fItarget\fR ?\fImessage\fR?\fR
.sp
\fInet\fR \fBmode\fR \fItarget\fR \fIargs\fR\fR
.sp
\fInet\fR \fBtopic\fR \fIchannel\fR \fImessage\fR\fR
.sp
\fInet\fR \fBinvite\fR \fIchannel\fR \fItarget\fR\fR
.sp
\fInet\fR \fBsend\fR \fItext\fR\fR
.sp
\fBwho\fR ?\fBaddress\fR?\fR
.sp
\fBaction\fR \fR
.sp
\fBtarget\fR \fR
.sp
\fBadditional\fR \fR
.sp
\fBheader\fR \fR
.sp
\fBmsg\fR \fR
.sp
.BE
.SH "DESCRIPTION"
This package provides low-level commands to deal with the IRC protocol
(Internet Relay Chat) for immediate and interactive multi-cast
communication.
.PP
.TP
\fB::irc::config\fR \fIkey\fR \fIvalue\fR\fR
Sets configuration \fIkey\fR to \fIvalue\fR.  Currently, the only
configuration key defined is the boolean flag \fBdebug\fR which,
when turned on, makes \fBirc\fR print more information about what
is going on.
.TP
\fB::irc::connection\fR \fR
The command creates a new object to deal with an IRC connection.
Creating this IRC object does not automatically create the network
connection.  It returns a new irc namespace command which can be used
to interact with the new IRC connection.  NOTE: the old form of the
connection command, which took a hostname and port as arguments, is
deprecated.  Use \fBconnect\fR instead to specify this information.
.TP
\fB::irc::connections\fR \fR
Returns a list of all the current connections that were created with
\fBconnection\fR
.TP
\fB::irc::destroy\fR \fInet\fR\fR
Deletes a connection and its associated namespace and information.
.SH "Per-connection Commands"
.PP
In the following list of available connection methods \fInet\fR
represents a connection command as returned by
\fB::irc::connection\fR.
.TP
\fInet\fR \fBregisterevent\fR \fIevent\fR \fIscript\fR\fR
Registers a callback handler for the specific event.  Events available
are those described in RFC 1459
\fIhttp://www.rfc-editor.org/rfc/rfc1459.txt\fR.
In addition, there are several other events defined.
\fBdefaultcmd\fR adds a command that is called if no other callback
is present.  \fBEOF\fR is called if the connection signals an End of
File condition. The events \fBdefaultcmd\fR, \fBdefaultnumeric\fR,
and \fBdefaultevent\fR are required.
\fIscript\fR is executed in the connection namespace, which can take
advantage of several commands (see \fBCallback Commands\fR
below) to aid in the parsing of data.
.TP
\fInet\fR \fBgetevent\fR \fIevent\fR \fIscript\fR\fR
Returns the current handler for the event if one exists. Otherwise an
empty string is returned.
.TP
\fInet\fR \fBeventexists\fR \fIevent\fR \fIscript\fR\fR
Returns a boolean value indicating the existence of the event handler.
.TP
\fInet\fR \fBconnect\fR \fIhostname\fR ?\fIport\fR?\fR
This causes the socket to be established.  \fB::irc::connection\fR
created the namespace and the commands to be used, but did not
actually open the socket. This is done here.  NOTE: the older form of
'connect' did not require the user to specify a hostname and port,
which were specified with 'connection'.  That form is deprecated.
.TP
\fInet\fR \fBconnected\fR\fR
Returns a boolean value indicating if this connection is connected to a server.
.TP
\fInet\fR \fBsockname\fR\fR
Returns a 3 element list consisting of the ip address, the hostname, and the port
of the local end of the connection, if currently connected.
.TP
\fInet\fR \fBpeername\fR\fR
Returns a 3 element list consisting of the ip address, the hostname, and the port
of the remote end of the connection, if currently connected.
.TP
\fInet\fR \fBuser\fR \fIusername\fR \fIlocalhostname\fR \fIlocaldomainname\fR \fIuserinfo\fR\fR
Sends USER command to server.  \fIusername\fR is the username you want
to appear.  \fIlocalhostname\fR is the host portion of your hostname, \fIlocaldomainname\fR
is your domain name, and \fIuserinfo\fR is a short description of who you are. The 2nd and 3rd
arguments are normally ignored by the IRC server.
.TP
\fInet\fR \fBnick\fR \fInick\fR\fR
NICK command.  \fInick\fR is the nickname you wish to use for the
particular connection.
.TP
\fInet\fR \fBping\fR \fItarget\fR\fR
Send a CTCP PING to \fItarget\fR.
.TP
\fInet\fR \fBserverping\fR\fR
PING the server.
.TP
\fInet\fR \fBjoin\fR \fIchannel\fR ?\fIkey\fR?\fR
\fIchannel\fR is the IRC channel to join.  IRC channels typically
begin with a hashmark ("#") or ampersand ("&").
.TP
\fInet\fR \fBpart\fR \fIchannel\fR ?\fImessage\fR?\fR
Makes the client leave \fIchannel\fR. Some networks may support the optional
argument \fImessage\fR
.TP
\fInet\fR \fBquit\fR ?\fImessage\fR?\fR
Instructs the IRC server to close the current connection. The package
will use a generic default if no \fImessage\fR was specified.
.TP
\fInet\fR \fBprivmsg\fR \fItarget\fR \fImessage\fR\fR
Sends \fImessage\fR to \fItarget\fR, which can be either a channel, or
another user, in which case their nick is used.
.TP
\fInet\fR \fBnotice\fR \fItarget\fR \fImessage\fR\fR
Sends a \fBnotice\fR with message \fImessage\fR to \fItarget\fR,
which can be either a channel, or another user, in which case their nick is used.
.TP
\fInet\fR \fBctcp\fR \fItarget\fR \fImessage\fR\fR
Sends a CTCP of type \fImessage\fR to \fItarget\fR
.TP
\fInet\fR \fBkick\fR \fIchannel\fR \fItarget\fR ?\fImessage\fR?\fR
Kicks the user \fItarget\fR from the channel \fIchannel\fR with a \fImessage\fR.
The latter can be left out.
.TP
\fInet\fR \fBmode\fR \fItarget\fR \fIargs\fR\fR
Sets the mode \fIargs\fR on the target \fItarget\fR. \fItarget\fR may be a channel,
a channel user, or yourself.
.TP
\fInet\fR \fBtopic\fR \fIchannel\fR \fImessage\fR\fR
Sets the topic on \fIchannel\fR to \fImessage\fR specifying an empty string
will remove the topic.
.TP
\fInet\fR \fBinvite\fR \fIchannel\fR \fItarget\fR\fR
Invites \fItarget\fR to join the channel \fIchannel\fR
.TP
\fInet\fR \fBsend\fR \fItext\fR\fR
Sends \fItext\fR to the IRC server.
.SH "Callback Commands"
.PP
These commands can be used within callbacks
.TP
\fBwho\fR ?\fBaddress\fR?\fR
Returns the nick of the user who performed a command.  The optional
keyword \fBaddress\fR causes the command to return the user in the
format "username@address".
.TP
\fBaction\fR \fR
Returns the action performed, such as KICK, PRIVMSG, MODE, etc...
Normally not useful, as callbacks are bound to a particular event.
.TP
\fBtarget\fR \fR
Returns the target of a particular command, such as the channel or
user to whom a PRIVMSG is sent.
.TP
\fBadditional\fR \fR
Returns a list of any additional arguments after the target.
.TP
\fBheader\fR \fR
Returns the entire event header (everything up to the :) as a proper list.
.TP
\fBmsg\fR \fR
Returns the message portion of the command (the part after the :).
.SH "SEE ALSO"
rfc 1459
.SH "KEYWORDS"
chat, irc