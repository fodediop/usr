'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/trf/trf/doc/zip.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*- doctools = compress/header.inc
'\" -*- tcl -*- doctools = trf.inc
.so man.macros
.TH "zip" n 2.1p2  "Trf transformer commands"
.BS
.SH "NAME"
zip \- Data compression "zip"
'\" -*- tcl -*- doctools = trf_header.inc
.SH "SYNOPSIS"
package require \fBTcl  ?8.2?\fR
.sp
package require \fBTrf  ?2.1p2?\fR
.sp
\fBzip\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
.sp
.BE
.SH "DESCRIPTION"
The command \fBzip\fR is one of several data compressions
provided by the package \fBtrf\fR. See \fBtrf-intro\fR for an
overview of the whole package.
.PP
The command is based on the deflate compression algorithm as specified
in RFC 1951 (\fIhttp://www.rfc-editor.org/rfc/rfc1951.txt\fR) and as
implemented by the zlib compression library
(\fIhttp://www.gzip.org/zlib/\fR).
See also RFC 1950 (\fIhttp://www.rfc-editor.org/rfc/rfc1950.txt\fR)
.PP
.TP
\fBzip\fR ?\fIoptions...\fR? ?\fIdata\fR?\fR
.RS
'\" -*- tcl -*- doctools = compress/options.inc
.TP
\fB-mode\fR \fBcompress\fR|\fBdecompress\fR
This option has to be present and is always understood by the
compression.
.sp
For \fIimmediate\fR mode the argument value specifies the operation
to use.  For an \fIattached\fR compress it specifies the operation to
use for \fIwriting\fR. Reading will automatically use the reverse
operation.
See section \fBIMMEDIATE versus ATTACHED\fR for explanations of
these two terms.
.sp
Beyond the argument values listed above all unique abbreviations are
recognized too.
.sp
\fBCompress\fR causes the compression of arbitrary (most likely
binary) data. \fBDecompression\fR does the reverse .
.TP
\fB-level\fR \fIinteger\fR
Specifies the compression level. Is either the string \fBdefault\fR
or an integer number in the range \fB1\fR (minimal compression) to
\fB9\fR (maximal compression).
.TP
\fB-nowrap\fR \fIboolean\fR
If set to \fBtrue\fR the command will not create the zip specific
header (See RFC 1950) normally written before the compressed data. The
options defaults to \fBfalse\fR.
It has to be used when writing a \fBgzip\fR emulation in Tcl as
gzip creates a different header.
'\" -*- tcl -*- doctools = common_options.inc
.TP
\fB-attach\fR \fIchannel\fR
The presence/absence of this option determines the main operation mode
of the transformation.
.sp
If present the transformation will be stacked onto the \fIchannel\fR
whose handle was given to the option and run in \fIattached\fR
mode. More about this in section \fBIMMEDIATE versus ATTACHED\fR.
.sp
If the option is absent the transformation is used in \fIimmediate\fR
mode and the options listed below are recognized. More about this in
section \fBIMMEDIATE versus ATTACHED\fR.
.TP
\fB-in\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the data
to transform has to be read from.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the data to transform is expected as the last argument to the
transformation.
.TP
\fB-out\fR \fIchannel\fR
This options is legal if and only if the transformation is used in
\fIimmediate\fR mode. It provides the handle of the channel the
generated transformation result is written to.
.sp
If the transformation is in \fIimmediate\fR mode and this option is
absent the generated data is returned as the result of the command
itself.
.RE
'\" -*- tcl -*- doctools = compress/footer.inc
'\" -*- tcl -*- doctools = common_sections.inc
.SH "IMMEDIATE versus ATTACHED"
The transformation distinguishes between two main ways of using
it. These are the \fIimmediate\fR and \fIattached\fR operation
modes.
.PP
For the \fIattached\fR mode the option \fB-attach\fR is used to
associate the transformation with an existing channel. During the
execution of the command no transformation is performed, instead the
channel is changed in such a way, that from then on all data written
to or read from it passes through the transformation and is modified
by it according to the definition above.
This attachment can be revoked by executing the command \fBunstack\fR
for the chosen channel. This is the only way to do this at the Tcl
level.
.PP
In the second mode, which can be detected by the absence of option
\fB-attach\fR, the transformation immediately takes data from
either its commandline or a channel, transforms it, and returns the
result either as result of the command, or writes it into a channel.
The mode is named after the immediate nature of its execution.
.PP
Where the data is taken from, and delivered to, is governed by the
presence and absence of the options \fB-in\fR and \fB-out\fR.
It should be noted that this ability to immediately read from and/or
write to a channel is an historic artifact which was introduced at the
beginning of Trf's life when Tcl version 7.6 was current as this and
earlier versions have trouble to deal with \\0 characters embedded into
either input or output.
.SH "SEE ALSO"
bz2, trf-intro, zip
.SH "KEYWORDS"
compression, data compression, decompression, rfc 1950, rfc 1951, rfc 1952, zip
.SH "COPYRIGHT"
.nf
Copyright (c) 1996-2003, Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi