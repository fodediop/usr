'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/comm/comm.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 1995-1998 The Open Group. All Rights Reserved.
'\" Copyright (c) 2003 ActiveState Corporation.
'\"
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "comm" n 4.2 comm "remote communication"
.BS
.SH "NAME"
comm \- A remote communications facility for Tcl (7.6, 8.0, and later)
.SH "SYNOPSIS"
package require \fBTcl  8.2\fR
.sp
package require \fBcomm  ?4.2?\fR
.sp
\fB::comm::comm send\fR ?-async? ?-command \fIcallback\fR? \fIid\fR \fIcmd\fR ?\fIarg arg ...\fR?\fR
.sp
\fB::comm::comm self\fR \fR
.sp
\fB::comm::comm interps\fR \fR
.sp
\fB::comm::comm connect\fR ?\fIid\fR?\fR
.sp
\fB::comm::comm new\fR \fIchan\fR ?\fIname value ...\fR?\fR
.sp
\fB::comm::comm channels\fR \fR
.sp
\fB::comm::comm config\fR \fR
.sp
\fB::comm::comm config\fR \fIname\fR\fR
.sp
\fB::comm::comm config\fR ?\fIname value ...\fR?\fR
.sp
\fB::comm::comm shutdown\fR \fIIid\fR\fR
.sp
\fB::comm::comm abort\fR \fR
.sp
\fB::comm::comm destroy\fR \fR
.sp
\fB::comm::comm hook\fR \fIevent\fR ?\fB+\fR? ?\fIscript\fR?\fR
.sp
\fB::comm::comm remoteid\fR \fR
.sp
\fB::comm::comm_send\fR \fR
.sp
.BE
.SH "DESCRIPTION"
.PP
The \fBcomm\fR command provides an inter-interpreter remote
execution facility much like Tk's \fBsend(n)\fR, except that it uses
sockets rather than the X server for the communication path.  As a
result, \fBcomm\fR works with multiple interpreters, works on
Windows and Macintosh systems, and provides control over the remote
execution path.
.PP
These commands work just like \fBsend\fR and \fBwinfo interps\fR :
.nf
 ::comm::comm send ?-async? id cmd ?arg arg ...?
 ::comm::comm interps
.fi
.PP
This is all that is really needed to know in order to use
\fBcomm\fR
.SH "COMMANDS"
.PP
The package initializes \fB::comm::comm\fR as the default \fIchan\fR.
.PP
\fBcomm\fR names communication endpoints with an \fIid\fR unique
to each machine.  Before sending commands, the \fIid\fR of another
interpreter is needed.  Unlike Tk's send, \fBcomm\fR doesn't
implicitly know the \fIid\fR's of all the interpreters on the system.
The following four methods make up the basic \fBcomm\fR interface.
.TP
\fB::comm::comm send\fR ?-async? ?-command \fIcallback\fR? \fIid\fR \fIcmd\fR ?\fIarg arg ...\fR?\fR
This invokes the given command in the interpreter named by \fIid\fR.  The
command waits for the result and remote errors are returned unless the
\fB-async\fR or \fB-command\fR option is given.  If \fB-async\fR
is given, send returns immediately and there is no further notification of
result.  If \fB-command\fR is used, \fIcallback\fR specifies a command
to invoke when the result is received.  These options are mutually
exclusive.  The callback will receive arguments in the form
\fI-option value\fR, suitable for \fBarray set\fR.
The options are: \fI-id\fR, the comm id of the interpreter that received
the command; \fI-serial\fR, a unique serial for each command sent to a
particular comm interpreter; \fI-chan\fR, the comm channel name;
\fI-code\fR, the result code of the command; \fI-errorcode\fR, the
errorcode, if any, of the command; \fI-errorinfo\fR, the errorinfo, if
any, of the command; and \fI-result\fR, the return value of the command.
If connection is lost before a reply is received, the callback will be
invoked with a connection lost message with -code equal to -1.  When
\fB-command\fR is used, the command returns the unique serial for the
command.
.TP
\fB::comm::comm self\fR \fR
Returns the \fIid\fR for this channel.
.TP
\fB::comm::comm interps\fR \fR
Returns a list of all the remote \fIid\fR's to which this channel is
connected.  \fBcomm\fR learns a new remote \fIid\fR when a command
is first issued it, or when a remote \fIid\fR first issues a command
to this comm channel.  \fB::comm::comm ids\fR is an alias for this
method.
.TP
\fB::comm::comm connect\fR ?\fIid\fR?\fR
Whereas \fB::comm::comm send\fR will automatically connect to the
given \fIid\fR, this forces a connection to a remote \fIid\fR without
sending a command.  After this, the remote \fIid\fR will appear in
\fB::comm::comm interps\fR.
.SH "EVAL SEMANTICS"
.PP
The evaluation semantics of \fB::comm::comm send\fR are intended to
match Tk's \fBsend\fR \fIexactly\fR. This means that \fBcomm\fR
evaluates arguments on the remote side.
.PP
If you find that \fB::comm::comm send\fR doesn't work for a
particular command, try the same thing with Tk's send and see if the
result is different.  If there is a problem, please report it.  For
instance, there was had one report that this command produced an
error.  Note that the equivalent \fBsend\fR command also produces the
same error.
.PP
.nf
 % ::comm::comm send id llength {a b c}
 wrong # args: should be "llength list"
 % send name llength {a b c}
 wrong # args: should be "llength list"
.fi
.PP
The \fBeval\fR hook (described below) can be used to change from
\fBsend\fR's double eval semantics to single eval semantics.
.SH "MULTIPLE CHANNELS"
.PP
More than one \fBcomm\fR channel (or \fIlistener\fR) can be created
in each Tcl interpreter.  This allows flexibility to create full and
restricted channels.  For instance, \fIhook\fR scripts are specific
to the channel they are defined against.
.TP
\fB::comm::comm new\fR \fIchan\fR ?\fIname value ...\fR?\fR
This creates a new channel and Tcl command with the given channel
name.  This new command controls the new channel and takes all the
same arguments as \fB::comm::comm\fR.  Any remaining arguments are
passed to the \fBconfig\fR method.  The fully qualified channel
name is returned.
.TP
\fB::comm::comm channels\fR \fR
This lists all the channels allocated in this Tcl interpreter.
.PP
The default configuration parameters for a new channel are:
.PP
.nf
  "-port 0 -local 1 -listen 0"
.fi
.PP
The default channel \fB::comm::comm\fR is created with:
.PP
.nf
  "::comm::comm new ::comm::comm -port 0 -local 1 -listen 1"
.fi
.SH "CHANNEL CONFIGURATION"
.PP
The \fBconfig\fR method acts similar to \fBfconfigure\fR in that it
sets or queries configuration variables associated with a channel.
.TP
\fB::comm::comm config\fR \fR
.TP
\fB::comm::comm config\fR \fIname\fR\fR
.TP
\fB::comm::comm config\fR ?\fIname value ...\fR?\fR
When given no arguments, \fBconfig\fR returns a list of all variables
and their value With one argument, \fBconfig\fR returns the value of
just that argument.  With an even number of arguments, the given
variables are set to the given values.
.PP
These configuration variables can be changed (descriptions of them are
elsewhere in this manual page):
.TP
\fB-listen\fR ?\fI0|1\fR?
.TP
\fB-local\fR  ?\fI0|1\fR?
.TP
\fB-port\fR   ?\fIport\fR?
.PP
These configuration variables are readonly:
.TP
\fB-chan\fR    \fIchan\fR
.TP
\fB-serial\fR  \fIn\fR
.TP
\fB-socket\fR  sock\fIIn\fR
.PP
When \fBconfig\fR changes the parameters of an existing channel, it
closes and reopens the listening socket.  An automatically assigned
channel \fIid\fR will change when this happens.  Recycling the socket
is done by invoking \fB::comm::comm abort\fR, which causes all
active sends to terminate.
.SH "ID/PORT ASSIGNMENTS"
.PP
\fBcomm\fR uses a TCP port for endpoint \fIid\fR.  The
\fBinterps\fR (or \fBids\fR) method merely lists all the TCP ports
to which the channel is connected.  By default, each channel's
\fIid\fR is randomly assigned by the operating system (but usually
starts at a low value around 1024 and increases each time a new socket
is opened).  This behavior is accomplished by giving the
\fB-port\fR config option a value of 0.  Alternately, a specific
TCP port number may be provided for a given channel.  As a special
case, comm contains code to allocate a a high-numbered TCP port
(>10000) by using \fB-port {}\fR.  Note that a channel won't be
created and initialized unless the specific port can be allocated.
.PP
As a special case, if the channel is configured with
\fB-listen 0\fR, then it will not create a listening socket and
will use an id of \fI0\fR for itself.  Such a channel is only good
for outgoing connections (although once a connection is established,
it can carry send traffic in both directions).
.SH "REMOTE INTERPRETERS"
.PP
By default, each channel is restricted to accepting connections from
the local system.  This can be overridden by using the
\fB-local 0\fR configuration option For such channels, the
\fIid\fR parameter takes the form \fI{ id host }\fR.
.PP
\fIWARNING\fR: The \fIhost\fR must always be specified in the same
form (e.g., as either a fully qualified domain name, plain hostname or
an IP address).
.SH "CLOSING CONNECTIONS"
.PP
These methods give control over closing connections:
.TP
\fB::comm::comm shutdown\fR \fIIid\fR\fR
This closes the connection to \fIid\fR, aborting all outstanding
commands in progress.  Note that nothing prevents the connection from
being immediately reopened by another incoming or outgoing command.
.TP
\fB::comm::comm abort\fR \fR
This invokes shutdown on all open connections in this comm channel.
.TP
\fB::comm::comm destroy\fR \fR
This aborts all connections and then destroys the this comm channel
itself, including closing the listening socket.  Special code allows
the default \fB::comm::comm\fR channel to be closed such that the
\fB::comm::comm\fR command it is not destroyed.  Doing so closes the
listening socket, preventing both incoming and outgoing commands on
the channel.  This sequence reinitializes the default channel:
.sp
.nf
 "::comm::comm destroy; ::comm::comm new ::comm::comm"
.fi
.PP
When a remote connection is lost (because the remote exited or called
\fBshutdown\fR), \fBcomm\fR can invoke an application callback.
This can be used to cleanup or restart an ancillary process, for
instance.  See the \fIlost\fR callback below.
.SH "CALLBACKS"
.PP
This is a mechanism for setting hooks for particular events:
.TP
\fB::comm::comm hook\fR \fIevent\fR ?\fB+\fR? ?\fIscript\fR?\fR
This uses a syntax similar to Tk's \fBbind\fR command.  Prefixing
\fIscript\fR with a \fB+\fR causes the new script to be appended.
Without this, a new \fIscript\fR replaces any existing script.  When
invoked without a script, no change is made.  In all cases, the new
hook script is returned by the command.
.sp
When an \fIevent\fR occurs, the \fIscript\fR associated with it is
evaluated with the listed variables in scope and available.  The
return code (\fInot\fR the return value) of the script is commonly
used decide how to further process after the hook.
.sp
Common variables include:
.RS
.TP
\fBchan\fR
the name of the comm channel (and command)
.TP
\fBid\fR
the id of the remote in question
.TP
\fBfid\fR
the file id for the socket of the connection
.RE
.PP
These are the defined \fIevents\fR:
.TP
\fBconnecting\fR
Variables:
\fIchan id host port\fR
.sp
This hook is invoked before making a connection to the remote named in
\fIid\fR.  An error return (via \fBerror\fR) will abort the connection
attempt with the error.  Example:
.sp
.nf
 % ::comm::comm hook connecting {
     if [lb]string match {*[lb]02468[rb]} $id[rb] {
         error "Can't connect to even ids"
     }
 }
 % ::comm::comm send 10000 puts ok
 Connect to remote failed: Can't connect to even ids
 %
.fi
.TP
\fBconnected\fR
Variables:
\fIchan fid id host port\fR
.sp
This hook is invoked immediately after making a remote connection to
\fIid\fR, allowing arbitrary authentication over the socket named by
\fIfid\fR.  An error return (via \fBerror\fR ) will close the
connection with the error.  \fIhost\fR and \fIport\fR are merely
extracted from the \fIid\fR; changing any of these will have no effect
on the connection, however.  It is also possible to substitute and
replace \fIfid\fR.
.TP
\fBincoming\fR
Variables:
\fIchan fid addr remport\fR
.sp
Hook invoked when receiving an incoming connection, allowing arbitrary
authentication over socket named by \fIfid\fR.  An error return (via
\fBerror\fR) will close the connection with the error.  Note that the
peer is named by \fIremport\fR and \fIaddr\fR but that the remote
\fIid\fR is still unknown.  Example:
.sp
.nf
 ::comm::comm hook incoming {
     if [lb]string match 127.0.0.1 $addr[rb] {
         error "I don't talk to myself"
     }
 }
.fi
.TP
\fBeval\fR
Variables:
\fIchan id cmd buffer\fR
.sp
This hook is invoked after collecting a complete script from a remote
but \fIbefore\fR evaluating it.  This allows complete control over
the processing of incoming commands.  \fIcmd\fR contains either
\fBsend\fR or \fBasync\fR.  \fIbuffer\fR holds the script to
evaluate.  At the time the hook is called, \fI$chan remoteid\fR is
identical in value to \fIid\fR.
.sp
By changing \fIbuffer\fR, the hook can change the script to be
evaluated.  The hook can short circuit evaluation and cause a value to
be immediately returned by using \fBreturn\fR \fIresult\fR (or, from
within a procedure, \fBreturn -code return\fR \fIresult\fR).  An
error return (via \fBerror\fR) will return an error result, as is if
the script caused the error.  Any other return will evaluate the
script in \fIbuffer\fR as normal.  For compatibility with 3.2,
\fBbreak\fR and \fBreturn -code break\fR \fIresult\fR is supported,
acting similarly to \fBreturn {}\fR and \fBreturn -code return\fR
\fIresult\fR.
.sp
Examples:
.RS
.IP [1]
augmenting a command
.sp
.nf
 % ::comm::comm send [lb]::comm::comm self[rb] pid
 5013
 % ::comm::comm hook eval {puts "going to execute $buffer"}
 % ::comm::comm send [lb]::comm::comm self[rb] pid
 going to execute pid
 5013
.fi
.IP [2]
short circuiting a command
.sp
.nf
 % ::comm::comm hook eval {puts "would have executed $buffer"; return 0}
 % ::comm::comm send [lb]::comm::comm self[rb] pid
 would have executed pid
 0
.fi
.IP [3]
Replacing double eval semantics
.sp
.nf
 % ::comm::comm send [lb]::comm::comm self[rb] llength {a b c}
 wrong # args: should be "llength list"
 % ::comm::comm hook eval {return [uplevel #0 $buffer]}
 return [lb]uplevel #0 $buffer[rb]
 % ::comm::comm send [lb]::comm::comm self[rb] llength {a b c}
 3
.fi
.IP [4]
Using a slave interpreter
.sp
.nf
 % interp create foo
 % ::comm::comm hook eval {return [lb]foo eval $buffer[rb]}
 % ::comm::comm send [lb]::comm::comm self[rb] set myvar 123
 123
 % set myvar
 can't read "myvar": no such variable
 % foo eval set myvar
 123
.fi
.IP [5]
Using a slave interpreter (double eval)
.sp
.nf
 % ::comm::comm hook eval {return [lb]eval foo eval $buffer[rb]}
.fi
.IP [6]
Subverting the script to execute
.sp
.nf
 % ::comm::comm hook eval {
     switch -- $buffer {
         a {return A-OK} b {return B-OK} default {error "$buffer is a no-no"}
     }
 }
 % ::comm::comm send [lb]::comm::comm self[rb] pid
 pid is a no-no
 % ::comm::comm send [lb]::comm::comm self[rb] a
 A-OK
.fi
.RE
.TP
\fBreply\fR
Variables:
\fIchan id buffer ret return()\fR
.sp
This hook is invoked after collecting a complete reply script from a
remote but \fIbefore\fR evaluating it.  This allows complete
control over the processing of replies to sent commands.  The reply
\fIbuffer\fR is in one of the following forms
.RS
.IP \(bu
return result
.IP \(bu
return -code code result
.IP \(bu
return -code code -errorinfo info -errorcode ecode msg
.RE
For safety reasons, this is decomposed.  The return result is in
\fIret\fR, and the return switches are in the return array:
.RS
.IP \(bu
\fIreturn(-code)\fR
.IP \(bu
\fIreturn(-errorinfo)\fR
.IP \(bu
\fIreturn(-errordcode)\fR
.RE
Any of these may be the empty string.  Modifying these four variables
can change the return value, whereas modifying \fIbuffer\fR has no
effect.
.TP
\fBcallback\fR
Variables:
\fIchan id buffer ret return()\fR
.sp
Similar to \fIreply\fR, but used for callbacks.
.TP
\fBlost\fR
Variables:
\fIchan id reason\fR
.sp
This hook is invoked when the connection to \fIid\fR is lost.  Return
value (or thrown error) is ignored.  \fIreason\fR is an explanatory
string indicating why the connection was lost.  Example:
.sp
.nf
 ::comm::comm hook lost {
     global myvar
     if {$myvar(id) == $id} {
         myfunc
         return
     }
 }
.fi
.SH "UNSUPPORTED"
.PP
These interfaces may change or go away in subsequence releases.
.TP
\fB::comm::comm remoteid\fR \fR
Returns the \fIid\fR of the sender of the last remote command
executed on this channel.  If used by a proc being invoked remotely,
it must be called before any events are processed.  Otherwise, another
command may get invoked and change the value.
.TP
\fB::comm::comm_send\fR \fR
Invoking this procedure will substitute the Tk \fBsend\fR and
\fBwinfo interps\fR commands with these equivalents that use
\fB::comm::comm\fR.
.sp
.nf
 proc send {args} {
     eval ::comm::comm send $args
 }
 rename winfo tk_winfo
 proc winfo {cmd args} {
     if ![lb]string match in* $cmd[rb] {return [lb]eval [lb]list tk_winfo $cmd[rb] $args[rb]}
     return [lb]::comm::comm interps[rb]
 }
.fi
.SH "SECURITY"
.PP
Something here soon.
.SH "BLOCKING SEMANTICS"
.PP
There is one outstanding difference between \fBcomm\fR and
\fBsend\fR.  When blocking in a synchronous remote command, \fBsend\fR
uses an internal C hook (Tk_RestrictEvents) to the event loop to look
ahead for send-related events and only process those without
processing any other events.  In contrast, \fBcomm\fR uses the
\fBvwait\fR command as a semaphore to indicate the return message has
arrived.  The difference is that a synchronous \fBsend\fR will block
the application and prevent all events (including window related ones)
from being processed, while a synchronous \fB::comm::comm send\fR will block the
application but still allow other events will still get processed.  In
particular, \fBafter idle\fR handlers will fire immediately when
comm blocks.
.PP
What can be done about this?  First, note that this behavior will come
from any code using \fBvwait\fR to block and wait for an event to
occur.  At the cost of multiple channel support, \fBcomm\fR could
be changed to do blocking I/O on the socket, giving send-like blocking
semantics.  However, multiple channel support is a very useful feature
of comm that it is deemed too important to lose.  The remaining
approaches involve a new loadable module written in C (which is
somewhat against the philosophy of \fBcomm\fR) One way would be to
create a modified version of the \fBvwait\fR command that allow the
event flags passed to Tcl_DoOneEvent to be specified.  For \fBcomm\fR,
just the TCL_FILE_EVENTS would be processed.  Another way would be to
implement a mechanism like Tk_RestrictEvents, but apply it to the Tcl
event loop (since \fBcomm\fR doesn't require Tk).  One of these
approaches will be available in a future \fBcomm\fR release as an
optional component.
.SH "COMPATIBILITY"
.PP
\fBcomm\fR exports itself as a package.  The package version number
is in the form \fImajor . minor\fR, where the major version will
only change when a non-compatible change happens to the API or
protocol.  Minor bug fixes and changes will only affect the minor
version.  To load \fBcomm\fR this command is usually used:
.PP
.nf
 package require comm 3
.fi
.PP
Note that requiring no version (or a specific version) can also be done.
.PP
The revision history of \fBcomm\fR includes these releases:
.TP
4.2
Bugfixes, and most important, switched to utf-8 as default encoding
for full i18n without any problems.
.TP
4.1
Rewrite of internal code to remove old pseudo-object model.  Addition
of send -command asynchronous callback option.
.TP
4.0
Per request by John LoVerso. Improved handling of error for async
invoked commands.
.TP
3.7
Moved into tcllib and placed in a proper namespace.
.TP
3.6
A bug in the looking up of the remoteid for a executed command could
be triggered when the connection was closed while several asynchronous
sends were queued to be executed.
.TP
3.5
Internal change to how reply messages from a \fBsend\fR are handled.
Reply messages are now decoded into the \fIvalue\fR to pass to
\fBreturn\fR; a new return statement is then cons'd up to with this
value.  Previously, the return code was passed in from the remote as a
command to evaluate.  Since the wire protocol has not changed, this is
still the case.  Instead, the reply handling code decodes the
\fBreply\fR message.
.TP
3.4
Added more source commentary, as well as documenting config variables
in this man page.  Fixed bug were loss of connection would give error
about a variable named \fBpending\fR rather than the message about
the lost connection.  \fBcomm ids\fR is now an alias for
\fBcomm interps\fR (previously, it an alias for \fBcomm chans\fR).
Since the method invocation change of 3.0, break and other exceptional
conditions were not being returned correctly from \fBcomm send\fR.
This has been fixed by removing the extra level of indirection into
the internal procedure \fBcommSend\fR.  Also added propagation of
the \fIerrorCode\fR variable.  This means that these commands return
exactly as they would with \fBsend\fR:
.nf
 comm send id break
 catch {comm send id break}
 comm send id expr 1 / 0
.fi
.sp
Added a new hook for reply messages.  Reworked method invocation to
avoid the use of comm:* procedures; this also cut the invocation time
down by 40%.  Documented \fBcomm config\fR (as this manual page
still listed the defunct \fBcomm init\fR!)
.TP
3.3
Some minor bugs were corrected and the documentation was cleaned up.
Added some examples for hooks.  The return semantics of the \fBeval\fR
hook were changed.
.TP
3.2
A new wire protocol, version 3, was added.  This is backwards
compatible with version 2 but adds an exchange of supported protocol
versions to allow protocol negotiation in the future.  Several bugs
with the hook implementation were fixed.  A new section of the man
page on blocking semantics was added.
.TP
3.1
All the documented hooks were implemented.  \fBcommLostHook\fR was
removed.  A bug in \fBcomm new\fR was fixed.
.TP
3.0
This is a new version of \fBcomm\fR with several major changes.
There is a new way of creating the methods available under the
\fBcomm\fR command.  The \fBcomm init\fR method has been retired
and is replaced by \fBcomm configure\fR which allows access to many
of the well-defined internal variables.  This also generalizes the
options available to \fBcomm new\fR.  Finally, there is now a
protocol version exchanged when a connection is established.  This
will allow for future on-wire protocol changes.  Currently, the
protocol version is set to 2.
.TP
2.3
\fBcomm ids\fR was renamed to \fBcomm channels\fR.  General
support for \fBcomm hook\fR was fully implemented, but only the
\fIlost\fR hook exists, and it was changed to follow the general
hook API.  \fBcommLostHook\fR was unsupported (replaced by
\fBcomm hook lost\fR) and \fBcommLost\fR was removed.
.TP
2.2
The \fIdied\fR hook was renamed \fIlost\fR, to be accessed by
\fBcommLostHook\fR and an early implementation of
\fBcomm lost hook\fR.  As such, \fBcommDied\fR is now
\fBcommLost\fR.
.TP
2.1
Unsupported method \fBcomm remoteid\fR was added.
.TP
2.0
\fBcomm\fR has been rewritten from scratch (but is fully compatible
with Comm 1.0, without the requirement to use obTcl).
.SH "AUTHOR"
John LoVerso, John@LoVerso.Southborough.MA.US
.PP
\fIhttp://www.opengroup.org/~loverso/tcl-tk/#comm\fR
.SH "LICENSE"
Please see the file \fIcomm.LICENSE\fR that accompanied this source,
or
\fIhttp://www.opengroup.org/www/dist_client/caubweb/COPYRIGHT.free.html\fR.
.PP
This license for \fBcomm\fR, new as of version 3.2, allows it to be
used for free, without any licensing fee or royalty.
.SH "BUGS"
.IP \(bu
If there is a failure initializing a channel created with
\fB::comm::comm new\fR, then the channel should be destroyed.
Currently, it is left in an inconsistent state.
.IP \(bu
There should be a way to force a channel to quiesce when changing the
configuration.
.PP
The following items can be implemented with the existing hooks and are
listed here as a reminder to provide a sample hook in a future
version.
.IP \(bu
Allow easier use of a slave interp for actual command execution
(especially when operating in "not local" mode).
.IP \(bu
Add host list (xhost-like) or "magic cookie" (xauth-like)
authentication to initial handshake.
.PP
The following are outstanding todo items.
.IP \(bu
Add an interp discovery and name->port mapping.  This is likely to be
in a separate, optional nameserver.  (See also the related work,
below.)
.IP \(bu
Fix the \fI{id host}\fR form so as not to be dependent upon
canonical hostnames.  This requires fixes to Tcl to resolve hostnames!
.PP
This man page is bigger than the source file.
.SH "ON USING OLD VERSIONS OF TCL"
.PP
Tcl7.5 under Windows contains a bug that causes the interpreter to
hang when EOF is reached on non-blocking sockets.  This can be
triggered with a command such as this:
.PP
.nf
 "comm send $other exit"
.fi
.PP
Always make sure the channel is quiescent before closing/exiting or
use at least Tcl7.6 under Windows.
.PP
Tcl7.6 on the Mac contains several bugs.  It is recommended you use
at least Tcl7.6p2.
.PP
Tcl8.0 on UNIX contains a socket bug that can crash Tcl.  It is recommended
you use Tcl8.0p1 (or Tcl7.6p2).
.SH "RELATED WORK"
.PP
Tcl-DP provides an RPC-based remote execution interface, but is a
compiled Tcl extension.  See
\fIhttp://www.cs.cornell.edu/Info/Projects/zeno/Projects/Tcl-DP.html\fR.
.PP
Michael Doyle <miked@eolas.com> has code that implements the Tcl-DP
RPC interface using standard Tcl sockets, much like \fBcomm\fR
.PP
Andreas Kupries <andreas_kupries@users.sourceforge.net> uses
\fBcomm\fR and has built a simple nameserver as part of his Pool
library.  See \fIhttp://www.purl.org/net/akupries/soft/pool/index.htm\fR.
.SH "SEE ALSO"
send(n)
.SH "COPYRIGHT"
.nf
Copyright (c) 1995-1998 The Open Group. All Rights Reserved.
Copyright (c) 2003 ActiveState Corporation.
.fi