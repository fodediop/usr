'\"
'\" Generated from file '/SourceCache/tcl/tcl-20/tcl_ext/tcllib/tcllib/modules/struct/graph.man' by tcllib/doctools with format 'nroff'
'\" Copyright (c) 2002 Andreas Kupries <andreas_kupries@users.sourceforge.net>
'\"
'\" -*- tcl -*-
'\" The definitions below are for supplemental macros used in Tcl/Tk
'\" manual entries.
'\"
'\" .AP type name in/out ?indent?
'\"	Start paragraph describing an argument to a library procedure.
'\"	type is type of argument (int, etc.), in/out is either "in", "out",
'\"	or "in/out" to describe whether procedure reads or modifies arg,
'\"	and indent is equivalent to second arg of .IP (shouldn't ever be
'\"	needed;  use .AS below instead)
'\"
'\" .AS ?type? ?name?
'\"	Give maximum sizes of arguments for setting tab stops.  Type and
'\"	name are examples of largest possible arguments that will be passed
'\"	to .AP later.  If args are omitted, default tab stops are used.
'\"
'\" .BS
'\"	Start box enclosure.  From here until next .BE, everything will be
'\"	enclosed in one large box.
'\"
'\" .BE
'\"	End of box enclosure.
'\"
'\" .CS
'\"	Begin code excerpt.
'\"
'\" .CE
'\"	End code excerpt.
'\"
'\" .VS ?version? ?br?
'\"	Begin vertical sidebar, for use in marking newly-changed parts
'\"	of man pages.  The first argument is ignored and used for recording
'\"	the version when the .VS was added, so that the sidebars can be
'\"	found and removed when they reach a certain age.  If another argument
'\"	is present, then a line break is forced before starting the sidebar.
'\"
'\" .VE
'\"	End of vertical sidebar.
'\"
'\" .DS
'\"	Begin an indented unfilled display.
'\"
'\" .DE
'\"	End of indented unfilled display.
'\"
'\" .SO
'\"	Start of list of standard options for a Tk widget.  The
'\"	options follow on successive lines, in four columns separated
'\"	by tabs.
'\"
'\" .SE
'\"	End of list of standard options for a Tk widget.
'\"
'\" .OP cmdName dbName dbClass
'\"	Start of description of a specific option.  cmdName gives the
'\"	option's name as specified in the class command, dbName gives
'\"	the option's name in the option database, and dbClass gives
'\"	the option's class in the option database.
'\"
'\" .UL arg1 arg2
'\"	Print arg1 underlined, then print arg2 normally.
'\"
'\" RCS: @(#) $Id: man.macros,v 1.2 2004/01/15 06:36:12 andreas_kupries Exp $
'\"
'\"	# Set up traps and other miscellaneous stuff for Tcl/Tk man pages.
.if t .wh -1.3i ^B
.nr ^l \n(.l
.ad b
'\"	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ta \\n()Au \\n()Bu
.ie !"\\$3"" \{\
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'\"	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
.AS Tcl_Interp Tcl_CreateInterp in/out
'\"	# BS - start boxed text
'\"	# ^y = starting y location
'\"	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'\"	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'\"	# VS - start vertical sidebar
'\"	# ^Y = starting y location
'\"	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.if !"\\$2"" .br
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'\"	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'\"	# Special macro to handle page bottom:  finish off current
'\"	# box/sidebar if in box/sidebar mode, then invoked standard
'\"	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'\"	# DS - begin display
.de DS
.RS
.nf
.sp
..
'\"	# DE - end display
.de DE
.fi
.RE
.sp
..
'\"	# SO - start of list of standard options
.de SO
.SH "STANDARD OPTIONS"
.LP
.nf
.ta 4c 8c 12c
.ft B
..
'\"	# SE - end of list of standard options
.de SE
.fi
.ft R
.LP
See the \\fBoptions\\fR manual entry for details on the standard options.
..
'\"	# OP - start of full description for a single option
.de OP
.LP
.nf
.ta 4c
Command-Line Name:	\\fB\\$1\\fR
Database Name:	\\fB\\$2\\fR
Database Class:	\\fB\\$3\\fR
.fi
.IP
..
'\"	# CS - begin code excerpt
.de CS
.RS
.nf
.ta .25i .5i .75i 1i
..
'\"	# CE - end code excerpt
.de CE
.fi
.RE
..
.de UL
\\$1\l'|0\(ul'\\$2
..
.TH "graph" n 2.0 struct "Tcl Data Structures"
.BS
.SH "NAME"
graph \- Create and manipulate directed graph objects
.SH "SYNOPSIS"
package require \fBTcl  8.2\fR
.sp
package require \fBstruct  ?2.0?\fR
.sp
\fB::struct::graph\fR ?\fIgraphName\fR? ?\fB=\fR|\fB:=\fR|\fBas\fR|\fBdeserialize\fR \fIsource\fR?\fR
.sp
\fBgraphName\fR \fIoption\fR ?\fIarg arg ...\fR?\fR
.sp
\fIgraphName\fR \fB=\fR \fIsourcegraph\fR\fR
.sp
\fIgraphName\fR \fB-->\fR \fIdestgraph\fR\fR
.sp
\fIgraphName\fR \fBappend\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBdeserialize\fR \fIserialization\fR\fR
.sp
\fIgraphName\fR \fBdestroy\fR\fR
.sp
\fIgraphName\fR \fBarc append\fR \fIarc\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBarc attr\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-arcs\fR \fIlist\fR\fR
.sp
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-glob\fR \fIglobpattern\fR\fR
.sp
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-regexp\fR \fIrepattern\fR\fR
.sp
\fIgraphName\fR \fBarc delete\fR \fIarc\fR ?\fIarc\fR ...?\fR
.sp
\fIgraphName\fR \fBarc exists\fR \fIarc\fR\fR
.sp
\fIgraphName\fR \fBarc get\fR \fIarc\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBarc getall\fR \fIarc\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBarc keys\fR \fIarc\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBarc keyexists\fR \fIarc\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBarc insert\fR \fIstart\fR \fIend\fR ?\fIchild\fR?\fR
.sp
\fIgraphName\fR \fBarc lappend\fR \fIarc\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBarc rename\fR \fIarc\fR \fInewname\fR\fR
.sp
\fIgraphName\fR \fBarc set\fR \fIarc\fR \fIkey\fR ?\fIvalue\fR?\fR
.sp
\fIgraphName\fR \fBarc source\fR \fIarc\fR\fR
.sp
\fIgraphName\fR \fBarc target\fR \fIarc\fR\fR
.sp
\fIgraphName\fR \fBarc unset\fR \fIarc\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBarcs\fR ?-key \fIkey\fR? ?-value \fIvalue\fR? ?-filter \fIcmdprefix\fR? ?-in|-out|-adj|-inner|-embedding \fInodelist\fR?\fR
.sp
\fIgraphName\fR \fBlappend\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBnode append\fR \fInode\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBnode attr\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-nodes\fR \fIlist\fR\fR
.sp
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-glob\fR \fIglobpattern\fR\fR
.sp
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-regexp\fR \fIrepattern\fR\fR
.sp
\fIgraphName\fR \fBnode degree\fR ?-in|-out? \fInode\fR\fR
.sp
\fIgraphName\fR \fBnode delete\fR \fInode\fR ?\fInode\fR ...?\fR
.sp
\fIgraphName\fR \fBnode exists\fR \fInode\fR\fR
.sp
\fIgraphName\fR \fBnode get\fR \fInode\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBnode getall\fR \fInode\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBnode keys\fR \fInode\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBnode keyexists\fR \fInode\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBnode insert\fR ?\fIchild\fR?\fR
.sp
\fIgraphName\fR \fBnode lappend\fR \fInode\fR \fIkey\fR \fIvalue\fR\fR
.sp
\fIgraphName\fR \fBnode opposite\fR \fInode\fR \fIarc\fR\fR
.sp
\fIgraphName\fR \fBnode rename\fR \fInode\fR \fInewname\fR\fR
.sp
\fIgraphName\fR \fBnode set\fR \fInode\fR \fIkey\fR ?\fIvalue\fR?\fR
.sp
\fIgraphName\fR \fBnode unset\fR \fInode\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBnodes\fR ?-key \fIkey\fR? ?-value \fIvalue\fR? ?-filter \fIcmdprefix\fR? ?-in|-out|-adj|-inner|-embedding \fInodelist\fR?\fR
.sp
\fIgraphName\fR \fBget\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBgetall\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBkeys\fR ?\fIpattern\fR?\fR
.sp
\fIgraphName\fR \fBkeyexists\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBserialize\fR ?\fInode\fR...?\fR
.sp
\fIgraphName\fR \fBset\fR \fIkey\fR ?\fIvalue\fR?\fR
.sp
\fIgraphName\fR \fBswap\fR \fInode1\fR \fInode2\fR\fR
.sp
\fIgraphName\fR \fBunset\fR \fIkey\fR\fR
.sp
\fIgraphName\fR \fBwalk\fR \fInode\fR ?-order \fIorder\fR? ?-type \fItype\fR? ?-dir \fIdirection\fR? -command \fIcmd\fR\fR
.sp
.BE
.SH "DESCRIPTION"
.PP
A directed graph is a structure containing two collections of
elements, called \fInodes\fR and \fIarcs\fR respectively, together
with a relation ("connectivity") that places a general structure upon
the nodes and arcs.
.PP
Each arc is connected to two nodes, one of which is called the
\fIsource\fR and the other the \fItarget\fR. This imposes a
direction upon the arc, which is said to go from the source to the
target. It is allowed that source and target of an arc are the same
node. Such an arc is called a \fIloop\fR. Whenever a node is source
or target of an arc both are said to be \fIadjacent\fR. This extends
into a relation between nodes, i.e. if two nodes are connected through
at least one arc they are said to be \fIadjacent\fR too.
.PP
Each node can be the source and target for any number of arcs. The
former are called the \fIoutgoing arcs\fR of the node, the latter
the \fIincoming arcs\fR of the node. The number of edges in either
set is called the \fIin-\fR resp. the \fIout-degree\fR of the node.
.PP
In addition to maintaining the node and arc relationships, this graph
implementation allows any number of keyed values to be associated with
each node and arc.
.PP
\fINote:\fR The major version of the package \fBstruct\fR has
been changed to version 2.0, due to backward incompatible changes in
the API of this module. Please read the section
\fBChanges for 2.0\fR for a full list of all changes,
incompatible and otherwise.
.PP
\fINote:\fR A C-implementation of the command can be had from the
location \fIhttp://www.purl.org/NET/schlenker/tcl/cgraph\fR. See also
\fIhttp://wiki.tcl.tk/cgraph\fR.  This implementation uses a bit less
memory than the tcl version provided here directly, and is faster.
.PP
The main command of the package is:
.TP
\fB::struct::graph\fR ?\fIgraphName\fR? ?\fB=\fR|\fB:=\fR|\fBas\fR|\fBdeserialize\fR \fIsource\fR?\fR
The command creates a new graph object with an associated global Tcl
command whose name is \fIgraphName\fR.  This command may be used to
invoke various operations on the graph.  It has the following general
form:
.RS
.TP
\fBgraphName\fR \fIoption\fR ?\fIarg arg ...\fR?\fR
\fIOption\fR and the \fIarg\fRs determine the exact behavior of the
command.
.RE
.sp
If \fIgraphName\fR is not specified a unique name will be generated by
the package itself. If a \fIsource\fR is specified the new graph will
be initialized to it. For the operators \fB=\fR, \fB:=\fR, and
\fBas\fR \fIsource\fR is interpreted as the name of another graph
object, and the assignment operator \fB=\fR will be executed. For
\fBdeserialize\fR the \fIsource\fR is a serialized graph object and
\fBdeserialize\fR will be executed.
.sp
In other words
.sp
.nf
    ::struct::graph mygraph = b
.fi
.sp
is equivalent to
.sp
.nf
    ::struct::graph mygraph
    mygraph = b
.fi
.sp
and
.sp
.nf
    ::struct::graph mygraph deserialize $b
.fi
.sp
is equivalent to
.sp
.nf
    ::struct::graph mygraph
    mygraph deserialize $b
.fi
.PP
The following commands are possible for graph objects:
.TP
\fIgraphName\fR \fB=\fR \fIsourcegraph\fR\fR
This is the assignment operator for graph objects. It copies the graph
contained in the graph object \fIsourcegraph\fR over the graph data in
\fIgraphName\fR. The old contents of \fIgraphName\fR are deleted by
this operation.
.sp
This operation is in effect equivalent to
.sp
.nf
    \fIgraphName\fR \fBdeserialize\fR [\fIsourcegraph\fR \fBserialize\fR]
.fi
.TP
\fIgraphName\fR \fB-->\fR \fIdestgraph\fR\fR
This is the reverse assignment operator for graph objects. It copies
the graph contained in the graph object \fIgraphName\fR over the graph
data in the object \fIdestgraph\fR.
The old contents of \fIdestgraph\fR are deleted by this operation.
.sp
This operation is in effect equivalent to
.sp
.nf
    \fIdestgraph\fR \fBdeserialize\fR [\fIgraphName\fR \fBserialize\fR]
.fi
.TP
\fIgraphName\fR \fBappend\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR to one of the keyed values associated with the graph.
Returns the new value given to the attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBdeserialize\fR \fIserialization\fR\fR
This is the complement to \fBserialize\fR. It replaces graph data
in \fIgraphName\fR with the graph described by the \fIserialization\fR
value. The old contents of \fIgraphName\fR are deleted by this
operation.
.TP
\fIgraphName\fR \fBdestroy\fR\fR
Destroy the graph, including its storage space and associated command.
.TP
\fIgraphName\fR \fBarc append\fR \fIarc\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR to one of the keyed values associated with an
\fIarc\fR. Returns the new value given to the attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBarc attr\fR \fIkey\fR\fR
.TP
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-arcs\fR \fIlist\fR\fR
.TP
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-glob\fR \fIglobpattern\fR\fR
.TP
\fIgraphName\fR \fBarc attr\fR \fIkey\fR \fB-regexp\fR \fIrepattern\fR\fR
This method retrieves the value of the attribute named \fIkey\fR, for
all arcs in the graph (matching the restriction specified via one of
the possible options) and having the specified attribute.
.sp
The result is a dictionary mapping from arc names to the value of
attribute \fIkey\fR at that arc.
Arcs not having the attribute \fIkey\fR, or not passing a
specified restriction, are not listed in the result.
.sp
The possible restrictions are:
.RS
.TP
\fB-arcs\fR
The value is a list of arcs. Only the arcs mentioned in this list
are searched for the attribute.
.TP
\fB-glob\fR
The value is a glob pattern. Only the arcs in the graph whose names
match this pattern are searched for the attribute.
.TP
\fB-regexp\fR
The value is a regular expression. Only the arcs in the graph whose
names match this pattern are searched for the attribute.
.RE
.sp
.TP
\fIgraphName\fR \fBarc delete\fR \fIarc\fR ?\fIarc\fR ...?\fR
Remove the specified arcs from the graph.
.TP
\fIgraphName\fR \fBarc exists\fR \fIarc\fR\fR
Return true if the specified \fIarc\fR exists in the graph.
.TP
\fIgraphName\fR \fBarc get\fR \fIarc\fR \fIkey\fR\fR
Return the value associated with the key \fIkey\fR for the \fIarc\fR.
.TP
\fIgraphName\fR \fBarc getall\fR \fIarc\fR ?\fIpattern\fR?\fR
Returns a dictionary (suitable for use with [\fBarray set\fR])
for the \fIarc\fR.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned dictionary. The pattern
is a \fBglob\fR pattern.
.TP
\fIgraphName\fR \fBarc keys\fR \fIarc\fR ?\fIpattern\fR?\fR
Returns a list of keys for the \fIarc\fR.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned list. The pattern is a
\fBglob\fR pattern.
.TP
\fIgraphName\fR \fBarc keyexists\fR \fIarc\fR \fIkey\fR\fR
Return true if the specified \fIkey\fR exists for the \fIarc\fR.
.TP
\fIgraphName\fR \fBarc insert\fR \fIstart\fR \fIend\fR ?\fIchild\fR?\fR
Insert an arc named \fIchild\fR into the graph beginning at the node
\fIstart\fR and ending at the node \fIend\fR. If the name of the new
arc is not specified the system will generate a unique name of the
form \fIarc\fR\fIx\fR.
.TP
\fIgraphName\fR \fBarc lappend\fR \fIarc\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR (as a list) to one of the keyed values
associated with an \fIarc\fR. Returns the new value given to the
attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBarc rename\fR \fIarc\fR \fInewname\fR\fR
Renames the arc \fIarc\fR to \fInewname\fR. An error is thrown if
either the arc does not exist, or a arc with name \fInewname\fR does
exist. The result of the command is the new name of the arc.
.TP
\fIgraphName\fR \fBarc set\fR \fIarc\fR \fIkey\fR ?\fIvalue\fR?\fR
Set or get one of the keyed values associated with an arc.
An arc may have any number of keyed values associated with it.
If \fIvalue\fR is not specified, this command returns the current value assigned to the key;
if \fIvalue\fR is specified, this command assigns that value to the key, and returns
that value.
.TP
\fIgraphName\fR \fBarc source\fR \fIarc\fR\fR
Return the node the given \fIarc\fR begins at.
.TP
\fIgraphName\fR \fBarc target\fR \fIarc\fR\fR
Return the node the given \fIarc\fR ends at.
.TP
\fIgraphName\fR \fBarc unset\fR \fIarc\fR \fIkey\fR\fR
Remove a keyed value from the arc \fIarc\fR. The method will do
nothing if the \fIkey\fR does not exist.
.TP
\fIgraphName\fR \fBarcs\fR ?-key \fIkey\fR? ?-value \fIvalue\fR? ?-filter \fIcmdprefix\fR? ?-in|-out|-adj|-inner|-embedding \fInodelist\fR?\fR
Return a list of arcs in the graph. If no restriction is specified a
list containing all arcs is returned. Restrictions can limit the list
of returned arcs based on the nodes that are connected by the arc, on
the keyed values associated with the arc, or both. A general filter
command can be used as well. The restrictions that involve connected
nodes have a list of nodes as argument, specified after the name of
the restriction itself.
.RS
.TP
\fB-in\fR
Return a list of all arcs whose target is one of the nodes in the
\fInodelist\fR.
.TP
\fB-out\fR
Return a list of all arcs whose source is one of the nodes in the
\fInodelist\fR.
.TP
\fB-adj\fR
Return a list of all arcs adjacent to at least one of the nodes in the
\fInodelist\fR. This is the union of the nodes returned by
\fB-in\fR and \fB-out\fR.
.TP
\fB-inner\fR
Return a list of all arcs adjacent to two of the nodes in the
\fInodelist\fR. This is the set of arcs in the subgraph spawned by the
specified nodes.
.TP
\fB-embedding\fR
Return a list of all arcs adjacent to exactly one of the nodes in the
\fInodelist\fR. This is the set of arcs connecting the subgraph
spawned by the specified nodes to the rest of the graph.
.TP
\fB-key\fR \fIkey\fR
Limit the list of arcs that are returned to those arcs that have an
associated key \fIkey\fR.
.TP
\fB-value\fR \fIvalue\fR
This restriction can only be used in combination with
\fB-key\fR. It limits the list of arcs that are returned to those
arcs whose associated key \fIkey\fR has the value \fIvalue\fR.
.TP
\fB-filter\fR \fIcmdrefix\fR
Limit the list of arcs that are returned to those arcs that pass the
test. The command in \fIcmdprefix\fR is called with two arguments, the
name of the graph object, and the name of the arc in question. It is
executed in the context of the caller and has to return a boolean
value. Arcs for which the command returns \fBfalse\fR are removed
from the result list before it is returned to the caller.
.RE
.sp
The restrictions imposed by either \fB-in\fR, \fB-out\fR,
\fB-adj\fR, \fB-inner\fR, or \fB-embedded\fR are applied
first.
After that the restrictions set via \fB-key\fR
(and \fB-value\fR) are applied.
Any restriction set through \fB-filter\fR is applied last.
.TP
\fIgraphName\fR \fBlappend\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR (as a list) to one of the keyed values
associated with the graph. Returns the new value given to the
attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBnode append\fR \fInode\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR to one of the keyed values associated with an
\fInode\fR. Returns the new value given to the attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBnode attr\fR \fIkey\fR\fR
.TP
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-nodes\fR \fIlist\fR\fR
.TP
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-glob\fR \fIglobpattern\fR\fR
.TP
\fIgraphName\fR \fBnode attr\fR \fIkey\fR \fB-regexp\fR \fIrepattern\fR\fR
This method retrieves the value of the attribute named \fIkey\fR, for
all nodes in the graph (matching the restriction specified via one of
the possible options) and having the specified attribute.
.sp
The result is a dictionary mapping from node names to the value of
attribute \fIkey\fR at that node.
Nodes not having the attribute \fIkey\fR, or not passing a
specified restriction, are not listed in the result.
.sp
The possible restrictions are:
.RS
.TP
\fB-nodes\fR
The value is a list of nodes. Only the nodes mentioned in this list
are searched for the attribute.
.TP
\fB-glob\fR
The value is a glob pattern. Only the nodes in the graph whose names
match this pattern are searched for the attribute.
.TP
\fB-regexp\fR
The value is a regular expression. Only the nodes in the graph whose
names match this pattern are searched for the attribute.
.RE
.sp
.TP
\fIgraphName\fR \fBnode degree\fR ?-in|-out? \fInode\fR\fR
Return the number of arcs adjacent to the specified \fInode\fR. If one
of the restrictions \fB-in\fR or \fB-out\fR is given only the
incoming resp. outgoing arcs are counted.
.TP
\fIgraphName\fR \fBnode delete\fR \fInode\fR ?\fInode\fR ...?\fR
Remove the specified nodes from the graph.  All of the nodes' arcs
will be removed as well to prevent unconnected arcs.
.TP
\fIgraphName\fR \fBnode exists\fR \fInode\fR\fR
Return true if the specified \fInode\fR exists in the graph.
.TP
\fIgraphName\fR \fBnode get\fR \fInode\fR \fIkey\fR\fR
Return the value associated with the key \fIkey\fR for the \fInode\fR.
.TP
\fIgraphName\fR \fBnode getall\fR \fInode\fR ?\fIpattern\fR?\fR
Returns a dictionary (suitable for use with [\fBarray set\fR])
for the \fInode\fR.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned dictionary. The pattern
is a \fBglob\fR pattern.
.TP
\fIgraphName\fR \fBnode keys\fR \fInode\fR ?\fIpattern\fR?\fR
Returns a list of keys for the \fInode\fR.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned list. The pattern is a
\fBglob\fR pattern.
.TP
\fIgraphName\fR \fBnode keyexists\fR \fInode\fR \fIkey\fR\fR
Return true if the specified \fIkey\fR exists for the \fInode\fR.
.TP
\fIgraphName\fR \fBnode insert\fR ?\fIchild\fR?\fR
Insert a node named \fIchild\fR into the graph. The nodes has no arcs
connected to it. If the name of the new child is not specified the
system will generate a unique name of the form \fInode\fR\fIx\fR.
.TP
\fIgraphName\fR \fBnode lappend\fR \fInode\fR \fIkey\fR \fIvalue\fR\fR
Appends a \fIvalue\fR (as a list) to one of the keyed values
associated with an \fInode\fR. Returns the new value given to the
attribute \fIkey\fR.
.TP
\fIgraphName\fR \fBnode opposite\fR \fInode\fR \fIarc\fR\fR
Return the node at the other end of the specified \fIarc\fR, which has
to be adjacent to the given \fInode\fR.
.TP
\fIgraphName\fR \fBnode rename\fR \fInode\fR \fInewname\fR\fR
Renames the node \fInode\fR to \fInewname\fR. An error is thrown if
either the node does not exist, or a node with name \fInewname\fR does
exist. The result of the command is the new name of the node.
.TP
\fIgraphName\fR \fBnode set\fR \fInode\fR \fIkey\fR ?\fIvalue\fR?\fR
Set or get one of the keyed values associated with a node. A node may have any
number of keyed values associated with it.  If \fIvalue\fR is not
specified, this command returns the current value assigned to the key;
if \fIvalue\fR is specified, this command assigns that value to the
key.
.TP
\fIgraphName\fR \fBnode unset\fR \fInode\fR \fIkey\fR\fR
Remove a keyed value from the node \fInode\fR. The method will do
nothing if the \fIkey\fR does not exist.
.TP
\fIgraphName\fR \fBnodes\fR ?-key \fIkey\fR? ?-value \fIvalue\fR? ?-filter \fIcmdprefix\fR? ?-in|-out|-adj|-inner|-embedding \fInodelist\fR?\fR
Return a list of nodes in the graph. Restrictions can limit the list
of returned nodes based on neighboring nodes, or based on the keyed
values associated with the node. The restrictions that involve
neighboring nodes have a list of nodes as argument, specified after
the name of the restriction itself.
.sp
The possible restrictions are the same as for method
\fBarcs\fR. The set of nodes to return is computed as the union of
all source and target nodes for all the arcs satisfying the
restrictions as defined for \fBarcs\fR.
.sp
\fINote\fR that here the \fB-filter\fR command is applied to the
list of nodes, not arcs.
.TP
\fIgraphName\fR \fBget\fR \fIkey\fR\fR
Return the value associated with the key \fIkey\fR for the graph.
.TP
\fIgraphName\fR \fBgetall\fR ?\fIpattern\fR?\fR
Returns a dictionary (suitable for use with [\fBarray set\fR])
for the whole graph.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned dictionary. The pattern
is a \fBglob\fR pattern.
.TP
\fIgraphName\fR \fBkeys\fR ?\fIpattern\fR?\fR
Returns a list of keys for the whole graph.
If the \fIpattern\fR is specified only the attributes whose names
match the pattern will be part of the returned list. The pattern is a
\fBglob\fR pattern.
.TP
\fIgraphName\fR \fBkeyexists\fR \fIkey\fR\fR
Return true if the specified \fIkey\fR exists for the whole graph.
.TP
\fIgraphName\fR \fBserialize\fR ?\fInode\fR...?\fR
This method serializes the sub-graph spanned up by the \fInode\fRs. In
other words it returns a tcl \fIvalue\fR completely describing that
graph. If no nodes are specified the whole graph will be serialized.
This allows, for example, the transfer of graph objects (or parts
thereof) over arbitrary channels, persistence, etc.
This method is also the basis for both the copy constructor and
the assignment operator.
.sp
The result of this method has to be semantically identical over all
implementations of the graph interface. This is what will enable us to
copy graph data between different implementations of the same
interface.
.sp
The result is a list containing a multiple of three items, plus one!
In other words, '[llength $serial] % 3 == 1'. Valid values
include 1, 4, 7, ...
.sp
The last element of the list is a dictionary containing the attributes
associated with the whole graph.
Regarding the other elements; each triple consists of
.RS
.IP [1]
The name of the node to be described,
.IP [2]
A dictionary containing the attributes associated with the node,
.IP [3]
And a list describing all the arcs starting at that node.
.RE
.sp
The elements of the arc list are lists containing three elements each, i.e.
.RS
.IP [1]
The name of the arc described by the element,
.IP [2]
A reference to the destination node of the arc. This reference is an
integer number given the index of that node in the main serialization
list. As that it is greater than or equal to zero, less than the
length of the serialization, and a multiple of three.
\fINote:\fR For internal consistency no arc name may be used twice,
whether in the same node, or at some other node. This is a global
consistency requirement for the serialization.
.IP [3]
And a dictionary containing the attributes associated with the arc.
.RE
.sp
For all attribute dictionaries they keys are the names of the
attributes, and the values are the values for each name.
.sp
\fINote:\fR The order of the nodes in the serialization has no
relevance, nor has the order of the arcs per node.
.nf
    # A possible serialization for the graph structure
    #
    #        d -----> %2
    #       /         ^ \\\\
    #      /         /   \\\\
    #     /         b     \\\\
    #    /         /       \\\\
    #  %1 <- a - %0         e
    #    ^         \\\\      /
    #     \\\\        c     /
    #      \\\\        \\\\  /
    #       \\\\        v v
    #        f ------ %3
    # is
    #
    # %3 {} {{f 6 {}}} %0 {} {{a 6 {}} {b 9 {}} {c 0 {}}} %1 {} {{d 9 {}}} %2 {} {{e 0 {}}} {}
    #
    # This assumes that the graph has no attribute data.
.fi
.sp
.TP
\fIgraphName\fR \fBset\fR \fIkey\fR ?\fIvalue\fR?\fR
Set or get one of the keyed values associated with a graph. A graph
may have any number of keyed values associated with it. If \fIvalue\fR
is not specified, this command returns the current value assigned to
the key; if \fIvalue\fR is specified, this command assigns that value
to the key.
.TP
\fIgraphName\fR \fBswap\fR \fInode1\fR \fInode2\fR\fR
Swap the position of \fInode1\fR and \fInode2\fR in the graph.
.TP
\fIgraphName\fR \fBunset\fR \fIkey\fR\fR
Remove a keyed value from the graph. The method will do nothing if the
\fIkey\fR does not exist.
.TP
\fIgraphName\fR \fBwalk\fR \fInode\fR ?-order \fIorder\fR? ?-type \fItype\fR? ?-dir \fIdirection\fR? -command \fIcmd\fR\fR
Perform a breadth-first or depth-first walk of the graph starting at
the node \fInode\fR going in either the direction of outgoing or
opposite to the incoming arcs.
.sp
The type of walk, breadth-first or depth-first, is determined by the
value of \fItype\fR; \fBbfs\fR indicates breadth-first,
\fBdfs\fR indicates depth-first.  Depth-first is the default.
.sp
The order of the walk, pre-order, post-order or both-order is
determined by the value of \fIorder\fR; \fBpre\fR indicates
pre-order, \fBpost\fR indicates post-order, \fBboth\fR indicates
both-order. Pre-order is the default. Pre-order walking means that a
node is visited before any of its neighbors (as defined by the
\fIdirection\fR, see below). Post-order walking means that a parent is
visited after any of its neighbors. Both-order walking means that a
node is visited before \fIand\fR after any of its neighbors. The
combination of a bread-first walk with post- or both-order is illegal.
.sp
The direction of the walk is determined by the value of \fIdir\fR;
\fBbackward\fR indicates the direction opposite to the incoming
arcs, \fBforward\fR indicates the direction of the outgoing arcs.
.sp
As the walk progresses, the command \fIcmd\fR will be evaluated at
each node, with the mode of the call (\fBenter\fR or
\fBleave\fR) and values \fIgraphName\fR and the name of the current
node appended. For a pre-order walk, all nodes are \fBenter\fRed, for a
post-order all nodes are left. In a both-order walk the first visit of
a node \fBenter\fRs it, the second visit \fBleave\fRs it.
.SH "Changes for 2.0"
The following noteworthy changes have occurred:
.IP [1]
The API for accessing attributes and their values has been
simplified.
.sp
All functionality regarding the default attribute "data" has been
removed. This default attribute does not exist anymore. All accesses
to attributes have to specify the name of the attribute in
question. This backward \fIincompatible\fR change allowed us to
simplify the signature of all methods handling attributes.
.sp
Especially the flag \fB-key\fR is not required anymore, even more,
its use is now forbidden. Please read the documentation for the arc
and node methods \fBset\fR, \fBget\fR, \fBgetall\fR,
\fBunset\fR, \fBappend\fR, \fBlappend\fR, \fBkeyexists\fR
and \fBkeys\fR for a description of the new API's.
.IP [2]
The methods \fBkeys\fR and \fBgetall\fR now take an optional
pattern argument and will return only attribute data for keys matching
this pattern.
.IP [3]
Arcs and nodes can now be renamed. See the documentation for the
methods \fBarc rename\fR and \fBnode rename\fR.
.IP [4]
The structure has been extended with API's for the serialization and
deserialization of graph objects, and a number of operations based on
them (graph assignment, copy construction).
.sp
Please read the documentation for the methods \fBserialize\fR,
\fBdeserialize\fR, \fB=\fR, and \fB-->\fR, and the
documentation on the construction of graph objects.
.sp
Beyond the copying of whole graph objects these new API's also enable
the transfer of graph objects over arbitrary channels and for easy
persistence.
.IP [5]
A new method, \fBattr\fR, was added to both \fBarc\fR and
\fBnode\fR allowing the query and retrieval of attribute data
without regard to arc and node relationships.
.IP [6]
Both methods \fBarcs\fR and \fBnodes\fR have been extended with
the ability to select arcs and nodes based on an arbitrary filtering
criterium.
.SH "KEYWORDS"
cgraph, graph, serialization
.SH "COPYRIGHT"
.nf
Copyright (c) 2002 Andreas Kupries <andreas_kupries@users.sourceforge.net>
.fi