{ ====================================================================
Windows calendar access

Copyright (C) 2001 FORTH, Inc.
==================================================================== }

?( Windows calendar access)

{ --------------------------------------------------------------------
-------------------------------------------------------------------- }

?( ... Time and date)

: TIME&DATE ( -- sec min hour day month year )
   R-BUF  R@ GetLocalTime DROP
   R@ 12 + W@  R@ 10 + W@  R@ 8 + W@
   R@ 6 + W@  R@ 2+ W@  R> W@ ;

: !TIME&DATE ( sec min hour day month year -- )
   R-BUF  R@ W!  R@ 2+ W!  R@ 6 + W!
   R@ 8 + W!  R@ 10 + W!  R@ 12 + W!
   0 R@ 14 + W!
   R> SetLocalTime DROP ;

{ ------------------------------------------------------------------------

Windows sets and returns date and time information in a structure:

typedef struct _SYSTEMTIME   // st
    WORD wYear;
    WORD wMonth;
    WORD wDayOfWeek;
    WORD wDay;
    WORD wHour;
    WORD wMinute;
    WORD wSecond;
    WORD wMilliseconds;
 SYSTEMTIME;

So, if we are to allow the user to set the date, we must first
retrieve the correct time...

(!date) updates the DOS date given year, day, month.
(@date) gets the date from DOS as year, day, month.

------------------------------------------------------------------------ }

: (@date) ( -- d m y )   R-BUF
   R@ GetLocalTime DROP
   R@ 6 + W@  R@ 2+ W@  R> W@ ;

: (!date) ( d m y -- )   R-BUF
   R@ GetLocalTime DROP
   R@ W!  R@ 2+ W!  R@ 6 + W!
   R> SetLocalTime DROP ;

: (@time) ( -- s m h )
   R-BUF  R@ GetLocalTime DROP
   R@ 12 + W@   R@ 10 + W@  R> 8 + W@ ;

: (!time) ( s m h -- )
   R-BUF  R@ GetLocalTime DROP
   R@ 8 + W!  R@ 10 + W!  R@ 12 + W!
   R> SetLocalTime DROP ;

{ =====================================================================
Calendar

Copyright (c) 1972-1998, FORTH, Inc.

This calendar represents the date as the number of days since 01/01/1900,
or "modified Julian date" (MJD).

Requires: @NOW  !NOW

Exports: D/M/Y  M/D/Y  (DATE)  .DATE  @DATE  DATE  NOW
===================================================================== }

{ ---------------------------------------------------------------------
Modified Julian Date

D/Y is the number of days per year for a four-year period.

DAYS is the lookup table of total days in the year at the start of
each month.  @MTH  returns the value from days for the given month.

D/M/Y converts day, month, year into MJD.
M/D/Y takes a double number mm/dd/yyyy and converts it to MJD.
Y-DD and DM split the serial number back into its components.
--------------------------------------------------------------------- }

365 4 * 1+ CONSTANT D/Y

CREATE DAYS   -1 ,  0 ,  31 ,  59 ,  90 ,  120 ,  151 ,
   181 ,  212 ,  243 ,  273 ,  304 ,  334 ,  367 ,

: @MTH ( u1 -- u2)   CELLS DAYS + @ ;

: D-M-Y ( d m y -- u)   >R  @MTH
   58 OVER < IF  R@ 3 AND 0= - THEN + 1-
   R> 1900 -  D/Y UM*  4 UM/MOD SWAP 0<> - + ;

: M/D/Y ( ud -- u)   10000 UM/MOD  100 /MOD  ROT D-M-Y ;

: Y-DD ( u1 -- y u2 u3)   4 UM* D/Y  UM/MOD 1900 +  SWAP 4 /MOD 1+
   DUP ROT 0= IF  DUP 60 > +  SWAP DUP 59 > +  THEN ;

: DM ( u1 u2 -- d m)   1 BEGIN  1+  2DUP @MTH > NOT UNTIL  1-
   SWAP DROP SWAP  OVER @MTH - SWAP ;

{ ---------------------------------------------------------------------
Date display and setting

(MM/DD/YYYY) formats the system date (u1) as a string with the format
mm/dd/yyyy and is the default for (DATE) output.

(DD-MMM-YYYY) formats the date as "dd-MMM-yyyy" where MMM is a
3-letter month abbreviation.

.DATE displays the system date (u) as mm/dd/yyyy.
DATE gets the current system date and displays it.
--------------------------------------------------------------------- }

: (MM/DD/YYYY) ( u1 -- c-addr u2)   BASE @ >R  DECIMAL  Y-DD
   ROT 0 <#  # # # #  2DROP  [CHAR] / HOLD  DM SWAP
   0 # #  2DROP   [CHAR] / HOLD  0 # #  #>  R> BASE ! ;

: (DD-MMM-YYYY) ( u1 -- c-addr u2)   BASE @ >R  DECIMAL  Y-DD
   ROT 0 <#  # # # #  2DROP  [CHAR] - HOLD  DM 3 *
   C" JanFebMarAprMayJunJulAugSepOctNovDec" +
   3 0 DO  DUP C@ HOLD 1-  LOOP DROP  [CHAR] - HOLD
   0 # #  #>  R> BASE ! ;

DEFER (DATE)   ' (MM/DD/YYYY) IS (DATE)

: .DATE ( u -- )   (DATE) TYPE SPACE ;

{ --------------------------------------------------------------------
User API for time and date

-------------------------------------------------------------------- }

: @DATE ( -- n )   (@date) D-M-Y ;
: !DATE ( n -- )   Y-DD DM ROT (!date) ;

: @TIME ( -- ud )   (@time) 60 * + 60 * + 0 ;
: !TIME ( ud -- )   DROP  60 /MOD  60 /MOD (!time) ;

: @NOW ( -- ud n )   @TIME @DATE ;
: !NOW ( ud n -- )   !DATE !TIME ;

{ --------------------------------------------------------------------
-------------------------------------------------------------------- }

: :00 ( ud1 -- ud2)   DECIMAL  #  6 BASE !  # [CHAR] : HOLD ;

: (TIME) ( ud -- c-addr u)   BASE @ >R  <#  :00 :00
   DECIMAL # #  #>  R> BASE ! ;

: .TIME ( ud -- )   (TIME) TYPE SPACE ;

: TIME ( -- )   @TIME .TIME ;

: HOURS ( ud -- )   100 UM/MOD 100 /MOD  60 * +  60 UM*  ROT M+
   @NOW NIP NIP !NOW ;

: DATE ( -- )   @DATE .DATE ;

: NOW ( ud -- )   M/D/Y  @NOW DROP  ROT !NOW ;

{ --------------------------------------------------------------------
Vectors for various date format routines
-------------------------------------------------------------------- }

: (WINDATE) ( method n -- addr n )
   R-BUF  R@ 16 ERASE
   DUP  Y-DD DM ( y d m)  R@ 2+ W!  R@ 6 + W!  R@ W!
   7 MOD R@ 4 + W!
      LOCALE_SYSTEM_DEFAULT
      SWAP
      R>
      0
      PAD
      256
   GetDateFormat IF PAD ZCOUNT ELSE PAD 0 THEN ;

: (WINLONGDATE)  ( n -- addr n )   DATE_LONGDATE  SWAP (WINDATE) ;
: (WINSHORTDATE) ( n -- addr n )   DATE_SHORTDATE SWAP (WINDATE) ;

\ ' (WINLONGDATE) IS (DATE)
\ ' (WINSHORTDATE) IS (DATE)



