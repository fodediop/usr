{ ====================================================================
Help files

Copyright (C) 2001 FORTH, Inc.
Rick VanNorman

Access to various help and documentation files
==================================================================== }

?( ... Help files)

PACKAGE SHELL-TOOLS

: HELP-PARTIALKEY ( zhelpname zkeyname -- )
   >R  HWND SWAP HELP_PARTIALKEY R> WinHelp DROP ;

: HELP-LOOKUP ( zaddr -- )   256 R-ALLOC >R
   BL WORD COUNT  R@ ZPLACE R> HELP-PARTIALKEY ;

CREATE WINAPI 256 /ALLOT
CONFIG: WINAPI ( -- addr len )  WINAPI 256 ;

: >DOCPATH ( addr n -- zaddr )   ROOTPATH COUNT PAD ZPLACE
   S" SwiftForth\Doc\" PAD ZAPPEND  PAD ZAPPEND  PAD ;

: WINAPI-FILE ( -- zname )
   WINAPI C@ IF WINAPI ELSE S" WIN32API.HLP" >DOCPATH THEN ;

: HELPCMD ( zaddr -- )
   HWND 0 ROT 0 0 SW_NORMAL ShellExecute  32 < IF
      HWND Z" Help file not found"  Z" Error" MB_OK MessageBox DROP
   THEN ;

PUBLIC

: API-HELP        WINAPI-FILE HELPCMD ;
: SWIFT-PDF       S" SwiftForth-Win32.pdf" >DOCPATH HELPCMD ;
: FORTH-HANDBOOK  S" Handbook.pdf" >DOCPATH HELPCMD ;
: RELEASE-NOTES   S" http://www.forth.com/swiftforth/version.html" >SHELL ;
: SWF-ONLINE      S" http://www.forth.com/swiftforth/ref.html" >SHELL ;
: DPANS-PDF       S" DPANS94.pdf" >DOCPATH HELPCMD ;
: API             WINAPI-FILE HELP-LOOKUP ;
: API-MSDN        S" http://msdn.microsoft.com/en-us/library/aa383749(VS.85).aspx" >SHELL ;

END-PACKAGE
