{ ====================================================================
File extensions

Copyright (C) 2008  FORTH, Inc.  All rights reserved.

Non-ANS file operations supplied for application support.

==================================================================== }

{ --------------------------------------------------------------------
-------------------------------------------------------------------- }

: SEEK-FILE ( ud mode fileid -- ior )
   ROT >R ( l m f) -ROT RP@ SWAP SetFilePointer R>2DROP 0 ;

: COPY-FILE ( caddr1 u1 caddr2 u2 -- ior )
   R-BUF R@ ZPLACE  R>  -ROT
   R-BUF R@ ZPLACE  R>  SWAP 1 CopyFile 0= -192 AND ;

: COPY-REPLACE-FILE ( caddr1 u1 caddr2 u2 -- ior )
   R-BUF R@ ZPLACE  R>  -ROT
   R-BUF R@ ZPLACE  R>  SWAP 0 CopyFile 0= -192 AND ;
