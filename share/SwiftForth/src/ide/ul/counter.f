{ ====================================================================
System timer

Copyright (C) 2008 FORTH, Inc.  All rights reserved.

This file implements counter, timer, and delay functions.

==================================================================== }

{ --------------------------------------------------------------------
System time access

uCOUNTER returns the system time in microseconds.
COUNTER returns the system time, scaled to milliseconds.

TIMER and uTIMER display elapsed time in corresponding units since
the given timer value.  Usage:

   COUNTER <process to be timed> TIMER
   uCOUNTER <process to be timed> uTIMER

EXPIRED returns true if COUNTER has exceeded n.  There is a 1-day
window in which this routine must be called to allow for slow
monitoring routines.
-------------------------------------------------------------------- }

?( Timer access)

FUNCTION: gettimeofday ( tv tz -- ior )
FUNCTION: nanosleep ( req rem -- ior )

: GET-TIME ( -- u1 u2 )
   0 0 SP@ 0 gettimeofday DROP ;

: uCOUNTER ( -- d )   GET-TIME 1000000 UM* ROT M+ ;
: COUNTER ( -- ms )   GET-TIME 1000 * SWAP 1000 / + ;

: (uTIMER) ( d1 -- d2 )   uCOUNTER 2SWAP D- ;
: (TIMER) ( ms1 -- ms2 )   COUNTER SWAP - ;

: uTIMER ( d -- )   (uTIMER) D. ;
: TIMER ( ms -- )   (TIMER) . ;

: EXPIRED ( ms -- t )   COUNTER -  -86400000 1 WITHIN ;

{ --------------------------------------------------------------------
MS  provides a delay of the given milliseconds.
-------------------------------------------------------------------- }

: MS ( u -- )   ?DUP IF
      1000000 UM* 1000000000 UM/MOD
      SP@ 0 nanosleep DROP 2DROP
   THEN ;
