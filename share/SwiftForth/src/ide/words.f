{ ====================================================================
words.f
Wordlist display

Copyright (C) 2001 FORTH, Inc.   <br> Rick VanNorman  rvn@forth.com
==================================================================== }

{ ------------------------------------------------------------------------
The wordlists in SwiftForth are multi-stranded, each with potentially a
unique number of strands.  To produce the list of words in such a
wordlist, we copy the array of strands from the wordlist to an array on
the return stack, then traverse the list by processing the strand with
the highest value pointed to and updating the temporary copy. In this
manner, the entire wordlist is traversed in the order that it was
created.  The process is finished when all the links are zero.

FILTER  is a holding area for a word filter.

FILTERED checks the input stream for a word following WORDS by exactly
   one space. If present, this becomes the filter.

.FILTER is a version of .ID which will use the filter if present.

LARGEST  finds the largest entry in the LINKS array beginning at A,
   returning the link and its predecessor.

ANOTHER  produces the next highest NFA from the LINKS array, unlinking
   it from the list.

COPY-WID  copys a wordlist to a new address, relocating it properly.
   This may be un-necessary since the "relatve" links are now relative
   to the origin and not the instance address.
------------------------------------------------------------------------ }

?( ... WORDS, filtered and otherwise)

PACKAGE NAME-TOOLS

VARIABLE #WORDS

CREATE FILTER   32 ALLOT

: FILTERED ( -- )   FILTER OFF
   /SOURCE DROP C@ BL <> IF ( text follows immediately)
      BL WORD COUNT 31 MIN FILTER PLACE  FILTER COUNT UPCASE
   THEN ;

: FILTER? ( nfa -- flag )
   FILTER C@ IF
      COUNT R-BUF R@ PLACE  R> COUNT 2DUP UPCASE
      FILTER COUNT -MATCH NIP 0=
   THEN 0<> ;

: .NAME ( nfa -- )    #WORDS ++  .ID ;

: .FILTER ( nfa -- )   DUP FILTER? IF .NAME ELSE DROP THEN ;

: LARGEST ( a -- 'l l )
   0 OVER BEGIN ( 'l l a)
      DUP @ 1+ WHILE ( not -1)
      2DUP @REL
      U< IF ( new largest!)     ( 'l l a)
         -ROT 2DROP             ( a)
         DUP @REL OVER          ( 'l l a)
      THEN
      CELL+
   REPEAT DROP ;

: ANOTHER ( a -- nfa )   LARGEST ( a n)
   DUP IF  DUP @REL  ROT !REL  L>NAME ELSE  NIP  THEN ;

{ --------------------------------------------------------------------
.WID-WORDS traverses the wordlist, executing the xt for each nfa in
   the list.

TALLIES is a proxy for .ID used to count filter matches. ALL-WORDS
   won't display a wordlist name if there are no filter matches in it.
-------------------------------------------------------------------- }

: .WID-WORDS ( wid xt -- )
   OVER +ORIGIN @ 1+ CELLS R-ALLOC >R
   SWAP R@ COPY-WID  BEGIN
      R@ ANOTHER ?DUP WHILE  OVER EXECUTE
   REPEAT R> 2DROP ;

: TALLIES ( nfa -- )   FILTER? IF HERE ++ THEN ;

{ --------------------------------------------------------------------
WID-WORDS  displays all the words, sorted, from a wordlist.

WORDS displays either all or a filtered subset of the words in the
   CONTEXT wordlist.
ALL-WORDS displays words from all wids in the WIDS list. Filters
   are accepted as for WORDS.
-------------------------------------------------------------------- }

: .REPORT ( -- )
   CR #WORDS ? ."  words found." ;

PUBLIC

: WID-WORDS ( wid -- )
   FILTER OFF  ['] .NAME .WID-WORDS ;

PRIVATE

: ALL-WORDS ( -- )   #WORDS OFF
   FILTERED  WIDS BEGIN
      @REL ?DUP WHILE DUP
      CELL+ -ORIGIN  HERE OFF
      DUP ['] TALLIES .WID-WORDS
      HERE @ IF
         CR CR ." wordlist> " DUP .WID CR
         DUP ['] .FILTER .WID-WORDS
      THEN DROP
   REPEAT .REPORT ;

: CONTEXT-WORDS ( -- )   #WORDS OFF
   FILTERED  CONTEXT @ ['] .FILTER .WID-WORDS  ;

PUBLIC

: ALL ['] ALL-WORDS ;

: WORDS ( | 'all -- )
   DEPTH 0> IF
      DUP ['] ALL-WORDS = IF EXECUTE EXIT THEN
   THEN CONTEXT-WORDS ;

END-PACKAGE


