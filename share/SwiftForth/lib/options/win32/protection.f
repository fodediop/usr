{ ====================================================================
Protect users from common errors

Copyright 2001  FORTH, Inc.
==================================================================== }

OPTIONAL PROTECTION Check for common relocation errors during compilation

{ --------------------------------------------------------------------
WARNING mask is extended to include

        0000.0000.0000.0001 display warnings on console
        0000.0000.0000.0010 display warnings in message box
        0000.0000.0001.0000 non-uniqueness
        0000.0001.0000.0000 address warning for , (comma)
        0000.0010.0000.0000 address warning for CONSTANT
        0000.0100.0000.0000 address warning for ! (store)

: BOX.PROTECTED ( zaddr -- )
   HWND SWAP Z" Warning:" [(OR MB_OKCANCEL MB_ICONWARNING)] MessageBox
   IDCANCEL = -9900 ?THROW ;
-------------------------------------------------------------------- }

PACKAGE PROTECTION-LAYER

: BOX.PROTECTED ( zaddr -- )
   0 SWAP Z" Warning:" 49 MessageBox  2 = -9900 ?THROW ;

: (.PROTECTED) ( a buf -- )   >R   0 R@ !
   SOURCE-ID IF
      #TIB 2@ R@ ZAPPEND
      <EOL> COUNT R@ ZAPPEND
      <EOL> COUNT R@ ZAPPEND
   THEN
   S" WARNING: Possible non-relocatable data "  R@ ZAPPEND
   8 (H.0) R@ ZAPPEND
   SOURCE-ID IF
      <EOL> COUNT R@ ZAPPEND
      S" at line " R@ ZAPPEND  LINE @ (.) R@ ZAPPEND
      S"  in file " R@ ZAPPEND  INCLUDING R@ ZAPPEND
   THEN
   R> DROP ;

: .PROTECTED ( a -- )
   256 R-ALLOC >R   R@ (.PROTECTED)
   1 WARNS IF  CR R@ ZCOUNT BOUNDS ?DO I C@ EMIT LOOP CR  THEN
   2 WARNS IF  R@ BOX.PROTECTED  THEN
   R> DROP ;

: ?PROTECTED ( a mask -- )
   WARNING @ IF
      ( mask) WARNS IF
         DUP ORIGIN PAD WITHIN IF
            DUP .PROTECTED
         THEN
      THEN 0
   THEN 2DROP ;

{ --------------------------------------------------------------------
-------------------------------------------------------------------- }

DIALOG (WARNCFG1)
    [MODAL " System warning configuration" 10 20 248 104
        (FONT 8, MS Sans Serif) ]

    [GROUPBOX      " System warnings"             100   4  4 120 88 ]
    [AUTOCHECKBOX  " Enable system warnings"      101   8 16  88  8 ]
    [AUTOCHECKBOX  " Warn for redefinitions"      102  16 28  88  8 ]

    [GROUPBOX      " Address warnings"            103  16 40  84 48 ]
    [AUTOCHECKBOX  " Comma ( , ) "                  104  28 52  48  8 ]
    [AUTOCHECKBOX  " Constants"                   105  28 64  48  8 ]
    [AUTOCHECKBOX  " Store ( ! )"                   106  28 76  48  8 ]

    [GROUPBOX      " Display warnings and errors" 107 128  4 104 40 ]
    [AUTOCHECKBOX  " In command window"           108 132 16  88  8 ]
    [AUTOCHECKBOX  " In message box"              109 132 28  88  8 ]

    [DEFPUSHBUTTON " OK"                         IDOK 140 76  40 14 ]
    [PUSHBUTTON    " Cancel"                 IDCANCEL 188 76  40 14 ]

END-DIALOG

ERROR-HANDLERS +ORDER

' (WARNCFG1) IS (WARNCFG)

ERROR-HANDLERS -ORDER

{ --------------------------------------------------------------------
-------------------------------------------------------------------- }

PUBLIC

-? : , ( a -- )
   DUP $100 ?PROTECTED , ;

-? : CONSTANT ( n -- )
   DUP $200 ?PROTECTED CONSTANT ;

-? : ! ( n a -- )
   STATE @ IF ( compiling)
      POSTPONE ! EXIT
   THEN
   SOURCE=FILE IF
      OVER $400 ?PROTECTED
   THEN ! ; IMMEDIATE

END-PACKAGE
