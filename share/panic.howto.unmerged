From pkoch@apple.com Tue Feb 13 14:22:32 2001
Date: Tue, 13 Feb 2001 14:20:47 -0800
From: Philip Koch <pkoch@apple.com>
To: macosx-all@group.apple.com
Subject: Kernel Panics, Hangs, and the PanicHotline

Here's what to do if your machine panics or hangs.  Most of this has 
been said before, but I'm repeating as a reminder and for the benefit of 
newcomers to this list.


PANIC HOTLINE

For the Cheetah bake cycle, we have created a group address for rapid 
response to low-level problems.  If you get a panic or hard system hang 
and are in a rush, just file the radar (below.)  But if you have time to 
let us troubleshoot, DO NOT REBOOT, and send a message to:

	PanicHotline@group.apple.com

briefly describing the problem.  We will try to get back to you as soon 
as possible, and if the problem is not already understood, we will send 
an engineer to investigate.  Please write up a radar while you're 
waiting.  If you do not hear from us within an hour or get tired of 
waiting, just file the radar and reboot.


KERNEL PANICS

If you get a panic, as a minimum file a radar against component "kernel" 
with the following information:

	1. The entire panic message, INCLUDING THE HEX BACKTRACE and the
	   NAME AND BASE ADDRESS OF ANY KERNEL EXTENSIONS in the backtrace.
	   Alas, this needs to be typed in manually, unless you attach (below.)

	2. The build and configuration information.

	3. What were you doing when the panic occurred, plus potentially
  	   relevent information such as apps running, other weird behavior
	   just before the panic, etc.

Don't worry about searching radar for duplicates, just write up the 
bug.  Its a lot easier for us to sort out dups than it is for you, and 
doing so gives us valuable information about how often the problem is 
occuring.

Then if you are in a rush, just reboot.

But if you have the time and patience, please consider attaching to the 
panic'd machine, to read out register contents and get a symbolic 
backtrace with gdb.  This only takes a few minutes more and sometimes 
provides valuable information.  Besides, its fun.  Here's how to do this:

First, you need a second machine on which to run gdb and attach to the 
panic'd machine.  They must be on the same IP subnet.  If the build that 
failed is "Cheetah4K52" and the panic'd machine's name is "fred", then 
the sequence to attach is:

	> ~public/bin/arpit fred
	> cd ~rc/Updates/Cheetah/Cheetah452/Symbols/xnu/
	> gdb mach_kernel
	(gdb) target remote-kdp
	(gdb) attach fred

It is unfortunately common for the attach to fail (time out).

Then, copy and paste the following info into the radar bug report:

	(gdb) bt
	(gdb) info all-reg
	(gdb) source ~osdev/.gdbinit
	(gdb) p proc0
	(gdb) showallstacks

If you get a symbolic backtrace in this way, you obviously don't need to 
copy in the hex backtrace from the panic message, but PLEASE STILL 
MANUALLY COPY the panic message, DAR, DSISR, plus names and base 
addresses of kernel extensions, into the radar.  The 'showallstacks' 
macro can produce a lot of output, but for hangs in particular this 
information can be critical.


HANGS, AND DEBUG=4

Hangs are a little trickier.  The approach is to break into the kernel 
with cmd-power or NMI, and then attach and debug as with a panic.  But 
if the caps lock key does not light up when toggled, or if you cannot 
ping the hung machine, then the hang is probably not debugable: the 
kernel isn't handling interrupts, and/or the network interface is 
wedged.  (We have specially instrumented hardware that can give 
visibility into even these hard hangs, but using it requires a 
reproducible case.)

The ability to interrupt the kernel with cmd-power or nmi is NOT enabled 
by default.  To enable it, you must set "debug=4" in the boot parameters 
after EVERY install.  There are two ways to set debug=4 (these 
instructions assume a "new world" machine, i.e. NOT a powersurge, 
gossamer, hooper, kanga, or wallstreet):

1. Perhaps the easiest is from a shell running as root:

	root# /usr/sbin/nvram boot-args="debug=4"

If you think other boot params may already be set, it is a good idea to 
first print out the existing value of boot-args, then add debug=4:

	root# /usr/sbin/nvram boot-args

then, assuming boot-args was set to the value "maxmem=64",

	root# /usr/sbin/nvram boot-args="maxmem=64 debug=4"

2. Or boot into Open Firmware by holding down cmd-opt-o-f.  Then, type:

	setenv boot-args debug=4
	sync-nvram

Once debug=4 is set, you can break into a running or hung system with 
cmd-power or NMI, and then proceed to attach and debug as with a regular 
panic.  Note that no panic message is displayed after the cmd-power, and 
that if you enable debug=4, you will no longer be able to break into 
Classic's Macsbug using the same keystroke.
	
Philip.

