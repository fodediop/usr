From: cricket <cricket@apple.com>
Date: Tue Oct 17, 2000  02:37:15 PM US/Pacific
To: macosx-all@group.apple.com
Subject: Filing Radar bugs against Mail
X-Mailer: Apple Mail (2.352)

Since we get a lot of questions about what information is useful when
filing a bug against Mail in Mac OS X, I put together this document to
help anyone who wants to learn the ropes and help us more effectively
address issues with Mail. Much of this is probably applicable to other
component areas as well, so feel free to steal from it if you wish.

Filing Mail bugs in Radar

If you would like to report a bug against the Mail application, this
document should give you a better idea of the type of information that
is most effective in helping us track down the problem you are reporting.
For the purposes of this document, I'm assuming basic familiarity with
the Terminal application. This is divided into the following categories:

	� Mail crashes
	� Mail hangs
	� Other Mail bugs
	� Enhancement requests

Use the mail-help@group.apple.com address for Mail support questions or
mail-talk@group.apple.com for open discussion about Mail.

Note: If you don't want to receive two copies of the reply to either of
these lists, you can set your Reply-to: header to the group address. With
the compose window open, choose Add Reply-To Header Header from the
Message menu.

Mail crashes

	What to include in the bug:

� Description of the steps leading up to the crash (e.g. I was clicking on 
     an newly fetched unread message while the POP fetch was in progress and 
     POOF!)
� Backtrace from gdb (if possible)
� Relevant entries from the Console log (if any)

If the crash happened once and you cannot reproduce it, it's still useful
to file the bug, especially if you have a good recollection of the sequence
of actions that led up to the crash. Even if you think the steps leading
to the crash might be unrelated, they are worth documenting. Keep in mind
that the bug may get sent back if it cannot be reproduced by the Mail
team.

Here's how to gather the information:

	Attaching to Mail in gdb:

1. If Mail is already running, type 'su root' in Terminal, enter your
root password, then type 'gdb' -- Once you get a (gdb) prompt, type
'attach Mail' then hit return, then type 'c' then hit return.

2. If Mail is not running, type 
'gdb /Applications/Mail.app/Contents/MacOS/Mail' in Terminal, then type 'run'
when you get to the (gdb) prompt. (Note you can also just launch Mail
normally and follow #1 above).

	Getting a backtrace:

Try to reproduce the actions that led to the original crash. If Mail
crashes when gdb is attached, the Mail application will not quit as you
would expect in a crash. If Mail appears to be hung while attached with
gdb, be sure to check Terminal, as Mail may have crashed. If so, you'll
see a message similar to this in the Terminal window where you launched
gdb:

Program received signal EXEC_BAD_ACCESS, Could not access memory.
[Switching to process 317, thread 0x2103]
0x7143b0a4 in objc_msgSend()
(gdb)

At the gdb prompt, type 'bt' and wait till it returns you to the (gdb)
prompt again and then type 'thread apply all bt' -- this output will be
useful in filing the bug.

	Check the Console log:

Launch Console from /Applications/Utilities and take note of the contents
of the log. Each message in the console log begins with a time stamp
followed by the application that wrote to the log. Here's an example:

Oct 17 09:40:33 Mail[219] Exception raised during monitored invocation of getAttributedStringSynchronouslyForMessage:, exception: *** -[NSCFArray objectAtIndex:]: index (7) beyond bounds (7)
Oct 17 11:17:27 Mail[219] IMAPConnection got exception The attempt to read data from the server mail.apple.com failed (Connection reset by peer) while reading.

Anything from the 10 minutes prior to the crash to the last entry in the
log that are tagged with 'Mail' would be a good rule of thumb.

	Finally:

The most important point to make here is to file a new bug when you want
to report a crash or a hang in Mail (i.e. don't put new backtraces into
an existing bug -- this makes it much harder for us to manage). Don't
worry about duplicates!

Mail hangs

If Mail hangs, you'll find yourself in a state where the beachball cursor
is spinning either indefinitely, or spinning for longer than you would
expect it to be spinning (e.g. opening a Compose window takes 10 seconds
suddenly, rather than 1 second), during which time Mail is unresponsive
to user action.

	What to include in the bug:

� Description of the steps that happened before Mail became hung (e.g. 
     I was transferring 50 messages from IMAP to a local mailbox -or- I 
     quit Mail while I was compacting my IMAP mailbox)
� The output from sample (if possible)
� Relevant entries from the Console log (if any)

	Running sample:

If Mail is hung indefinitely, you can open a Terminal window and type
'sample Mail 5' -- where 5 is the number of seconds that the sample
utility will gather data before writing it to a file. If the hang is
caused by a specific user action, like clicking on a message, for example,
you might want to type the sample command in Terminal without pressing
return, go back to Mail, initiate the action that causes the hang, then
switch back to Terminal and execute the command. This is especially useful
if you are trying to catch a hang that's only a couple of seconds long.
(If Mail becomes responsive again before you initiate the sample command,
the output won't be useful).

When the sample is done, you'll get a line of output like:

Analysis written to file /tmp/Mail_468.sample

You can type 'more /tmp/Mail_468.sample' to output this data right to
the Terminal window. This data will be useful in filing the bug.

	Check the Console log:

Launch Console from /Applications/Utilities and take note of the contents
of the log. Each message in the console log begins with a time stamp
followed by the application that wrote to the log. Here's an example:

Oct 17 09:40:33 Mail[219] Exception raised during monitored invocation of getAttributedStringSynchronouslyForMessage:, exception: *** -[NSCFArray objectAtIndex:]: index (7) beyond bounds (7)
Oct 17 11:17:27 Mail[219] IMAPConnection got exception The attempt to read data from the server mail.apple.com failed (Connection reset by peer) while reading.

Anything from the 10 minutes prior to the hang to the last entry in the
log that are tagged with 'Mail' would be a good rule of thumb.

Other Mail bugs

	What to include in the bug:

� Description of the functionality of Mail that did not work as expected 
     (e.g. I clicked on the Reply button and a picture of a banana wearing 
     water skis came up instead of a compose window)
� Relevant entries from the Console log (if any)

	Check the Console log:

Launch Console from /Applications/Utilities and take note of the contents
of the log. Each message in the console log begins with a time stamp
followed by the application that wrote to the log. Here's an example:

Oct 17 09:40:33 Mail[219] Exception raised during monitored invocation of getAttributedStringSynchronouslyForMessage:, exception: *** -[NSCFArray objectAtIndex:]: index (7) beyond bounds (7)
Oct 17 11:17:27 Mail[219] IMAPConnection got exception The attempt to read data from the server mail.apple.com failed (Connection reset by peer) while reading.

Anything from the 10 minutes prior to the occurence of the bug to the
last entry in the log that are tagged with 'Mail' would be a good rule
of thumb.

	Finally:

When filing a bug that's not related to a crash or a hang, the guidelines
that normally apply to filing a Radar bug are generally more than enough.

Enhancement requests

A majority of the bugs currently filed against Mail are enhancement
requests, typically for features that may exist on your current e-mail
application of choice, but aren't currently in Mail. The most important
thing to keep in mind when filing an enhancement request is that the goal
of the Mail team is to make an application that is extremely easy to use,
and that is sometimes at odds with the desires of many to have Mail
incorporate all of the features of whatever e-mail program they currently
use. Everything must be weighed on the ease-of-use scale before
implementation.

Also, we're more interested in why you feel a feature should be added to
Mail (i.e. what particular problem might it resolve) that we are in bugs
that just say 'Add feature xxx because Eudora does this.' In some cases,
we are working on solutions to problems that might be different from what
other e-mail applications have done in the past, so knowing the why part
will help us prioritize the adding of these features.

Please send suggestions or comments about this document to cricket@apple.com

v1.2 - 10/17/2000


--
Software Entomologist - Mail for Mac OS X             cricket@apple.com
Apple Computer, Inc.        2 Infinite Loop    Cupertino, CA 95014 USA
               ... It's a Beta thing, you wouldn't understand ...
